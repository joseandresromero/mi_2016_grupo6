//formulario UVS-04
var testingMode = false;
var enviarClicked = false;

$('#btnEnviar').click(function() {
       enviarClicked = true;
});
// VALIDACIONES
$('.form-proyecto').submit(function(event) {
//      event.preventDefault();
//        return;
   //El titulo es requerido tambien para guardar
   if ($('#titulo').val().trim() == '') {
       bootbox.alert("Ingrese el título del proyecto");
       $('#titulo').focus();
       enviarClicked = false;
       event.preventDefault();
       return;
   }

   //Los demas campos son requeridos solo para enviar
   if (enviarClicked == true && testingMode == false) {
       enviarClicked = false;

       //Programa
       if ($('input[name=programaProyecto]').length == 0) {
           bootbox.alert("Seleccione el programa al que pertenece el proyecto.");
           $('#buscar_programa').focus();
           event.preventDefault();
           return;
       }
        //Carrera
        if ($('input[name=carreraPrincipal]').length == 0) {
            bootbox.alert("Seleccione una carrera como principal del proyecto.");
            $('#carrera_principal').focus();
            event.preventDefault();
            return;
        }

        //Provincia
        if ($('#provincia').val().trim() == '') {
            bootbox.alert("Ingrese la provincia.");
            $('#provincia').focus();
            event.preventDefault();
            return;
        }

        //Canton
        if ($('#canton').val().trim() == '') {
            bootbox.alert("Ingrese el cantón.");
            $('#canton').focus();
            event.preventDefault();
            return;
        }

        //Codigo zona
        if ($('#codigo_zona').val().trim() == '') {
            bootbox.alert("Ingrese el código de zona.");
            $('#codigo_zona').focus();
            event.preventDefault();
            return;
        }

        //Codigo distrito
        if ($('#codigo_distrito').val().trim() == '') {
            bootbox.alert("Ingrese el código de distrito.");
            $('#codigo_distrito').focus();
            event.preventDefault();
            return;
        }

        //Codigo circuito
        if ($('#codigo_circuito').val().trim() == '') {
            bootbox.alert("Ingrese el código de circuito.");
            $('#codigo_circuito').focus();
            event.preventDefault();
            return;
        }

        //Fechas
        var rangoFechas = $('#rango_fechas_proyecto').val();
        var indexSeparator = rangoFechas.indexOf('-');
        var fechaInicio = rangoFechas.substring(0,indexSeparator).trim();
        var now = moment().format('DD/MM/YYYY');

        if(now > fechaInicio) {
          bootbox.alert("La fecha de inicio de proyecto ya ha pasado.");
          event.preventDefault();
          return;
        }

        //Area de conocimiento
        if ($('#area_conocimiento').val().trim() == '') {
            bootbox.alert("Ingrese el área de conocimiento.");
            $('#area_conocimiento').focus();
            event.preventDefault();
            return;
        }

        //Sub-area de conocimiento
        if ($('#sub_area_conocimiento').val().trim() == '') {
            bootbox.alert("Ingrese la sub-área de conocimiento.");
            $('#sub_area_conocimiento').focus();
            event.preventDefault();
            return;
        }

        //Sub-area especifica
        if ($('#sub_area_especifica').val().trim() == '') {
            bootbox.alert("Ingrese la sub-área específica.");
            $('#sub_area_especifica').focus();
            event.preventDefault();
            return;
        }

        //Linea de accion
        if ($('.linea-accion:checked').length == 0) {
            bootbox.alert("Seleccione al menos un item de la sección de la línea de acción del proyecto.");
            $('.linea-accion').focus();
            event.preventDefault();
            return;
        }

        //Informacion PNBV (Seccion 1.8)
        if ($('#informacion_pnbv').val().trim() == '') {
            bootbox.alert("Ingrese la información relacionada al PNBV.");
            $('#informacion_pnbv').focus();
            event.preventDefault();
            return;
        }

        //Informacion del perfil del practicante.
        if ($('#perfil_practicante').val().trim() == '') {
            bootbox.alert("Ingrese la información del perfil del practicante.");
            $('#perfil_practicante').focus();
            event.preventDefault();
            return;
        }

        //Beneficiarios directos.
        if ($('#numero_beneficiarios_directos').val() < 1) {
            bootbox.alert("Ingrese un número valido de beneficiarios directos.");
            $('#numero_beneficiarios_directos').focus();
            event.preventDefault();
            return;
        }

        //Beneficiarios indirectos.
        if ($('#numero_beneficiarios_indirectos').val() < 1) {
            bootbox.alert("Ingrese un número valido de beneficiarios indirectos.");
            $('#numero_beneficiarios_indirectos').focus();
            event.preventDefault();
            return;
        }

        //Organización.
        if (($('#id_organizacion').val() > 0) == false) {
          bootbox.alert("Seleccione una organización.");
          $('#numero_beneficiarios_indirectos').focus();
          event.preventDefault();
          return;
        }

        //Delegados de organización.
        if ($('.delegado').length == 0) {
            bootbox.alert("Seleccione al menos un delegado por parte de la organización.");
            $('#buscar_delegado').focus();
            event.preventDefault();
            return;
        }

        //Antecedentes.
        if ($('#antecedentes').val().trim() == '') {
            bootbox.alert("Ingrese los antecedentes del proyecto.");
            $('#antecedentes').focus();
            event.preventDefault();
            return;
        }

        //Contexto del proyecto.
        if ($('#contexto_proyecto').val().trim() == '') {
            bootbox.alert("Ingrese el contexto del proyecto.");
            $('#contexto_proyecto').focus();
            event.preventDefault();
            return;
        }

        //Definicion del problema.
        if ($('#definicion_problema').val().trim() == '') {
            bootbox.alert("Ingrese la definicion del problema.");
            $('#definicion_problema').focus();
            event.preventDefault();
            return;
        }

        //Justificación.
        if ($('#justificacion').val().trim() == '') {
            bootbox.alert("Ingrese la justificación del proyecto.");
            $('#justificacion').focus();
            event.preventDefault();
            return;
        }

        //Pertinencia.
        if ($('#pertinencia').val().trim() == '') {
            bootbox.alert("Ingrese la pertinencia del proyecto.");
            $('#pertinencia').focus();
            event.preventDefault();
            return;
        }

        //Objetivo General
        if ($('#objetivo_general').val().trim() == '') {
            bootbox.alert("Ingrese Objetivo General del proyecto.");
            $('#objetivo_general').focus();
            event.preventDefault();
            return;
        }

        //Objetivos Especificos.
        if($('input[name="objetivoSeleccionado"]').length == 0){
          bootbox.alert("No ha ingresado ningún objetivo específico.");
          $('#nombreObjetivoEspecifico').focus();
          event.preventDefault();
          return;
        }

        //Metodología.
        if ($('#metodologia').val().trim() == '') {
            bootbox.alert("Ingrese Metodología del proyecto.");
            $('#metodologia').focus();
            event.preventDefault();
            return;
        }

        //Productos esperados.
        if ($('#productos_esperados').val().trim() == '') {
            bootbox.alert("Ingrese los productos esperados.");
            $('#productos_esperados').focus();
            event.preventDefault();
            return;
        }

        //Compromisos organización.
        if ($('#compromisos_organizacion').val().trim() == '') {
            bootbox.alert("Ingrese los compromisos por parte de la organización.");
            $('#compromisos_organizacion').focus();
            event.preventDefault();
            return;
        }

        //Evaluación.
        if ($('#evaluacion').val().trim() == '') {
            bootbox.alert("Ingrese la evaluación del proyecto.");
            $('#evaluacion').focus();
            event.preventDefault();
            return;
        }

        //Bibliografía.
        if ($('#bibliografia').val().trim() == '') {
            bootbox.alert("Ingrese la bibliografía del proyecto.");
            $('#bibliografia').focus();
            event.preventDefault();
            return;
        }

        /*
        console.log('carrera',$('input[name=carreraPrincipal]').length)
        event.preventDefault();
        return;
        */
    }
});

/* Se agrega componente ckeditor a ciertos textarea del formulario */
$('#informacion_pnbv').ckeditor();
$('#perfil_practicante').ckeditor();

$('#antecedentes').ckeditor();
$('#contexto_proyecto').ckeditor();
$('#definicion_problema').ckeditor();

$('#justificacion').ckeditor();
$('#pertinencia').ckeditor();
$('#metodologia').ckeditor();
$('#productos_esperados').ckeditor();
$('#compromisos_organizacion').ckeditor();
$('#presupuesto').ckeditor();
$('#evaluacion').ckeditor();
$('#bibliografia').ckeditor();

$('input[class*="date_ranger_picker"]').daterangepicker({
  locale: {
    format: 'DD/MM/YYYY'
  }
});

var i=1;
var j=-1;

/* Se elimina la carrera seleccionada */
$(document).on('click', '.remove-carrera', function(){
     var button_id = $(this).attr("id");
     $('.carrera-'+button_id+'').remove();
});

/* Se elimina el delegado seleccionado. */
$(document).on('click', '.remove-delegado', function(){
     var button_id = $(this).attr("id");
     $('.delegado'+button_id+'').remove();
});

$('#buscar_delegado').autocomplete({
      source: 'autocompleteDelegado/'+$('#id_organizacion').val(),
      minlength: 3,
      autoFocus: true,
      select: function(event, response){
        console.log(response);
        $('#tablaDelegados').append('<tr class="delegado' + response.item.id + '" value="' + response.item.id + '"><td><input class="delegado" type="hidden" name="delegados[]" value="' + response.item.id + '"><p>' + response.item.value + '</p></td><td class="columna-small"><button type="button" name="remove" id="' + response.item.id + '" class="btn btn-danger btn_remove remove-delegado"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
    }
  });

$('#btnElegirOrganizacion').click(function(){
  $('#nombre_organizacion').show();
});

$('#btnAgregarOrganizacion').click(function(){
  $('#nombre_organizacion').hide();
});

$('#btnQuitarOrganizacion').click(function(){
  $('#nombre_organizacion').hide();
  $('#btnQuitarOrganizacion').hide();
  $('#id_organizacion').val('');
  $('#mensajeSeleccioneOrganizacion').show();
  $('#seccionConvenio').hide();
  $('#seccionDelegado').hide();
  $('#label_razon_social_organizacion').html('');
  $('#label_telefono_organizacion').html('');
  $('#label_direccion_organizacion').html('');
  $('#label_fax_organizacion').html('');
  $('#label_correo_electronico_organizacion').html('');
  $('#label_representante_legal_organizacion').html('');
  $('#label_fecha_creacion').html('');
  $('#label_nombramiento_representante_legal').html('');
});

/* Se elimina el programa seleccionado */
$(document).on('click', '.remove-programa', function(){
     var button_id = $(this).attr("id");
     $('.programa-'+button_id+'').remove();

     /* Se elimina el texto del objetivo general del programa en la matriz de marco lógica. */
     $('#textObjetivoProgramaMatrizML').text('');
});

/* Se elimina fila de la tabla resultado de aprendizaje */
$(document).on('click', '.btn_remove_resultado_aprendizaje', function(){
     var button_id = $(this).attr("id");
     $('#row-'+button_id+'').remove();
});
/*
  // When the document is ready
$(document).ready(function () {
  $('.fecha_picker').datepicker({
      format: "yyyy-mm-dd"
  });
});
*/
/* Se elimina campo de texto seleccionado */
$(document).on('click', '.btn_remove', function(){
     var button_id = $(this).attr("id");
     $('#row'+button_id+'').remove();
});

/***** Aqui añadiré objetivos especificos *****/
var numObjetivoEspecifico = 0;
/*
* Se añade nuevo objetivo.
*/
$('#addObjetivoEspecifico').click(function(){
  numObjetivoEspecifico++;
  var tituloObjetivoEspecifico = $('#nombreObjetivoEspecifico').val();
  var tipoProyecto = $(this).attr("name");

  if( tituloObjetivoEspecifico == ''){
    bootbox.alert("Ingrese un texto para agregar el objetivo específico.");
  }
  else{
    $('#nombreObjetivoEspecifico').val("");

    $('#tabla_objetivos_especificos').append(
      '<tr id="tablaObjetivosEspecificosRow-'+numObjetivoEspecifico+'">'+
        '<td>'+
          '<p id="textoObjetivoEspecifico'+numObjetivoEspecifico+'">'+
            tituloObjetivoEspecifico +
          '</p>'+
        '</td>'+
        '<td class="sv-cell-contain-width">'+
          '<button type="button" value="'+tipoProyecto+'" name="'+numObjetivoEspecifico+'" class="sv-icon-button edit-objetivoEspecifico">'+
            '<i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>'+
          '</button>'+
          '<button type="button" value="'+tipoProyecto+'" name="'+numObjetivoEspecifico+'" class="sv-icon-button remove-objetivoEspecifico">'+
            '<i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>'+
          '</button>'+
        '</td>'+
      '</tr>'
    );

    var rowActividades = ''
    if(tipoProyecto == 'comunitarias') {
      rowActividades = '<b>Actividades:</b>'+
                        '<div class="table-responsive">' +
                          '<table class="table table-bordered" id="tabla_actividades_objetivoEspecifico'+numObjetivoEspecifico+'">'+
                          '</table>' +
                          '<button type="button" name="addActividadObjEsp" id="addActividadObjEsp" value="'+numObjetivoEspecifico+'" class="btn btn-success btn_add_actividad small-button">' +
                            '<span class="glyphicon glyphicon-plus"></span>' +
                          '</button>' +
                        '</div>';
    }

    /* Row donde iran las actividades del objetivo especifico en la matriz de marco lógica */
    $('#tabla_matriz_MarcoLogico #body-matriz-marcoLogico').append(
      '<tr id="tablaMarcoLogicoRow-'+numObjetivoEspecifico+'">' +
        '<td colspan="4" style="background: rgb(170, 170, 158)">' +
          '<p id="textOEMatrizML'+numObjetivoEspecifico+'">'+
            tituloObjetivoEspecifico +
          '</p>'+
          '<input id="valueOEMatrizML'+numObjetivoEspecifico+'" name="valueObjetivosEspecificos[]" type="hidden"value="'+tituloObjetivoEspecifico+'" />' +
          '<input name="idObjetivosEspecificos[]" type="hidden" value="'+numObjetivoEspecifico+'" />' +
        '</td>' +
      '</tr>' +
      '<tr id="tablaMarcoLogicoRow-'+numObjetivoEspecifico+'-1">'+
        '<td>'+
          rowActividades+
        '</td>' +
        '<td>' +
          '<textarea name="indicadorVerificableObjetivosEspecificos[]" rows="5" class="form-control" style="resize:none; border:0;"></textarea>'+
        '</td>'+
        '<td>'+
          '<textarea name="mediosVerificacionObjetivosEspecificos[]" rows="5" class="form-control"  style="resize:none; border:0;"></textarea>' +
        '</td>'+
        '<td>'+
          '<textarea name="supuestosObjetivosEspecificos[]" rows="5" class="form-control" style="resize:none; border:0;"></textarea>' +
        '</td>'+
      '</tr>');

    if(tipoProyecto == 'comunitarias') {
      /* Se crea una nueva tabla del objetivo en la sección de CRONOGRAMA */
      $('#componenteActividades').append(
        '<div class="table-responsive" id="div-componenteActividadesObjetivo-'+numObjetivoEspecifico+'" style="margin-top: 25px">'+
          '<table class="table table-bordered" id="tabla-actividades-cronograma-objetivo'+numObjetivoEspecifico+'">'+
            '<thead>'+
              '<tr>'+
                '<th colspan="5" name="tablecomponente"> '+
                  '<p id="tituloOECronograma'+numObjetivoEspecifico+'">'+
                    tituloObjetivoEspecifico+
                  '</p>'+
                '</th>'+
              '</tr>'+
              '<tr>'+
                '<th width="50%">Actividades </th><th width="10%">Horas</th>'+
                '<th width="35%">Fecha de Inicio - Fin</th>'+
              '</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
          '</table>'+
          '<hr style="border-top: 3px solid #eee;">'+
        '</div>');
    }
  }
});

/*
* Elimina el objetivo especifico seleccionado de la lista de objetivos especificos.
*/
$('body').on('click', '.remove-objetivoEspecifico', function() {
  var rowChecked = $(this).attr("name"),
      tipoProyecto = $(this).val();

  bootbox.confirm("¿Esta seguro de eliminar el objetivo específico? También se eliminará el registro del objetivo específico en la matriz de marco lógica y en cronograma.", function(result) {
    if(result == true){
      $('#tablaObjetivosEspecificosRow-'+rowChecked).remove();

      //También se elimina la columna de la mmtriz de marco lógico.
      $('#tablaMarcoLogicoRow-'+rowChecked).remove();
      $('#tablaMarcoLogicoRow-'+rowChecked+'-1').remove();

      if (tipoProyecto == 'comunitarias') {
        //También se elimina la tabla correspondiente al cronograma del objetivo especificode.
        $('#div-componenteActividadesObjetivo-'+rowChecked).remove();
      }
    }
  });
});

/*
* Edita el objetivo especifico seleccionado de la lista de objetivos especificos.
*/
$('body').on('click', '.edit-objetivoEspecifico', function() {
  var rowChecked = $(this).attr("name");
  var valObjEsp = $('#textoObjetivoEspecifico'+rowChecked).text();

  var tipoProyecto = $(this).val();
  console.info('tipoProyecto edit: ', tipoProyecto);

  bootbox.prompt({
    title: "Editar objetivo específico.",
    value: valObjEsp,
    callback: function(newValueObjesp) {
      if (newValueObjesp != null) {
        if(newValueObjesp == ''){
          bootbox.alert("No está permitido cambiar el objetivo específico por un texto vacio .");
        }
        else{
          /* Edito valor en tabla de objetivos especificos */
          $('#textoObjetivoEspecifico'+rowChecked).text(newValueObjesp);

          /* Edito valor en matriz de marco lógico */
          $('#textOEMatrizML'+rowChecked).text(newValueObjesp);
          $('#valueOEMatrizML'+rowChecked).val(newValueObjesp);

          if (tipoProyecto == 'comunitarias') {
            /* Edito valor en cronograma */
            $('#tituloOECronograma'+rowChecked).text(newValueObjesp);
          }
        }
      }
    }
  });
});

/*
* Se agrega nueva actividad a componente (Objetivo especifico).
*/
var numActividad = 0;
$(document).on('click', '.btn_add_actividad', function(){
     var button_value = $(this).attr("value");
     var cent = $(this).attr("value");
     numActividad++;

     var res = button_value.substring(5);

     /* Se trata de un un objetivo especifico que ya estaba guardada y que se le está asignados nuevos objetivps */
     if(res == ''){
       res = button_value;
     }
     else{
       res = 'New'+res;
     }

     /* Se agrega actividad en matriz de marco lógico */
     $('#tabla_actividades_objetivoEspecifico'+button_value+'').append(
       '<tr id="row-AOE'+res+'-'+numActividad+'">'+
        '<td>'+
          '<input type="textarea" id="row-actOE'+res+'-'+numActividad+'" rows="2" class="form-control actividad-obj", style="resize:none; border:0;"></input>'+
        '</td>'+
        '<td width="5%">'+
          '<button type="button" name="remove" id="AOE'+res+'-'+numActividad+'" class="btn btn-danger btn_remove_actividad small-button"><span class="glyphicon glyphicon-remove"></span></button>'+
        '</td>'+
      '</tr>');


     /* Se agrega fila de actividad en la tabla de cronograma del objetivo especifico */
     $('#tabla-actividades-cronograma-objetivo'+button_value+' tbody').append(
      '<tr id="rowactividad-cronograma-AOE'+res+'-'+numActividad+'">' +
        '<td>' +
          '<p id="textActividad-row-actOE'+res+'-'+numActividad+'"> </p>' +
          '<input id="inputActividad-row-actOE'+res+'-'+numActividad+'" name="descripcionActividadesObjetivoEspecifico'+res+'[]" rows="2" type="hidden"></input>'+
        '</td>'+
        '<td>'+
          '<input type="text" class="form-control" name="horasActividadesObjetivoEspecifico'+res+'[]" style="border:0"/>'+
        '</td>'+
        '<td>'+
          '<input type="text" class="form-control date_ranger_picker" name="rangoFechaActividadesObjetivoEspecifico'+res+'[]" style="border:0"/>'+
        '</td>'+
      '</tr>');

     $('.actividad-obj').bind('blur', function(){
       var valueActividad = $(this).val();
       var idActividad = $(this).attr("id");

        $('#textActividad-'+idActividad).html(valueActividad);
        $('#inputActividad-'+idActividad).val(valueActividad);
     });

     $('input[class*="date_ranger_picker"]').daterangepicker({
       locale: {
         format: 'DD/MM/YYYY',
       }
     });
});

/*
* Se elimina la actividad seleccionada
*/
$(document).on('click', '.btn_remove_actividad', function(){
  var button_id = $(this).attr("id");

  bootbox.confirm("¿Está seguro de eliminar la actividad? También se eliminará el registro de la actividad en la sección de cronograma.", function(result) {
    if(result == true){
      /* Se elimina la actividad en la sección de matriz de marco lógico */
      $('#row-'+button_id+'').remove();

      /* Se elimina actividad de la sección del cronograma */
      $('#rowactividad-cronograma-'+button_id+'').remove();
    }
  });
});

/*
*  Evento que se dispara luego de escribir texto en el campo de actividad de un objetivo especifico, se obtiene
*  el texto y se lo copia en la tabla correspondiente de la sección de cronograma.
*/
$('.actividad-obj').bind('blur', function(){
  var valueActividad = $(this).val();
  var idActividad = $(this).attr("id");

  /* Se agrega texto de la actividad en la tabla correspondiente de la sección de cronograma */
  $('#textActividad-'+idActividad).html(valueActividad);
  $('#inputActividad-'+idActividad).val(valueActividad);
});

$('.fecha_picker').datepicker({
  format: "yyyy-mm-dd"
});

/*
*  Evento que se dispara luego de escribir texto en el campo de Objetivo General, se obtiene
*  el texto y se lo copia en el row correspondiente en la matriz de marco lógica.
*/
$('#objetivo_general').bind('blur', function(){
  var valueObjetivoGeneral = $(this).val();

  /* Copio texto del objetivo especificio en el row correspondiente de la matriz de marco lógica. */
  $('#textObjetivoGeneralMatrizML').html(valueObjetivoGeneral);
});
