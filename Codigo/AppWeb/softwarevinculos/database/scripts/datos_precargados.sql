USE vinculos_db;

/* MATERIAS */
INSERT INTO materia (codigo, nombre) VALUES ('FIEC06460', 'HERRAMIENTAS DE COLABORACIÓN DIGITAL');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC04341', 'FUNDAMENTOS DE PROGRAMACIÓN');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC06411', 'COMPUTACIÓN Y SOCIEDAD');
INSERT INTO materia (codigo, nombre) VALUES ('ICM00901', 'MATEMÁTICAS DISCRETAS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC04622', 'PROGRAMACIÓN ORIENTADA A OBJETOS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC01735', 'ANÁLISIS DE REDES ELÉCTRICAS I');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC03012', 'ESTRUCTURAS DE DATOS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC01552', 'LENGUAJES DE PROGRAMACIÓN');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC04366', 'ANÁLISIS DE ALGORITMOS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC00299', 'SISTEMAS DIGITALES I');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC04705', 'REDES DE COMPUTADORES');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05553', 'SISTEMAS DE BASES DE DATOS I');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC06437', 'MÉTODOS DE LA INVESTIGACIÓN APLICADOS A LA COMPUTACIÓN');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC03319', 'ORGANIZACIÓN Y ARQUITECTURA DE COMPUTADORES');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC03046', 'INGENIERÍA DE SOFTWARE ');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05884', 'DESARROLLO DE APLICACIONES WEB');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC02097', 'SISTEMAS OPERATIVOS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC03053', 'INGENIERÍA DE SOFTWARE II');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC01545', 'INTERACCIÓN HOMBRE-MÁQUINA');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC03459', 'INTELIGENCIA ARTIFICIAL');
INSERT INTO materia (codigo, nombre) VALUES ('ICHE03541', 'EMPRENDIMIENTO E INNOVACIÓN TECNOLÓGICA');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05306', 'FORMULACIÓN Y EVALUACIÓN DE PROYECTOS INFORMÁTICOS');
INSERT INTO materia (codigo, nombre) VALUES ('ICHE01693', 'ADMINISTRACIÓN');
INSERT INTO materia (codigo, nombre) VALUES ('ICHE00893', 'MICROECONOMÍA');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC02105', 'SISTEMAS DE INFORMACIÓN');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05322', 'SISTEMAS DE TOMA DE DECISIONES');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC06445', 'SIMULACIÓN DE NEGOCIOS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05405', 'GRÁFICOS POR COMPUTADOR I');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC04382', 'SEÑALES Y SISTEMAS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05413', 'PROCESAMIENTO DE AUDIO Y VIDEO');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05439', 'PROCESAMIENTO DIGITAL DE IMÁGENES');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05462', 'APLICACIONES MULTIMEDIA INTERACTIVAS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC01800', 'LABORATORIO DE REDES ELÉCTRICAS');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC00075', 'ELECTRÓNICA I');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC01099', 'LABORATORIO DE ELECTRÓNICA A');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC05561', 'MICROCONTROLADORES');
INSERT INTO materia (codigo, nombre) VALUES ('FIEC06429', 'DISEÑO DE SISTEMAS CONTROLADOS POR COMPUTADOR');

/* UNIDADES ACADEMICAS */
INSERT INTO unidad_academica (nombre, abreviatura) VALUES ('Facultad de Ingeniaría en Electricidad y Computación', 'FIEC');

/* CARRERAS */
INSERT INTO carrera (nombre, id_unidad_academica) VALUES ('Ingeniería en Ciencias Computacionales', 1);
INSERT INTO carrera (nombre, id_unidad_academica) VALUES ('Ingeniería en Telemática', 1);
INSERT INTO carrera (nombre, id_unidad_academica) VALUES ('Ingeniería en Electrónica y Telecomunicaciones', 1);

/* ESTADOS DEL ESTUDIANTE */
INSERT INTO estado_estudiante (descripcion) VALUES ('Sin Proyecto');
INSERT INTO estado_estudiante (descripcion) VALUES ('En proyecto');
INSERT INTO estado_estudiante (descripcion) VALUES ('Actividades iniciadas');
INSERT INTO estado_estudiante (descripcion) VALUES ('Actividades en seguimiento');
INSERT INTO estado_estudiante (descripcion) VALUES ('Actividades terminadas');

/* TIPOS DE PROYECTO */
INSERT INTO tipo_proyecto (descripcion) VALUES ('Pasantías Comunitarias');
INSERT INTO tipo_proyecto (descripcion) VALUES ('Pasantías Pre-profesionales');

/* ESTADOS DEL PROYECTO */
INSERT INTO estado_proyecto (descripcion) VALUES ('Pendiente');
INSERT INTO estado_proyecto (descripcion) VALUES ('Aprobado');
INSERT INTO estado_proyecto (descripcion) VALUES ('Rechazado');
INSERT INTO estado_proyecto (descripcion) VALUES ('Iniciado');
INSERT INTO estado_proyecto (descripcion) VALUES ('Terminado');

/* ESTADOS DEL FORMULARIO */
INSERT INTO estado_formulario (descripcion) VALUES ('No enviado');
INSERT INTO estado_formulario (descripcion) VALUES ('Enviado (Pendiente de revisión)');
INSERT INTO estado_formulario (descripcion) VALUES ('Aprobado');
INSERT INTO estado_formulario (descripcion) VALUES ('Rechazado');

/* ESTADOS DE LA ACTIVIDAD */
INSERT INTO estado_actividad (descripcion) VALUES ('Creada');
INSERT INTO estado_actividad (descripcion) VALUES ('En progreso');
INSERT INTO estado_actividad (descripcion) VALUES ('Terminada');

/* TIPOS DE FORMULARIO */
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-01');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-04');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-05');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-13');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-14');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-15');
INSERT INTO tipo_formulario (descripcion) VALUES ('FOR-UVS-16');

/* TIPOS DE NOTIFICACION */
INSERT INTO tipo_notificacion (descripcion) VALUES ('Formulario Creado');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Formulario Aprobado');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Formulario Rechazado');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Tutor Asignado a Proyecto');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Estudiante Asignado a Proyecto');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Estudiante Asignado a Tutor');
INSERT INTO tipo_notificacion (descripcion) VALUES ('Rol Asignado');

/* TIPOS DE FORMULARIO */
INSERT INTO linea_accion (descripcion) VALUES ('Ciencia y tecnología al servicio del desarrollo humano');
INSERT INTO linea_accion (descripcion) VALUES ('Transformación de la matriz productiva');
INSERT INTO linea_accion (descripcion) VALUES ('Desarrollo de la economía popular y solidaria');
INSERT INTO linea_accion (descripcion) VALUES ('Generación de beneficios económicos y sociales amigables con el ambiente');
INSERT INTO linea_accion (descripcion) VALUES ('Fortalecimiento de organizaciones sociales y gubernamentales');
INSERT INTO linea_accion (descripcion) VALUES ('Fortalecimiento de la interculturalidad');

/* INSTRUMENTOS DE EVALUACION DE LA PRACTICA */
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Rúbrica de evaluación');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Prueba escrita');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Prueba oral');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Desarrollo de innovaciones');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Desarrollo de manuales o guías');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Sustentaciones');
INSERT INTO instrumento_evaluacion_practica (descripcion) VALUES ('Otro');

/* ROLES */
INSERT INTO rol (descripcion) VALUES ('Director');
INSERT INTO rol (descripcion) VALUES ('Tutor');
INSERT INTO rol (descripcion) VALUES ('Coordinador de Pasantías Comunitarias');
INSERT INTO rol (descripcion) VALUES ('Coordinador de Pasantías Pre-profesionales');
INSERT INTO rol (descripcion) VALUES ('Estudiante');
INSERT INTO rol (descripcion) VALUES ('Administrador');

/* ADMINISTRADOR */ /**** NO MODIFICAR ****/
INSERT INTO usuario (username, password, nombres, apellidos) VALUES ('admin', 'espolPasantiasAdmin2016', 'Administrador', 'Del Sistema');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (1, 6);
INSERT INTO administrador (id_usuario) VALUES (1);


/************************************* DATOS DE PRUEBA *******************************************/

/* USUARIOS */

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('director', 'Jaime Alberto', 'Macias Guerrero', '0914687653', '2080907', 'director@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (2, 1);
INSERT INTO director (id_usuario) VALUES (2);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador', 'Ligia Daniela', 'Loor Castro', '0987761144', '2080907', 'coordinador@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (3, 3);
INSERT INTO coordinador_carrera_comunitarias (id_usuario, id_carrera) VALUES (3, 1);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador2', 'Carlos Alberto', 'Garcia Plaza', '0911123687', '2080907', 'coordinador2@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (4, 3);
INSERT INTO coordinador_carrera_comunitarias (id_usuario, id_carrera) VALUES (4, 2);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador3', 'Veronica Lucia', 'Quinde Ortiz', '0977448825', '2080907', 'coordinador3@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (5, 3);
INSERT INTO coordinador_carrera_comunitarias (id_usuario, id_carrera) VALUES (5, 3);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador4', 'Dario Sergio', 'Leon Pasmay', '0984466378', '2080907', 'coordinador4@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (6, 4);
INSERT INTO coordinador_carrera_empresariales (id_usuario, id_carrera) VALUES (6, 1);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador5', 'Miriam Rocio', 'Naula Rios', '0911252684', '2080907', 'coordinador5@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (7, 4);
INSERT INTO coordinador_carrera_empresariales (id_usuario, id_carrera) VALUES (7, 2);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('coordinador6', 'Fernando Galo', 'Pelaez Ramos', '0971239863', '2080907', 'coordinador6@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (8, 4);
INSERT INTO coordinador_carrera_empresariales (id_usuario, id_carrera) VALUES (8, 3);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('tutor', 'Sergio Roberto', 'Wong Diaz', '0977856245', '2080907', 'tutor@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (9, 2);
INSERT INTO tutor (id_usuario, id_carrera) VALUES (9, 2);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('tutor2', 'Carlos Vicente', 'Mendoza Perez', '0922547634', '2080907', 'tutor2@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (10, 2);
INSERT INTO tutor (id_usuario, id_carrera) VALUES (10, 1);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('tutor3', 'Luis Dario', 'Palacios Contreras', '0999874562', '2080907', 'tutor3@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (11, 2);
INSERT INTO tutor (id_usuario, id_carrera) VALUES (11, 3);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('tutor4', 'Maritza Adriana', 'Mora Salazar', '0971114765', '2080907', 'tutor4@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (12, 2);
INSERT INTO tutor (id_usuario, id_carrera) VALUES (12, 1);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('estudiante2', 'Jaime Andres', 'Montenegro Nuñez', '0971154765', '2080907', 'jjmont@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (13, 5);
INSERT INTO estudiante (nivel, matricula, id_usuario, id_estado_estudiante_proyectoEmpresarial, id_estado_estudiante_proyectoComunitario) VALUES ('500', '200920148', 13, 1, 1);

INSERT INTO usuario (username, nombres, apellidos, cedula, telefono_fijo, correo_electronico, informacion_actualizada) VALUES ('estudiante3', 'Cristhian Luis', 'Pendolema Vallejo', '0998547765', '2080907', 'crimpend@espol.edu.ec', '0');
INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (14, 5);
INSERT INTO estudiante (nivel, matricula, id_usuario, id_estado_estudiante_proyectoEmpresarial, id_estado_estudiante_proyectoComunitario) VALUES ('500', '200720658', 14, 1, 1);

/* ROLES DE DIRECTOR PARA COORDINADORES DE PASANTIAS PRE'PROFESIONALES */

INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (6, 1);
INSERT INTO director (id_usuario) VALUES (6);

INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (7, 1);
INSERT INTO director (id_usuario) VALUES (7);

INSERT INTO rol_usuario (id_usuario, id_rol) VALUES (8, 1);
INSERT INTO director (id_usuario) VALUES (8);

/* PROGRAMAS */

INSERT INTO programa (nombre, objetivo_general, objetivos_especificos, fecha_inicio, fecha_finalizacion) VALUES ('Programa 1', 'Este es el objetico general del Programa 1', 'Este es uno de los objetivos especificos del Programa 1', '2016-07-30', '2016-09-28');
INSERT INTO programa (nombre, objetivo_general, objetivos_especificos, fecha_inicio, fecha_finalizacion) VALUES ('Programa 2', 'Este es el objetico general del Programa 2', 'Este es uno de los objetivos especificos del Programa 2', '2016-07-30', '2016-09-28');
INSERT INTO programa (nombre, objetivo_general, objetivos_especificos, fecha_inicio, fecha_finalizacion) VALUES ('Programa 3', 'Este es el objetico general del Programa 3', 'Este es uno de los objetivos especificos del Programa 3', '2016-07-30', '2016-09-28');

/**************************************************************************************************/
