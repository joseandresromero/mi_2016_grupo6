<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelegadoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delegado_organizacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('cargo')->nullable();
            $table->string('area_seccion')->nullable();
            $table->integer('id_organizacion')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_organizacion')->references('id')->on('organizacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delegado_organizacion');
    }
}
