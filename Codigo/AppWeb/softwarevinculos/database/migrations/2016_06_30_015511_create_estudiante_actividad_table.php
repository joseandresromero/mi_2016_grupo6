<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudianteActividadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('estudiante_actividad', function (Blueprint $table) {
          $table->increments('id');
          $table->boolean('actividad_completada')->nullable();
          $table->text('sugerencia')->nullable();
          $table->date('fecha_revisada')->nullable();
          $table->boolean('cumplimiento_actividad_revision_por_tutor')->nullable();
          $table->text('comentario_revision_tutor')->nullable();
          $table->integer('id_estudiante')->unsigned();
          $table->integer('id_actividad')->unsigned();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_estudiante')->references('id')->on('estudiante');
          $table->foreign('id_actividad')->references('id')->on('actividad');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estudiante_actividad');
    }
}
