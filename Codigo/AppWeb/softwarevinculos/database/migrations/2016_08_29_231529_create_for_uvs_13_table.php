<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForUvs13Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('for_uvs_13', function (Blueprint $table) {
          $table->increments('id');
          $table->boolean('revisado_por_tutor')->nullable();
          $table->integer('id_formulario')->unsigned();
          $table->integer('id_estudiante')->unsigned();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_formulario')->references('id')->on('formulario');
          $table->foreign('id_estudiante')->references('id')->on('estudiante');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('for_uvs_13');
    }
}
