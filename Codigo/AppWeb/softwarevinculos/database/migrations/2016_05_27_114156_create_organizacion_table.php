<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizacion', function (Blueprint $table) {
            $table->increments('id');
            //2.2
            $table->string('nombre');
            $table->string('abreviatura', 25)->nullable();
            //2.4
            $table->string('direccion')->nullable();
            //2.5
            $table->string('telefono')->nullable();
            $table->string('fax')->nullable();
            $table->string('correo_electronico')->nullable();

            $table->string('resumen')->nullable();
            
            //2.6
            $table->string('nombre_representante_legal')->nullable();
            //2.7
            $table->string('fecha_creacion')->nullable();
            $table->string('url_acuerdo_legalizacion')->nullable();
            //2.8
            $table->string('nombramiento_representante_legal')->nullable();
            $table->string('url_nombramiento_representante')->nullable();

            $table->string('url_convenio')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizacion');
    }
}
