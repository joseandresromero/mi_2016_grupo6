<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormularioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->string('url_carta_compromiso')->nullable();
            $table->text('comentario_rechazo')->nullable();
            $table->string('url_subido')->nullable();
            $table->integer('id_creado_por')->unsigned()->nullable();
            $table->integer('id_proyecto')->unsigned();
            $table->integer('id_tipo_formulario')->unsigned()->nullable();
            $table->integer('id_estado_formulario')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_creado_por')->references('id')->on('usuario');
            $table->foreign('id_proyecto')->references('id')->on('proyecto');
            $table->foreign('id_tipo_formulario')->references('id')->on('tipo_formulario');
            $table->foreign('id_estado_formulario')->references('id')->on('estado_formulario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('formulario');
    }
}
