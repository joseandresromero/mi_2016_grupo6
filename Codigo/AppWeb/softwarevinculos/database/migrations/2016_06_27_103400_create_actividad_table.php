<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_finalizacion')->nullable();
            $table->integer('duracion_horas')->nullable();
            $table->boolean('es_actividad_macro')->nullable();
            $table->boolean('es_actividad_micro')->nullable();
            $table->integer('id_estado_actividad')->unsigned()->nullable();
            //$table->integer('id_creado_por')->unsigned();
            //$table->integer('id_proyecto')->unsigned();
            $table->integer('id_objetivo_especifico')->unsigned();
            $table->integer('id_actividad')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('id_creado_por')->references('id')->on('usuario');
            //$table->foreign('id_proyecto')->references('id')->on('proyecto');
            $table->foreign('id_objetivo_especifico')->references('id')->on('objetivo_especifico');
            $table->foreign('id_actividad')->references('id')->on('actividad');
            $table->foreign('id_estado_actividad')->references('id')->on('estado_actividad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actividad');
    }
}
