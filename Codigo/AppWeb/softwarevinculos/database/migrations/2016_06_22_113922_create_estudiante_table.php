<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel')->nullable();
            $table->string('matricula')->nullable();
            //$table->boolean('revisado_for_uvs_13')->nullable();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_estado_estudiante_proyectoComunitario')->unsigned()->nullable();
            $table->integer('id_estado_estudiante_proyectoEmpresarial')->unsigned()->nullable();
            $table->integer('id_proyecto_comunitaria')->unsigned()->nullable();
            $table->integer('id_proyecto_empresarial')->unsigned()->nullable();
            $table->integer('id_tutor_comunitaria')->unsigned()->nullable();
            $table->integer('id_tutor_empresarial')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_usuario')->references('id')->on('usuario');
            $table->foreign('id_estado_estudiante_proyectoComunitario')->references('id')->on('estado_estudiante');
            $table->foreign('id_estado_estudiante_proyectoEmpresarial')->references('id')->on('estado_estudiante');
            $table->foreign('id_proyecto_comunitaria')->references('id')->on('proyecto');
            $table->foreign('id_proyecto_empresarial')->references('id')->on('proyecto');
            $table->foreign('id_tutor_comunitaria')->references('id')->on('tutor');
            $table->foreign('id_tutor_empresarial')->references('id')->on('tutor');
            //$table->foreign('id_formulario_uvs_13')->references('id')->on('for_uvs_13');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estudiante');
    }
}
