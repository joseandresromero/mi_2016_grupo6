<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetivoEspecificoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetivo_especifico', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion')->nullable();
            $table->text('indicadores_verificables')->nullable();
            $table->text('medios_verificacion')->nullable();
            $table->text('supuestos')->nullable();
            $table->integer('id_proyecto')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_proyecto')->references('id')->on('proyecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objetivo_especifico');
    }
}
