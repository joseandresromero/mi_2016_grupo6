<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvidenciaResultadoAprendizajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('evidencia_resultado_aprendizaje', function (Blueprint $table) {
          $table->increments('id');
          $table->text('descripcion')->nullable();
          $table->integer('id_resultado_aprendizaje')->unsigned();
          $table->integer('id_for_uvs_16')->unsigned();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_for_uvs_16')->references('id')->on('for_uvs_16');
          $table->foreign('id_resultado_aprendizaje')->references('id')->on('resultado_aprendizaje');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('evidencia_resultado_aprendizaje');
    }
}
