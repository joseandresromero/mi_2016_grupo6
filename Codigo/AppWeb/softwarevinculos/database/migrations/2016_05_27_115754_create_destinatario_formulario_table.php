<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinatarioFormularioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinatario_formulario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_destinatario')->unsigned();
            $table->integer('id_formulario')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_destinatario')->references('id')->on('usuario');
            $table->foreign('id_formulario')->references('id')->on('formulario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('destinatario_formulario');
    }
}
