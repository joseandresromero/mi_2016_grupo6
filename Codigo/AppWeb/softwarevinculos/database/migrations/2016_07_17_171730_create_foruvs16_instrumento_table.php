<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForuvs16InstrumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('foruvs16_instrumento', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_foruvs16')->unsigned();
        $table->integer('id_instrumento')->unsigned();
        $table->timestamps();
        $table->softDeletes();

        $table->foreign('id_foruvs16')->references('id')->on('for_uvs_16');
        $table->foreign('id_instrumento')->references('id')->on('instrumento_evaluacion_practica');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('foruvs16_instrumento');
    }
}
