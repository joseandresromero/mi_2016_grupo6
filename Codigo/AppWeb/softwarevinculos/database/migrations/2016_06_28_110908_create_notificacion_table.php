<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacion', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('visto');
            $table->integer('id_formulario')->unsigned()->nullable();
            $table->integer('id_proyecto')->unsigned()->nullable();
            $table->integer('id_destinatario')->unsigned();
            $table->integer('id_tutor_asignado')->unsigned()->nullable();
            $table->integer('id_estudiante_asignado')->unsigned()->nullable();
            $table->integer('id_usuario_rol')->unsigned()->nullable();
            $table->integer('id_rol_asignado')->unsigned()->nullable();
            $table->integer('id_tipo_notificacion')->unsigned()->nullable();
            $table->integer('id_revisado_por')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_formulario')->references('id')->on('formulario');
            $table->foreign('id_proyecto')->references('id')->on('proyecto');
            $table->foreign('id_destinatario')->references('id')->on('usuario');
            $table->foreign('id_tutor_asignado')->references('id')->on('tutor');
            $table->foreign('id_estudiante_asignado')->references('id')->on('estudiante');
            $table->foreign('id_usuario_rol')->references('id')->on('usuario');
            $table->foreign('id_rol_asignado')->references('id')->on('rol');
            $table->foreign('id_tipo_notificacion')->references('id')->on('tipo_notificacion');
            $table->foreign('id_revisado_por')->references('id')->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notificacion');
    }
}
