<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadoAprendizajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultado_aprendizaje', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_proyecto')->unsigned();
            $table->integer('id_materia')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_proyecto')->references('id')->on('proyecto');
            $table->foreign('id_materia')->references('id')->on('materia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resultado_aprendizaje');
    }
}
