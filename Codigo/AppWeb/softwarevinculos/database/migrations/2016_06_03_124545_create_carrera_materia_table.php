<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreraMateriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('carrera_materia', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_carrera')->unsigned();
          $table->integer('id_materia')->unsigned();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_carrera')->references('id')->on('carrera');
          $table->foreign('id_materia')->references('id')->on('materia');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrera_materia');
    }
}
