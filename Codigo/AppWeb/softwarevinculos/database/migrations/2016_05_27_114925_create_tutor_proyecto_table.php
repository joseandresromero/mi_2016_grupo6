<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tutor')->unsigned();
            $table->integer('id_proyecto')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_tutor')->references('id')->on('tutor');
            $table->foreign('id_proyecto')->references('id')->on('proyecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_proyecto');
    }
}
