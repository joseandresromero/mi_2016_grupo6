<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function (Blueprint $table) {
            $table->increments('id');
            //1.1
            $table->string('titulo')->nullable();
            //1.4
            $table->string('provincia')->nullable();
            $table->string('canton')->nullable();
            $table->string('codigo_zona')->nullable();
            $table->string('codigo_distrito')->nullable();
            $table->string('codigo_circuito')->nullable();
            //1.5
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_finalizacion')->nullable();
            //1.6
            $table->text('area_conocimiento')->nullable();
            $table->text('sub_area_conocimiento')->nullable();
            $table->text('sub_area_especifica')->nullable();

            //========== RICHTEXT =============
            //1.8
            $table->text('informacion_pnbv')->nullable();
            //1.10
            $table->text('perfil_practicante')->nullable();
            //=================================

            //1.10
            $table->text('perfil_egreso')->nullable();
            $table->integer('numero_practicantes')->unsigned()->nullable();
            //2.1
            $table->integer('numero_beneficiarios_directos')->unsigned()->nullable();
            //2.2
            $table->integer('numero_beneficiarios_indirectos')->unsigned()->nullable();

            //3.1
            $table->text('antecedentes')->nullable();
            //3.2
            $table->text('contexto_proyecto')->nullable();
            //3.3
            $table->text('definicion_problema')->nullable();
            //4
            $table->text('justificacion')->nullable();
            //5
            $table->text('pertinencia')->nullable();
            //6
            $table->text('objetivo_general')->nullable();
            $table->text('indicador_verificable_objetivo_general')->nullable();
            $table->text('medio_verificacion_objetivo_general')->nullable();
            $table->text('supuestos_objetivo_general')->nullable();

            //7 (Indicadores verificables, medios de verificación, supuestos del programa)
            $table->text('indicador_verificable_objetivo_programa')->nullable();
            $table->text('medio_verificacion_objetivo_programa')->nullable();
            $table->text('supuestos_objetivo_programa')->nullable();

            //8
            $table->text('metodologia')->nullable();
            //10
            $table->text('productos_esperados')->nullable();
            //11
            $table->text('compromisos_organizacion')->nullable();
            //12
            $table->text('presupuesto')->nullable();

            $table->string('url_cronograma')->nullable();

            $table->string('url_presupuesto')->nullable();

            //13
            $table->text('evaluacion')->nullable();
            //14
            $table->text('bibliografia')->nullable();

            $table->string('url_convenio')->nullable();

            $table->string('url_evidencia_inclusion_poa')->nullable();

            $table->string('url_resolucion_consejo')->nullable();

            $table->integer('id_organizacion')->unsigned()->nullable();
            $table->integer('id_director')->unsigned();
            $table->integer('id_estado_proyecto')->unsigned();
            $table->integer('id_tipo_proyecto')->unsigned();
            $table->integer('id_programa')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_organizacion')->references('id')->on('organizacion');
            $table->foreign('id_director')->references('id')->on('director');
            $table->foreign('id_estado_proyecto')->references('id')->on('estado_proyecto');
            $table->foreign('id_tipo_proyecto')->references('id')->on('tipo_proyecto');
            $table->foreign('id_programa')->references('id')->on('programa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto');
    }
}
