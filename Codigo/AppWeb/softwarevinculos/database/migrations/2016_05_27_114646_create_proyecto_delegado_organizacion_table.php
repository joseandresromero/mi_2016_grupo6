<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoDelegadoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_delegado_organizacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proyecto')->unsigned();
            $table->integer('id_delegado_organizacion')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_proyecto')->references('id')->on('proyecto');
            $table->foreign('id_delegado_organizacion')->references('id')->on('delegado_organizacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto_delegado_organizacion');
    }
}