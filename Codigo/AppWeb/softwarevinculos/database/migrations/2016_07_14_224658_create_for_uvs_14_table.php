<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForUvs14Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('for_uvs_14', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_formulario')->unsigned();
          $table->text('analisis_evaluacion_actividades')->nullable();
          $table->text('conclusiones')->nullable();
          $table->text('recomendaciones')->nullable();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_formulario')->references('id')->on('formulario');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('for_uvs_14');
    }
}
