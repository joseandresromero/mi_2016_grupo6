<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreraProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrera_proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('carrera_principal');
            $table->integer('id_carrera')->unsigned();
            $table->integer('id_proyecto')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_carrera')->references('id')->on('carrera');
            $table->foreign('id_proyecto')->references('id')->on('proyecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrera_proyecto');
    }
}
