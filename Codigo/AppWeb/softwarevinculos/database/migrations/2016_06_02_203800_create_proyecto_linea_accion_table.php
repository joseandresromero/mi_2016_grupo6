<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoLineaAccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('proyecto_linea_accion', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_proyecto')->unsigned();
          $table->integer('id_linea_accion')->unsigned();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('id_proyecto')->references('id')->on('proyecto');
          $table->foreign('id_linea_accion')->references('id')->on('linea_accion');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto_linea_accion');
    }
}
