<?php

namespace SoftwareVinculos\Utilities;

//use SoftwareVinculos\Utilities\XMLParser;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;

class EspolWS
{
    //private static $serviceUrl = 'http://ws.espol.edu.ec/saac/wsandroid.asmx?wsdl';

    private static $serviceUrl = 'http://ws.espol.edu.ec/saac/directorioespol.asmx?wsdl';

    /*
     * Retorna true si las credenciales de ESPOL son correctas
     */
    public static function autenticacion($username, $password) {
        $cliente = new \SoapClient(EspolWS::$serviceUrl);

        //Creo un objeto que contendra los parametros que recibira el web service
        $credenciales = new \stdClass();
        $credenciales->varUser = $username;
        $credenciales->varContrasenia = $password;

        //Retorno si el usuario fue autenticado o no
        return $cliente->autenticacion($credenciales)->autenticacionResult;
    }

    /*
     * Retorna un diccionario con las datos del usuario
     */
    public static function datosUsuario($username, $password) {
        $cliente = new \SoapClient(EspolWS::$serviceUrl);

        //Creo un objeto que contendra los parametros que recibira el web service
        $wsDatosUsuario = new \stdClass();
        $wsDatosUsuario->varUser = $username;
        $wsDatosUsuario->varContrasenia = $password;

        //Obtengo el string del XML con la informacion del usuario
        $respuestaXML = $cliente->datosUsuario($wsDatosUsuario)->datosUsuarioResult->any;

        //Convierto el string a un objeto
        $xmlObject = new \SimpleXMLElement($respuestaXML);

        //Retorno la informacion del usuario
        return $xmlObject->NewDataSet->DATOS_USUARIO;
    }

    /*
     * Retorna un diccionario con la informacion del profesional de espol según su numero de cedula
     */
    public static function datosProfesionalCedula($numero_cedula) {
        $cliente = new \SoapClient('http://ws.espol.edu.ec/saac/wsSODIS.asmx?wsdl');

        //Creo un objeto que contendra los parametros que recibira el web service
        $DatosProfesionalCedula = new \stdClass();
        $DatosProfesionalCedula->cedula = $numero_cedula;

        //Obtengo el string del XML con la informacion del usuario
        $respuestaXML = $cliente->DatosProfesionalCedula($DatosProfesionalCedula)->DatosProfesionalCedulaResult->any;

        //Convierto el string a un objeto
        $xmlObject = new \SimpleXMLElement($respuestaXML);

        //Retorno la informacion del usuario
        return $xmlObject->NewDataSet->PROFESIONAL;
    }

    /*
     * Retorna un diccionario con la informacion del estudiante de espol según su numero de matricula
     */
    public static function datosEstudianteMatricula($matricula) {
        $cliente = new \SoapClient('http://ws.espol.edu.ec/saac/wsSODIS.asmx?wsdl');

        //Creo un objeto que contendra los parametros que recibira el web service
        $DatosEstudianteMatricula = new \stdClass();
        $DatosEstudianteMatricula->matricula = $matricula;

        //Obtengo el string del XML con la informacion del usuario
        $respuestaXML = $cliente->DatosEstudianteMatricula($DatosEstudianteMatricula)->DatosEstudianteMatriculaResult->any;

        //Convierto el string a un objeto
        $xmlObject = new \SimpleXMLElement($respuestaXML);

        //Retorno la informacion del usuario
        return $xmlObject->NewDataSet->ESTUDIANTE;
    }

    private static function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
