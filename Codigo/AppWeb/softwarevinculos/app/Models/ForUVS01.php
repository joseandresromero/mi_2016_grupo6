<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForUVS01 extends Model
{
  use SoftDeletes;

  protected $table = 'for_uvs_01';
  protected $primaryKey  = 'id';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['deleted_at'];

  /**
   * Obtiene el formulario generico.
   */
  public function formulario()
  {
      return $this->belongsTo('SoftwareVinculos\Models\Formulario', 'id_formulario');
  }
}
