<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutor extends Model
{
    use SoftDeletes;

    protected $table = 'tutor';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el usuario
     */
    public function usuario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_usuario');
    }

    /**
     * Obtiene la carrera que representa.
     */
    public function carrera()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Carrera', 'id_carrera');
    }

    /**
     * Obtiene los tutores asignados al proyecto
     */
    public function proyectos()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Proyecto', 'tutor_proyecto', 'id_tutor', 'id_proyecto');
    }

    /**
     * Obtiene los estudiantes asignados a tutor de proyecto comunitario
     */
    public function estudiantesComunitarias()
    {
        return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_tutor_comunitaria');
    }

    /**
     * Obtiene los estudiantes asignados a tutor de proyecto empresarial
     */
    public function estudiantesEmpresarial()
    {
        return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_tutor_empresarial');
    }

    /**
     * Obtiene las notificaciones en las que esta involucrado este usuario tutor
     */
    public function notificacionesAparentes()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_tutor_asignado');
    }

    /**
    * Busca lista de tutores según el nombre
    */
    public function scopeTutoresSearchByNombre($query, $value){
      $query->join('usuario',  'tutor.id_usuario' , '=',  'usuario.id')
            ->where(\DB::raw("CONCAT(usuario.nombres, ' ', usuario.apellidos)"), "LIKE", "%$value%")
            ->select('tutor.id', 'usuario.nombres', 'usuario.apellidos');
    }

    /**
    * Obtiene lista de todos los tutores asignados a un proyecto en especifico
    */
    public function scopeTutoresAsignadosAProyecto($query, $id_proyecto){
      $query->join('usuario',  'tutor.id_usuario' , '=',  'usuario.id')
            ->join('tutor_proyecto',  'tutor.id' , '=',  'tutor_proyecto.id_tutor')
            ->where('tutor_proyecto.id_proyecto', $id_proyecto)
            ->select('tutor.id', \DB::raw("CONCAT(usuario.nombres,' ', usuario.apellidos) AS full_name"));

    }
}
