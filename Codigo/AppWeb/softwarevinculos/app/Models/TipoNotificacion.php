<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoNotificacion extends Model
{
  use SoftDeletes;

  protected $table = 'tipo_notificacion';
  protected $primaryKey  = 'id';

  /**
   * Obtiene las notificaciones de este tipo
   */
  public function notificaciones()
  {
      return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_tipo_notificacion');
  }
}
