<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DelegadoOrganizacion extends Model
{
    use SoftDeletes;

    protected $table = 'delegado_organizacion';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

   /**
    * Obtiene la organizacion del delegado
    */
   public function organizacion()
   {
       return $this->belongsTo('SoftwareVinculos\Models\Organizacion', 'id_organizacion');
   }

   /**
    * Obtiene los proyectos a los que esta asignado este delegado
    */
   public function proyectos()
   {
       return $this->belongsToMany('SoftwareVinculos\Models\Proyecto', 'proyecto_delegado_organizacion', 'id_delegado_organizacion', 'id_proyecto');
   }
}
