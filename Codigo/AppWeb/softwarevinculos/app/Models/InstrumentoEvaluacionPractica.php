<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstrumentoEvaluacionPractica extends Model
{
  use SoftDeletes;

  protected $table = 'instrumento_evaluacion_practica';
  protected $primaryKey  = 'id';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['deleted_at'];

  /**
   * Obtiene los formularios UVS16 que contienen este instrumento de evaluación.
   */
  public function formulariosUVS16()
  {
    return $this->belongsToMany('SoftwareVinculos\Models\ForUVS16', 'foruvs16_instrumento', 'id_instrumento', 'id_foruvs16');
  }
}
