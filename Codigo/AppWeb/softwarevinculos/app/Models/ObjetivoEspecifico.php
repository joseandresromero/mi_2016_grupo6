<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjetivoEspecifico extends Model
{
    use SoftDeletes;

    protected $table = 'objetivo_especifico';
    protected $primaryKey  = 'id';

    /**
     * Obtiene el proyecto al que pertenece el objetivo especifico
     */
    public function proyecto()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto');
    }

    /**
     * Obtiene los actividades MACRO del objetivo especifico
     */
    public function actividades()
    {
        return $this->hasMany('SoftwareVinculos\Models\Actividad', 'id_objetivo_especifico')->where('es_actividad_macro', 1);
    }
}
