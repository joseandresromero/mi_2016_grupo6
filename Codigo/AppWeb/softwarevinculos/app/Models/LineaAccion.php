<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LineaAccion extends Model
{
    use SoftDeletes;

    protected $table = 'linea_accion';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene los proyectos que contienen esta linea de accion
     */
    public function proyectos()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Proyecto', 'proyecto_linea_accion', 'id_linea_accion', 'id_proyecto');
    }
}
