<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvidenciaResultadoAprendizaje extends Model
{
  use SoftDeletes;

  protected $table = 'evidencia_resultado_aprendizaje';
  protected $primaryKey  = 'id';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['deleted_at'];

  /**
   * Obtiene el formularioUvs16 al que pertenece la evaluacion de resultado de aprendizaje.
   */
  public function formularioUVS16()
  {
      return $this->belongsTo('SoftwareVinculos\Models\ForUVS16', 'id_for_uvs_16');
  }

  /**
   * Obtiene el resultado de arendizaje al que pertenece la evaluacion
   */
  public function resultadoAprendizaje()
  {
      return $this->belongsTo('SoftwareVinculos\Models\ResultadoAprendizaje', 'id_resultado_aprendizaje');
  }
}
