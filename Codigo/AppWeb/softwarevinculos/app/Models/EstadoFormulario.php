<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoFormulario extends Model
{
    use SoftDeletes;

    protected $table = 'estado_formulario';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

   public function formularios()
   {
       return $this->hasMany('SoftwareVinculos\Models\Formulario', 'id_estado_formulario');
   }
}
