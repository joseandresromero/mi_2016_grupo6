<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoordinadorCarreraComunitarias extends Model
{
    use SoftDeletes;

    protected $table = 'coordinador_carrera_comunitarias';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el usuario
     */
    public function usuario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_usuario');
    }

    /**
     * Obtiene la carrera que coordina.
     */
    public function carrera()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Carrera', 'id_carrera');
    }
}
