<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoFormulario extends Model
{
  use SoftDeletes;

  protected $table = 'tipo_formulario';
  protected $primaryKey  = 'id';

  /**
   * Obtiene los formularios que tienen este tipo de formulario
   */
  public function formularios()
  {
      return $this->hasMany('SoftwareVinculos\Models\Formulario', 'id_tipo_formulario');
  }
}
