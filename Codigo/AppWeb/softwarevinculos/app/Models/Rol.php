<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use SoftDeletes;

    protected $table = 'rol';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

   /**
    * Obtiene los usuarios con este rol
    */
   public function usuarios()
   {
       return $this->belongsToMany('SoftwareVinculos\Models\Usuario', 'rol_usuario', 'id_rol', 'id_tipo_usuario');
   }

   /**
    * Obtiene las notificaciones de asignacion de este rol
    */
   public function notificacionesRolAsignado()
   {
       return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_rol_asignado');
   }
}
