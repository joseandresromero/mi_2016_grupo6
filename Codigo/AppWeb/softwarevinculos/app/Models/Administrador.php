<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Administrador extends Model
{
    use SoftDeletes;

    protected $table = 'administrador';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el usuario
     */
    public function usuario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_usuario');
    }
}
