<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Organizacion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'organizacion';
    protected $primaryKey  = 'id';

    /**
     * Obtiene los delegados de esta organizacion
     */
    public function delegados()
    {
        return $this->hasMany('SoftwareVinculos\Models\DelegadoOrganizacion', 'id_organizacion');
    }

    /**
     * Obtiene los proyecto en los que participa la organizacion
     */
    public function proyectos()
    {
        return $this->hasMany('SoftwareVinculos\Models\Proyecto', 'id_organizacion');
    }

}
