<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResultadoAprendizaje extends Model
{
  use SoftDeletes;

  protected $table = 'resultado_aprendizaje';
  protected $primaryKey  = 'id';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['deleted_at'];

  /**
   * Obtiene el proyecto al que pertenece el resultado de aprendizaje
   */
  public function proyecto()
  {
      return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto');
  }

  /**
   * Obtiene materia a la que pertenece el resultado de aprendizaje
   */
  public function materia()
  {
      return $this->belongsTo('SoftwareVinculos\Models\Materia', 'id_materia');
  }

  public function evidenciasResultadosAprendizaje()
  {
      return $this->hasMany('SoftwareVinculos\Models\EvidenciaResultadoAprendizaje', 'id_resultado_aprendizaje');
  }
}
