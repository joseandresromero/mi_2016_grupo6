<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Carrera extends Model
{
    //
    use SoftDeletes;

    protected $table = 'carrera';
    protected $primaryKey  = 'id';

    /**
     * Obtiene los proyectos en los cuales participa esta carrera
     */
    public function proyectos()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Proyecto', 'carrera_proyecto', 'id_carrera', 'id_proyecto')->withPivot('carrera_principal');
    }

    /**
     * Obtiene el coordinador de la carrera
     */
    public function coordinadorComunitarias()
    {
        return $this->hasOne('SoftwareVinculos\Models\CoordinadorCarreraComunitarias', 'id_carrera');
    }

    /**
     * Obtiene el coordinador de la carrera
     */
    public function coordinadorEmpresariales()
    {
        return $this->hasOne('SoftwareVinculos\Models\CoordinadorCarreraEmpresariales', 'id_carrera');
    }

    /**
     * Obtiene los tutores de la carrera.
     */
    public function tutores()
    {
        return $this->hasMany('SoftwareVinculos\Models\Tutor', 'id_carrera');
    }

    /**
     * Obtiene la unidad academica a la que pertenece
     */
    public function unidadAcademica()
    {
        return $this->belongsTo('SoftwareVinculos\Models\UnidadAcademica', 'id_unidad_academica');
    }
}
