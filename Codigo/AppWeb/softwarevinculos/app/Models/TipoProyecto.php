<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoProyecto extends Model
{
    use SoftDeletes;

    protected $table = 'tipo_proyecto';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

   public function proyectos()
   {
       return $this->hasMany('SoftwareVinculos\Models\Proyecto', 'id_tipo_proyecto');
   }
}
