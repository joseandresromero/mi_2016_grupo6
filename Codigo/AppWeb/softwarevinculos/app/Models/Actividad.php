<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actividad extends Model
{
  use SoftDeletes;

  protected $table = 'actividad';
  protected $primaryKey  = 'id';

  /**
   * Obtiene el objetivo especifico al que pertenece la actividad
   */
  public function objetivoEspecifico()
  {
      return $this->belongsTo('SoftwareVinculos\Models\ObjetivoEspecifico', 'id_objetivo_especifico');
  }

  /**
   * Obtiene los estudiantes que tiene asignada esta actividad
   */
  public function estudiantes()
  {
      return $this->belongsToMany('SoftwareVinculos\Models\Estudiante', 'estudiante_actividad', 'id_actividad', 'id_estudiante')->withPivot('actividad_completada', 'sugerencia', 'fecha_revisada');
  }

  /**
   * Obtiene las subactividades que tiene asignadas esta actividad.
   */
  public function subactividades()
  {
    return $this->hasMany('SoftwareVinculos\Models\Actividad', 'id_actividad');
  }

  /* Obtiene la actividad padre a la que pertenece la subactividad */
  public function actividadPadre(){
    return $this->belongsTo('SoftwareVinculos\Models\Actividad', 'id_actividad');
  }

  /**
  * Obtiene toda la lista de actividades (MACRO Y MICRO) que un estudiante tiene asignado en un proyecto en especifico.
  */
  public function scopeAllActividadesEstudianteProyecto($query, $estudiante, $proyecto){

    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          //->select('actividad.*');
          ->select('actividad.*', 'estudiante_actividad.actividad_completada','estudiante_actividad.fecha_revisada', 'estudiante_actividad.sugerencia', 'estudiante_actividad.cumplimiento_actividad_revision_por_tutor', 'estudiante_actividad.comentario_revision_tutor');
  }

  /**
  * Obtiene lista de actividades MACRO que un estudiante tiene asignado en un proyecto en especifico.
  */
  public function scopeActividadesEstudiante($query, $estudiante, $proyecto){

    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->where('actividad.es_actividad_macro','=', 1)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          //->select('actividad.*');
          ->select('actividad.*', 'estudiante_actividad.actividad_completada','estudiante_actividad.fecha_revisada', 'estudiante_actividad.sugerencia', 'estudiante_actividad.cumplimiento_actividad_revision_por_tutor', 'estudiante_actividad.comentario_revision_tutor');
  }

  /**
  * Obtiene lista de subactividades MICRO de una actividad MACRO que un estudiante tiene asignado en un proyecto en especifico.
  */
  public function scopeSubActividadesActividadMacroEstudiante($query, $estudiante, $proyecto, $actividad){

    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->where('actividad.es_actividad_micro','=', 1)
          ->where('actividad.id_actividad', $actividad->id)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          //->select('actividad.*');
          ->select('actividad.*', 'estudiante_actividad.fecha_revisada', 'estudiante_actividad.sugerencia', 'estudiante_actividad.cumplimiento_actividad_revision_por_tutor', 'estudiante_actividad.comentario_revision_tutor');
  }

  /**
  * Busca una actividad por 'id' de un estudiante en un proyecto en específico
  */
  public function scopeActividadAsignadaEstudiante($query, $estudiante, $proyecto, $id_actividad){
    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->where('actividad.id', '=', $id_actividad)
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          ->select('actividad.*', 'estudiante_actividad.fecha_revisada', 'estudiante_actividad.sugerencia', 'estudiante_actividad.cumplimiento_actividad_revision_por_tutor', 'estudiante_actividad.comentario_revision_tutor');
  }

  /*
  * Oobtiene las actividades MACRO que un estudiante tiene asignado de un proyecto especifico.
  */
  public function scopeActividadAsignadaEstudiantePorObjetivo($query, $estudiante, $proyecto, $id_objetivo_especifico){

    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->where('actividad.es_actividad_macro','=', 1)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->where('objetivo_especifico.id','=',$id_objetivo_especifico)
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          ->select('actividad.*');
          /*
    $query->join('estudiante_actividad', 'estudiante_actividad.id_actividad' , '=',  'actividad.id')
          ->where('estudiante.id','=', $estudiante->id)
          ->join('estudiante', 'estudiante_actividad.id_estudiante' , '=',  'estudiante.id' )
          ->where('actividad.id', '=', $id_actividad)
          ->join('objetivo_especifico', 'actividad.id_objetivo_especifico' , '=',  'objetivo_especifico.id' )
          ->where('objetivo_especifico.id','=',$id_objetivo_especifico)
          ->join('proyecto', 'objetivo_especifico.id_proyecto' , '=',  'proyecto.id' )
          ->where('proyecto.id','=', $proyecto->id)
          ->select('actividad.*');
          */
  }
}
