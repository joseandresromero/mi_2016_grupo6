<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materia extends Model
{
  use SoftDeletes;

  protected $table = 'materia';
  protected $primaryKey  = 'id';

  /**
   * Obtiene los resultados de aprendizaje
   */
  public function resultadosAprendizaje()
  {
      return $this->hasMany('SoftwareVinculos\Models\ResultadoAprendizaje', 'id_materia');
  }
}
