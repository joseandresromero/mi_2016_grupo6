<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudiante extends Model
{
    use SoftDeletes;

    protected $table = 'estudiante';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el usuario
     */
    public function usuario()
    {
      return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_usuario');
    }

    /**
     * Obtiene el estado del estudiante en proyecto comunitario.
     */
    public function estadoEstudianteProyectoComunitario()
    {
      return $this->belongsTo('SoftwareVinculos\Models\EstadoEstudiante', 'id_estado_estudiante_proyectoComunitario');
    }

    /**
     * Obtiene el estado del estudiante en proyecto de practica pre-profesional.
     */
    public function estadoEstudianteProyectoEmpresarial()
    {
      return $this->belongsTo('SoftwareVinculos\Models\EstadoEstudiante', 'id_estado_estudiante_proyectoEmpresarial');
    }

    /**
     * Obtiene el proyecto comunitario que el estudiante tiene asignado
     */
    public function proyectoComunitaria()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto_comunitaria');
    }

    /**
     * Obtiene el proyecto empresarial que el estudiante tiene asignado
     */
    public function proyectoEmpresarial()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto_empresarial');
    }

    /**
     * Obtiene el tutor de proyecto comunitario que el estudiante tiene asignado
     */
    public function tutorProyectoComunitaria()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Tutor', 'id_tutor_comunitaria');
    }

    /**
     * Obtiene el tutor de proyecto empresarial que el estudiante tiene asignado
     */
    public function tutorProyectoEmpresarial()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Tutor', 'id_tutor_empresarial');
    }

    /**
     * Obtiene el formulario 13 al cual esta ligado este estudiante
     */
    /*public function formularioUVS13()
    {
        return $this->belongsTo('SoftwareVinculos\Models\ForUVS13', 'id_formulario_uvs_13');
    }*/

    /**
     * Obtiene las actividades de un proyecto que tiene asignadas el estudiante
     */
    public function actividadesProyecto()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Actividad', 'estudiante_actividad', 'id_estudiante', 'id_actividad')->withPivot('actividad_completada', 'sugerencia', 'fecha_revisada', 'cumplimiento_actividad_revision_por_tutor', 'comentario_revision_tutor');
    }

    /**
     * Obtiene los formularios FOR-UVS-16 en el que el estudiante esta asociado.
     */
    public function formulariosUVS16()
    {
        return $this->hasMany('SoftwareVinculos\Models\ForUVS16', 'id_estudiante');
    }

    /*
    * Busca un estudiante por numero de matricula
    */
    public function scopeEstudiantePorMatricula($query, $matricula){
      $query->where('matricula', $matricula);
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-01 para un proyecto de pasantias comunitarias
    */
    public function scopeEstudiantesForUVS01AprobadoComunitarias($query, $id_proyecto){
      $query->join('usuario', 'usuario.id', '=', 'estudiante.id_usuario')
            ->join('formulario', 'formulario.id_creado_por', '=', 'usuario.id')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 1)
            ->where('estudiante.id_proyecto_comunitaria', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 1)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-01 para un proyecto de pasantias empresariales
    */
    public function scopeEstudiantesForUVS01AprobadoEmpresariales($query, $id_proyecto){
      $query->join('usuario', 'usuario.id', '=', 'estudiante.id_usuario')
            ->join('formulario', 'formulario.id_creado_por', '=', 'usuario.id')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 2)
            ->where('id_proyecto_empresarial', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 1)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-13 para un proyecto de pasantias comunitarias
    */
    public function scopeEstudiantesForUVS13AprobadoComunitarias($query, $id_proyecto){
      $query->join('for_uvs_13', 'for_uvs_13.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_13.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 1)
            ->where('estudiante.id_proyecto_comunitaria', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 4)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-13 para un proyecto de pasantias empresariales
    */
    public function scopeEstudiantesForUVS13AprobadoEmpresariales($query, $id_proyecto){
      $query->join('for_uvs_13', 'for_uvs_13.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_13.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 2)
            ->where('id_proyecto_empresarial', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 4)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-14 para un proyecto de pasantias comunitarias
    */
    public function scopeEstudiantesForUVS14AprobadoComunitarias($query, $id_proyecto){
      $query->join('usuario', 'usuario.id', '=', 'estudiante.id_usuario')
            ->join('formulario', 'formulario.id_creado_por', '=', 'usuario.id')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 1)
            ->where('id_proyecto_comunitaria', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 5)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-14 para un proyecto de pasantias empresariales
    */
    public function scopeEstudiantesForUVS14AprobadoEmpresariales($query, $id_proyecto){
      $query->join('usuario', 'usuario.id', '=', 'estudiante.id_usuario')
            ->join('formulario', 'formulario.id_creado_por', '=', 'usuario.id')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 2)
            ->where('id_proyecto_empresarial', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 5)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-15 para un proyecto de pasantias comunitarias
    */
    public function scopeEstudiantesForUVS15AprobadoComunitarias($query, $id_proyecto){
      $query->join('for_uvs_15', 'for_uvs_15.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_15.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 1)
            ->where('id_proyecto_comunitaria', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 6)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-15 para un proyecto de pasantias empresariales
    */
    public function scopeEstudiantesForUVS15AprobadoEmpresariales($query, $id_proyecto){
      $query->join('for_uvs_15', 'for_uvs_15.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_15.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 2)
            ->where('id_proyecto_empresarial', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 6)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-16 para un proyecto de pasantias comunitarias
    */
    public function scopeEstudiantesForUVS16AprobadoComunitarias($query, $id_proyecto){
      $query->join('for_uvs_16', 'for_uvs_16.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_16.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 1)
            ->where('id_proyecto_comunitaria', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 7)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /*
    * Busca los estudiantes que ya llenaron el formulario UVS-16 para un proyecto de pasantias empresariales
    */
    public function scopeEstudiantesForUVS16AprobadoEmpresariales($query, $id_proyecto){
      $query->join('for_uvs_16', 'for_uvs_16.id_estudiante', '=', 'estudiante.id')
            ->join('formulario', 'formulario.id', '=', 'for_uvs_16.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.id_tipo_proyecto', 2)
            ->where('id_proyecto_empresarial', $id_proyecto)
            ->where('formulario.id_tipo_formulario', 7)
            ->where('formulario.id_estado_formulario', 3)
            ->select('estudiante.*');
    }

    /**
    * Obtiene lista de estudiantes que tiene asignado un tutor en un proyecto comunitario en especifico.
    */
    public function scopeEstudiantesAsignadosTutorProyectoComunitaria($query, $id_proyecto, $id_tutor){
      $query->join('tutor', 'tutor.id', '=', 'estudiante.id_tutor_comunitaria')
            ->where('tutor.id', $id_tutor)
            ->where('estudiante.id_proyecto_comunitaria', $id_proyecto)
            ->select('estudiante.*');
    }

    /**
    * Obtiene lista de estudiantes que tiene asignado un tutor en un proyecto empresarial en especifico.
    */
    public function scopeEstudiantesAsignadosTutorProyectoEmpresarial($query, $id_proyecto, $id_tutor){
      $query->join('tutor', 'tutor.id', '=', 'estudiante.id_tutor_empresarial')
            ->where('tutor.id', $id_tutor)
            ->where('estudiante.id_proyecto_empresarial', $id_proyecto)
            ->select('estudiante.*');
    }

    /**
     * Obtiene las notificaciones en las que esta involucrado este usuario estudiante
     */
    public function notificacionesAparentes()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_estudiante_asignado');
    }
}
