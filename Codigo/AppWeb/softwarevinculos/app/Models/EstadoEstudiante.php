<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoEstudiante extends Model
{
    use SoftDeletes;

    protected $table = 'estado_estudiante';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

   /*
   * Obtiene los estudiantes que tienen estado en proyecto comunitario.
   */
   public function estudiantesProyectoComunitario()
   {
       return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_estado_estudiante_proyectoComunitario');
   }

   /*
   * Obtiene los estudiantes que tienen estado en proyecto empresarial.
   */
   public function estudiantesProyectoEmpresarial()
   {
       return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_estado_estudiante_proyectoEmpresarial');
   }
}
