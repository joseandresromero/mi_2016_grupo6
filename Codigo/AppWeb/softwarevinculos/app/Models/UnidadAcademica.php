<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UnidadAcademica extends Model
{
    //
    use SoftDeletes;

    protected $table = 'unidad_academica';
    protected $primaryKey  = 'id';

    /**
     * Obtiene las carreras que contiene
     */
    public function carreras()
    {
        return $this->hasMany('SoftwareVinculos\Models\Carrera', 'id_unidad_academica');
    }
}
