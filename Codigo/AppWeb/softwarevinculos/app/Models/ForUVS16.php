<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForUVS16 extends Model
{
  use SoftDeletes;

  protected $table = 'for_uvs_16';
  protected $primaryKey  = 'id';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['deleted_at'];

  /**
   * Obtiene el formulario generico.
   */
  public function formulario()
  {
      return $this->belongsTo('SoftwareVinculos\Models\Formulario', 'id_formulario');
  }

  /**
   * Obtiene el estudiante asociado a este formularioUVS16.
   */
  public function estudiante()
  {
      return $this->belongsTo('SoftwareVinculos\Models\Estudiante', 'id_estudiante');
  }

  /**
   * Obtiene los instrumentos de evaluacion del formulario FOR-UVS-16
   */
  public function intrumentosEvaluacion()
  {
    return $this->belongsToMany('SoftwareVinculos\Models\InstrumentoEvaluacionPractica', 'foruvs16_instrumento', 'id_foruvs16', 'id_instrumento');
  }

  public function evidenciasResultadosAprendizaje()
  {
      return $this->hasMany('SoftwareVinculos\Models\EvidenciaResultadoAprendizaje', 'id_for_uvs_16');
  }
}
