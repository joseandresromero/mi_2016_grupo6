<?php

namespace SoftwareVinculos\Models;

use Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model
{
    use SoftDeletes;

    protected $table = 'proyecto';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el director
     */
    public function director()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Director', 'id_director');
    }

    /**
     * Obtiene el estado del proyecto
     */
    public function estado()
    {
        return $this->belongsTo('SoftwareVinculos\Models\EstadoProyecto', 'id_estado_proyecto');
    }

    /**
     * Obtiene el tipo del proyecto
     */
    public function tipo()
    {
        return $this->belongsTo('SoftwareVinculos\Models\TipoProyecto', 'id_tipo_proyecto');
    }

    /**
     * Obtiene los objetivos especificos del proyecto
     */
    public function objetivosEspecificos()
    {
        return $this->hasMany('SoftwareVinculos\Models\ObjetivoEspecifico', 'id_proyecto');
    }

    /**
     * Obtiene los formularios del proyecto
     */
    public function formularios()
    {
        return $this->hasMany('SoftwareVinculos\Models\Formulario', 'id_proyecto');
    }

    /**
     * Obtiene el formulario UVS04 del proyecto.
     */
    public function formularioUVS04()
    {
        return $this->hasOne('SoftwareVinculos\Models\Formulario', 'id_proyecto')
                    ->where('id_tipo_formulario', 2);
    }

    /**
     * Obtiene las lineas de accion del proyecto
     */
    public function lineasAccion()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\LineaAccion', 'proyecto_linea_accion', 'id_proyecto', 'id_linea_accion');
    }

    /**
     * Obtiene los delegados asignados al proyecto
     */
    public function delegados()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\DelegadoOrganizacion', 'proyecto_delegado_organizacion', 'id_proyecto', 'id_delegado_organizacion');
    }

    /**
     * Obtiene los estudiantes asignados a proyecto comunitario
     */
    public function estudiantesComunitarias()
    {
        return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_proyecto_comunitaria');
    }

    /**
     * Obtiene los estudiantes asignados a proyecto empresarial
     */
    public function estudiantesEmpresarial()
    {
        return $this->hasMany('SoftwareVinculos\Models\Estudiante', 'id_proyecto_empresarial');
    }

    /**
     * Obtiene los tutores asignados al proyecto
     */
    public function tutores()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Tutor', 'tutor_proyecto', 'id_proyecto', 'id_tutor');
    }

    /**
     * Obtiene los resultados de aprendizaje
     */
    public function resultadosAprendizaje()
    {
        return $this->hasMany('SoftwareVinculos\Models\ResultadoAprendizaje', 'id_proyecto');
    }

    /**
     * Obtiene las carreras que participan en el proyecto
     */
    public function carreras()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Carrera', 'carrera_proyecto', 'id_proyecto', 'id_carrera')->withPivot('carrera_principal');
    }

    /**
     * Obtiene la organizacion del proyecto
     */
    public function organizacion()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Organizacion', 'id_organizacion');
    }

    /**
     * Obtiene los objetivos de desarrollo del milenio del proyecto
     */
    public function objetivosDesarrolloMilenio()
    {
        return $this->hasMany('SoftwareVinculos\Models\ObjetivoDesarrolloMilenio', 'id_proyecto');
    }

    /**
    * Obtiene el programa al que pertenece el proyecto
    */
   public function programa()
   {
       return $this->belongsTo('SoftwareVinculos\Models\Programa', 'id_programa');
   }

   /**
    * Obtiene las notificaciones en las que esta involucrado este proyecto
    */
   public function notificaciones()
   {
       return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_proyecto');
   }

   /**
   * Filtra Lista de proyectos del director por estado
   */
   public function scopeFiltrarPorEstado($query, $value_estado){
     //$estados = config('options.estadosProyecto');
     $estados_ids = \SoftwareVinculos\Models\EstadoProyecto::all()->lists('id');

     //dd($estados_ids);

     $id_director = Auth::user()->director->id;

     if($value_estado != "" && isset($estados_ids[$value_estado])){
       $query->join('estado_proyecto', 'estado_proyecto.id', '=', 'proyecto.id_estado_proyecto')
             ->where('id_director', $id_director)
             ->where('estado_proyecto.id', $value_estado)
             ->select('proyecto.*');
     }
     else{
       //Retorna todos los proyectos del director
       $query->where('id_director', $id_director);
     }
   }

   /**
   * Obtiene todos los proyectos del director logueado
   */
    public function scopeProyectosDirector($query){
      $id_director = Auth::user()->director->id;

      $query->where('id_director', $id_director);
    }

    /**
    * Obtiene todos los proyectos del docente logueado en los cuales es director o tutor
    */
    public function scopeProyectosDirectorTutor($query){
      $director = \Auth::user()->director;
      $tutor = \Auth::user()->tutor;

      if ($director != null && $tutor != null) {
echo '<script>console.log("director:'.$director->id.'/tutor:'.$tutor->id.'");</script>';
//$query->where('id_director', $id_director);
        $query->leftJoin('tutor_proyecto', 'tutor_proyecto.id_proyecto', '=', 'proyecto.id')
              ->leftJoin('tutor', 'tutor.id', '=', 'tutor_proyecto.id_tutor')
              ->where('proyecto.id_director', $director->id)
              ->orWhere('tutor_proyecto.id_tutor', $tutor->id)
              ->select('proyecto.*');
      } else if ($director != null && $tutor == null) {
        $query->where('id_director', $director->id);
      } else if ($director == null && $tutor != null) {
        $query->join('tutor_proyecto', 'tutor_proyecto.id_proyecto', '=', 'proyecto.id')
              ->join('tutor', 'tutor.id', '=', 'tutor_proyecto.id_tutor')
              ->where('tutor.id', $tutor->id)
              ->select('proyecto.*');
      } else {

      }
    }

    /**
    * Filtra la lista de proyectos (por titulo) del docente logueado en los cuales es director o tutor
    */
    public function scopeFiltrarPorTituloDirectorTutor($query, $tituloProyecto){
      $director = \Auth::user()->director;
      $tutor = \Auth::user()->tutor;

      if ($director != null && $tutor != null) {
        $query->leftJoin('tutor_proyecto', 'tutor_proyecto.id_proyecto', '=', 'proyecto.id')
              ->leftJoin('tutor', 'tutor.id', '=', 'tutor_proyecto.id_tutor')
              ->where('proyecto.id_director', $director->id)
              ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%" )
              ->orWhere('tutor_proyecto.id_tutor', $tutor->id)
              ->select('proyecto.*');
      } else if ($director != null && $tutor == null) {
        $query->where('id_director', $director->id)
              ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%");
      } else if ($director == null && $tutor != null) {
        $query->join('tutor_proyecto', 'tutor_proyecto.id_proyecto', '=', 'proyecto.id')
              ->join('tutor', 'tutor.id', '=', 'tutor_proyecto.id_tutor')
              ->where('tutor.id', $tutor->id)
              ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%")
              ->select('proyecto.*');
      }
    }

    /**
    * Obtiene todos los proyectos del docente logueado en los cuales es tutor
    */
    public function scopeProyectosTutor($query){
      $tutor = \Auth::user()->tutor;

      $query->leftJoin('tutor_proyecto', 'tutor_proyecto.id_proyecto', '=', 'proyecto.id')
              ->leftJoin('tutor', 'tutor.id', '=', 'tutor_proyecto.id_tutor')
              ->orWhere('tutor_proyecto.id_tutor', $tutor->id)
              ->select('proyecto.*');
    }

    /**
    * Obtiene todos los proyectos de pasantias comunitarias de la carrera a la que pertenece el coordinador
    */
    public function scopeProyectosCarreraCoordinadorComunitarias($query){
      $id_carrera = Auth::user()->coordinadorCarreraComunitarias->carrera->id;

      $query->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }

    /**
    * Obtiene todos los proyectos de pasantias profesionales de la carrera a la que pertenece el coordinador
    */
    public function scopeProyectosCarreraCoordinadorEmpresariales($query){
      $id_carrera = Auth::user()->coordinadorCarreraEmpresariales->carrera->id;

      $query->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }

    /**
    * Obtiene todos los proyectos aprobados o iniciados de pasantias comunitarias de la carrera a la que pertenece el coordinador
    */
    public function scopeProyectosAprobadosIniciadosCoordinadorComunitarias($query) {
      $id_carrera = Auth::user()->coordinadorCarreraComunitarias->carrera->id;

      $query->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('id_tipo_proyecto', 1)
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->where(function ($query) {
                $query->where('id_estado_proyecto', 2)
                      ->orWhere('id_estado_proyecto', 4)
                      ->orWhere('id_estado_proyecto', 5);
            })
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }

    /**
    * Filtra la lista de proyectos (por titulo) de pasantias comunitarias de la carrera a la que pertenece el coordinador
    */
    public function scopeFiltrarProyectosAprobadosIniciadosCoordinadorComunitarias($query, $tituloProyecto) {
      $id_carrera = Auth::user()->coordinadorCarreraComunitarias->carrera->id;

      $query->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('id_tipo_proyecto', 1)
            ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%")
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->where(function ($query) {
                $query->where('id_estado_proyecto', 2)
                      ->orWhere('id_estado_proyecto', 4)
                      ->orWhere('id_estado_proyecto', 5);
            })
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }

    /**
    * Obtiene todos los proyectos aprobados o iniciados de pasantias profesionales de la carrera a la que pertenece el coordinador
    */
    public function scopeProyectosAprobadosIniciadosCoordinadorEmpresariales($query) {
      $id_carrera = Auth::user()->coordinadorCarreraEmpresariales->carrera->id;

      $query->leftJoin('director', 'director.id', '=', 'proyecto.id_director')
            ->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('id_tipo_proyecto', 2)
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->where(function ($query) {
                $query->where('id_estado_proyecto', 2)
                      ->orWhere('id_estado_proyecto', 4)
                      ->orWhere('id_estado_proyecto', 5)
                      ->orWhere(function ($query) {
                          $query->where('id_estado_proyecto', 1)
                                ->where('id_director', Auth::user()->director->id);
                      });
            })
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }

    /**
    * Filtra la lista de proyectos (por titulo) de pasantias profesionales de la carrera a la que pertenece el coordinador
    */
    public function scopeFiltrarProyectosAprobadosIniciadosCoordinadorEmpresariales($query, $tituloProyecto) {
      $id_carrera = Auth::user()->coordinadorCarreraEmpresariales->carrera->id;

      $query->leftJoin('director', 'director.id', '=', 'proyecto.id_director')
            ->join('carrera_proyecto', 'carrera_proyecto.id_proyecto', '=', 'proyecto.id')
            ->where('id_tipo_proyecto', 2)
            ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%")
            ->where('carrera_proyecto.id_carrera', $id_carrera)
            ->where(function ($query) {
                $query->where('id_estado_proyecto', 2)
                      ->orWhere('id_estado_proyecto', 4)
                      ->orWhere('id_estado_proyecto', 5)
                      ->orWhere(function ($query) {
                          $query->where('id_estado_proyecto', 1)
                                ->where('id_director', Auth::user()->director->id);
                      });
            })
            ->select('proyecto.*');// Esto me asegura que retorne solamente las columnas de la tabla Proyecto
    }
}
