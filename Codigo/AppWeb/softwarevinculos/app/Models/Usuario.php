<?php

namespace SoftwareVinculos\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Usuario extends Model implements AuthenticatableContract
{
    use Authenticatable;
    use SoftDeletes;

    protected $table = 'usuario';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'apellidos', 'username', 'password', 'identificacion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function estudiante()
    {
        return $this->hasOne('SoftwareVinculos\Models\Estudiante', 'id_usuario');
    }

    public function director()
    {
        return $this->hasOne('SoftwareVinculos\Models\Director', 'id_usuario');
    }

    public function tutor()
    {
        return $this->hasOne('SoftwareVinculos\Models\Tutor', 'id_usuario');
    }

    public function coordinadorCarreraComunitarias()
    {
        return $this->hasOne('SoftwareVinculos\Models\CoordinadorCarreraComunitarias', 'id_usuario');
    }

    public function coordinadorCarreraEmpresariales()
    {
        return $this->hasOne('SoftwareVinculos\Models\CoordinadorCarreraEmpresariales', 'id_usuario');
    }

    public function administrador()
    {
        return $this->hasOne('SoftwareVinculos\Models\Administrador', 'id_usuario');
    }

    /**
     * Obtiene los roles del usuario
     */
    public function roles()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Rol', 'rol_usuario', 'id_usuario', 'id_rol');
    }

    /**
     * Obtiene los formularios creados por este usuario
     */
    public function formulariosCreados()
    {
        return $this->hasMany('SoftwareVinculos\Models\Formulario', 'id_creado_por');
    }

    /**
     * Obtiene los formularios dirigidos a este usuario
     */
    public function formulariosRecibidos()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Formulario', 'destinatario_formulario', 'id_destinatario', 'id_formulario');
    }

    /**
     * Obtiene las notificaciones dirigidas a este usuario
     */
    public function notificacionesRecibidas()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_destinatario');
    }

    /**
     * Obtiene las notificaciones dirigidas a este usuario
     */
    public function notificacionesFormularioRevisado()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_revisado_por');
    }

    /**
     * Obtiene las notificaciones de asignacion de rol a este usuario
     */
    public function notificacionesRolAsignadoUsuario()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_usuario_rol');
    }

    /**
    * Busca lista de usuarios según el nombre
    */
    public function scopeUsuariosSearchByNombre($query, $value){
      $query->where(\DB::raw("CONCAT(nombres, ' ', apellidos)"), "LIKE", "%$value%");
    }
}
