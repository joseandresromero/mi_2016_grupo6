<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programa extends Model
{
  use SoftDeletes;

  protected $table = 'programa';
  protected $primaryKey  = 'id';

  public function proyectos()
  {
      return $this->hasMany('SoftwareVinculos\Models\Proyecto', 'id_programa');
  }
}
