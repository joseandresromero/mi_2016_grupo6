<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Formulario extends Model
{
    use SoftDeletes;

    protected $table = 'formulario';
    protected $primaryKey  = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el usuario que creo el formulario
     */
    public function creadoPor()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_creado_por');
    }

    /**
     * Obtiene los delegados asignados al proyecto
     */
    public function destinatarios()
    {
        return $this->belongsToMany('SoftwareVinculos\Models\Usuario', 'destinatario_formulario', 'id_formulario', 'id_destinatario');
    }

    /**
     * Obtiene el proyecto al que pertenece el formulario
     */
    public function proyecto()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto');
    }

    /**
     * Obtiene el estado del formulario
     */
    public function estado()
    {
        return $this->belongsTo('SoftwareVinculos\Models\EstadoFormulario', 'id_estado_formulario');
    }

    /**
     * Obtiene el usuario al que pertenece el formulario
     */
    /*
    public function usuario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_creado_por');
    }
    */

    /**
     * Obtiene el tipo de formulario al que pertenece el formulario
     */
    public function tipoFormulario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\TipoFormulario', 'id_tipo_formulario');
    }

    /**
     * Obtiene el formulario FOR-UVS-01 asociado.
     */
    public function formularioUVS01()
    {
        return $this->hasOne('SoftwareVinculos\Models\ForUVS01', 'id_formulario');
    }

    /**
     * Obtiene los formularios FOR-UVS-04
     */
    public function formulariosUVS04()
    {
        return $this->hasMany('SoftwareVinculos\Models\ForUVS04', 'id_formulario');
    }

    /**
     * Obtiene el formulario FOR-UVS-13 asociado.
     */
    public function formularioUVS13()
    {
        return $this->hasOne('SoftwareVinculos\Models\ForUVS13', 'id_formulario');
    }

    /**
     * Obtiene los formularios FOR-UVS-14
     */
    public function formulariosUVS14()
    {
        return $this->hasOne('SoftwareVinculos\Models\ForUVS14', 'id_formulario');
    }

    /**
     * Obtiene el formulario FOR-UVS-15 asociado.
     */
    public function formularioUVS15()
    {
        return $this->hasOne('SoftwareVinculos\Models\ForUVS15', 'id_formulario');
    }

    /**
     * Obtiene el formulario FOR-UVS-16 asociado.
     */
    public function formularioUVS16()
    {
        return $this->hasOne('SoftwareVinculos\Models\ForUVS16', 'id_formulario');
    }

    /**
     * Obtiene las notificaciones de formularios
     */
    public function notificaciones()
    {
        return $this->hasMany('SoftwareVinculos\Models\Notificacion', 'id_formulario');
    }

    /**
    * Obtiene los formularios(de estudiantes) pendientes de revisión para un tutor en especifico de un proyecto comunitario.
    */
    public function scopeFormulariosProyectoComunitarioPendienteRevision($query, $id_proyecto_comunitario, $id_tutor){
      $users = DB::table('usuario')->join('estudiante',  'estudiante.id_usuario' , '=',  'usuario.id')
      ->where('estudiante.id_proyecto_comunitaria','=',$id_proyecto_comunitario)
      ->where('estudiante.id_tutor_comunitaria', '=', $id_tutor)
      ->lists('usuario.id');

      $query->where('id_proyecto', '=', $id_proyecto_comunitario)
            ->where('id_estado_formulario','=',2)
            ->whereIn('id_creado_por', $users)
            ->select('formulario.*');
    }

    /**
    * Obtiene los formularios(de estudiantes) pendientes de revisión para un tutor en especifico de un proyecto empresarial.
    */
    public function scopeFormulariosProyectoEmpresarialPendienteRevision($query, $id_proyecto_empresarial, $id_tutor){
      $users = DB::table('usuario')->join('estudiante',  'estudiante.id_usuario' , '=',  'usuario.id')
                                  ->where('estudiante.id_proyecto_empresarial','=',$id_proyecto_empresarial)
                                  ->where('estudiante.id_tutor_empresarial', '=', $id_tutor)
                                  ->lists('usuario.id');

      $query->where('id_proyecto', '=', $id_proyecto_empresarial)
            ->where('id_estado_formulario','=',2) //Estado formulario 'Enviado'
            ->whereIn('id_creado_por', $users)
            ->select('formulario.*');
    }

    /**
    * Obtiene los formularios disponibles para el DIRECTOR (formularios uvs 01) que tiene por llenar de un
    * proyecto en específico.
    */
    public function scopeFormulariosDisponibleDirector($query, $id_proyecto) {
      $query->where('id_proyecto','=' ,$id_proyecto)
            ->join('for_uvs_01', 'formulario.id' , '=',  'for_uvs_01.id_formulario')
            ->where('for_uvs_01.revisado_por_director', false)
            ->select('formulario.*');
    }

    /**
    * Obtiene los formularios disponibles para el TUTOR que tiene por llenar de un proyecto en específico.
    */
    public function scopeFormulariosDisponibleTutor($query, $id_proyecto, $id_usuario_tutor) {
      $query->where('id_proyecto','=' ,$id_proyecto)
            ->where('id_creado_por','=' , $id_usuario_tutor)
            ->select('formulario.*');
    }

    /**
    * Obtiene formulario FOR-UVS-13 de un estudiante en un proyecto especifico.
    */
    public function scopeForUVS13Estudiante($query, $id_proyecto, $id_estudiante) {
      $query->join('for_uvs_13', 'formulario.id', '=', 'for_uvs_13.id_formulario')
            ->where('id_tipo_formulario', 4)
            ->where('id_proyecto', $id_proyecto)
            ->where('for_uvs_13.id_estudiante', $id_estudiante)
            ->select('formulario.*');
    }

    /**
    * Obtiene formulario FOR-UVS-15 de un estudiante en un proyecto especifico.
    */
    public function scopeForUVS15Estudiante($query, $id_proyecto, $id_estudiante) {
      $query->join('for_uvs_15', 'formulario.id', '=', 'for_uvs_15.id_formulario')
            ->where('id_tipo_formulario', 6)
            ->where('id_proyecto', $id_proyecto)
            ->where('for_uvs_15.id_estudiante', $id_estudiante)
            ->select('formulario.*');
    }

    /**
    * Obtiene formulario FOR-UVS-16 de un estudiante en un proyecto especifico.
    */
    public function scopeForUVS16Estudiante($query, $id_proyecto, $id_estudiante) {
      $query->join('for_uvs_16', 'formulario.id', '=', 'for_uvs_16.id_formulario')
            ->where('id_tipo_formulario', 7)
            ->where('id_proyecto', $id_proyecto)
            ->where('for_uvs_16.id_estudiante', $id_estudiante)
            ->select('formulario.*');
    }

    /**
    * Obtiene formularios recibidos para un usuario.
    */
    public function scopeFormulariosRecibidosUsuario($query, $id_usuario) {
      $query->join('destinatario_formulario', 'formulario.id', '=', 'destinatario_formulario.id_formulario')
            ->where('destinatario_formulario.id_destinatario', $id_usuario)
            ->select('formulario.*');
    }

    /**
    * Filtra la lista de formularios recibidos para un usuario por el titulo del proyecto al cual pertenece el formulario.
    */
    public function scopeFiltrarFormulariosRecibidosUsuarioPorTituloProyecto($query, $id_usuario, $tituloProyecto) {
      $query->join('destinatario_formulario', 'formulario.id', '=', 'destinatario_formulario.id_formulario')
            ->join('proyecto', 'formulario.id_proyecto', '=', 'proyecto.id')
            ->where('proyecto.titulo', "LIKE", "%$tituloProyecto%")
            ->where('destinatario_formulario.id_destinatario', $id_usuario)
            ->select('formulario.*');
    }
}
