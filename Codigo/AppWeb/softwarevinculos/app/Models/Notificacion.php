<?php

namespace SoftwareVinculos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notificacion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'notificacion';
    protected $primaryKey  = 'id';

    /**
     * Obtiene el formulario correspondiente a la notificacion
     */
    public function formulario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Formulario', 'id_formulario');
    }

    /**
     * Obtiene el usuario al que va dirigida la notificacion
     */
    public function destinatario()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_destinatario');
    }

    /**
     * Obtiene el proyecto involucrado en esta notificacion
     */
    public function proyecto()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Proyecto', 'id_proyecto');
    }

    /**
     * Obtiene el usuario tutor involucrado en la notificacion
     */
    public function tutorAsignado()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Tutor', 'id_tutor_asignado');
    }

    /**
     * Obtiene el usuario estudiante involucrado en la notificacion
     */
    public function estudianteAsignado()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Estudiante', 'id_estudiante_asignado');
    }

    /**
     * Obtiene el tipo de notificacion
     */
    public function tipo()
    {
        return $this->belongsTo('SoftwareVinculos\Models\TipoNotificacion', 'id_tipo_notificacion');
    }

    /**
     * Obtiene el usuario que aprobo o rechazo el formulario correspondiente a la notificacion
     */
    public function revisadoPor()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_revisado_por');
    }

    /**
     * Obtiene el usuario al que se le asigno el rol
     */
    public function usuarioRol()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Usuario', 'id_usuario_rol');
    }

    /**
     * Obtiene el usuario al que se le asigno el rol
     */
    public function rolAsignado()
    {
        return $this->belongsTo('SoftwareVinculos\Models\Rol', 'id_rol_asignado');
    }

    /**
    * Filtra las notificaciones de este usuario por proyecto
    */
    public function scopeFiltrarPorProyecto($query, $value_proyecto){
      $id_usuario = \Auth::user()->id;
      $query->where('notificacion.id_destinatario', $id_usuario)
            ->where('notificacion.id_proyecto', $value_proyecto);
    }

    /**
    * Filtra todas las notificaciones de este usuario
    */
    public function scopeNotificacionesUsuario($query){
      $id_usuario = \Auth::user()->id;
      $query->where('notificacion.id_destinatario', $id_usuario);
    }
}
