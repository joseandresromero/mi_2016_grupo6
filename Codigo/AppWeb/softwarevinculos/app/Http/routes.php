<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['auth']], function() {

    route::get('logout', ['as' => 'logout', 'uses' => 'AuthenticationController@logout']);

    route::resource('usuario','UsuarioController');

    route::get('subirFormularios/{id_proyecto}', ['as' => 'subirFormularios', 'uses' => 'FormularioController@subirFormularios']);

    route::post('subirFormularioFirmado', ['as' => 'subirFormularioFirmado', 'uses' => 'FormularioController@subirFormularioFirmado']);

    route::get('descargarFormularioFirmado/{id_formulario}', ['as' => 'descargarFormularioFirmado', 'uses' => 'FormularioController@descargarFormularioFirmado']);

    route::get('downloadForUVS15/{id_formulario}', ['as' => 'downloadForUVS15', 'uses' => 'FormularioController@downloadForUVS15']);

    //ESTO DEBERIA IR EN UN MIDDLEWARE COMPARTIDO ENTRE ESTUDIANTE Y director
    route::put('updateForUVS01/{id_proyecto}/{id_estudiante}/',  ['as' => 'updateForUVS01', 'uses' => 'ForUVS01Controller@updateForUVS01']);

    /*
    * PAGINAS DE ERROR
    */
    route::get('accesoDenegado', ['as' => 'accesoDenegado', 'uses' => 'UsuarioController@accesoDenegado']);

    Route::group(['middleware' => ['informacion_actualizada']], function() {

      route::get('/', ['as' => 'home', 'uses' => 'UsuarioController@mostrarHome']);

      route::get('formulariosDisponibles', ['as' => 'formulariosDisponibles', 'uses' => 'UsuarioController@formulariosDisponibles']);
      route::get('formulariosDisponiblesTutor/{id_proyecto}', ['as' => 'formulariosDisponiblesTutor', 'uses' => 'TutorController@formulariosDisponiblesTutor']);

      route::post('existePresupuesto',  ['as' => 'existePresupuesto', 'uses' => 'ProyectoController@existePresupuesto']);
      route::post('existeCronograma',  ['as' => 'existeCronograma', 'uses' => 'ProyectoController@existeCronograma']);
      route::post('existeConvenio',  ['as' => 'existeConvenio', 'uses' => 'OrganizacionController@existeConvenio']);

      route::get('pasantiasComunitarias', ['as' => 'pasantiasComunitarias', 'uses' => 'EstudianteController@pasantiasComunitarias']);
      route::get('pasantiasEmpresariales', ['as' => 'pasantiasEmpresariales', 'uses' => 'EstudianteController@pasantiasEmpresariales']);

      route::get('seguimiento', ['as' => 'seguimiento', 'uses' => 'NotificacionController@seguimiento']);

      route::get('flujo_proyecto/{id_proyecto}', ['as' => 'flujo_proyecto', 'uses' => 'ProyectoController@flujoProyecto']);
      route::get('detalle_proyecto/{id_proyecto}', ['as' => 'detalle_proyecto', 'uses' => 'ProyectoController@detalleProyecto']);
      route::get('detalle_estudiante_proyecto/{id_estudiante}/{id_proyecto}/', ['as' => 'detalle_estudiante_proyecto', 'uses' => 'EstudianteController@detalleEstudianteProyecto']);

      route::post('adjuntar_carta_compromiso_comunitarias',  ['as' => 'adjuntar_carta_compromiso_comunitarias', 'uses' => 'EstudianteController@adjuntarCartaCompromisoComunitarias']);

      route::post('adjuntar_carta_compromiso_empresariales',  ['as' => 'adjuntar_carta_compromiso_empresariales', 'uses' => 'EstudianteController@adjuntarCartaCompromisoEmpresariales']);


      route::get('verForUVS05', ['as' => 'verForUVS05', 'uses' => 'FormularioController@verForUVS05']);


      //Route::group(['middleware' => ['CoordinadorCarrera']], function() {
      route::get('listaestudiantesproyecto/{id_proyecto}/',  ['as' => 'listaestudiantesproyecto', 'uses' => 'ProyectoController@listaEstudiantesProyecto']);
      route::get('listatutoresproyecto/{id_proyecto}/',  ['as' => 'listatutoresproyecto', 'uses' => 'ProyectoController@listaTutoresProyecto']);

      route::get('autocompleteTutor', ['as' => 'autocompleteTutor', 'uses' => 'TutorController@autocomplete']);
      route::resource('estudiante','EstudianteController');
      route::get('asignarproyectoestudiante/{id_proyecto}/',  ['as' => 'asignarproyectoestudiante', 'uses' => 'EstudianteController@asignarEstudianteProyecto']);
      route::get('asignarproyectotutor/{id_proyecto}/',  ['as' => 'asignarproyectotutor', 'uses' => 'TutorController@asignarTutorProyecto']);
      route::get('asignartutorestudiante/{id_estudiante}/{id_proyecto}/',  ['as' => 'asignartutorestudiante', 'uses' => 'TutorController@asignarTutorEstudiante']);

      route::get('desasignarproyectotutor/{id_proyecto}/{id_tutor}',  ['as' => 'desasignarproyectotutor', 'uses' => 'TutorController@desasignarTutorProyecto']);
      route::get('desasignarproyectoestudiante/{id_proyecto}/{id_estudiante}',  ['as' => 'desasignarproyectoestudiante', 'uses' => 'EstudianteController@desasignarEstudianteProyecto']);

      route::get('modoEditarFormularioUVS01/{id_formulario}', ['as' => 'modoEditarFormularioUVS01', 'uses' => 'ForUVS01Controller@modoEditarFormularioUVS01']);
      route::get('modoVerFormularioUVS01/{id_formulario}', ['as' => 'modoVerFormularioUVS01', 'uses' => 'ForUVS01Controller@modoVerFormularioUVS01']);

      route::get('modoVerPorEstudianteForUVS13/{id_formulario}/{id_estudiante}',  ['as' => 'modoVerPorEstudianteForUVS13', 'uses' => 'ForUVS13Controller@modoVerPorEstudianteForUVS13']);

      route::get('modoVerForUVS13Completo/{id_proyecto}/',  ['as' => 'modoVerForUVS13Completo', 'uses' => 'ForUVS13Controller@modoVerForUVS13Completo']);

      route::get('modoEditarFormularioUVS14/{id_formulario}', ['as' => 'modoEditarFormularioUVS14', 'uses' => 'ForUVS14Controller@modoEditarFormularioUVS14']);
      route::get('modoVerFormularioUVS14/{id_formulario}', ['as' => 'modoVerFormularioUVS14', 'uses' => 'ForUVS14Controller@modoVerFormularioUVS14']);

      route::get('modoEditarFormularioUVS16/{id_formulario}', ['as' => 'modoEditarFormularioUVS16', 'uses' => 'ForUVS16Controller@modoEditarFormularioUVS16']);



      route::resource('foruvs01','ForUVS01Controller');
      route::get('enviarForUVS01/{id_proyecto}', ['as' => 'enviarForUVS01', 'uses' => 'ForUVS01Controller@enviarFormularioUVS01']);

      route::resource('foruvs14','ForUVS14Controller');
      route::get('enviarForUVS14/{id_proyecto}', ['as' => 'enviarForUVS14', 'uses' => 'ForUVS14Controller@enviarFormularioUVS14']);

      route::resource('foruvs16','ForUVS16Controller');
      route::get('enviarForUVS16/{id_proyecto}/{id_forUVS16}', ['as' => 'enviarForUVS16', 'uses' => 'ForUVS16Controller@enviarFormularioUVS16']);
      //});

      route::get('modoVerProyecto/{id_proyecto}', ['as' => 'modoVerProyecto', 'uses' => 'ProyectoController@modoVerProyecto']);

      route::get('formulariospendientesrevisiontutor/{id_proyecto}/',  ['as' => 'formulariospendientesrevisiontutor', 'uses' => 'TutorController@formulariosPendientesRevisionProyecto']);

      route::put('revisar_foruvs01/{id_formulario}',  ['as' => 'revisar_foruvs01', 'uses' => 'ForUVS01Controller@revisarForUVS01']);
      route::put('revisar_foruvs14/{id_formulario}',  ['as' => 'revisar_foruvs14', 'uses' => 'ForUVS14Controller@revisarForUVS14']);

      Route::group(['middleware' => ['tutor']], function() {
        route::get('modoEditarPorEstudianteForUVS13/{id_formulario}/{id_estudiante}',  ['as' => 'modoEditarPorEstudianteForUVS13', 'uses' => 'ForUVS13Controller@modoEditarPorEstudianteForUVS13']);
        route::post('guardarForUVS13PorEstudiante/{id_formulario}/{id_estudiante}',  ['as' => 'guardarForUVS13PorEstudiante', 'uses' => 'ForUVS13Controller@guardarForUVS13PorEstudiante']);
        route::get('modoVerFormularioUVS15/{id_formulario}', ['as' => 'modoVerFormularioUVS15', 'uses' => 'ForUVS15Controller@modoVerFormularioUVS15']);
      });

      /* ESTE MODOO VER TIENE QUE SER COMPARTIDO PARA TUTOR Y COORDINADOR */
      route::get('modoVerFormularioUVS16/{id_formulario}', ['as' => 'modoVerFormularioUVS16', 'uses' => 'ForUVS16Controller@modoVerFormularioUVS16']);

      Route::group(['middleware' => ['coordinador']], function() {
        route::put('revisar_proyecto/{id_proyecto}',  ['as' => 'revisar_proyecto', 'uses' => 'ProyectoController@revisarProyecto']);
        route::get('revisar_formularios',  ['as' => 'revisar_formularios', 'uses' => 'FormularioController@revisarFormularios']);

        route::put('revisar_foruvs16/{id_formulario}',  ['as' => 'revisar_foruvs16', 'uses' => 'ForUVS16Controller@revisarForUVS16']);
      });

      Route::group(['middleware' => ['director']], function() {

          route::resource('proyecto','ProyectoController');

          route::get('formulariosDisponiblesDirector/{id_proyecto}', ['as' => 'formulariosDisponiblesDirector', 'uses' => 'DirectorController@formulariosDisponiblesDirector']);

          ///
          /*
          route::get('homeproyecto/{id_proyecto}/',  ['as' => 'homeproyecto', 'uses' => 'ProyectoController@indexProyectoEspecifico']);
          route::get('homeestudiantes/{id_proyecto}/',  ['as' => 'homeestudiantes', 'uses' => 'ProyectoController@listaEstudiantes']);
          route::get('hometutores/{id_proyecto}/',  ['as' => 'hometutores', 'uses' => 'ProyectoController@listaTutores']);
          route::get('autocompleteTutor', ['as' => 'autocompleteTutor', 'uses' => 'TutorController@autocomplete']);
          route::resource('estudiante','EstudianteController');
          route::get('asignarproyectoestudiante/{id_proyecto}/',  ['as' => 'asignarproyectoestudiante', 'uses' => 'EstudianteController@asignarEstudianteProyecto']);
          route::get('asignarproyectotutor/{id_proyecto}/',  ['as' => 'asignarproyectotutor', 'uses' => 'TutorController@asignarTutorProyecto']);
          */
          ////


          route::get('downloadForUVS01', ['as' => 'downloadForUVS01', 'uses' => 'FormularioController@downloadForUVS01']);

          route::get('downloadForUVS04/{id_proyecto}', ['as' => 'downloadForUVS04', 'uses' => 'FormularioController@downloadForUVS04']);

          route::get('enviarForUVS04/{id_proyecto}', ['as' => 'enviarForUVS04', 'uses' => 'FormularioController@enviarFormularioUVS04']);

          route::get('verForUVS02', ['as' => 'verForUVS02', 'uses' => 'FormularioController@verForUVS02']);
          route::get('verForUVS07', ['as' => 'verForUVS07', 'uses' => 'FormularioController@verForUVS07']);
          route::get('verForUVS12', ['as' => 'verForUVS12', 'uses' => 'FormularioController@verForUVS12']);
          route::get('verForUVS13', ['as' => 'verForUVS13', 'uses' => 'FormularioController@verForUVS13']);
          route::get('verForUVS17', ['as' => 'verForUVS17', 'uses' => 'FormularioController@verForUVS17']);
          route::get('verForUVS21', ['as' => 'verForUVS21', 'uses' => 'FormularioController@verForUVS21']);
          route::get('verForUVS28', ['as' => 'verForUVS28', 'uses' => 'FormularioController@verForUVS28']);
          route::get('verForUVS29', ['as' => 'verForUVS29', 'uses' => 'FormularioController@verForUVS29']);

          route::get('autocompleteCarrera', ['as' => 'autocompleteCarrera', 'uses' => 'CarreraController@autocomplete']);
          route::get('autocompleteOrganizacion', ['as' => 'autocompleteOrganizacion', 'uses' => 'OrganizacionController@autocomplete']);
          route::get('autocompletePrograma', ['as' => 'autocompletePrograma', 'uses' => 'ProgramaController@autocomplete']);
          route::get('autocompleteMateria', ['as' => 'autocompleteMateria', 'uses' => 'MateriaController@autocomplete']);

          route::resource('organizacion','OrganizacionController');

          route::get('eliminar_organizacion/{id_organizacion}',  ['as' => 'eliminar_organizacion', 'uses' => 'OrganizacionController@eliminarOrganizacion']);

          route::post('guardar_organizacion',  ['as' => 'guardar_organizacion', 'uses' => 'OrganizacionController@guardarOrganizacion']);

          route::post('guardar_organizacion_base',  ['as' => 'guardar_organizacion_base', 'uses' => 'OrganizacionController@guardarOrganizacionBase']);

          route::get('organizaciones',  ['as' => 'organizaciones', 'uses' => 'OrganizacionController@index']);

          route::post('guardar_delegado',  ['as' => 'guardar_delegado', 'uses' => 'DelegadoOrganizacionController@guardarDelegado']);
          route::get('proyecto/autocompleteDelegado/{id_organizacion}', ['as' => 'autocompleteDelegado', 'uses' => 'DelegadoOrganizacionController@autocomplete']);
          route::get('proyecto/{id_proyecto}/autocompleteDelegado/{id_organizacion}', ['as' => 'autocompleteDelegado', 'uses' => 'DelegadoOrganizacionController@autocomplete']);

          route::post('obtener_estudiante',  ['as' => 'obtener_estudiante', 'uses' => 'EstudianteController@obtenerDatosEstudiante']);
          route::post('obtener_programa',  ['as' => 'obtener_programa', 'uses' => 'ProgramaController@obtenerDatosPrograma']);

          route::post('get_coordinador_por_tipo_proyecto',  ['as' => 'get_coordinador_por_tipo_proyecto', 'uses' => 'CarreraController@obtenerCoordinadorPorTipoProyecto']);

          route::post('iniciar_proyecto',  ['as' => 'iniciar_proyecto', 'uses' => 'ProyectoController@iniciarProyecto']);

          route::post('adjuntar_convenio_organizacion',  ['as' => 'adjuntar_convenio_organizacion', 'uses' => 'OrganizacionController@adjuntarConvenioOrganizacion']);

          route::get('verCronograma/{id_proyecto}', ['as' => 'verCronograma', 'uses' => 'ProyectoController@verCronograma']);

          route::get('verPresupuesto/{id_proyecto}', ['as' => 'verPresupuesto', 'uses' => 'ProyectoController@verPresupuesto']);

          route::get('proyecto/verConvenio/{id_organizacion}', ['as' => 'verConvenio', 'uses' => 'OrganizacionController@verConvenioCrearProyecto']);

         route::get('proyecto/{id_proyecto}/verConvenio/{id_organizacion}', ['as' => 'verConvenio', 'uses' => 'OrganizacionController@verConvenioEditarProyecto']);
      });

      route::resource('forUVS01','ForUVS01Controller');
      route::resource('forUVS08','ForUVS08Controller');
      route::resource('forUVS13','ForUVS13Controller');
      route::resource('forUVS14','ForUVS14Controller');
      route::resource('forUVS15','ForUVS15Controller');
      route::resource('forUVS16','ForUVS16Controller');
      route::resource('forUVS18','ForUVS18Controller');
      route::resource('forUVS24','ForUVS24Controller');
    });

    Route::group(['middleware' => ['administrador']], function() {
      route::resource('programa','ProgramaController');
      route::get('programas', ['as' => 'programas', 'uses' => 'ProgramaController@index']);
      route::get('eliminar_programa/{id_programa}',  ['as' => 'eliminar_programa', 'uses' => 'ProgramaController@eliminarPrograma']);
      route::post('guardar_programa',  ['as' => 'guardar_programa', 'uses' => 'ProgramaController@guardarPrograma']);
      route::post('buscar_usuario',  ['as' => 'buscar_usuario', 'uses' => 'UsuarioController@buscarUsuario']);
      route::post('buscar_usuario_por_nombre',  ['as' => 'buscar_usuario_por_nombre', 'uses' => 'UsuarioController@buscarUsuarioPorNombre']);
      route::post('get_roles_usuario',  ['as' => 'get_roles_usuario', 'uses' => 'UsuarioController@getRolesUsuario']);
      route::post('guardar_usuario',  ['as' => 'guardar_usuario', 'uses' => 'UsuarioController@guardarUsuario']);
      route::get('quitar_rol/{id_usuario}/{id_rol}',  ['as' => 'quitar_rol', 'uses' => 'UsuarioController@quitarRol']);
      route::post('get_tipos_usuario',  ['as' => 'get_tipos_usuario', 'uses' => 'UsuarioController@getTiposUsuario']);
      route::post('asignar_rol',  ['as' => 'asignar_rol', 'uses' => 'UsuarioController@asignarRol']);
      route::post('cambiar_carrera',  ['as' => 'cambiar_carrera', 'uses' => 'UsuarioController@cambiarCarrera']);
    });
});

Route::group(['middleware' => ['guest']], function() {

    route::get('login', ['as' => 'login', function () {
        return view('auth.login');
    }]);

    route::post('authenticate', ['as' => 'authenticate', 'uses' => 'AuthenticationController@authenticate']);

    /*****/
    route::get('proyectosCoordinador', ['as' => 'proyectosCoordinador', function () {
        return view('coordinador.index');
    }]);

    route::get('ong', ['as' => 'ong', function () {
        return view('ong.ong');
    }]);

});
