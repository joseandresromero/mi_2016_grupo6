<?php

namespace SoftwareVinculos\Http\Middleware;

use Auth;
use Closure;

class RedirectIfNotAdministrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->administrador) {
            return redirect()->route('accesoDenegado');
        }

        return $next($request);
    }
}
