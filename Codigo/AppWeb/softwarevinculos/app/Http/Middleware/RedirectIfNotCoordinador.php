<?php

namespace SoftwareVinculos\Http\Middleware;

use Auth;
use Closure;

class RedirectIfNotCoordinador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->coordinadorCarreraComunitarias && !Auth::user()->coordinadorCarreraEmpresariales) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
