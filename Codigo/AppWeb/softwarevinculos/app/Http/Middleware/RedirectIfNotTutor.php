<?php

namespace SoftwareVinculos\Http\Middleware;

use Auth;
use Closure;

class RedirectIfNotTutor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->tutor) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
