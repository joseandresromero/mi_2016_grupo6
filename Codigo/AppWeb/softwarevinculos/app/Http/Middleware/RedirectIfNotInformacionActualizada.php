<?php

namespace SoftwareVinculos\Http\Middleware;

use Auth;
use Closure;

class RedirectIfNotInformacionActualizada
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = \SoftwareVinculos\Models\Usuario::find(Auth::user()->id);

        if ($usuario->informacion_actualizada == false && $usuario->administrador == null) {
            session()->flash('flash_message', 'Debe llenar todos los campos de información para habilitar las demas opciones del sistema.');
            session()->flash('flash_type', 'warning');
            return redirect()->route('usuario.edit', $usuario->id);
        }

        return $next($request);
    }
}
