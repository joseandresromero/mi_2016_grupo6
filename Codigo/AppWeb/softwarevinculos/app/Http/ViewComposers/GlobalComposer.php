<?php

namespace SoftwareVinculos\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class GlobalComposer {

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (\Auth::user() != null) {
            $usuario = \Auth::user();

            //Si la pagina anterior fue la de seguimiento, marco como vistas las notificaciones
            if (strpos(\URL::previous(), 'seguimiento') !== false) {
              foreach (\Auth::user()->notificacionesRecibidas as $notificacion) {
                $notificacion->visto = true;
                $notificacion->save();
              }
            }

            $notificaciones = \Auth::user()->notificacionesRecibidas->all();

            $numNotificaciones = \Auth::user()->notificacionesRecibidas()->where('visto', false)->count();

            view()->share([
              'notificacionesRecibidas' => $notificaciones,
              'numNotificaciones' => $numNotificaciones,
              'usuario' => $usuario
            ]);
        }
    }

}
