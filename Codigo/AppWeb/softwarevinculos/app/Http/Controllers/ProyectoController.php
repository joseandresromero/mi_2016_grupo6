<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Models\Proyecto;
use SoftwareVinculos\Models\EstadoProyecto;
use SoftwareVinculos\Models\Programa;
use SoftwareVinculos\Models\Organizacion;
use SoftwareVinculos\Models\ObjetivoEspecifico;
use SoftwareVinculos\Models\Actividad;
use SoftwareVinculos\Models\ProductoEsperado;
use SoftwareVinculos\Models\CompromisoOrganizacion;
use SoftwareVinculos\Models\LineaAccion;
use SoftwareVinculos\Models\ObjetivoDesarrolloMilenio;
use SoftwareVinculos\Models\ObjetivoPnbv;
use SoftwareVinculos\Models\ContextoPnbv;
use SoftwareVinculos\Models\EjePnbv;
use SoftwareVinculos\Models\TensionProblemaPnbv;
use SoftwareVinculos\Models\DelegadoOrganizacion;
use SoftwareVinculos\Models\Estudiante;
use SoftwareVinculos\Models\EstadoEstudiante;
use SoftwareVinculos\Models\Tutor;
use SoftwareVinculos\Models\ResultadoAprendizaje;
use SoftwareVinculos\Models\Materia;

use SoftwareVinculos\Http\Controllers\NotificacionController;
use SoftwareVinculos\Http\Controllers\FormularioController;

use Auth;

class ProyectoController extends Controller
{
    public function create(Request $request)
    {
        $idTipoProyecto = 1; //Pasantias Comunitarias
        if (\Auth::user()->coordinadorCarreraEmpresariales != null) {
          $idTipoProyecto = 2; //Pasantias Pre-profesionales
        }

        $lineasAccion = \SoftwareVinculos\Models\LineaAccion::all();

        $programas_options = Programa::all()->lists('nombre', 'id');
        $programas_options->prepend('Seleccione programa', 0);

        return view('formularios.forUVS04.modoEditar')->with(['lineasAccion' => $lineasAccion, 'idTipoProyecto' => $idTipoProyecto, 'programas_options' => $programas_options]);
    }

    public function listaEstudiantesProyecto($id_proyecto)
    {
      $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);
      $proyectoEspecifico = $proyecto;

      $tutores_options = Tutor::tutoresAsignadosAProyecto($id_proyecto)->lists('full_name','id');


      if(Auth::user()->coordinadorCarreraComunitarias || Auth::user()->coordinadorCarreraEmpresariales){
        return view('coordinador.proyecto.estudiantes', compact('proyecto', 'tutores_options', 'proyectoEspecifico'));
      }

    }

    public function listaTutoresProyecto($id_proyecto)
    {
      $proyectoEspecifico = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);

      return view('coordinador.proyecto.tutores', compact('proyectoEspecifico'));
    }

    public function flujoProyecto($id_proyecto)
    {
      $proyectoEspecifico = Proyecto::find($id_proyecto);

      if ($proyectoEspecifico->tipo->id == 1) { //Pasantias Comunitarias
        $estudiantesUVS01 = Estudiante::estudiantesForUVS01AprobadoComunitarias($id_proyecto);
        $estudiantesUVS13 = Estudiante::estudiantesForUVS13AprobadoComunitarias($id_proyecto);
        $estudiantesUVS14 = Estudiante::estudiantesForUVS14AprobadoComunitarias($id_proyecto);
        $estudiantesUVS15 = Estudiante::estudiantesForUVS15AprobadoComunitarias($id_proyecto);
        $estudiantesUVS16 = Estudiante::estudiantesForUVS16AprobadoComunitarias($id_proyecto);

        $numEstudiantesProyecto = $proyectoEspecifico->estudiantesComunitarias()->count();
      } else {
        $estudiantesUVS01 = Estudiante::estudiantesForUVS01AprobadoEmpresariales($id_proyecto);
        $estudiantesUVS13 = Estudiante::estudiantesForUVS13AprobadoEmpresariales($id_proyecto);
        $estudiantesUVS14 = Estudiante::estudiantesForUVS14AprobadoEmpresariales($id_proyecto);
        $estudiantesUVS15 = Estudiante::estudiantesForUVS15AprobadoEmpresariales($id_proyecto);
        $estudiantesUVS16 = Estudiante::estudiantesForUVS16AprobadoEmpresariales($id_proyecto);

        $numEstudiantesProyecto = $proyectoEspecifico->estudiantesEmpresarial()->count();
      }

      return view('proyecto.flujo', compact('proyectoEspecifico', 'estudiantesUVS01', 'estudiantesUVS13', 'numEstudiantesProyecto', 'estudiantesUVS14', 'estudiantesUVS15', 'estudiantesUVS16'));
    }

    public function detalleProyecto($id_proyecto){
      $proyectoEspecifico = Proyecto::find($id_proyecto);

      return view('proyecto.detalleProyecto', compact('proyectoEspecifico'));
    }

    public function store(Request $request)
    {
      //dd($request);
      //Se crea el nuevo proyecto
      $nuevoProyecto = new Proyecto;
      $this->fillProyectoConInputs($request, $nuevoProyecto);

      $estado = \SoftwareVinculos\Models\EstadoProyecto::find(1); //Pendiente
      $nuevoProyecto->estado()->associate($estado);

      $tipo = \SoftwareVinculos\Models\TipoProyecto::find($request->tipo_proyecto);
      $nuevoProyecto->tipo()->associate($tipo);

      $director = $request->user()->director;
      $nuevoProyecto->director()->associate($director);

      //Programa
      $idPrograma = $request->get('programaProyecto');
      if($idPrograma != null){
        $programa = \SoftwareVinculos\Models\Programa::find($idPrograma);
        $nuevoProyecto->programa()->associate($programa);
      }

      $organizacion = \SoftwareVinculos\Models\Organizacion::find($request->id_organizacion);
      $nuevoProyecto->organizacion()->associate($organizacion);

      $nuevoProyecto->save();

      $this->guardarDelegados($request, $nuevoProyecto);

      //
      //$this->guardarObjetivosActividades($request, $nuevoProyecto);

      $this->asignarEstudiantesAProyecto($request, $nuevoProyecto);

      $this->guardarResultadosAprendizaje($request, $nuevoProyecto);

      $this->guardarCarreras($request, $nuevoProyecto);

      $this->guardarLineasAccion($request, $nuevoProyecto);

      // Guardo objetivos especificos
      $this->guardarObjetivosEspecificos($request, $nuevoProyecto);

      $this->adjuntarConvenioOrganizacion($request, $organizacion);

      $this->adjuntarCronograma($request, $nuevoProyecto);

      $this->adjuntarPresupuesto($request, $nuevoProyecto);

      FormularioController::crearFormularioUVS04($nuevoProyecto->id, $request->submit);

      return redirect()->route('home');
    }

    public function adjuntarCronograma($request, $proyecto) {
      if ($request->hasFile('cronogramaUpload')) {
        $slash = config('softwarevinculos.slash');

        $file = $request->file('cronogramaUpload');
        $extension = $file->getClientOriginalExtension();
        // $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'organizaciones'.$slash.$organizacion->id.$slash.'convenioOrganizacion.'.$extension;
        $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'proyectos'.$slash.$proyecto->id;
        $relativeUrl = '/uploads/proyectos/'.$proyecto->id;
        $filename = 'cronograma.'.$extension;

        // $file->move($url);
        $file->move($url,$filename);

        $proyecto->url_cronograma = $relativeUrl.'/'.$filename;
        $proyecto->save();
      }
    }

    public function verCronograma($id_proyecto) {
      $proyecto = Proyecto::find($id_proyecto);

      if ($proyecto->url_cronograma != null) {
        if (file_exists($proyecto->url_cronograma) == true) {
          return response()->download($proyecto->url_cronograma);
        } else {
          dd('No se ha subido cronograma');
        }
      } else {
        dd('No se ha subido cronograma');
      }
    }

    public function existeCronograma(Request $request) {
      $slash = config('softwarevinculos.slash');
      $proyecto = Proyecto::find($request->id_proyecto);

      $result = null;

      if ($proyecto->url_cronograma != null) {

        if ($slash != '/') {
          $urlFull = base_path().$slash.'public'.str_replace("/", "\\", $proyecto->url_cronograma);
        } else {
          $urlFull = base_path().$slash.'public'.$proyecto->url_cronograma;
        }

        if (file_exists($urlFull) == true) {
          $result = \URL::to('/').$proyecto->url_cronograma;
        }
      }

      return response()->json(['result' => $result]);
    }

    public function adjuntarPresupuesto($request, $proyecto) {
      if ($request->hasFile('presupuestoUpload')) {
        $slash = config('softwarevinculos.slash');

        $file = $request->file('presupuestoUpload');
        $extension = $file->getClientOriginalExtension();
        // $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'organizaciones'.$slash.$organizacion->id.$slash.'convenioOrganizacion.'.$extension;
        $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'proyectos'.$slash.$proyecto->id;
        $relativeUrl = '/uploads/proyectos/'.$proyecto->id;
        $filename = 'presupuesto.'.$extension;

        // $file->move($url);
        $file->move($url,$filename);

        //$proyecto->url_presupuesto = \URL::to('/').$relativeUrl.'/'.$filename;
        $proyecto->url_presupuesto = $relativeUrl.'/'.$filename;
        $proyecto->save();
      }
    }

    public function verPresupuesto($id_proyecto) {
      $proyecto = Proyecto::find($id_proyecto);

      if ($proyecto->url_presupuesto != null) {
        if (file_exists($proyecto->url_presupuesto) == true) {
          return response()->download($proyecto->url_presupuesto);
        } else {
          dd('No se ha subido presupuesto');
        }
      } else {
        dd('No se ha subido presupuesto');
      }
    }

    public function existePresupuesto(Request $request) {
      $slash = config('softwarevinculos.slash');
      $proyecto = Proyecto::find($request->id_proyecto);

      $result = null;

      if ($proyecto->url_presupuesto != null) {

        if ($slash != '/') {
          $urlFull = base_path().$slash.'public'.str_replace("/", "\\", $proyecto->url_presupuesto);
        } else {
          $urlFull = base_path().$slash.'public'.$proyecto->url_presupuesto;
        }

        if (file_exists($urlFull) == true) {
          $result = \URL::to('/').$proyecto->url_presupuesto;
        }
      }

      return response()->json(['result' => $result]);
    }

    public function adjuntarConvenioOrganizacion($request, $organizacion) {
      if ($request->hasFile('convenioOrganizacionUpload')) {
        $slash = config('softwarevinculos.slash');

        $file = $request->file('convenioOrganizacionUpload');
        $extension = $file->getClientOriginalExtension();
        // $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'organizaciones'.$slash.$organizacion->id.$slash.'convenioOrganizacion.'.$extension;
        $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'organizaciones'.$slash.$organizacion->id;
        $relativeUrl = '/uploads/organizaciones/'.$organizacion->id;
        $filename = 'convenioOrganizacion.'.$extension;

        // $file->move($url);
        $file->move($url,$filename);

        $organizacion->url_convenio = $relativeUrl.'/'.$filename;
        $organizacion->save();
      }
    }

    public function edit($id_proyecto)
    {
        $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);

        if($proyecto == null){
          dd('Proyecto no encontrado');
        }

        $fechaInicio = date('d/m/Y',strtotime($proyecto->fecha_inicio));
        $fechaFinalizacion = date('d/m/Y',strtotime($proyecto->fecha_finalizacion));

        $tipoFormulario = \SoftwareVinculos\Models\TipoFormulario::find(2); //FOR-UVS-04
        $formularioUVS04 = $proyecto->formularios()->where('id_tipo_formulario', $tipoFormulario->id)->first();

        $tiposProyecto = \SoftwareVinculos\Models\TipoProyecto::all();
        foreach ($tiposProyecto as $tipo) {
          $arrayTiposProyecto[$tipo->id] = $tipo->descripcion;
        }

        $carrerasAdicionales = array();
        $carreraPrincipal = null;
        foreach ($proyecto->carreras as $carrera) {
            if ($carrera->pivot->carrera_principal == true) {
                $carreraPrincipal = $carrera;
            } else {
                array_push($carrerasAdicionales, $carrera);
            }
        }

        $programa = \SoftwareVinculos\Models\Programa::find($proyecto->id_programa);
        //$programa = $proyecto->programa();

        $objetivosEspecificos = \SoftwareVinculos\Models\ObjetivoEspecifico::all()->where('id_proyecto', $proyecto->id);

        $lineasAccion = \SoftwareVinculos\Models\LineaAccion::all();

        $lineasAccionConSeleccion = array();

        foreach ($lineasAccion as $lineaAccion) {
            $seleccionada = ProyectoController::estaSeleccionadaLaLineaDeAccion($lineaAccion, $proyecto->lineasAccion);

            $objLineaAccion = new \stdClass();
            $objLineaAccion->lineaAccion = $lineaAccion;
            $objLineaAccion->seleccionada = $seleccionada;

            array_push($lineasAccionConSeleccion, $objLineaAccion);
        }

        if($proyecto->tipo->id == 2) { //Pasantias empresariales
          //$estudiantesProyecto = $proyecto->estudiantesComunitarias();
          $estudiantesProyecto = Estudiante::all()->where('id_proyecto_empresarial', $proyecto->id);
        }
        else if($proyecto->tipo->id == 1){ //Pasantias Comunitarias
          //$estudiantesProyecto = $proyecto->estudiantesEmpresariales();
          $estudiantesProyecto = Estudiante::all()->where('id_proyecto_comunitaria', $proyecto->id);
        }

        $proyectoEspecifico = $proyecto;

        $programas_options = Programa::all()->lists('nombre', 'id');
        $programas_options->prepend('Seleccione programa', 0);


        return view('formularios.forUVS04.modoEditar')->with([
            'proyecto' => $proyecto,
            'fechaInicio' => $fechaInicio,
            'fechaFinalizacion' => $fechaFinalizacion,
            'proyectoEspecifico' => $proyectoEspecifico,
            'formularioUVS04' => $formularioUVS04,
            'tiposProyecto' => $arrayTiposProyecto,
            'tipoProyecto' => $proyecto->tipo,
            'lineasAccion' => $lineasAccionConSeleccion,
            'carreraPrincipal' => $carreraPrincipal,
            'carrerasAdicionales' => $carrerasAdicionales,
            'programa' => $programa,
            'programas_options' => $programas_options,
            'objetivosEspecificos' => $objetivosEspecificos,
            'estudiantesProyecto' => $estudiantesProyecto,
            'idTipoProyecto' => $proyecto->tipo->id
        ]);
    }

    public function update(Request $request, $id_proyecto)
    {
        $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);

        $this->actualizarPrograma($request, $proyecto);

        $organizacion = \SoftwareVinculos\Models\Organizacion::find($request->id_organizacion);
        $proyecto->organizacion()->associate($organizacion);

        $tipo = \SoftwareVinculos\Models\TipoProyecto::find($request->tipo_proyecto);
        $proyecto->tipo()->associate($tipo);

        $this->fillProyectoConInputs($request, $proyecto);
        $proyecto->save();

        $this->guardarDelegados($request, $proyecto);

        $this->actualizarCarreras($request, $proyecto);
        $this->actualizarLineasAccion($request, $proyecto);

        $this->actualizarObjetivosEspecificos($request, $proyecto);

        $this->actualizarResultadosAprendizaje($request, $proyecto);

        $this->actualizarAsignacionEstudiantesAProyecto($request, $proyecto);

        $this->adjuntarConvenioOrganizacion($request, $organizacion);

        $this->adjuntarCronograma($request, $proyecto);

        $this->adjuntarPresupuesto($request, $proyecto);

        // Se actualiza el formulario 04
        FormularioController::actualizarFormularioUVS04($proyecto->id, $request->submit);

        return redirect()->route('home');
    }

    public function fillProyectoConInputs($request, $proyecto)
    {
        $fechasParse = explode('-',$request->rango_fechas_proyecto,2);
        $fechaInicio = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
        $fechaFin = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

        //Sección 1
        $proyecto->titulo = $request->titulo;
        $proyecto->provincia = $request->provincia;
        $proyecto->canton = $request->canton;
        $proyecto->codigo_zona = $request->codigo_zona;
        $proyecto->codigo_distrito = $request->codigo_distrito;
        $proyecto->codigo_circuito = $request->codigo_circuito;
        $proyecto->fecha_inicio =  $fechaInicio;
        $proyecto->fecha_finalizacion =  $fechaFin;

        $proyecto->area_conocimiento = $request->area_conocimiento;
        $proyecto->sub_area_conocimiento = $request->sub_area_conocimiento;
        $proyecto->sub_area_especifica = $request->sub_area_especifica;

        $proyecto->informacion_pnbv = $request->informacion_pnbv;

        $proyecto->perfil_practicante = $request->perfil_practicante;

        //Sección 2
        $proyecto->numero_beneficiarios_directos = $request->numero_beneficiarios_directos;
        $proyecto->numero_beneficiarios_indirectos = $request->numero_beneficiarios_indirectos;

        //Sección 3
        $proyecto->antecedentes = $request->antecedentes;
        $proyecto->contexto_proyecto = $request->contexto_proyecto;
        $proyecto->definicion_problema = $request->definicion_problema;

        //Sección 4
        $proyecto->justificacion = $request->justificacion;

        //Sección 5
        $proyecto->pertinencia = $request->pertinencia;

        //Sección 6
        $proyecto->objetivo_general = $request->objetivo_general;
        $proyecto->indicador_verificable_objetivo_general = $request->indicador_verificable_objetivo_general;
        $proyecto->medio_verificacion_objetivo_general = $request->medio_verificacion_objetivo_general;
        $proyecto->supuestos_objetivo_general = $request->supuestos_objetivo_general;

        //Sección 7
        $proyecto->indicador_verificable_objetivo_programa = $request->indicador_verificable_objetivo_programa;
        $proyecto->medio_verificacion_objetivo_programa = $request->medio_verificacion_objetivo_programa;
        $proyecto->supuestos_objetivo_programa = $request->supuestos_objetivo_programa;

        //Sección 8
        $proyecto->metodologia = $request->metodologia;

        //Seccion 10
        $proyecto->productos_esperados = $request->productos_esperados;

        //Seccion 11
        $proyecto->compromisos_organizacion = $request->compromisos_organizacion;

        //Seccion 12
        $proyecto->presupuesto = $request->presupuesto;

        //Sección 13
        $proyecto->evaluacion = $request->evaluacion;

        //Sección 14
        $proyecto->bibliografia = $request->bibliografia;

    }

    public static function estaSeleccionadaLaLineaDeAccion($lineaAccion, $lineasAccionSeleccionadas) {
        foreach ($lineasAccionSeleccionadas as $lineaSeleccionada) {
            if ($lineaSeleccionada->id == $lineaAccion->id) {
                return true;
            }
        }
        return false;
    }

    public function guardarCarreras($request, $nuevoProyecto) {
        if ($request->carreraPrincipal != null) {
            $nuevoProyecto->carreras()->attach($request->carreraPrincipal, ['carrera_principal' => true]);
        }

        if ($request->get('carrerasAdicionales') != null) {
          foreach($request->get('carrerasAdicionales') as $id_carrera) {
              if ($id_carrera != null) {
                  $nuevoProyecto->carreras()->attach($id_carrera, ['carrera_principal' => false]);
              }
          }
        }
    }

    public function actualizarCarreras($request, $proyecto) {
        //Obtenemos la carrera principal guardada si es que la hay
        $carreraPrincipal = null;
        foreach ($proyecto->carreras as $carrera) {
            if ($carrera->pivot->carrera_principal == true) {
                $carreraPrincipal = $carrera;
            }
        }

        //Actualizamos primero la carrera principal
        if ($carreraPrincipal == null && $request->carreraPrincipal != null) {
            //No hay ninguna carrera principal guardada asi que guardamos la ingresada
            $proyecto->carreras()->attach($request->carreraPrincipal, ['carrera_principal' => true]);
        } elseif ($carreraPrincipal != null && $request->carreraPrincipal != null) {
            //Ya hay un carrera principal guardada, asi que si la que se desea guardar es diferente
            //quitamos la anterior y guardamos la nueva
            if ($carreraPrincipal->id != $request->carreraPrincipal) {
                $proyecto->carreras()->detach($carreraPrincipal);
                $proyecto->carreras()->attach($request->carreraPrincipal, ['carrera_principal' => true]);
            }
        } elseif ($carreraPrincipal != null && $request->carreraPrincipal == null) {
            //No se ha ingresado ninguna carrera principal asi que eliminamos la que ya esta guardada
            $proyecto->carreras()->detach($carreraPrincipal);
        }

        //Continuamos con las carreras adicionales
        if ($request->get('carrerasAdicionales') != null) {
            foreach ($request->get('carrerasAdicionales') as $id_carrera) {
                if ($proyecto->carreras()->find($id_carrera) != null) {
                    //Si la carrera ya esta guardada, la actualizo para asegurarme que no quede como carrera principal
                    $proyecto->carreras()->updateExistingPivot($id_carrera, ['carrera_principal' => false]);
                } else {
                    //Si la carrera no esta guardada, la guardamos
                    $proyecto->carreras()->attach($id_carrera, ['carrera_principal' => false]);
                }
            }
        }

        if ($proyecto->carreras != null) {
          //Si una de las carreras que estaban guardadas no fue recibida en el request entonces la borrramos
          foreach ($proyecto->carreras as $carrera) {

              if ($carrera->id != $request->carreraPrincipal &&
                  collect($request->get('carrerasAdicionales'))->contains($carrera->id) == false) {

                  $proyecto->carreras()->detach($carrera);
              }
          }
        }
    }

      public function guardarLineasAccion($request, $nuevoProyecto){
          if ($request->lineasAccion != null) {
              foreach ($request->lineasAccion as $idLineaAccion) {
                $nuevoProyecto->lineasAccion()->attach($idLineaAccion);
              }
          }
      }

    public function actualizarLineasAccion($request, $proyecto){
        //Guardamos las nuevas lineas de accion seleccionadas
        if ($request->lineasAccion != null) {
            foreach ($request->lineasAccion as $idLineaAccion) {
                if ($proyecto->lineasAccion()->find($idLineaAccion) == null) {
                    //Si la linea de accion no esta guardada, la guardamos
                    $proyecto->lineasAccion()->attach($idLineaAccion);
                }
            }
        }

        //Eliminamos las lineas de accion des-seleccionadas
        if ($proyecto->lineasAccion != null) {
          //Si una de las lineas de accion que estaban guardadas no fue recibida en el request entonces la borrramos
          foreach ($proyecto->lineasAccion as $lineaAccion) {

              if (collect($request->lineasAccion)->contains($lineaAccion->id) == false) {
                  $proyecto->lineasAccion()->detach($lineaAccion);
              }
          }
        }
    }

    public function guardarDelegados($request, $proyecto) {
      if ($request->get('delegados') != null) {
        foreach($request->get('delegados') as $idDelegado) {
          $delegado = \SoftwareVinculos\Models\DelegadoOrganizacion::find($idDelegado);
          //ERROR
          if ($this->proyectoContieneDelegado($proyecto, $delegado) == false) {
            $proyecto->delegados()->attach($idDelegado);
          }
        }
      }

      if ($proyecto->delegados != null) {
        foreach ($proyecto->delegados as $delegado) {
          if (collect($request->get('delegados'))->contains($delegado->id) == false) {
              $proyecto->delegados()->detach($delegado);
          }
        }
      }
    }

    public function proyectoContieneDelegado($proyecto, $delegado) {
      foreach($proyecto->delegados as $delegadoProyecto) {
        if ($delegado->id == $delegadoProyecto->id) {
          return true;
        }
      }
      return false;
    }

    public function guardarObjetivosEspecificos($request, $nuevoProyecto){

      $valueObjetivosEspecificos = $request->get('valueObjetivosEspecificos');
      $indicadorVerificableObjetivosEspecificos = $request->get('indicadorVerificableObjetivosEspecificos');
      $mediosVerificacionObjetivosEspecificos = $request->get('mediosVerificacionObjetivosEspecificos');
      $supuestosObjetivosEspecificos = $request->get('supuestosObjetivosEspecificos');
      $idObjetivosEspecificos = $request->get('idObjetivosEspecificos');

      if($idObjetivosEspecificos != null){

        foreach($idObjetivosEspecificos as $index => $id_objetivo_especifico) {
          $value_objetivo_especifico = $valueObjetivosEspecificos[$index];
          $indicador_verificable_objetivo_especifico = $indicadorVerificableObjetivosEspecificos[$index];
          $medio_verificacion_objetivo_especifico = $mediosVerificacionObjetivosEspecificos[$index];
          $supuesto_objetivo_especifico = $supuestosObjetivosEspecificos[$index];

          $nuevoObjetivoEspecifico = new ObjetivoEspecifico;

          if ( $value_objetivo_especifico != null) {
            $nuevoObjetivoEspecifico->descripcion = $value_objetivo_especifico;
            $nuevoObjetivoEspecifico->indicadores_verificables = $indicador_verificable_objetivo_especifico;
            $nuevoObjetivoEspecifico->medios_verificacion = $medio_verificacion_objetivo_especifico;
            $nuevoObjetivoEspecifico->supuestos = $supuesto_objetivo_especifico;
            $nuevoObjetivoEspecifico->proyecto()->associate($nuevoProyecto);
            $nuevoObjetivoEspecifico->save();

            /**
            * A continuación se guardan las actividades del objetivo especifico.
            */
            $descripcionActividadsObjetivoEspecifico = $request->get('descripcionActividadesObjetivoEspecifico'.$id_objetivo_especifico);
            $listaHorasActividades = $request->get('horasActividadesObjetivoEspecifico'.$id_objetivo_especifico);
            $listaRangoFechas = $request->get('rangoFechaActividadesObjetivoEspecifico'.$id_objetivo_especifico);

            if($descripcionActividadsObjetivoEspecifico != null){
              foreach($descripcionActividadsObjetivoEspecifico as $index => $actividadValue) {
                $nuevaActividad = new Actividad;

                if( $actividadValue != '' || $listaHorasActividades[$index] != '' || $listaRangoFechas[$index] != ''){
                  $nuevaActividad->descripcion = $actividadValue;

                  $nuevaActividad->duracion_horas = $listaHorasActividades[$index];
                  if($listaRangoFechas[$index] != ''){
                    $fechasParse = explode('-',$listaRangoFechas[$index],2);
                    $fechaInicioActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                    $fechaFinActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                    $nuevaActividad->fecha_inicio = $fechaInicioActividad;
                    $nuevaActividad->fecha_finalizacion = $fechaFinActividad;
                  }
                  $nuevaActividad->es_actividad_macro = true;//1
                  $nuevaActividad->es_actividad_micro = false;//0

                  $nuevaActividad->objetivoEspecifico()->associate($nuevoObjetivoEspecifico);
                  $nuevaActividad->save();
                }
              }
            }
          }
        }
      }
    }

    public function actualizarObjetivosEspecificos($request, $proyecto){
      //Actualizo los objetivos especificos que el proyecto ya tenía.
      $objetivosEspecificos = \SoftwareVinculos\Models\ObjetivoEspecifico::all()->where('id_proyecto',$proyecto->id);

      foreach ($objetivosEspecificos as $oe) {
        $idObjetivoEspecifico = $oe->id;
        $reqDescripcionObjetivoEspecifico = $request->get('valueObjetivoEspecifico-'.$idObjetivoEspecifico);

        if($reqDescripcionObjetivoEspecifico != null){
          $reqIndicadorVerificableObjetivoEspecifico = $request->get('indicadorVerificableObjetivoEspecifico-'.$idObjetivoEspecifico);
          $reqMediosVerificacionObjetivoEspecifico = $request->get('mediosVerificacionObjetivoEspecifico-'.$idObjetivoEspecifico);
          $reqSupuestosObjetivoEspecifico = $request->get('supuestosObjetivoEspecifico-'.$idObjetivoEspecifico);

          //Aqui se hace update del registro en la bd
          $oe->descripcion = $reqDescripcionObjetivoEspecifico;
          $oe->indicadores_verificables = $reqIndicadorVerificableObjetivoEspecifico;
          $oe->medios_verificacion = $reqMediosVerificacionObjetivoEspecifico;
          $oe->supuestos = $reqSupuestosObjetivoEspecifico;
          $oe->save();


          //Actualizo las actividades que ya tenia el objetivo especifico.
          $actividadesObjetivoEspecifico = $oe->actividades;
          if(  $actividadesObjetivoEspecifico != null) {
            foreach ($actividadesObjetivoEspecifico as $index => $actividad) {
              $idActividad = $actividad->id;
              $reqDescripcionActividadObjEspecifico = $request->get('descripcionActividadObjetivoEspecificoSaved'.$idObjetivoEspecifico.'-'.$idActividad);
              $reqDuracionHorasActividadObjEspecifico = $request->get('horasActividadObjetivoEspecificoSaved'.$idObjetivoEspecifico.'-'.$idActividad);
              $reqRangoFechaActividadObjEspecifico = $request->get('rangoFechaActividadObjetivoEspecificoSaved'.$idObjetivoEspecifico.'-'.$idActividad);

              if($reqDescripcionActividadObjEspecifico != null || $reqDuracionHorasActividadObjEspecifico != null ||
                 $reqRangoFechaActividadObjEspecifico  != null) {
                   $actividad->descripcion = $reqDescripcionActividadObjEspecifico;
                   $actividad->duracion_horas = $reqDuracionHorasActividadObjEspecifico;

                   if($reqRangoFechaActividadObjEspecifico  != '') {
                     $fechasParse = explode('-',$reqRangoFechaActividadObjEspecifico,2);
                     $fechaInicioActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                     $fechaFinActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                     $actividad->fecha_inicio = $fechaInicioActividad;
                     $actividad->fecha_finalizacion = $fechaFinActividad;
                   }

                   $actividad->save();
              }
              else{
                $actividad->delete();
              }
            }
          }

          //Se guardan las nuevas actividades agregadas al objetivo especifico
          $descripcionActividadesObjetivoEspecifico = $request->get('descripcionActividadesObjetivoEspecificoNew'.$idObjetivoEspecifico);
          $listaHorasActividadesObjetivoEspecifico = $request->get('horasActividadesObjetivoEspecificoNew'.$idObjetivoEspecifico);
          $listaRangoFechaActividadesObjetivoEspecifico = $request->get('rangoFechaActividadesObjetivoEspecificoNew'.$idObjetivoEspecifico);

          if($descripcionActividadesObjetivoEspecifico){
            foreach ($descripcionActividadesObjetivoEspecifico as $index => $descripcionActividad) {
              $nuevaActividad = new Actividad;

              if($descripcionActividad != '' || $listaHorasActividadesObjetivoEspecifico[$index] != '' || $listaRangoFechaActividadesObjetivoEspecifico != ''){
                $nuevaActividad->descripcion = $descripcionActividad;

                $nuevaActividad->duracion_horas = $listaHorasActividadesObjetivoEspecifico[$index];
                if($listaRangoFechaActividadesObjetivoEspecifico[$index] != ''){
                  $fechasParseNew = explode('-',$listaRangoFechaActividadesObjetivoEspecifico[$index],2);
                  $fechaInicioNewActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParseNew[0]))));
                  $fechaFinNewActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParseNew[1]))));

                  $nuevaActividad->fecha_inicio = $fechaInicioNewActividad;
                  $nuevaActividad->fecha_finalizacion = $fechaFinNewActividad;
                }

                $nuevaActividad->es_actividad_macro = true;//1
                $nuevaActividad->es_actividad_micro = false;//0

                $nuevaActividad->objetivoEspecifico()->associate($oe);
                $nuevaActividad->save();
              }
            }
          }
        }
        else{
          //Aqui deberia borrarse el registro en la bd
          $oe->delete();
        }
      }

      //Se guardan los nuevos Objetivos especificos
      $this->guardarObjetivosEspecificos($request, $proyecto);
    }

    public function guardarProductosEsperados($request, $nuevoProyecto){
      foreach($request->get('productoEsperado') as $productoEsperado) {
        $nuevoProductoEsperado = new ProductoEsperado;

        if ( $productoEsperado != null) {
          $nuevoProductoEsperado->descripcion = $productoEsperado;
          $nuevoProductoEsperado->proyecto()->associate($nuevoProyecto);

          $nuevoProductoEsperado->save();
        }
      }
    }

    public function actualizarProductosEsperados($request, $proyecto){
      //Actualizo los productos esperados que el proyecto ya tenía.
      $productosEsperados = \SoftwareVinculos\Models\ProductoEsperado::all()->where('id_proyecto', $proyecto->id);

      foreach ($productosEsperados as $pe) {
        $idProductoEsperado= $pe->id;
        $reqProductoEsperado = $request->get('productoEsperado-'.$idProductoEsperado);

        if($reqProductoEsperado != null){
          $pe->descripcion = $reqProductoEsperado;
          $pe->save();
        }
        else{
          $pe->delete();
        }
      }
      //Se guardan los nuevos Productos esperados
      $this->guardarProductosEsperados($request, $proyecto);
    }

    public function guardarCompromisosOng($request, $nuevoProyecto){
      foreach($request->get('compromisoOrganizacion') as $compromisoOrganizacion) {
        $nuevoCompromisoOrganizacion = new CompromisoOrganizacion;

        if ( $compromisoOrganizacion != null) {
          $nuevoCompromisoOrganizacion->descripcion = $compromisoOrganizacion;
          $nuevoCompromisoOrganizacion->proyecto()->associate($nuevoProyecto);

          $nuevoCompromisoOrganizacion->save();
        }
      }
    }

    public function actualizarCompromisosOrganizacion($request, $proyecto){
      //Actualizo Compromizos Organización que el proyecto ya tenia
      $compromisosOrganizacion = \SoftwareVinculos\Models\CompromisoOrganizacion::all()->where('id_proyecto', $proyecto->id);

      foreach ($compromisosOrganizacion as $co) {
        $idCompromisoOrganizacion= $co->id;
        $reqCompromisoOrganizacion = $request->get('compromisoOrganizacion-'.$idCompromisoOrganizacion);

        if($reqCompromisoOrganizacion != null){
          $co->descripcion = $reqCompromisoOrganizacion;
          $co->save();
        }
        else{
          $co->delete();
        }
      }
      //Se guardan los nuevos Productos esperados
      $this->guardarCompromisosOng($request, $proyecto);

    }

    public function actualizarPrograma($request, $proyecto){
      //Actualizo Programa
      $idPrograma = $request->get('programaProyecto');
      if($idPrograma != null){
        $programa = \SoftwareVinculos\Models\Programa::find($idPrograma);
        $proyecto->programa()->associate($programa);
      }
      else{
        //Ningún programa fue seleccionado
        $proyecto->programa()->associate(null);
      }
    }

    public function guardarResultadosAprendizaje($request, $nuevoProyecto) {

        if ($request->get('materias') != null) {
          $descripcionResultadosAprendizaje = $request->get('resultadosAprendizaje');
          foreach($request->get('materias') as $index => $id_materia) {
              if ($id_materia != null) {
                  //if ($nuevoProyecto->resultadosAprendizaje->where('id_materia', $id_materia)->count() == 0) {
                    $materia = Materia::find($id_materia);

                    $resultadoAprendizaje = new ResultadoAprendizaje;
                    $resultadoAprendizaje->descripcion = $descripcionResultadosAprendizaje[$index];
                    $resultadoAprendizaje->proyecto()->associate($nuevoProyecto);
                    $resultadoAprendizaje->materia()->associate($materia);

                    $resultadoAprendizaje->save();
                  //}
              }
          }
        }
    }

    public function actualizarResultadosAprendizaje($request, $proyecto){
      //Actualizo los resultados de aprendizaje que el proyecto ya tenía.
      $resultadosAprendizaje = \SoftwareVinculos\Models\ResultadoAprendizaje::all()->where('id_proyecto', $proyecto->id);
      /*$descripcionResultadosAprendizaje = $request->get('resultadosAprendizaje');

      foreach ($resultadosAprendizaje as $ra) {

        foreach($request->get('materias') as $index => $id_materia) {

          if ($ra->materia->id == $id_materia) {
            $ra->descripcion = $descripcionResultadosAprendizaje[$index];
            $ra->save();
          } else {
            $ra->delete();
          }
        }
      }*/

      foreach ($proyecto->resultadosAprendizaje as $ra) {
        $ra->forceDelete();
      }
      //Se guardan los nuevos Productos esperados
      $this->guardarResultadosAprendizaje($request, $proyecto);
    }

    public function asignarEstudiantesAProyecto($request, $proyecto){

      $lista_id_estudiantes = $request->get('estudianteProyecto');

      if($lista_id_estudiantes != null ){
        foreach($lista_id_estudiantes as $id_estudiante) {
          $estudiante = Estudiante::find($id_estudiante);
          //Aqui antes de asignar proyecto al estudiante, primero deberia verificar si el estudiante ya forma
          // parte de otro proyecto, de ser el caso, entonces no se le asigna el proyecto actual
          // (PENDIENTE, NO ESTOY SEGURO SI ESTO DEBERIA HACERSE)

          //Actualmente, se está asignando el proyecto sin importar si ya tenia uno antes, lo reemplaza por el
          //que tenia asignado antes (TAMPOCO ESTOY SEGURO SI DEBERIA SER ASI) (Si va a ser asi, entonces tambien deberia eliminar
          //al estudiante el tutor que tenia asignado del proyecto anterior)
          if($proyecto->tipo->id == 2){ //Pasantias empresariales
            $estudiante->proyectoEmpresarial()->associate($proyecto);

            $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(2); //En proyecto
            $estudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);
          }
          else if($proyecto->tipo->id == 1){ //Comunitarias
            $estudiante->proyectoComunitaria()->associate($proyecto);

            $estadoEstProyectoComunitario = EstadoEstudiante::find(2); //En proyecto
            $estudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);
          }
          $estudiante->save();
        }
      }
    }

    public function actualizarAsignacionEstudiantesAProyecto($request, $proyecto){

      $tipoProyecto = $proyecto->tipo->id;

      if($tipoProyecto == 2){ //Pasantias Empresariales
        $estudiantes = Estudiante::all()->where('id_proyecto_empresarial', $proyecto->id);
      }
      else if($tipoProyecto == 1){ //Pasantias Comunitarias
        $estudiantes = Estudiante::all()->where('id_proyecto_comunitaria', $proyecto->id);
      }

      foreach ($estudiantes as $estudiante) {
        $reqEstudiante = $request->get('estudianteProyecto-'.$estudiante->id);

        if($reqEstudiante == null){
          //Si es null, entonces quiere decir que el estudiante fue eliminado desde el formulario uvs04, asi que
          //aqui se debe desasignar el proyecto y tutor al estudiante (según el tipo de proyecto)
          //No se elimina el registro de estudiante
          if($tipoProyecto == 2){
            $estudiante->id_proyecto_empresarial = null;
            $estudiante->id_tutor_empresarial = null;
            $estudiante->id_estado_estudiante_proyectoEmpresarial = 1; //Sin Proyecto
          }
          else if($tipoProyecto == 1){
            $estudiante->id_proyecto_comunitaria = null;
            $estudiante->id_tutor_comunitaria = null;
            $estudiante->id_estado_estudiante_proyectoComunitario = 1;  //Sin Proyecto
          }

          $estudiante->save();
        }
      }

      //Se asignan los nuevos estudiantes agregados al proyecto
      $this->asignarEstudiantesAProyecto($request, $proyecto);
    }

    public static function getInfoModoVerProyecto($id_proyecto) {
      $info = array();

      $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);
      $info['proyecto'] = $proyecto;
      $info['modoVer'] = true;

      $lineasAccion = \SoftwareVinculos\Models\LineaAccion::all();
      $lineasAccionConSeleccion = array();

      foreach ($lineasAccion as $lineaAccion) {
          $seleccionada = ProyectoController::estaSeleccionadaLaLineaDeAccion($lineaAccion, $proyecto->lineasAccion);

          $objLineaAccion = new \stdClass();
          $objLineaAccion->lineaAccion = $lineaAccion;
          $objLineaAccion->seleccionada = $seleccionada;

          array_push($lineasAccionConSeleccion, $objLineaAccion);
      }

      $info['lineasAccion'] = $lineasAccionConSeleccion;

      $info['mostrarBotones'] = false;
      if ((\Auth::user()->coordinadorCarreraComunitarias != null || \Auth::user()->coordinadorCarreraEmpresariales != null)) {
        $info['mostrarBotones'] = true;
      }

      $carreraPrincipal = null;
      foreach ($proyecto->carreras as $carrera) {
          if ($carrera->pivot->carrera_principal == true) {
              $carreraPrincipal = $carrera;
          }
      }
      $info['carreraPrincipal'] = $carreraPrincipal;

      if($proyecto->tipo->id == 2){ //Pasantias empresariales
        //$estudiantesProyecto = $proyecto->estudiantesComunitarias();
        $estudiantesProyecto = Estudiante::all()->where('id_proyecto_empresarial', $proyecto->id);
      }
      else if($proyecto->tipo->id == 1){ //Pasantias Comunitarias
        //$estudiantesProyecto = $proyecto->estudiantesEmpresariales();
        $estudiantesProyecto = Estudiante::all()->where('id_proyecto_comunitaria', $proyecto->id);
      }
      $info['estudiantesProyecto'] = $estudiantesProyecto;

      $objetivosEspecificos = ObjetivoEspecifico::all()->where('id_proyecto', $proyecto->id);
      $info['objetivosEspecificos'] = $objetivosEspecificos;

      return $info;
    }

    public function modoVerProyecto($id_proyecto) {
      $infoModoVer = ProyectoController::getInfoModoVerProyecto($id_proyecto);

      return view('formularios.forUVS04.modoVer')->with($infoModoVer);
    }

    /*
    public function guardarObjetivosActividades($request, $nuevoProyecto){
      $listaObjetivos = $request->get('componente');
      if ($listaObjetivos != null) {
        $numeroObjetivos = $request->get('numerocomponente');
        foreach($listaObjetivos as $index => $objetivoEspecifico) {

          //Primero se guarda el objetivo especifico
          $nuevoObjetivoEspecifico = new ObjetivoEspecifico;
          if ( $objetivoEspecifico != null) {
            $nuevoObjetivoEspecifico->descripcion = $objetivoEspecifico;
            $nuevoObjetivoEspecifico->proyecto()->associate($nuevoProyecto);

            $nuevoObjetivoEspecifico->save();
          }

          //Ahora guardo las actividades del objetivo especifico
          $numObjetivo = $numeroObjetivos[$index];
          $listaActividades = $request->get('actividad-componente-'.$numObjetivo);
          $listaHorasActividades = $request->get('horasActividad-componente-'.$numObjetivo);
          $listaFechaInicio = $request->get('fechaInicio-componente-'.$numObjetivo);
          $listaFechaFin = $request->get('fechaFin-componente-'.$numObjetivo);

          foreach($listaActividades as $index => $actividadValue) {
            $nuevaActividad = new Actividad;
            $nuevaActividad->descripcion = $actividadValue;
            $nuevaActividad->duracion_horas = $listaHorasActividades[$index];

            if($listaFechaInicio[$index] != ''){
              $nuevaActividad->fecha_inicio = date('Y-m-d',strtotime($listaFechaInicio[$index]));
            }
            if($listaFechaFin[$index] != ''){
              $nuevaActividad->fecha_finalizacion = date('Y-m-d',strtotime($listaFechaFin[$index]));
            }

            $nuevaActividad->objetivoEspecifico()->associate($nuevoObjetivoEspecifico);
            $nuevaActividad->save();
          }
        }
      }
    }
    */

    public function revisarProyecto(Request $request, $id_proyecto) {
      if($request->submit == 'aprobar_proyecto') {
        $aprobado = true;
      } else {
        $aprobado = false;
      }

      $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);
      $tipoFormulario = \SoftwareVinculos\Models\TipoFormulario::find(2); //FOR-UVS-04

      $estadoProyecto = \SoftwareVinculos\Models\EstadoProyecto::find(2); //Aprobado
      $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(3); //Aprobado

      $formularioUVS04 = $proyecto->formularios()->where('id_tipo_formulario', $tipoFormulario->id)->first();

      if($aprobado == true) {
        $estadoProyecto = \SoftwareVinculos\Models\EstadoProyecto::find(2); //Aprobado
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(3); //Aprobado
      } else {
        $estadoProyecto = \SoftwareVinculos\Models\EstadoProyecto::find(3); //Rechazado
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(4); //Rechazado
        $formularioUVS04->comentario_rechazo = $request->comentario_rechazo;
      }

      $formularioUVS04->estado()->associate($estadoFormulario);
      $formularioUVS04->save();

      $proyecto->estado()->associate($estadoProyecto);
      $proyecto->save();

      if($aprobado == true) {
        NotificacionController::enviarNotificacionFormularioAprobado(\Auth::user(), $formularioUVS04, $proyecto, $formularioUVS04->creadoPor);
        //FormularioController::crearFormularioUVS13($proyecto);
      } else {
        NotificacionController::enviarNotificacionFormularioRechazado(\Auth::user(), $formularioUVS04, $proyecto, $formularioUVS04->creadoPor);
      }

      return redirect()->route('home');
    }

    public function iniciarProyecto(Request $request) {
      $proyecto = \SoftwareVinculos\Models\Proyecto::find($request->id_proyecto);
      $estadoProyecto = \SoftwareVinculos\Models\EstadoProyecto::find(4); //Iniciado
      $proyecto->estado()->associate($estadoProyecto);
      $proyecto->save();

      return response()->json(['success' => true]);
    }
}
