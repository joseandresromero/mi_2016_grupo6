<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Models\Proyecto;
use SoftwareVinculos\Models\Formulario;

use Auth;

class DirectorController extends Controller
{
    public function crearDirector(Request $request)
    {

    }

    /*
    * Me redirijo hacia la página de formularios disponibles por llenar del director en un proyecto en específico.
    */
    public function formulariosDisponiblesDirector($id_proyecto) {

      //$formulariosDisponibles = Auth::user()->formulariosCreados;
      $formulariosDisponibles = Formulario::formulariosDisponibleDirector($id_proyecto)->get();
      $proyectoEspecifico = Proyecto::find($id_proyecto);

      //dd($formulariosDisponibles);
      //dd($proyectoEspecifico);
      return view('profesor.director.formulariosDisponibles')->with([
        'formulariosDisponibles' => $formulariosDisponibles,
        'proyectoEspecifico' => $proyectoEspecifico
      ]);
    }
}
