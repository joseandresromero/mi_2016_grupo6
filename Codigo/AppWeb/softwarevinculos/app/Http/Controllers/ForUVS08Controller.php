<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

class ForUVS08Controller extends Controller
{
  public function create()
  {
      return view('formularios.forUVS08');
  }

  public function edit($id_forUVS13)
  {

  }

  public function store(Request $request)
  {

  }

  public function update(Request $request, $id_forUVS13)
  {

  }
}
