<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;

class ForUVS18Controller extends Controller
{
    public function create()
    {
        return view('formularios.forUVS18');
    }

    public function edit($id_forUVS18)
    {

    }

    public function store(Request $request)
    {

    }

    public function update(Request $request, $id_forUVS18)
    {

    }

}
