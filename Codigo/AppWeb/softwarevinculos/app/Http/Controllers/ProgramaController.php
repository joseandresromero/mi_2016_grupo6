<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\Programa;
use Input;

class ProgramaController extends Controller
{
  public function edit($id_programa) {
    $programa = Programa::find($id_programa);

    $fechaInicio = date('d/m/Y',strtotime($programa->fecha_inicio));
    $fechaFinalizacion = date('d/m/Y',strtotime($programa->fecha_finalizacion));

    return view('administrador.editarPrograma')->with(['programa' => $programa, 'fechaInicio' => $fechaInicio, 'fechaFinalizacion' => $fechaFinalizacion]);
  }

  public function update(Request $request, $id_programa) {
    $programa = Programa::find($id_programa);

    $fechasParse = explode('-',$request->fecha_inicio_fin,2);
    $fechaInicio = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
    $fechaFin = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

    $programa->nombre = $request->nombre;
    $programa->objetivo_general = $request->objetivo_general;
    $programa->objetivos_especificos = $request->objetivos_especificos;
    $programa->fecha_inicio = $fechaInicio;
    $programa->fecha_finalizacion = $fechaFin;

    $programa->save();

    $programas = Programa::all();
    return redirect()->route('programas', ['programas' => $programas]);
  }

  public function eliminarPrograma($id_programa) {
    $programa = Programa::find($id_programa);

    if ($programa->proyectos->count() > 0) {
      $mensaje = 'No se pudo eliminar el proyecto debido a que contiene uno o varios proyectos.';
      $tipo = 'error';
    } else {
      $programa->delete();
      $mensaje = 'Se ha eliminado satisfactoriamente el programa.';
      $tipo = 'success';
    }

    $programas = Programa::all();
    session()->flash('flash_type', $tipo);
    session()->flash('flash_message', $mensaje);
    return redirect()->route('programas', ['programas' => $programas]);
  }

  public function autocomplete(Request $request){
    $term = $request->term;
    $data = Programa::where('nombre','LIKE','%'.$term.'%')->take(10)->get();
    $results = array();
    foreach ($data as $programa) {
      $results[] = ['id' => $programa->id, 'value'=>$programa->nombre, 'objetivo_general'=>$programa->objetivo_general];
    }
    return response()->json($results);
  }

  public function index() {
    $programas = Programa::all();

    return view('administrador.programas')->with([
      'programas' => $programas
    ]);
  }

  public function guardarPrograma(Request $request) {
    $fechasParse = explode('-',$request->fecha_inicio_fin,2);
    $fechaInicio = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
    $fechaFin = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

    $programa = new Programa;
    $programa->nombre = $request->nombre;
    $programa->objetivo_general = $request->objetivo_general;
    $programa->objetivos_especificos = $request->objetivos_especificos;
    $programa->fecha_inicio = $fechaInicio;
    $programa->fecha_finalizacion = $fechaFin;
    $programa->save();

    $programas = Programa::all();
    return redirect()->route('programas', ['programas' => $programas]);
  }

  /*
  * Esta función es usada cada vez que se hace un ajax request cuando se selecciona un programa
  * desde la vista de creación del formulario uvs04
  */
  public function obtenerDatosPrograma(Request $request) {
    $id_programa = $request->id_programa;

    $programa = Programa::find($id_programa);

    if($programa != null) {
      return response()->json(['sucess' => true, 'nombre' => $programa->nombre, 'objetivoGeneral' => $programa->objetivo_general]);
    }
    else{
      return response()->json(['sucess' => false]);
    }
  }
}
