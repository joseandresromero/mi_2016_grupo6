<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;
use \SoftwareVinculos\Models\Formulario;
use \SoftwareVinculos\Models\TipoFormulario;
use \SoftwareVinculos\Models\EstadoFormulario;
use \SoftwareVinculos\Models\ForUVS13;
use \SoftwareVinculos\Models\ForUVS14;
use \SoftwareVinculos\Models\Estudiante;
use \SoftwareVinculos\Models\Proyecto;
use \SoftwareVinculos\Models\Actividad;

use Auth;

use SoftwareVinculos\Http\Requests;

class ForUVS01Controller extends Controller
{
    public function create()
    {
        return view('formularios.forUVS01');
    }

    public function edit($id_forUVS01)
    {

    }

    public function store(Request $request, $id_proyecto)
    {

    }

    public function update(Request $request, $id_proyecto, $id_estudiante)
    {

    }

    public function updateForUVS01(Request $request, $id_proyecto, $id_estudiante) {
      $proyecto = Proyecto::find($id_proyecto);
      $estudiante = Estudiante::find($id_estudiante);

      if(Auth::user()->director != null) {
        $this->actualizarActividadesMacroEstudiante($request, $proyecto, $estudiante);

        if ($request->submit == 'enviar') {
            //return redirect()->route('enviarForUVS01', [$proyecto->id]);
            //Se cambia el estado del formulario 1 como revisado por el director
            $formulario = Formulario::where('id_proyecto', $id_proyecto)->where('id_creado_por', $estudiante->usuario->id)->where('id_tipo_formulario', 1)->first();
            $forUVS01 = $formulario->formularioUVS01;
            $forUVS01->revisado_por_director = true;
            $forUVS01->save();
        }
        //return redirect()->route('home');
        return redirect()->route('formulariosDisponiblesDirector', [$proyecto->id]);

      }
      elseif(Auth::user()->estudiante != null && $proyecto->tipo->id == 1) {
        //Es proyecto comunitario, asi que estudiante debe guardar sus subactividades.
        //Aqui se actualizan las Subactividades que el estudiante ya habia llenado previamente.
        $actividadesAsignadasEstudiante =  Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

        if($actividadesAsignadasEstudiante->isEmpty() == false) {
          foreach ($actividadesAsignadasEstudiante as $actividad) {
            $subactividadesActividadEstudiante = Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividad)->get();

            foreach ($subactividadesActividadEstudiante as $index => $subactividad) {
              $idSubActividad = $subactividad->id;
              $reqDescripcionSubActividad = $request->get('descripcionSubActividadSaved'.$actividad->id.'-'.$idSubActividad);
              $reqDuracionHorasSubActividad = $request->get('horasSubActividadSaved'.$actividad->id.'-'.$idSubActividad);
              $reqRangoFechasSubActividad = $request->get('rangoFechasSubActividadSaved'.$actividad->id.'-'.$idSubActividad);

              if($reqDescripcionSubActividad != null || $reqDuracionHorasSubActividad != null || $reqRangoFechasSubActividad  != null) {
                   $subactividad->descripcion = $reqDescripcionSubActividad;
                   $subactividad->duracion_horas = $reqDuracionHorasSubActividad;

                   if($reqRangoFechasSubActividad  != '') {
                     $fechasParse = explode('-',$reqRangoFechasSubActividad,2);
                     $fechaInicioSubActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                     $fechaFinSubActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                     $subactividad->fecha_inicio = $fechaInicioSubActividad;
                     $subactividad->fecha_finalizacion = $fechaFinSubActividad;
                   }

                   $subactividad->save();
              }
              else {
                $subactividad->delete();
              }
            }
          }
        }

        //Aqui se guardan las nuevas subactividades
        $this->guardarNuevasSubActividadesEstudiante($request, $proyecto, $estudiante);

        if ($request->submit == 'enviar') {
            return redirect()->route('enviarForUVS01', [$proyecto->id]);
        }
        return redirect()->route('home');
      }
      elseif(Auth::user()->estudiante != null && $proyecto->tipo->id == 2) {
        //Es un poryecto empresarial, entonces el estudiante debe crear su cronograma.

        //Aqui se actualizan las actividades que el estudiante ya habia llenado previamente.
        foreach ($proyecto->objetivosEspecificos as $objetivoEspecifico) {
          $actividadesEstudiantePorObjetivo = Actividad::actividadAsignadaEstudiantePorObjetivo(Auth::user()->estudiante, $proyecto, $objetivoEspecifico->id)->get();

          foreach ($actividadesEstudiantePorObjetivo as $actividad) {
            $idActividad = $actividad->id;
            $reqDescripcionActividadObjEspecifico = $request->get('descripcionActividadObjetivoEspecificoSaved'.$objetivoEspecifico->id.'-'.$idActividad);
            $reqDuracionHorasActividadObjEspecifico = $request->get('horasActividadObjetivoEspecificoSaved'.$objetivoEspecifico->id.'-'.$idActividad);
            $reqRangoFechaActividadObjEspecifico = $request->get('rangoFechaActividadObjetivoEspecificoSaved'.$objetivoEspecifico->id.'-'.$idActividad);

            if($reqDescripcionActividadObjEspecifico != null || $reqDuracionHorasActividadObjEspecifico != null ||
               $reqRangoFechaActividadObjEspecifico  != null) {
                 $actividad->descripcion = $reqDescripcionActividadObjEspecifico;
                 $actividad->duracion_horas = $reqDuracionHorasActividadObjEspecifico;

                 if($reqRangoFechaActividadObjEspecifico  != '') {
                   $fechasParse = explode('-',$reqRangoFechaActividadObjEspecifico,2);
                   $fechaInicioActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                   $fechaFinActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                   $actividad->fecha_inicio = $fechaInicioActividad;
                   $actividad->fecha_finalizacion = $fechaFinActividad;
                 }

                 $actividad->save();
            }
            else {
              $actividad->delete();
            }
          }
        }

        $this->guardarNuevasActividadesEstudiante($request, $proyecto);

        if ($request->submit == 'enviar') {
            return redirect()->route('enviarForUVS01', [$proyecto->id]);
        }

        return redirect()->route('pasantiasEmpresariales');
      }
    }

    public function guardarNuevasSubActividadesEstudiante($request, $proyecto, $estudiante) {
      $actividadesAsignadasEstudiante =  Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      if($actividadesAsignadasEstudiante->isEmpty() == false) {
        foreach ($actividadesAsignadasEstudiante as $index => $actividad) {
          $listaDescripcionSubActividades = $request->get('descripcionSubActividades-actividad'.$actividad->id);
          if( $listaDescripcionSubActividades != null) {
            $listaHorasSubActividades = $request->get('horasSubActividades-actividad'.$actividad->id);
            $listaRangoFechasSubActividades = $request->get('rangoFechasSubactividades-actividad'.$actividad->id);

            foreach($listaDescripcionSubActividades as $index => $descripcionSubactividad) {

              if( $descripcionSubactividad != '' || $listaHorasSubActividades[$index] != '' || $listaRangoFechasSubActividades[$index] != '') {
                $nuevaSubActividad = new Actividad;
                $nuevaSubActividad->descripcion = $descripcionSubactividad;
                $nuevaSubActividad->duracion_horas = $listaHorasSubActividades[$index];
                if($listaRangoFechasSubActividades[$index] != ''){
                  $fechasParse = explode('-',$listaRangoFechasSubActividades[$index],2);
                  $fechaInicioSubActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                  $fechaFinSubActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                  $nuevaSubActividad->fecha_inicio = $fechaInicioSubActividad;
                  $nuevaSubActividad->fecha_finalizacion = $fechaFinSubActividad;
                }

                $nuevaSubActividad->es_actividad_macro = false;//0
                $nuevaSubActividad->es_actividad_micro = true;//1

                $nuevaSubActividad->actividadPadre()->associate($actividad->id);
                $nuevaSubActividad->objetivoEspecifico()->associate($actividad->objetivoEspecifico->id);
                $nuevaSubActividad->save();

                //Ahora se asigna la subactividad al estudiante.
                $estudiante->actividadesProyecto()->attach($nuevaSubActividad->id);
              }
            }
          }
        }
      }
    }

    public function guardarNuevasActividadesEstudiante($request, $proyecto) {
      foreach ($proyecto->objetivosEspecificos as $objetivoEspecifico) {
        /**
        * A continuación se guardan las actividades del objetivo especifico.
        */
        $descripcionActividadesObjetivoEspecifico = $request->get('descripcionActividadesObjetivospecific'.$objetivoEspecifico->id);
        $listaHorasActividades = $request->get('horasActividadesObjetivospecific'.$objetivoEspecifico->id);
        $listaRangoFechas = $request->get('rangoFechaActividadesObjetivospecific'.$objetivoEspecifico->id);

        if($descripcionActividadesObjetivoEspecifico != null){
          foreach($descripcionActividadesObjetivoEspecifico as $index => $actividadValue) {
            $nuevaActividad = new Actividad;

            if( $actividadValue != '' || $listaHorasActividades[$index] != '' || $listaRangoFechas[$index] != ''){
              $nuevaActividad->descripcion = $actividadValue;

              $nuevaActividad->duracion_horas = $listaHorasActividades[$index];
              if($listaRangoFechas[$index] != ''){
                $fechasParse = explode('-',$listaRangoFechas[$index],2);
                $fechaInicioActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[0]))));
                $fechaFinActividad = date('Y-m-d',strtotime(str_replace('/', '-', trim($fechasParse[1]))));

                $nuevaActividad->fecha_inicio = $fechaInicioActividad;
                $nuevaActividad->fecha_finalizacion = $fechaFinActividad;
              }
              $nuevaActividad->es_actividad_macro = true;//1
              $nuevaActividad->es_actividad_micro = false;//0

              $nuevaActividad->objetivoEspecifico()->associate($objetivoEspecifico);
              $nuevaActividad->save();

              //Ahora se asigna la nueva actividad al estudiante.
              Auth::user()->estudiante->actividadesProyecto()->attach($nuevaActividad);
            }
          }
        }
      }
    }

    public function enviarFormularioUVS01($id_proyecto) {
        $proyecto = Proyecto::find($id_proyecto);
        $formulario = Formulario::where('id_proyecto', $id_proyecto)->where('id_creado_por', Auth::user()->id)->where('id_tipo_formulario', 1)->first();

        //Cambia el estado del formulario a 'Enviado'
        $estado = EstadoFormulario::find(2); //Enviado

        $formulario->estado()->associate($estado);
        $formulario->comentario_rechazo = null;

        $formulario->save();

        if ($proyecto->tipo->id == 1) { //Pasantias comunitarias
          $tutor = $formulario->creadoPor->estudiante->tutorProyectoComunitaria;
        } else {
          $tutor = $formulario->creadoPor->estudiante->tutorProyectoEmpresarial;
        }

        NotificacionController::enviarNotificacionFormularioCreado($formulario, $tutor->usuario);
        NotificacionController::enviarNotificacionFormularioCreado($formulario, $proyecto->director->usuario);

        if($proyecto->tipo->id == 1) {
          return redirect()->route('home');
        }
        else{
          return redirect()->route('pasantiasEmpresariales');
        }
    }

    public function actualizarActividadesMacroEstudiante($request, $proyecto, $estudiante){
      $objetivosEspecificos = $proyecto->objetivosEspecificos;

      //if(Auth::user()->estudiante != null){
        //$estudiante = Auth::user()->estudiante;

        //Aqui obtengo las actividades asignadas que actualmente tiene el estudiante.
        $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

        //dd($actividadesAsignadasEstudiante);

        if($objetivosEspecificos != null) {
          foreach ($objetivosEspecificos as $objetivoEspecifico) {
            $actividades = $request->get('actividadesObjetivo-'.$objetivoEspecifico->id);

            //Guardamos las nuevas actividades seleccionadas.
            if($actividades != null) {
              foreach($actividades  as $id_actividad) {
                //Busco si la actividad del proyecto ha sido asignada al estudiante.
                $actividad = Actividad::actividadAsignadaEstudiante($estudiante, $proyecto, $id_actividad)->get();

                //$actividad = Actividad::actividadAsignadaEstudiante($estudiante, $proyecto, $id_actividad)->first();
                //dd($actividad);
                //dd($actividad->descripcion);
                //dd($actividad->cumplimiento_actividad_revision_por_tutor);

                if($actividad->isEmpty()){
                  //Si la actividad aún no está asignada al estudiante, se la asignamos.
                  $estudiante->actividadesProyecto()->attach($id_actividad);
                  //$estudiante->actividadesProyecto()->updateExistingPivot($id_actividad, ['cumplimiento_actividad_revision_por_tutor' => false]);
                }
              }
            }

            if ($actividadesAsignadasEstudiante != null) {
              //Si una de las actividades que estaban guardadas no fue recibida en el request entonces la borrramos
              foreach ($actividadesAsignadasEstudiante  as $actividadAsignada) {
                //Debo verificar que la actividad que estoy chequeando corresponde al objetivo especifico correcto.
                if (collect($actividades)->contains($actividadAsignada->id) == false  && $actividadAsignada->objetivoEspecifico->id == $objetivoEspecifico->id) {
                  $estudiante->actividadesProyecto()->detach($actividadAsignada);
                  //(NO SEGURO) Aqui deberian borrarse las subactividades que el estudiante haya llenado previamente
                  //para esta actividad.
                }
              }
            }
          }
        }
      //}
    }

    public function modoEditarFormularioUVS01(Request $request, $id_formulario){

      $formularioUVS01 = Formulario::find($id_formulario);
      $proyecto = $formularioUVS01->proyecto;
      $objetivosEspecificos = null;
      $actividadesTotales = null;

      $estudiante = $formularioUVS01->creadoPor->estudiante;

      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      //dd($actividadesAsignadasEstudiante);
      $objetivosEspecificos = $proyecto->objetivosEspecificos;
      //dd($estudiante->actividadesProyecto);
      //dd($actividadesAsignadasEstudiante);

      if(Auth::user()->director != null) {
        //dd('edit foruvs01 director');

        $actividadesTotales = array();

        foreach ($objetivosEspecificos as $objetivoEspecifico) {

          $actividadesObjetivo = array();
          //Obtengo todas las actividades MACRO del objetivo especifico.
          $actividades = $objetivoEspecifico->actividades;

          //dd($actividades[1]->subactividades);
          //$subactividades = Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividades[1])->get();
          //dd($subactividades);

          if($actividades != null){
            foreach($actividades as $actividad){
              $seleccionada = $this->estaSeleccionadaLaActividad($actividad, $actividadesAsignadasEstudiante);

              $objActividad = new \stdClass();
              $objActividad->actividad = $actividad;
              $objActividad->seleccionada = $seleccionada;

              array_push($actividadesObjetivo, $objActividad);
            }
          }

          array_push($actividadesTotales, $actividadesObjetivo);
        }

        return view('formularios.forUVS01.modoEditar', compact('proyecto', 'objetivosEspecificos', 'actividadesTotales', 'formularioUVS01', 'estudiante'));
      }
      elseif(Auth::user()->estudiante != null) {
        //$objetivosEspecificosEstudiante
        //dd('edit foruvs01 estudiante');
        //dd($actividadesAsignadasEstudiante);


        // /$actividadesPorObjetivo = Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivosEspecificos[1]->id)->get();

        //dd($actividadesPorObjetivo->isEmpty());
        //dd($objetivosEspecificos);

      //  $subactividades = Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividadesPorObjetivo[1])->get();
        //dd($subactividades);

        return view('formularios.forUVS01.modoEditar', compact('proyecto', 'formularioUVS01', 'estudiante', 'objetivosEspecificos'));
      }

    }

    public function estaSeleccionadaLaActividad($actividad, $actividadesAsignadasEstudiante) {
      foreach ($actividadesAsignadasEstudiante as $actividadAsignada) {
        if ($actividadAsignada->id == $actividad->id) {
            return true;
        }
      }
      return false;
    }

    public function revisarForUVS01(Request $request, $id_formulario){
      $formularioUVS01 = Formulario::find($id_formulario);
      $proyecto = $formularioUVS01->proyecto;

      if($request->submit == 'aprobar_foruvs01') {
        $estadoFormulario = EstadoFormulario::find(3); //Aprobado

        //SI SE LO APRUEBA, ENTONCES AHORA SE LE HABILITA EL FORMULARIO FOR-UVS-13 ESPECIFICO DEL ESTUDIANTE AL TUTOR
        $creadoPor = null;
        if(Auth::user()->tutor != null){
          $creadoPor = Auth::user()->id;
        }
        FormularioController::crearFormularioUVS13($proyecto, $creadoPor, $formularioUVS01->creadoPor->estudiante);
      } else {
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(4); //Rechazado
        $formularioUVS01->comentario_rechazo = $request->comentario_rechazo;

        //Se cambia el estado del formulario 1 como NO revisado por el director para que edite sus actividades
        //$formulario = Formulario::where('id_proyecto', $id_proyecto)->where('id_creado_por', $estudiante->usuario->id)->where('id_tipo_formulario', 1)->first();
        $forUVS01 = $formularioUVS01->formularioUVS01;
        if($proyecto->tipo->id == 1) { // Si es proyecto comunitario.
          $forUVS01->revisado_por_director = false;
        }
        $forUVS01->save();
      }

      $formularioUVS01->estado()->associate($estadoFormulario);
      $formularioUVS01->save();

      return redirect()->route('formulariospendientesrevisiontutor', $proyecto->id);
    }

    public function modoVerFormularioUVS01(Request $request, $id_formulario) {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $mostrarBotones = false;
      if($formulario->estado->id == 2 && Auth::user()->tutor != null){ //Enviado
        $mostrarBotones = true;
      }

      $objetivosEspecificos = $proyecto->objetivosEspecificos;

      $estudiante = $formulario->creadoPor->estudiante;

      return view('formularios.forUVS01.modoVer')->with([
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'modoVer' => true,
        'estudiante' => $estudiante,
        'objetivosEspecificos' => $objetivosEspecificos,
        'mostrarBotones' => $mostrarBotones
      ]);
    }
}
