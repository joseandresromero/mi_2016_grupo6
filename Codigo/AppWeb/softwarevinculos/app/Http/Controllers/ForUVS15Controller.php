<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;
use SoftwareVinculos\Http\Requests;

use \SoftwareVinculos\Models\Formulario;
use \SoftwareVinculos\Models\Actividad;

class ForUVS15Controller extends Controller
{
    public function create()
    {
        return view('formularios.forUVS15');
    }

    public function edit($id_forUVS15)
    {

    }

    public function store(Request $request)
    {

    }

    public function update(Request $request, $id_forUVS15)
    {

    }

    public function modoVerFormularioUVS15(Request $request, $id_formulario) {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $formulario15 = $formulario->formularioUVS15;
      $estudiante = $formulario15->estudiante;

      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      return view('formularios.forUVS15.modoVer')->with([
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'modoVer' => true,
        'actividadesAsignadasEstudiante' => $actividadesAsignadasEstudiante,
        'mostrarBotones' => true
      ]);
    }

}
