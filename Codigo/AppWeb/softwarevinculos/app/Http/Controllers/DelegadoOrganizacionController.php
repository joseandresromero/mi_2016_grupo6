<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\DelegadoOrganizacion;
use Input;

use Log;

class DelegadoOrganizacionController extends Controller
{
    public function autocomplete(Request $request) {
      $term = $request->term;
      $data = DelegadoOrganizacion::where('nombre','LIKE','%'.$term.'%')->where('id_organizacion',$request->id_organizacion)->take(10)->get();
      $results = array();
      foreach ($data as $delegado) {
        $results[] = ['id' => $delegado->id, 'value'=>$delegado->nombre, 'delegado' => $delegado];
      }
      return response()->json($results);
    }

    public function guardarDelegado(Request $request) {
      $organizacion = \SoftwareVinculos\Models\Organizacion::find($request->id_organizacion);

      $nuevoDelegado = new DelegadoOrganizacion;

      $nuevoDelegado->nombre = $request->nombre_delegado;
      $nuevoDelegado->cargo = $request->cargo_delegado;
      $nuevoDelegado->area_seccion = $request->area_seccion_delegado;
      $nuevoDelegado->organizacion()->associate($organizacion);
      $nuevoDelegado->save();

      return response()->json(['id_delegado' => $nuevoDelegado->id, 'nombre_delegado' => $nuevoDelegado->nombre]);
    }
}
