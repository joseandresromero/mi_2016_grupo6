<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;
use SoftwareVinculos\Http\Controllers\FormularioController;

use SoftwareVinculos\Models\Usuario;
use SoftwareVinculos\Models\Estudiante;
use \SoftwareVinculos\Models\EstadoEstudiante;
use \SoftwareVinculos\Models\Proyecto;
use \SoftwareVinculos\Models\Formulario;

use SoftwareVinculos\Utilities\EspolWS;
use Auth;

use Input;

use Log;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pasantiasComunitarias()
    {
        $usuario = Auth::user();
        $formulariosCreados = $usuario->formulariosCreados;
        $proyecto = Auth::user()->estudiante->proyectoComunitaria;
        $tipoProyecto = 'Pasantías Comunitarias';
        $tutor = Auth::user()->estudiante->tutorProyectoComunitaria;

        $formularios = array();
        foreach ($formulariosCreados as $formulario) {
          if ($formulario->proyecto->tipo->id == 1) {
            //Pasantias Comunitarias
            array_push($formularios, $formulario);
          }
        }

        return view('estudiante.proyecto', compact('formularios', 'proyecto', 'tipoProyecto', 'tutor'));
    }

    public function pasantiasEmpresariales()
    {
      $usuario = Auth::user();
      $formulariosCreados = $usuario->formulariosCreados;
      $proyecto = Auth::user()->estudiante->proyectoEmpresarial;
      $tipoProyecto = 'Pasantías Empresariales';
      $tutor = $tutor = Auth::user()->estudiante->tutorProyectoEmpresarial;

      $formularios = array();
      foreach ($formulariosCreados as $formulario) {
        if ($formulario->proyecto->tipo->id == 2) {
          //Pasantias Empresariales
          array_push($formularios, $formulario);
        }
      }

      return view('estudiante.proyecto', compact('formularios', 'proyecto', 'tipoProyecto', 'tutor'));
    }

    public function detalleEstudianteProyecto(Request $request, $id_estudiante, $id_proyecto){
      $estudiante = Estudiante::find($id_estudiante);
      $proyectoEspecifico = Proyecto::find($id_proyecto);
      $formularios = $formularios = array();

      //Se obtiene formulario FOR-UVS-01 y FOR-UVS-14 del estudiante.
      $formulariosEstudiante = $estudiante->usuario->formulariosCreados->where('id_proyecto', $proyectoEspecifico->id);

      foreach ($formulariosEstudiante as $formulario) {
        array_push($formularios, $formulario);
      }

      $formularioUVS13 = Formulario::forUVS13Estudiante($proyectoEspecifico->id, $estudiante->id)->get();
      if(!$formularioUVS13->isEmpty() ) {
        array_push($formularios, $formularioUVS13->first());
      }

      $formularioUVS15 = Formulario::forUVS15Estudiante($proyectoEspecifico->id, $estudiante->id)->get();
      if(!$formularioUVS15->isEmpty() ) {
        array_push($formularios, $formularioUVS15->first());
      }

      $formularioUVS16 = Formulario::forUVS16Estudiante($proyectoEspecifico->id, $estudiante->id)->get();
      if(!$formularioUVS16->isEmpty() ) {
        array_push($formularios, $formularioUVS16->first());
      }

      return view('proyecto.detalleEstudianteProyecto', compact('estudiante', 'proyectoEspecifico', 'formularios'));
    }

    /*
    * Función utilizada cuando se quieren asignar estudiantes a un proyecto desde el panel de
    * 'Asignar Estudiantes' del Coordinador de Carrera
    */
    public function asignarEstudianteProyecto(Request $request, $id_proyecto){
      $matricula = $request->get('matriculaEstudiante');
      $proyecto = Proyecto::find($id_proyecto);

      /* Primero busco si el estudiante solicitado ya existe en la base. */
      $estudiante = Estudiante::estudiantePorMatricula($matricula)->first();

      if($estudiante != null){
        /* Si se encontró al estudiante en nuestra bd, entonces ahora debo verificar si el estudiante ya fue asignado o no
           al actual proyecto (o a cualquier otro proyecto, según el tipo de proyecto). */
        if($proyecto->tipo->id == 2){
          //Pasantias Empresariales
          if($estudiante->id_proyecto_empresarial != null){
            $mensaje= 'El estudiante '.$estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos.' actualmente ya tiene asignado un proyecto de pasantías empresariales';
            session()->flash('flash_message', $mensaje);
          }
          else{
            //Asigno el proyecto empresarial al estudiante
            $estudiante->proyectoEmpresarial()->associate($proyecto);
            //$estadoEstudiante = EstadoEstudiante::find(2); //En proyecto

            $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(2); //En proyecto
            $estudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);

            //$estudiante->estado()->associate($estadoEstudiante);
            $estudiante->save();
          }
        }
        else if($proyecto->tipo->id == 1){
          //Pasantiias Comunitarias
          if($estudiante->id_proyecto_comunitaria != null){
            $mensaje = 'El estudiante '.$estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos.' actualmente ya tiene asignado un proyecto de pasantías comunitarias.';
           session()->flash('flash_message', $mensaje);
          }
          else{
            /* Asigno el proyecto comunitario al estudiante */
            $estudiante->proyectoComunitaria()->associate($proyecto);
          //  $estadoEstudiante = EstadoEstudiante::find(2); //En proyecto
            //$estudiante->estado()->associate($estadoEstudiante);
            $estadoEstProyectoComunitario = EstadoEstudiante::find(2); //En proyecto
            $estudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);

            $estudiante->save();
          }
        }
      }
      else{
        //Si el estudiante no se encontró en nuestra base con el número de matricula,
        //entonces obtengo los datos del estudiante usando los web-services de ESPOL
        $infoEstudiante = EspolWS::datosEstudianteMatricula($matricula);
        if($infoEstudiante != null){
          $proyecto = Proyecto::find($id_proyecto);

          //Registro al estudiante en nuestra base de datos y le asigno el proyecto actual
          $estudiante = $this->registerEstudiante($infoEstudiante, $proyecto, $matricula);
        }
        else{
          $mensaje = 'No existe estudiante con el número de matricula proporcionado';
          session()->flash('flash_message', $mensaje);
        }
      }
      return redirect()->route('listaestudiantesproyecto', $proyecto);
    }

    public function desasignarEstudianteProyecto(Request $request, $id_proyecto, $id_estudiante){
      $estudiante = Estudiante::find($id_estudiante);
      $proyecto = Proyecto::find($id_proyecto);

      if($proyecto->tipo->id == 2){
        //Pasantias Empresariales
        $estudiante->proyectoEmpresarial()->dissociate();

        $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(1); //Sin proyecto
        $estudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);

        //Si tenia asignado tutor, se lo desasigna.
        $estudiante->tutorProyectoEmpresarial()->dissociate();

        $estudiante->save();
      }
      else if($proyecto->tipo->id == 1){
        //Pasantias Comunitarias
        $estudiante->proyectoComunitaria()->dissociate();

        $estadoEstProyectoComunitario = EstadoEstudiante::find(1); //Sin proyecto
        $estudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);

        //Si tenia asignado tutor, se lo desasigna.
        $estudiante->tutorProyectoComunitaria()->dissociate();

        $estudiante->save();
      }

      //Aqui se deben eliminar tambien los formularios  de este proyecto que pertenecen al estudiante.
      $formulariosEstudiante = $estudiante->usuario->formulariosCreados->where('id_proyecto', $proyecto->id);

      if( $formulariosEstudiante != null) {
        foreach ($formulariosEstudiante as $formulario) {
          $formulario->delete();
        }
      }

      return redirect()->route('listaestudiantesproyecto', $proyecto);
    }

    public function registerEstudiante($infoEstudiante, $proyecto, $matricula) {
        $usernameParsing = explode('@', $infoEstudiante->EMAIL, 2);
        mb_internal_encoding('UTF-8');

        //Creo el usuario generico
        $nuevoUsuario = new Usuario;
        $nuevoUsuario->username = $usernameParsing[0];
        $nuevoUsuario->nombres = ucwords(mb_strtolower($infoEstudiante->NOMBRES));
        $nuevoUsuario->apellidos = ucwords(mb_strtolower($infoEstudiante->APELLIDOS));
        $nuevoUsuario->cedula = $infoEstudiante->CEDULA;
        $nuevoUsuario->correo_electronico = $infoEstudiante->EMAIL;
        $nuevoUsuario->informacion_actualizada = false;

        $nuevoUsuario->save();

        //Se asigna el rol al usuario
        $rol = \SoftwareVinculos\Models\Rol::find(5); //Estudiante
        $nuevoUsuario->roles()->attach($rol->id);

        //Se crea el usuario estudiante
        $nuevoEstudiante = new Estudiante;
        $nuevoEstudiante->matricula = $matricula;
        /*
        if ($infoUsuario->TIPOIDENTIFICACION == 'MATRICULA') {
            $nuevoEstudiante->matricula = $infoUsuario->IDENTIFICACION;
        }
        */
        $estadoEstProyectoComunitario = EstadoEstudiante::find(1); //Sin proyecto
        $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(1); //Sin proyecto

        if($proyecto != null){
          if($proyecto->tipo->id == 2){ //Pasantias empresariales
            $nuevoEstudiante->proyectoEmpresarial()->associate($proyecto);

            $estadoEstProyectoComunitario = EstadoEstudiante::find(1); //Sin proyecto
            $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(2); //En proyecto
          }
          else if($proyecto->tipo->id == 1){ //PAsantias comunitarias
            $nuevoEstudiante->proyectoComunitaria()->associate($proyecto);

            $estadoEstProyectoComunitario = EstadoEstudiante::find(2); //En proyecto
            $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(1); //Sin proyecto
          }
        }

        $nuevoEstudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);
        $nuevoEstudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);

        $nuevoEstudiante->usuario()->associate($nuevoUsuario);
        //$nuevoEstudiante->estado()->associate($estado);
        $nuevoEstudiante->save();

        return $nuevoEstudiante;
    }

    /*
    * Esta función es usada cada vez que se hace un ajax request cuando se quiere agregar un estudiante
    * desde la vista de creación del formulario uvs04
    */
    public function obtenerDatosEstudiante(Request $request) {
      $matricula = $request->matricula_estudiante;

      // Primero busco si el estudiante solicitado ya existe en la base
      $estudiante = Estudiante::estudiantePorMatricula($matricula)->first();

      if($estudiante != null) {
        //Si se encuentra al estudiante, entonces ahora se verifica si ya tiene un proyecto asignado (PROYECTO YA APROBADO INICIADO O FINALIZADO).
        if($request->id_tipo_proyecto == 1 && $estudiante->proyectoComunitaria != null && $estudiante->proyectoComunitaria->estado->id != 1 && $estudiante->proyectoComunitaria->estado->id != 3 ) {
          return response()->json(['sucess' => false, 'message' => 'El estudiante '.$estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos.' ya fue asignado previamente a un proyecto comunitario.']);
        }
        elseif($request->id_tipo_proyecto == 2 && $estudiante->proyectoEmpresarial != null && $estudiante->proyectoEmpresarial->estado->id != 1 && $estudiante->proyectoEmpresarial->estado->id != 3 ) {
          return response()->json(['sucess' => false, 'message' => 'El estudiante '.$estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos.' ya fue asignado previamente a un proyecto de práctica Pre-Profesional.']);
        }

        return response()->json(['id_estudiante' => $estudiante->id, 'nombrecompleto' => $estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos,
                                 'telefono' =>$estudiante->usuario->celular, 'correo_electronico' => $estudiante->usuario->correo_electronico,
                                 'matricula' => $estudiante->matricula, 'sucess' => true]);
      }
      else {
        /*
        * Si no encuentra al estudiante, entonces obtiene los Datos
        * del estudiante usando los web-services de ESPOL.
        */
        $infoEstudiante = EspolWS::datosEstudianteMatricula($matricula);

        if($infoEstudiante != null) {
          // Registra al estudiante con los datos obtenidos del web-service de ESPOL.
          $estudiante = $this->registerEstudiante($infoEstudiante, null, $matricula);

          return response()->json(['id_estudiante' => $estudiante->id, 'nombrecompleto' => $estudiante->usuario->nombres.' '.$estudiante->usuario->apellidos,
                                   'telefono' =>$estudiante->usuario->celular, 'correo_electronico' => $estudiante->usuario->correo_electronico,
                                   'matricula' => $estudiante->matricula, 'sucess' => true]);
        }
        else {
          /*
          * Si se llega a este punto, entonces no existe ningún estudiante que contenga
          * el número de matricula proporcionado .
          */
          return response()->json(['sucess' => false, 'message' => 'No existe estudiante con el numero de matricula proporcionado']);
        }
      }

    }

    public function adjuntarCartaCompromisoComunitarias(Request $request) {
      if ($request->hasFile('cartaComunitariasUpload')) {
          $estudiante = Estudiante::find($request->id_estudiante);
          $formulario = Formulario::find($request->id_formulario);

          $file = $request->file('cartaComunitariasUpload');
          $extension = $file->getClientOriginalExtension();
          $url = base_path().'\\public\\uploads\\estudiantes\\'.$estudiante->usuario->id.'\\'.'cartaCompromisoComunitarias.'.$extension;

          $file->move($url);

          $formulario->url_carta_compromiso = $url;
          $formulario->save();

//          FormularioController::crearFormularioUVS01($formulario->proyecto, $estudiante);
      }
      return redirect()->route('home');
    }

    public function adjuntarCartaCompromiso(Request $request) {
      if ($request->hasFile('cartaEmpresarialesUpload')) {
          //$estudiante = Estudiante::find($request->id_estudiante);
          $formulario = Formulario::find($request->id_formulario);

          $file = $request->file('cartaEmpresarialesUpload');
          $extension = $file->getClientOriginalExtension();
          $url = base_path().'\\public\\uploads\\estudiantes\\'.$request->id_estudiante.'\\'.'cartaCompromisoEmpresariales.'.$extension;

          $file->move($url);

          $formulario->url_carta_compromiso = $url;
          $formulario->save();

//          FormularioController::crearFormularioUVS01($formulario->proyecto, $estudiante);
      }
      return redirect()->route('home');
    }
}
