<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\Organizacion;
use Input;

use Log;

class OrganizacionController extends Controller
{
    public function index() {
      $organizaciones = Organizacion::all();

      return view('profesor.director.organizaciones')->with([
        'organizaciones' => $organizaciones
      ]);
    }

    public function edit($id_organizacion) {
      $organizacion = Organizacion::find($id_organizacion);
      return view('profesor.director.editarOrganizacion')->with(['organizacion' => $organizacion]);
    }

    public function update(Request $request, $id_organizacion) {
      $organizacion = Organizacion::find($id_organizacion);

      $organizacion->nombre = $request->nombre;
      $organizacion->direccion = $request->direccion;
      $organizacion->telefono = $request->telefono;
      $organizacion->fax = $request->fax;
      $organizacion->correo_electronico = $request->correo_electronico;
      $organizacion->resumen = $request->resumen;
      $organizacion->nombre_representante_legal = $request->nombre_representante_legal;
      $organizacion->fecha_creacion = $request->fecha_creacion;
      $organizacion->nombramiento_representante_legal = $request->nombramiento_representante_legal;
      $organizacion->save();

      return redirect()->route('organizaciones');
    }

    public function autocomplete(Request $request) {
      $term = $request->term;
      $data = Organizacion::where('nombre','LIKE','%'.$term.'%')->take(10)->get();
      $results = array();
      foreach ($data as $organizacion) {
        $results[] = ['id' => $organizacion->id, 'value'=>$organizacion->nombre, 'organizacion' => $organizacion];
      }
      return response()->json($results);
    }

    public function guardarOrganizacionBase(Request $request) {
      $this->guardarOrganizacionFromRequest($request);

      return redirect()->route('organizaciones');
    }

    public function guardarOrganizacionFromRequest($request) {
      $nuevaOrganizacion = new Organizacion;

      $nuevaOrganizacion->nombre = $request->razon_social_organizacion;
      $nuevaOrganizacion->direccion = $request->direccion_organizacion;
      $nuevaOrganizacion->telefono = $request->telefono_organizacion;
      $nuevaOrganizacion->fax = $request->fax_organizacion;
      $nuevaOrganizacion->correo_electronico = $request->correo_electronico_organizacion;
      $nuevaOrganizacion->resumen = $request->resumen_organizacion;
      $nuevaOrganizacion->nombre_representante_legal = $request->representante_legal_organizacion;
      $nuevaOrganizacion->fecha_creacion = $request->fecha_creacion;
      $nuevaOrganizacion->nombramiento_representante_legal = $request->nombramiento_representante_legal;
      $nuevaOrganizacion->save();

      return $nuevaOrganizacion;
    }

    public function guardarOrganizacion(Request $request) {
      $nuevaOrganizacion = $this->guardarOrganizacionFromRequest($request);

      return response()->json(['id_organizacion' => $nuevaOrganizacion->id]);
    }

    public function eliminarOrganizacion($id_organizacion) {
      $organizacion = Organizacion::find($id_organizacion);

      if ($organizacion->proyectos->count() > 0) {
        $mensaje = 'No se pudo eliminar la organización debido a que está ligada a uno o varios proyectos.';
        $tipo = 'error';
      } else {
        $organizacion->delete();
        $mensaje = 'Se ha eliminado satisfactoriamente la organización.';
        $tipo = 'success';
      }

      $organizaciones = Organizacion::all();
      session()->flash('flash_type', $tipo);
      session()->flash('flash_message', $mensaje);
      return redirect()->route('organizaciones');
    }

    public function verConvenioCrearProyecto($id_organizacion) {
      return $this->verConvenio($id_organizacion);
    }

    public function verConvenioEditarProyecto($id_director, $id_organizacion) {
      return $this->verConvenio($id_organizacion);
    }

    public function verConvenio($id_organizacion) {
      $organizacion = Organizacion::find($id_organizacion);

      if ($organizacion->url_convenio != null) {
        if (file_exists($organizacion->url_convenio) == true) {
          return response()->download($organizacion->url_convenio);
        } else {
          dd('No se ha subido un convenio');
        }
      } else {
        dd('No se ha subido un convenio');
      }
   }

   public function existeConvenio(Request $request) {
     $slash = config('softwarevinculos.slash');
     $organizacion = Organizacion::find($request->id_organizacion);

     $result = null;

     if ($request->id_organizacion != 0) {
       if ($organizacion->url_convenio != null) {

         if ($slash != '/') {
           $urlFull = base_path().$slash.'public'.str_replace("/", "\\", $organizacion->url_convenio);
         } else {
           $urlFull = base_path().$slash.'public'.$organizacion->url_convenio;
         }

         if (file_exists($urlFull) == true) {
           $result = \URL::to('/').$organizacion->url_convenio;
         }
       }
     }

     return response()->json(['result' => $result]);
   }
}
