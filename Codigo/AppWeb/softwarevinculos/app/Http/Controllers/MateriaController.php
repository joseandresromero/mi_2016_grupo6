<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\Materia;
use Input;

class MateriaController extends Controller
{
  public function autocomplete(Request $request){
    $term = $request->term;
    $data = Materia::where('nombre','LIKE','%'.$term.'%')->take(10)->get();
    $results = array();
    foreach ($data as $materia) {
      $results[] = ['id' => $materia->id, 'value'=>$materia->nombre];
    }
    return response()->json($results);
  }
}
