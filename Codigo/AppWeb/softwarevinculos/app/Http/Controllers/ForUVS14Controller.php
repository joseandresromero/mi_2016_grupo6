<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;
use SoftwareVinculos\Http\Requests;

use \SoftwareVinculos\Models\Formulario;
use \SoftwareVinculos\Models\TipoFormulario;
use \SoftwareVinculos\Models\EstadoFormulario;
use \SoftwareVinculos\Models\ForUVS15;
use \SoftwareVinculos\Models\ForUVS16;
use \SoftwareVinculos\Models\Tutor;
use \SoftwareVinculos\Models\Proyecto;

use Auth;

class ForUVS14Controller extends Controller
{
    public function create()
    {
        return view('formularios.forUVS14');
    }

    public function edit($id_forUVS14)
    {

    }

    public function store(Request $request)
    {

    }

    public function update(Request $request, $id_formulario)
    {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;
      $formulariouvs14 = $formulario->formulariosUVS14->first();

      $formulariouvs14->analisis_evaluacion_actividades = $request->analisis_evaluacion_actividades;
      $formulariouvs14->conclusiones = $request->conclusiones;
      $formulariouvs14->recomendaciones = $request->recomendaciones;
      $formulariouvs14->save();

      //Verificar si se esta guardando o enviando, y si se esta enviando se debe cambiar el estado del formulario a enviado
      if($request->submit == 'guardar') {
          return redirect()->route('home');
      } else if ($request->submit == 'enviar') {
          return redirect()->route('enviarForUVS14', [$proyecto->id]);
      } else {
          return redirect()->route('home');
      }
    }

    public function enviarFormularioUVS14($id_proyecto) {
        $proyecto = Proyecto::find($id_proyecto);
        $formulario = Formulario::where('id_proyecto', $id_proyecto)->where('id_creado_por', Auth::user()->id)->where('id_tipo_formulario', 5)->first();

        //Cambia el estado del formulario a 'Enviado'
        $estado = EstadoFormulario::find(2); //Enviado

        $formulario->estado()->associate($estado);
        $formulario->comentario_rechazo = null;

        $formulario->save();

        return redirect()->route('home');
    }

    public function modoEditarFormularioUVS14(Request $request, $id_formulario){
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $formularioUVS14 = $formulario->formulariosUVS14()->first();

      $tutor = null;

      if(Auth::user()->estudiante != null){
        $estudiante = Auth::user()->estudiante;
        if ($proyecto->tipo->id = 1) { //Pasantias Comunitarias
          $tutor = Tutor::find($estudiante->id_tutor_comunitaria);
        }
        else{
          $tutor = Tutor::find($estudiante->id_tutor_empresarial);
        }
      }

      return view('formularios.forUVS14.modoEditar', compact('proyecto', 'formulario', 'tutor'));
    }

    public function revisarForUVS14(Request $request, $id_formulario){
      $formularioUVS14 = Formulario::find($id_formulario);

      $id_proyecto = $formularioUVS14->proyecto->id;

      if($request->submit == 'aprobar_foruvs14') {
        $estadoFormulario = EstadoFormulario::find(3); //Aprobado

        //Si se aprueba el formulario 14, entonces se habilita el formulario 15 y 16 para ser llenado por el tutor.
        $proyecto = Proyecto::find($id_proyecto);
        $estado = EstadoFormulario::find(1); //No enviado

        $creadoPor = null;
        if(Auth::user()->tutor != null){
          $creadoPor = Auth::user()->id;
        }

        //FOR-UVS-15
        FormularioController::crearFormularioUVS15($proyecto, $creadoPor, $formularioUVS14->creadoPor->estudiante); //Formulario14 pertenece al estudiante, asi que de aqui obtenemos su usuario.

        //FOR-UVS-16
        FormularioController::crearFormularioUVS16($proyecto, $creadoPor, $formularioUVS14->creadoPor->estudiante); //Formulario14 pertenece al estudiante, asi que de aqui obtenemos su usuario.

      } else {
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(4); //Rechazado
        $formularioUVS14->comentario_rechazo = $request->comentario_rechazo;
      }

      $formularioUVS14->estado()->associate($estadoFormulario);
      $formularioUVS14->save();

      return redirect()->route('formulariospendientesrevisiontutor', $id_proyecto);
    }

    public function modoVerFormularioUVS14(Request $request, $id_formulario) {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $mostrarBotones = false;
      if($formulario->estado->id == 2 && Auth::user()->tutor != null){ //Enviado
        $mostrarBotones = true;
      }

      $tutor = null;
      $estudiante = $formulario->creadoPor->estudiante;
      if ($proyecto->tipo->id = 1) { //Pasantias Comunitarias
        $tutor = Tutor::find($estudiante->id_tutor_comunitaria);
      }
      else{
        $tutor = Tutor::find($estudiante->id_tutor_empresarial);
      }

      return view('formularios.forUVS14.modoVer')->with([
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'tutor' => $tutor,
        'modoVer' => true,
        'mostrarBotones' => $mostrarBotones
      ]);
    }

}
