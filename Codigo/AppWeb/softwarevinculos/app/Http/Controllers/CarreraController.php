<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\Carrera;
use SoftwareVinculos\Models\TipoProyecto;
use Input;

use Log;

class CarreraController extends Controller
{

    public function autocomplete(Request $request){

      $term = $request->term;
      $data = Carrera::where('nombre','LIKE','%'.$term.'%')->take(10)->get();
      $tiposProyecto = TipoProyecto::all();
      $results = array();
      foreach ($data as $carrera) {
        $results[] = ['id' => $carrera->id, 'value'=>$carrera->nombre, 'coordinadorComunitarias' => ($carrera->coordinadorComunitarias == null) ? null : $carrera->coordinadorComunitarias->usuario, 'coordinadorEmpresariales' => ($carrera->coordinadorEmpresariales == null) ? null : $carrera->coordinadorEmpresariales->usuario, 'tiposProyecto' => $tiposProyecto];
      }
      return response()->json($results);
    }

    public function obtenerCoordinadorPorTipoProyecto(Request $request) {
      $carrera = Carrera::find($request->id_carrera);

      $tipoProyecto = TipoProyecto::find($request->id_tipo_proyecto);

      if ($tipoProyecto->id == 1) {
        //Pasantias Comunitarias
        $coordinador = $carrera->coordinadorComunitarias->usuario;
      } else {
        //Pasantias Empresariales
        $coordinador = $carrera->coordinadorEmpresariales->usuario;
      }

      return response()->json(['coordinador' => $coordinador]);
    }
}
