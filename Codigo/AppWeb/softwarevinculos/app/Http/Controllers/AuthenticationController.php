<?php

namespace SoftwareVinculos\Http\Controllers;

use Auth;
use Log;

use SoftwareVinculos\Models\Usuario;
use SoftwareVinculos\Models\Estudiante;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;

use SoftwareVinculos\Utilities\EspolWS;

class AuthenticationController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $testUsers = ['director', 'coordinador', 'coordinador2', 'coordinador3', 'coordinador4', 'coordinador5', 'coordinador6', 'tutor', 'tutor2', 'tutor3', 'tutor4', 'estudiante2', 'estudiante3'];

        //Obtengo el usuario y contraseña ingresado por el usuario
        $username = $request->input('username');
        $password = $request->input('password');

        //Se verifica primero con el web service de ESPOL la autenticacion del usuario
        //$autenticado = EspolWS::autenticacion($username,$password);
        //TEST
        //$autenticado = true;

        if ($username == 'admin' || in_array($username, $testUsers)) {
          $autenticado = true;
        } else {
          $autenticado = EspolWS::autenticacion($username,$password);
        }

        if ($autenticado == true) {
            //Autenticacion con ESPOL exitosa
            Log::info('Usuario '.$username.' autenticado por ESPOL');

            //Luego autenticamos el usuario con nuestra base de datos, si no se autentica es porque el usuario
            //no existe, asi que lo creamos.
            if (Auth::attempt(['username' => $username])) {
                //Usuario autenticado
                return $this->manejarAutenticacionExitosa();

            } else {
              return $this->manejarAutenticacionFallida('Usuario no encontrado en la base de datos');
            }
        } else {
            //Autenticacion con ESPOL fallida
            Log::info('El usuario '.$username.' no pudo ser autenticado por ESPOL');
            return $this->manejarAutenticacionFallida('Usuario o contraseña incorrectos');
        }

    }

    public function manejarAutenticacionExitosa() {
      return redirect()->route('home');
    }

    public function manejarAutenticacionFallida($mensaje) {
      session()->flash('flash_message', $mensaje);
      return redirect()->route('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    //Funcion que simula la obtencion de la informacion del usuario desde el web service
    public function infoUsuarioStubWS() {
        $infoUsuario = new \stdClass();
        $infoUsuario->NOMBRES = 'USUARIO';
        $infoUsuario->APELLIDOS = 'PRUEBA';
        $infoUsuario->IDENTIFICACION = '123456789';
        $infoUsuario->TIPO = 'ALUMNO';

        return $infoUsuario;
    }

    public function logJS($titulo, $mensaje) {
        echo '<script>console.log("'.$titulo.': '.$mensaje.'");</script>';
    }
}
