<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;
use SoftwareVinculos\Http\Controllers\NotificacionController;
use SoftwareVinculos\Http\Controllers\FormularioController;

use SoftwareVinculos\Models\Tutor;
use SoftwareVinculos\Models\Estudiante;
use SoftwareVinculos\Models\EstadoEstudiante;
use SoftwareVinculos\Models\Proyecto;
use SoftwareVinculos\Models\NotificacionInformativa;

use SoftwareVinculos\Models\Formulario;
use SoftwareVinculos\Models\TipoFormulario;
use SoftwareVinculos\Models\EstadoFormulario;

use Input;
use Auth;

class TutorController extends Controller
{
  public function autocomplete(Request $request){
    $term = $request->term;

    $data = Tutor::tutoresSearchByNombre($term)->take(10)->get();
    $results = array();
    foreach ($data as $tutor) {
      $results[] = ['id' => $tutor->id, 'value'=> $tutor->nombres.' '.$tutor->apellidos];
    }
    return response()->json($results);
  }

  public function asignarTutorProyecto(Request $request, $id_proyecto){
    $proyectoEspecifico = Proyecto::find($id_proyecto);
    $tutoresProyecto = $proyectoEspecifico->tutores;
    $id_tutor = $request->get('tutorSeleccionado');
    $tutor = Tutor::find($id_tutor);

    if($this->existeTutorEnProyecto($tutoresProyecto, $id_tutor)){
      /* El tutor ya ya fue asignado previamente al proyecto*/
      $mensaje= 'El profesor '.$tutor->usuario->nombres.' '.$tutor->usuario->apellidos.' ya fua asignado previamente a este proyecto.';
      session()->flash('flash_message', $mensaje);
    }
    else{
      /* Se asigna tutor al proyecto si es que no ha sido asignado aún */
      $proyectoEspecifico->tutores()->attach($tutor->id);

      NotificacionController::enviarNotificacionTutorAsignadoAProyecto($tutor, $proyectoEspecifico, $tutor->usuario);
    }

    return redirect()->route('listatutoresproyecto', $proyectoEspecifico);
  }

  public function existeTutorEnProyecto($tutoresProyecto, $id_tutor) {
    foreach ($tutoresProyecto as $tutor) {
      if($tutor->id == $id_tutor) {
        return true;
      }
    }
    return false;
  }

  public function desasignarTutorProyecto(Request $request, $id_proyecto, $id_tutor){
    $proyectoEspecifico = Proyecto::find($id_proyecto);
    $tutor = Tutor::find($id_tutor);

    $proyectoEspecifico->tutores()->detach($tutor->id);

    /* Se desasignan los estudiantes que el tutor ya tenia asignado en este proyecto */
    $estudiantesTutor = null;

    if($proyectoEspecifico->tipo->id == 1){ //Pasantias Comunitarias
      $estudiantesTutor = $tutor->estudiantesComunitarias;

      /////Aqui falta una validaci;on que no estoy seguro si debe ir, si el tutor es desasignado del proyecto, entonces el
      // estudiante no tendrá tutor asignado y es como si no hubiera sido asignado al proyecto, en ese caso, deberian
      // eliminarse de la base los formularios creados por ese estudiante en el proyecto?por ejm el formuvs01, si se le asigna otro tutor, este deberia revisar
      // otra vez el formulariouvs01 para aprobaro, pero el estudiante deberia llenar otra vez dicho formulario.
      if( $estudiantesTutor != null){
        foreach ($estudiantesTutor as $estudiante) {
          $estudiante->tutorProyectoComunitaria()->dissociate();
          $estudiante->save();
        }
      }
    }
    else{
      $estudiantesTutor = $tutor->estudiantesEmpresarial;
      if( $estudiantesTutor != null){
        foreach ($estudiantesTutor as $estudiante) {
          $estudiante->tutorProyectoEmpresarial()->dissociate();
          $estudiante->save();
        }
      }
    }

    return redirect()->route('listatutoresproyecto', $proyectoEspecifico);
  }

  public function asignarTutorEstudiante(Request $request, $id_estudiante, $id_proyecto) {
    $id_tutor = $request->get('tutoresDisponible');

    $proyecto = Proyecto::find($id_proyecto);
    $estudiante = Estudiante::find($id_estudiante);
    $tutor = Tutor::find($id_tutor);

    if($proyecto->tipo->id == 2) { //Pasantias Empresariales
      if($estudiante->tutorProyectoEmpresarial == null) {
        FormularioController::crearFormularioUVS01($proyecto, $estudiante);

        //Se cambia estado del estudiante en el proyecto empresarial
        $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(3); //Actividades Iniciadas
        $estudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);
      }
      $estudiante->tutorProyectoEmpresarial()->associate($tutor);
    }
    else if($proyecto->tipo->id == 1) { //Pasantias Comunitarias
      if($estudiante->tutorProyectoComunitaria == null) {
        FormularioController::crearFormularioUVS01($proyecto, $estudiante);

        //Se cambia estado del estudiante en el proyecto comunitario
        $estadoEstProyectoComunitario = EstadoEstudiante::find(3); //Actividades Iniciadas
        $estudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);

      }
      $estudiante->tutorProyectoComunitaria()->associate($tutor);
    }

    $estudiante->save();


/*
    if ($proyecto->organizacion->url_convenio != null) {
      FormularioController::crearFormularioUVS01($proyecto, $estudiante);
    } else {
      FormularioController::crearFormularioUVS05($proyecto, $estudiante);
    }
*/
    //Se envia la notificacion al tutor y al estudiante
    NotificacionController::enviarNotificacionEstudianteAsignadoATutor($estudiante, $tutor, $proyecto, $tutor->usuario);
    NotificacionController::enviarNotificacionEstudianteAsignadoATutor($estudiante, $tutor, $proyecto, $estudiante->usuario);

    return redirect()->route('listaestudiantesproyecto', $proyecto);
  }

  public function enviarNotificacionesAsignacionEstudianteATutor($usuarioEstudiante, $usuarioTutor, $proyecto) {
    $notificacionEstudiante = new NotificacionInformativa;
    $notificacionEstudiante->visto = false;
    $notificacionEstudiante->mensaje = 'Usted ha sido asignado al proyecto: ' . $proyecto->titulo . '. Su tutor es ' . $usuarioTutor->nombres . ' ' . $usuarioTutor->apellidos . '.';
    $notificacionEstudiante->destinatario()->associate($usuarioEstudiante);
    $notificacionEstudiante->save();

    $notificacionTutor = new NotificacionInformativa;
    $notificacionTutor->visto = false;
    $notificacionTutor->mensaje = 'Usted ha sido asignado como tutor del estudiante ' . $usuarioEstudiante->nombr0es . ' ' . $usuarioEstudiante->apellidos . ' en el proyecto ' . $proyecto->titulo . '.';
    $notificacionTutor->destinatario()->associate($usuarioTutor);
    $notificacionTutor->save();
  }

  public function indexProyectoEspecifico(Request $request, $id_proyecto)
  {
    $proyecto = \SoftwareVinculos\Models\Proyecto::find($id_proyecto);

    return view('tutor.proyecto', compact('proyecto'));
  }

  /*
  * Me redirijo hacia la página de formularios disponibles por llenar del tutor en un proyecto en específico.
  */
  public function formulariosDisponiblesTutor($id_proyecto) {
    //$formulariosDisponibles = Auth::user()->formulariosCreados;
    $proyectoEspecifico = Proyecto::find($id_proyecto);
    $formulariosDisponibles = Formulario::formulariosDisponibleTutor($id_proyecto, Auth::user()->id)->get();

    //dd($formulariosDisponibles);

    return view('profesor.tutor.formulariosDisponibles')->with([
      'formulariosDisponibles' => $formulariosDisponibles,
      'proyectoEspecifico' => $proyectoEspecifico
    ]);
    /*
    return view('profesor.formularios')->with([
      'formulariosDisponibles' => $formulariosDisponibles,
      'proyectoEspecifico' => $proyectoEspecifico
    ]);
    */
  }

  /*
  * Me redirijo hacia la página de formularios pendientesde revisión por parte del tutor en un proyecto en especifico
  */
  public function formulariosPendientesRevisionProyecto(Request $request, $id_proyecto){

    $proyectoEspecifico = Proyecto::find($id_proyecto);
    $formularios = null;
    $tutor = Auth::user()->tutor;

    if( $tutor != null){
      if($proyectoEspecifico->tipo->id == 1){ //Pasantias comunitarias
        $formularios = Formulario::formulariosProyectoComunitarioPendienteRevision($proyectoEspecifico->id, Auth::user()->tutor->id)->get();
      }
      elseif($proyectoEspecifico->tipo->id == 2){ //Pasantias Empresariales
        $formularios = Formulario::formulariosProyectoEmpresarialPendienteRevision($proyectoEspecifico->id, Auth::user()->tutor->id)->get();
      }

      return view('profesor.tutor.formulariosrevision', compact('formularios', 'proyectoEspecifico'));
    }
    else{
      dd('Tutor no logueado');
    }
  }
}
