<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Models\Proyecto;
use SoftwareVinculos\Models\Actividad;
use SoftwareVinculos\Models\Formulario;
use SoftwareVinculos\Models\TipoFormulario;
use SoftwareVinculos\Models\EstadoFormulario;
use SoftwareVinculos\Models\EstadoProyecto;
use SoftwareVinculos\Models\ForUVS01;
use SoftwareVinculos\Models\ForUVS04;
use SoftwareVinculos\Models\ForUVS13;
use SoftwareVinculos\Models\ForUVS14;
use SoftwareVinculos\Models\ForUVS15;
use SoftwareVinculos\Models\ForUVS16;
use SoftwareVinculos\Models\EvidenciaResultadoAprendizaje;

use SoftwareVinculos\Http\Controllers\ProyectoController;

class FormularioController extends Controller
{
  public static function crearFormularioUVS04($id_proyecto, $modo) {

      // Se crea el formulario
      $proyecto = Proyecto::find($id_proyecto);
      $tipo = TipoFormulario::find(2); //FOR-UVS-04
      $formulario = FormularioController::crearFormulario('FOR-UVS-04', $proyecto, $tipo);

      // Creado por el usuario actual
      $creadoPor = \Auth::user();
      $formulario->creadoPor()->associate($creadoPor);
      $formulario->save();

      // Se agregan los destinatarios (coordinador)
      FormularioController::agregarDestinatarios($formulario);

      // Se maneja el boton de submit clickeado
      FormularioController::manejarSubmit($formulario, $modo);
  }

  public static function actualizarFormularioUVS04($id_proyecto, $modo) {

      // Se obtiene el formulario
      $proyecto = Proyecto::find($id_proyecto);
      $formulario = Formulario::where('id_proyecto', $id_proyecto)->where('id_creado_por', Auth::user()->id)->where('id_tipo_formulario', 2)->first();

      // Se agregan los destinatarios (coordinador)
      FormularioController::agregarDestinatarios($formulario);

      // Se maneja el boton de submit clickeado
      FormularioController::manejarSubmit($formulario, $modo);
  }

  public static function agregarDestinatarios($formulario) {
      $proyecto = $formulario->proyecto;

      //Busco la carrera principal para obtener el coordinador y agregarlo como destinatario al formulario
      $coordinador = null;
      foreach ($proyecto->carreras as $carrera) {
          if ($carrera->pivot->carrera_principal == true) {
              if ($proyecto->tipo->id == 1) { //Pasantias comunitarias
                $coordinador = $carrera->coordinadorComunitarias;
              } else {
                $coordinador = $carrera->coordinadorEmpresariales;
              }

              if ($coordinador != null && ((Auth::user()->coordinadorCarreraEmpresariales != null && $coordinador->id == Auth::user()->coordinadorCarreraEmpresariales->id) || Auth::user()->coordinadorCarreraEmpresariales == null)) {
                  if ($formulario->destinatarios->count() == 0) {
                    $formulario->destinatarios()->attach($coordinador->usuario);
                  } elseif ($coordinador->usuario->id != $formulario->destinatarios->first()->id) {
                    $formulario->destinatarios()->detach($formulario->destinatarios->first());
                    $formulario->destinatarios()->attach($coordinador->usuario);
                  } elseif ($coordinador == null && $formulario->destinatarios->count() != 0) {
                    $formulario->destinatarios()->detach($formulario->destinatarios->first());
                  }
              }
          }
      }
  }

  public static function manejarSubmit($formulario, $modoSubmit) {
      // Verificar si el formulario fue enviado, aprobado (en el caso del coordinador de empresariales), o solamente guardado
      if ($modoSubmit == 'enviar') {
          // El director envio el proyecto

          $estado = EstadoFormulario::find(2); //Enviado
          $formulario->estado()->associate($estado);
          $formulario->save();
          //$coordinador = $formulario->destinatarios->first();

          $coordinador = null;
          foreach ($formulario->proyecto->carreras as $carrera) {
              if ($carrera->pivot->carrera_principal == true) {
                  if ($formulario->proyecto->tipo->id == 1) { //Pasantias comunitarias
                    $coordinador = $carrera->coordinadorComunitarias;
                  } else {
                    $coordinador = $carrera->coordinadorEmpresariales;
                  }

                  NotificacionController::enviarNotificacionFormularioCreado($formulario, $coordinador);
              }
          }

      } elseif ($modoSubmit == 'aprobar') {
          // El coordinador de pasantias pre-profesionales termino de crear el proyecto

          $estado = EstadoFormulario::find(3); //Aprobado
          $formulario->estado()->associate($estado);
          $formulario->save();
          $estadoProyecto = EstadoProyecto::find(2); //Aprobado
          $proyecto = $formulario->proyecto;
          $proyecto->estado()->associate($estadoProyecto);
          $proyecto->save();
      }
  }

    public static function crearFormularioUVS01($proyecto, $estudiante) {

        $tipo = TipoFormulario::find(1); //FOR-UVS-01

        $formulario = FormularioController::crearFormulario('FOR-UVS-01', $proyecto, $tipo);
        $formulario->creadoPor()->associate($estudiante->usuario);

        $formulario->save();

        $nuevoForUVS01 = new ForUVS01;
        if($proyecto->tipo->id == 2) {
          //Para proyecto empresarial el formulario 01 no debe ser revisado pr el director, el estudiante
          //ingresará su cronograma sn el formulario 01
          $nuevoForUVS01->revisado_por_director = true;
        }
        else {
          $nuevoForUVS01->revisado_por_director = false;
        }

        $nuevoForUVS01->formulario()->associate($formulario);
        $nuevoForUVS01->save();
    }

    public static function crearFormularioUVS05($proyecto, $estudiante) {

        $tipo = TipoFormulario::find(3); // FOR-UVS-05

        $formulario = FormularioController::crearFormulario('FOR-UVS-05', $proyecto, $tipo);
        $formulario->creadoPor()->associate($estudiante->usuario);

        $formulario->save();
    }

    public static function crearFormularioUVS13($proyecto, $creadoPor, $estudiante) {
      $formulario = new Formulario;
      $tipoFormulario = TipoFormulario::find(4); //FOR-UVS-13
      $estado = EstadoFormulario::find(1); //No enviado

      $formulario->nombre = 'FOR-UVS-13';
      $formulario->tipoFormulario()->associate($tipoFormulario);
      $formulario->creadoPor()->associate($creadoPor); //Creado para el tutor(logueado actualmente) que aprobo el formulario UVS01
      $formulario->estado()->associate($estado);
      $formulario->proyecto()->associate($proyecto);
      $formulario->save();

      $nuevoForUVS13 = new ForUVS13;
      $nuevoForUVS13->revisado_por_tutor = false;
      $nuevoForUVS13->formulario()->associate($formulario);
      $nuevoForUVS13->estudiante()->associate($estudiante);
      $nuevoForUVS13->save();
    }

    public static function crearFormularioUVS14($proyecto, $estudiante) {
      $formulario = new Formulario;
      $tipoFormulario = TipoFormulario::find(5); //FOR-UVS-14
      $estado = EstadoFormulario::find(1); //No enviado

      $formulario->nombre = 'FOR-UVS-14';
      $formulario->tipoFormulario()->associate($tipoFormulario);
      $formulario->creadoPor()->associate($estudiante->usuario);
      $formulario->estado()->associate($estado);
      $formulario->proyecto()->associate($proyecto);
      $formulario->save();

      $nuevoForUVS14 = new ForUVS14;
      $nuevoForUVS14->formulario()->associate($formulario);
      $nuevoForUVS14->save();
    }

    public static function crearFormularioUVS15($proyecto, $creadoPor, $estudiante) {
        $tipo = TipoFormulario::find(6); //FOR-UVS-15
        $formulario = FormularioController::crearFormulario('FOR-UVS-15', $proyecto, $tipo);
        $formulario->creadoPor()->associate($creadoPor);
        $formulario->save();

        $nuevoForUVS15 = new ForUVS15;
        $nuevoForUVS15->formulario()->associate($formulario);
        $nuevoForUVS15->estudiante()->associate($estudiante);
        $nuevoForUVS15->save();
    }

    public static function aprobarFormularioUVS15($formulario) {
      $estadoAprobado = EstadoFormulario::find(3); //Aprobado

      $formulario->estado()->associate($estadoAprobado);

      $formulario->save();
    }

    public static function crearFormularioUVS16($proyecto, $creadoPor, $estudiante) {
        $tipo = TipoFormulario::find(7); //FOR-UVS-16
        $formulario = FormularioController::crearFormulario('FOR-UVS-16', $proyecto, $tipo);
        $formulario->creadoPor()->associate($creadoPor);
        $formulario->save();

        //El destinatario del formulario 16 es el coordinador
        foreach ($proyecto->carreras as $carrera) {
            if ($carrera->pivot->carrera_principal == true) {
                if ($proyecto->tipo->id == 1) { //Pasantias comunitarias
                  $coordinador = $carrera->coordinadorComunitarias;
                } else {
                  $coordinador = $carrera->coordinadorEmpresariales;
                }

                if ($coordinador != null) {
                    $formulario->destinatarios()->attach($coordinador->usuario);
                }
            }
        }

        $nuevoForUVS16 = new ForUVS16;
        $nuevoForUVS16->formulario()->associate($formulario);
        $nuevoForUVS16->estudiante()->associate($estudiante);
        $nuevoForUVS16->save();

        // Se crean resultados de aprendizaje vacios
        foreach ($proyecto->resultadosAprendizaje as $resultadoAprendizaje) {
          $evidencia = new EvidenciaResultadoAprendizaje;
          $evidencia->descripcion = '';
          $evidencia->formularioUVS16()->associate($nuevoForUVS16);
          $evidencia->resultadoAprendizaje()->associate($resultadoAprendizaje);

          $evidencia->save();
        }
    }

    private static function crearFormulario($nombre, $proyecto, $tipo) {

        $nuevoFormulario = new Formulario;
        $nuevoFormulario->nombre = $nombre;
        $nuevoFormulario->comentario_rechazo = null;

        $estado = \SoftwareVinculos\Models\EstadoFormulario::find(1); //No enviado
        $nuevoFormulario->estado()->associate($estado);
        $nuevoFormulario->tipoFormulario()->associate($tipo);

        $nuevoFormulario->proyecto()->associate($proyecto);
        $nuevoFormulario->save();

        return $nuevoFormulario;
    }


    ///*** ESTO DEBERIA IR EN UN CONTROLLER DE COORDINADOR ***///
    public function revisarFormularios(Request $request) {
      /*
      if (Auth::user()->coordinadorCarreraComunitarias != null) {
        $formularios = Auth::user()->coordinadorCarreraComunitarias->usuario->formulariosRecibidos;


      } else {
        $formularios = Auth::user()->coordinadorCarreraEmpresariales->usuario->formulariosRecibidos;
      }
      */
      $titulo_proyecto = $request->get('tituloProyecto');
      if($titulo_proyecto != null) {
        $formularios =  Formulario::filtrarFormulariosRecibidosUsuarioPorTituloProyecto(Auth::user()->id, $titulo_proyecto)->get();
      }
      else {
        $formularios =  Formulario::formulariosRecibidosUsuario(Auth::user()->id)->get();
      }

      return view('coordinador.revisarFormularios')->with(['formularios' => $formularios]);
    }

    public function subirFormularios($id_proyecto) {

      $formulariosCreados = Auth::user()->formulariosCreados->where('id_estado_formulario', 3);
      $proyectoEspecifico = Proyecto::find($id_proyecto);

      //dd($formulariosDisponibles);
      //dd($proyectoEspecifico);
      return view('profesor.subirFormularios')->with([
        'formulariosCreados' => $formulariosCreados,
        'proyectoEspecifico' => $proyectoEspecifico
      ]);
    }

    public function subirFormularioFirmado(Request $request) {
      $formulario = Formulario::find($request->id_formulario);

      if ($request->hasFile('formularioUpload')) {

        $slash = config('softwarevinculos.slash');

        $file = $request->file('formularioUpload');
        $extension = $file->getClientOriginalExtension();
        // $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'organizaciones'.$slash.$organizacion->id.$slash.'convenioOrganizacion.'.$extension;
        $url = base_path().$slash.'public'.$slash.'uploads'.$slash.'proyectos'.$slash.$formulario->proyecto->id.$slash.'formularios'.$slash.$formulario->id;
        $filename = $formulario->tipoFormulario->descripcion.'.'.$extension;

        if ($formulario->tipoFormulario->id == 6) { //FOR-UVS-15
          FormularioController::aprobarFormularioUVS15($formulario);
        }

        // $file->move($url);
        $file->move($url,$filename);

        $formulario->url_subido = $url.$slash.$filename;
        $formulario->save();
      }
      return redirect()->route('formulariosDisponiblesTutor', $formulario->proyecto->id);
      //return redirect()->route('subirFormularios', $formulario->proyecto->id);
    }

    public function descargarFormularioFirmado($id_formulario) {
      $formulario = Formulario::find($id_formulario);

      if ($formulario->url_subido != null) {
        if (file_exists($formulario->url_subido) == true) {
          return response()->download($formulario->url_subido);
        } else {
          dd('No se ha subido este formulario');
        }
      } else {
        dd('No se ha subido este formulario');
      }
   }

    public function downloadForUVS01() {
      return \PDF::loadView('formularios.forUVS01')->download('forUVS01.pdf');
    }

    public function downloadForUVS04($id_proyecto) {
      $infoModoVer = ProyectoController::getInfoModoVerProyecto($id_proyecto);
      $infoModoVer['print'] = true;

      return \PDF::loadView('formularios.forUVS04.modoVer', $infoModoVer)->download('forUVS04.pdf');
    }

    public function downloadForUVS15($id_formulario) {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $formulario15 = $formulario->formularioUVS15;
      $estudiante = $formulario15->estudiante;

      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      return \PDF::loadView('formularios.forUVS15.modoVer', [
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'modoVer' => true,
        'actividadesAsignadasEstudiante' => $actividadesAsignadasEstudiante,
        'mostrarBotones' => false
      ])->download('forUVS15.pdf');

      return view('formularios.forUVS15.modoVer')->with([
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'modoVer' => true,
        'actividadesAsignadasEstudiante' => $actividadesAsignadasEstudiante,
        'mostrarBotones' => true
      ]);
    }

    public function verForUVS02() {
      return $this->verFormulario('FOR-UVS-02.pdf');
    }

    public function verForUVS05() {
      return $this->verFormulario('FOR-UVS-05.pdf');
    }

    public function verForUVS07() {
      return $this->verFormulario('FOR-UVS-07.pdf');
    }

    public function verForUVS12() {
      return $this->verFormulario('FOR-UVS-12.pdf');
    }

    public function verForUVS13() {
      return $this->verFormulario('FOR-UVS-13.pdf');
    }

    public function verForUVS17() {
      return $this->verFormulario('FOR-UVS-17.pdf');
    }

    public function verForUVS21() {
      return $this->verFormulario('FOR-UVS-21.pdf');
    }

    public function verForUVS28() {
      return $this->verFormulario('FOR-UVS-28.pdf');
    }

    public function verForUVS29() {
      return $this->verFormulario('FOR-UVS-29.pdf');
    }

    public function verFormulario($filename) {
     $slash = config('softwarevinculos.slash');

     $path = base_path().$slash.'public'.$slash.'formularios_extra'.$slash.$filename;

     return \Response::make(file_get_contents($path), 200, [
         'Content-Type' => 'application/pdf',
         'Content-Disposition' => 'inline; filename="'.$filename.'"'
     ]);
   }
}
