<?php

namespace SoftwareVinculos\Http\Controllers;
use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Utilities\EspolWS;

use SoftwareVinculos\Models\Usuario;
use SoftwareVinculos\Models\Director;
use SoftwareVinculos\Models\Tutor;
use SoftwareVinculos\Utilities\CoordinadorUnidadAcademica;
use SoftwareVinculos\Models\CoordinadorCarreraComunitarias;
use SoftwareVinculos\Models\CoordinadorCarreraEmpresariales;
use SoftwareVinculos\Models\Administrador;
use SoftwareVinculos\Models\Proyecto;
use SoftwareVinculos\Models\EstadoProyecto;
use SoftwareVinculos\Models\Formulario;
use SoftwareVinculos\Models\Carrera;
use Input;
use Auth;

use Log;

use DB;

class UsuarioController extends Controller
{
    public function edit($id_usuario) {
      $usuario = Usuario::find($id_usuario);
      return view('usuario.informacion')->with(['usuario' => $usuario]);
    }

    public function update(Request $request, $id_usuario) {
      $usuario = Usuario::find($id_usuario);
      $usuario->telefono_fijo = $request->telefono_fijo;
      $usuario->celular = $request->celular;
      $usuario->correo_electronico = $request->correo_electronico;

      if (trim($request->telefono_fijo) == '' || trim($request->celular) == '' || trim($request->correo_electronico) == '') {
        $mensaje = 'Se ha actualizado la inforfación, pero aun hay campos vacios.';
        $tipo = 'error';
        $usuario->informacion_actualizada = false;
      } else {
        $mensaje = 'Inforfación actualizada satisfactoriamente.';
        $tipo = 'success';
        $usuario->informacion_actualizada = true;
      }

      $usuario->save();

      session()->flash('flash_message', $mensaje);
      session()->flash('flash_type', $tipo);

      return redirect()->route('usuario.edit', $id_usuario);
    }

    public function mostrarHome(Request $request) {
      if (Auth::user()->coordinadorCarreraComunitarias != null) {
        $titulo_proyecto = $request->get('tituloProyecto');
        if($titulo_proyecto != null) {
          $proyectos = Proyecto::filtrarProyectosAprobadosIniciadosCoordinadorComunitarias($titulo_proyecto)->get();
        }
        else {
          $proyectos = Proyecto::proyectosAprobadosIniciadosCoordinadorComunitarias()->get();
        }
        return view('coordinador.index', compact('proyectos'));
      } else if (Auth::user()->coordinadorCarreraEmpresariales != null) {
        $titulo_proyecto = $request->get('tituloProyecto');
        if($titulo_proyecto != null) {
          $proyectos = Proyecto::filtrarProyectosAprobadosIniciadosCoordinadorEmpresariales($titulo_proyecto)->get();
        }
        else {
          $proyectos = Proyecto::proyectosAprobadosIniciadosCoordinadorEmpresariales()->get();
        }

        return view('coordinador.index', compact('proyectos'));
      } else if(Auth::user()->tutor != null || Auth::user()->director != null) {
        /*
        $value_estado = $request->get('estadoProyecto');
        $estados_options = EstadoProyecto::all()->lists('descripcion', 'id');
        $estados_options->prepend('Seleccione estado', 'Selecione estado');

        if($value_estado != null ){
          $proyectos = Proyecto::filtrarPorEstado($value_estado)->get();

          return view('profesor.index', compact('proyectos', 'estados_options'));
        }
        else{
          $proyectos = Proyecto::proyectosDirectorTutor()->get();

          return view('profesor.index', compact('proyectos', 'estados_options'));
        }
        */
        $titulo_proyecto = $request->get('tituloProyecto');
        if($titulo_proyecto != null) {
          $proyectos = Proyecto::filtrarPorTituloDirectorTutor($titulo_proyecto)->get();
        }
        else {
          $proyectos = Proyecto::proyectosDirectorTutor()->get();
        }

        return view('profesor.index', compact('proyectos'));

      } else if(Auth::user()->estudiante != null){

        return redirect()->route('pasantiasComunitarias');

      } else if(Auth::user()->administrador != null){
        $nombreUsuario = $request->get('nombreUsuario');

        if($nombreUsuario != null ){
          $usuarios = Usuario::usuariosSearchByNombre($nombreUsuario)->get();
        } else {
          $usuarios = Usuario::all();
        }

        $carreras = Carrera::all();

        $arrayCarreras = array();
        foreach ($carreras as $carrera) {
          $arrayCarreras[$carrera->id] = $carrera->nombre;
        }

        return view('administrador.index')->with(['usuarios' => $usuarios, 'carreras' => $arrayCarreras]);
      }
    }

    public function formulariosDisponibles() {
      $formulariosDisponibles = Auth::user()->formulariosCreados;

      return view('profesor.formularios')->with([
        'formulariosDisponibles' => $formulariosDisponibles
      ]);
    }

    public function contieneElRol($usuario, $rol) {
      foreach ($usuario->roles as $rolUsuario) {
        if ($rolUsuario->descripcion == $rol->descripcion) {
          return true;
        }
      }
      return false;
    }

    public function buscarUsuario(Request $request) {
      $webServiceUsed = false;
      $roles = \SoftwareVinculos\Models\Rol::all();
      $arrayRoles = array();
      $usuario = Usuario::where('cedula', $request->cedulaUsuario)->first();

      if ($usuario == null || $usuario->estudiante != null || $usuario->administrador != null) {
        $usuario = EspolWS::datosProfesionalCedula($request->cedulaUsuario);
        if ($usuario != null) {
          $webServiceUsed = true;
          $arrayRoles = array();
          foreach ($roles as $rol) {
            if ($rol->id != 5) { //No es estudiante
                $arrayRoles[$rol->id] = $rol->descripcion;
            }
          }
        }
      } else {
        $arrayRoles = array();
        foreach ($roles as $rol) {
          if ($rol->id != 5 && $this->contieneElRol($usuario, $rol) == false) {//No es estudiante
              $arrayRoles[$rol->id] = $rol->descripcion;
          }
        }
      }

      return response()->json(['usuario' => $usuario, 'webServiceUsed' => $webServiceUsed, 'roles' => $roles]);
    }

    public function buscarUsuarioPorNombre(Request $request) {
      $usuarios = Usuario::where('nombres', 'LIKE', '%'.$request->nombreUsuario.'%')->orWhere('apellidos', 'LIKE', '%'.$request->nombreUsuario.'%')->get();

      return response()->json(['usuarios' => $usuarios]);
    }

    public function getRolesUsuario(Request $request) {
      $usuario = Usuario::find($request->id_usuario);

      return response()->json(['roles' => $usuario->roles]);
    }

    public function getTiposUsuario(Request $request) {
      $webServiceUsed = false;
      $roles = \SoftwareVinculos\Models\Rol::all();
      $arrayRoles = array();
      $usuario = Usuario::find($request->id_usuario);

      foreach ($roles as $rol) {
        if ($rol->id != 5 && $this->contieneElRol($usuario, $rol) == false) { //No es estudiante
            $objRol = new \stdClass();
            $objRol->id = $rol->id;
            $objRol->descripcion = $rol->descripcion;
            array_push($arrayRoles, $objRol);
        }
      }

      return response()->json(['roles' => $arrayRoles]);
    }

    public function guardarUsuario(Request $request) {
      $usuario = Usuario::where('cedula', $request->cedulaUsuario)->first();

      if ($usuario == null) {
        $usuario = new Usuario;
        $usuario->nombres = ucwords(strtolower($request->nombresUsuario));
        $usuario->apellidos = ucwords(strtolower($request->apellidosUsuario));
        $usuario->cedula = $request->cedulaUsuario;
        $usuario->correo_electronico = $request->correoUsuario;
        $substrings = explode('@', $request->correoUsuario);
        $usuario->username = $substrings[0];
        $usuario->informacion_actualizada = false;
        $usuario->save();
      }

      return redirect()->route('home');
    }

    public function quitarRol($id_usuario, $id_rol) {
      $usuario = Usuario::find($id_usuario);

      $usuario->roles()->detach($id_rol);

      $rol = \SoftwareVinculos\Models\Rol::find($id_rol);
      if ($rol->id == 1) { //Director
        $usuarioRol = $usuario->director;
        //Si se borra el rol de director a un coordinador empresarial deja de ser coordinador empresarial tambien
        if ($usuario->coordinadorCarreraEmpresariales != null) {
          $usuario->roles()->detach(4);
          $usuarioRolCoordinador = $usuario->coordinadorCarreraEmpresariales;
          $usuarioRolCoordinador->delete();
        }
      } else if ($rol->id == 2) { //Tutor
        $usuarioRol = $usuario->tutor;
      } else if ($rol->id == 3) { //Coordinador de pasantias comunitarias
        $usuarioRol = $usuario->coordinadorCarreraComunitarias;
      } else if ($rol->id == 4) { //Coordinador de pasantias empresariales
        $usuarioRol = $usuario->coordinadorCarreraEmpresariales;
      } else if ($rol->id == 6) { //Administrador
        $usuarioRol = $usuario->administrador;
      }

      $usuarioRol->delete();

      return redirect()->route('home');
    }

    public function asignarRol(Request $request) {
      $usuario = Usuario::find($request->id_usuario);
      $carrera = Carrera::find($request->id_carrera);

      $usuario->roles()->attach($request->rol);

      $rol = \SoftwareVinculos\Models\Rol::find($request->rol);
      if ($rol->id == 1) { //Director
        $usuarioRol = new Director;
      } else if ($rol->id == 2) { //Tutor
        $usuarioRol = new Tutor;
        $usuarioRol->carrera()->associate($carrera);
      } else if ($rol->id == 3) { //Coordinador de pasantias comunitarias
        $usuarioRol = new CoordinadorCarreraComunitarias;
        $usuarioRol->carrera()->associate($carrera);
      } else if ($rol->id == 4) {//Coordinador de pasantias empresariales
        $usuarioRol = new CoordinadorCarreraEmpresariales;
        $usuarioRol->carrera()->associate($carrera);
        // El coordinador empresarial debe tambien ser director para crear proyectos
        $usuario->roles()->attach(1); //Director
        $usuarioRolDirector = new Director;
        $usuarioRolDirector->usuario()->associate($usuario);
        $usuarioRolDirector->save();
      } else if ($rol->id == 6) { //Administrador
        $usuarioRol = new Administrador;
      }

      $usuarioRol->usuario()->associate($usuario);
      $usuarioRol->save();

      NotificacionController::enviarNotificacionRolAsignado($usuario, $rol, $usuario);

      return redirect()->route('home');
    }

    public function cambiarCarrera(Request $request) {
      $usuario = Usuario::find($request->id_usuario);
      $carrera = Carrera::find($request->id_carrera);

      $rol = \SoftwareVinculos\Models\Rol::find($request->id_rol);

      if ($rol->id == 2) { //Tutor
        $usuarioRol = $usuario->tutor;
      } else if ($rol->id == 3) { //Coordinador de pasantias comunitarias
        $usuarioRol = $usuario->coordinadorCarreraComunitarias;
      } else if ($rol->id == 4) {//Coordinador de pasantias empresariales
        $usuarioRol = $usuario->coordinadorCarreraEmpresariales;
      }

      $usuarioRol->carrera()->associate($carrera);
      $usuarioRol->save();

      return redirect()->route('home');
    }

    public function accesoDenegado() {
      return view('errors.403');
    }
}
