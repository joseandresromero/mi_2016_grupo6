<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use SoftwareVinculos\Models\Carrera;
use SoftwareVinculos\Models\Notificacion;
use SoftwareVinculos\Models\Proyecto;
use Input;

use Log;
use Auth;

class NotificacionController extends Controller
{
  public static function enviarNotificacionRolAsignado($usuario, $rol, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(7); //Rol asignado

    $notificacion = new Notificacion;
    $notificacion->visto = false;
    $notificacion->tipo()->associate($tipoNotificacion);
    $notificacion->destinatario()->associate($destinatario);
    $notificacion->usuarioRol()->associate($usuario);
    $notificacion->rolAsignado()->associate($rol);
    $notificacion->save();
  }

  public static function enviarNotificacionFormularioCreado($formulario, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(1); //Formulario Creado

    $notificacion = new Notificacion;
    $notificacion->visto = false;
    $notificacion->tipo()->associate($tipoNotificacion);
    $notificacion->destinatario()->associate($destinatario);
    $notificacion->formulario()->associate($formulario);
    $notificacion->proyecto()->associate($formulario->proyecto);
    $notificacion->save();
  }

  public static function enviarNotificacionTutorAsignadoAProyecto($tutor, $proyecto, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(4); //Tutor asignado a proyecto

    $notificacion = new Notificacion;
    $notificacion->visto = false;
    $notificacion->tipo()->associate($tipoNotificacion);
    $notificacion->destinatario()->associate($destinatario);
    $notificacion->tutorAsignado()->associate($tutor);
    $notificacion->proyecto()->associate($proyecto);
    $notificacion->save();
  }

  public static function enviarNotificacionEstudianteAsignadoATutor($estudiante, $tutor, $proyecto, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(6); //Estudiante asignado a tutor

    $notificacion = new Notificacion;
    $notificacion->visto = false;
    $notificacion->tipo()->associate($tipoNotificacion);
    $notificacion->destinatario()->associate($destinatario);
    $notificacion->tutorAsignado()->associate($tutor);
    $notificacion->estudianteAsignado()->associate($estudiante);
    $notificacion->proyecto()->associate($proyecto);
    $notificacion->save();
  }

  public static function enviarNotificacionFormularioRechazado($revisadoPor, $formulario, $proyecto, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(3); //Formulario rechazado
    NotificacionController::revisarNotificacion($revisadoPor, $formulario, $proyecto, $destinatario, $tipoNotificacion);
  }

  public static function enviarNotificacionFormularioAprobado($revisadoPor, $formulario, $proyecto, $destinatario) {
    $tipoNotificacion = \SoftwareVinculos\Models\TipoNotificacion::find(2); //Formulario aprobado
    NotificacionController::revisarNotificacion($revisadoPor, $formulario, $proyecto, $destinatario, $tipoNotificacion);
  }

  private static function revisarNotificacion($revisadoPor, $formulario, $proyecto, $destinatario, $tipoNotificacion) {
    $notificacion = new Notificacion;
    $notificacion->visto = false;
    $notificacion->tipo()->associate($tipoNotificacion);
    $notificacion->destinatario()->associate($destinatario);
    $notificacion->revisadoPor()->associate($revisadoPor);
    $notificacion->proyecto()->associate($proyecto);
    $notificacion->formulario()->associate($formulario);
    $notificacion->save();
  }

  public function seguimiento(Request $request)
  {
      // Se obtiene los proyectos dependiendo del tipo de usuario
      if (Auth::user()->coordinadorCarreraComunitarias != null) {
        $proyectos = Proyecto::proyectosAprobadosIniciadosCoordinadorComunitarias()->get();
      } else if (Auth::user()->coordinadorCarreraEmpresariales != null) {
        $proyectos = Proyecto::proyectosAprobadosIniciadosCoordinadorEmpresariales()->get();
      } else if(Auth::user()->tutor != null || Auth::user()->director != null) {
        $proyectos = Proyecto::proyectosDirectorTutor()->get();
      } else if(Auth::user()->estudiante != null){
        $proyectos = array();
        if (Auth::user()->estudiante->proyectoComunitaria != null) {
          array_push($proyectos, Auth::user()->estudiante->proyectoComunitaria);
        }
        if (Auth::user()->estudiante->proyectoEmpresarial != null) {
          array_push($proyectos, Auth::user()->estudiante->proyectoEmpresarial);
        }
      }

      $value_proyecto = $request->get('proyecto');

      if($value_proyecto != null && $value_proyecto != 0){
        $notificaciones = \SoftwareVinculos\Models\Notificacion::filtrarPorProyecto($value_proyecto)->get();
        $proyectoDefault = $value_proyecto;
      }
      else{
        $notificaciones = \SoftwareVinculos\Models\Notificacion::notificacionesUsuario()->get();
        $proyectoDefault = 0;
      }

      $arrayProyectos = array();

      $arrayProyectos[0] = "Seleccione un proyecto";
      foreach ($proyectos as $proyecto) {

        $arrayProyectos[$proyecto->id] = $proyecto->titulo;
      }

      return view('seguimiento.index')->with(['proyectos' => $arrayProyectos, 'proyectoDefault' => $proyectoDefault, 'notificaciones' => $notificaciones]);
  }
}
