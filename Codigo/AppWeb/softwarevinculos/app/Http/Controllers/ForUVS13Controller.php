<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;

use SoftwareVinculos\Http\Requests;
use SoftwareVinculos\Http\Controllers\Controller;

use \SoftwareVinculos\Models\Proyecto;
use \SoftwareVinculos\Models\Formulario;
use \SoftwareVinculos\Models\EstadoFormulario;
use \SoftwareVinculos\Models\Estudiante;
use \SoftwareVinculos\Models\Actividad;

class ForUVS13Controller extends Controller
{
  public function create()
  {
      return view('formularios.forUVS13');
  }

  public function modoEditarPorEstudianteForUVS13($id_formulario, $id_estudiante) {
      $formulario = Formulario::find($id_formulario);
      $estudiante = Estudiante::find($id_estudiante);

      $carreraPrincipal = null;
      foreach ($formulario->proyecto->carreras as $carrera) {
          if ($carrera->pivot->carrera_principal == true) {
              $carreraPrincipal = $carrera;
          }
      }

      $actividadesEstudianteProyecto = Actividad::allActividadesEstudianteProyecto($estudiante, $formulario->proyecto)->get();

      return view('formularios.forUVS13.modoEditarPorEstudiante')->with([
        'forUVS13' => $formulario,
        'estudiante' => $estudiante,
        'actividadesEstudianteProyecto' => $actividadesEstudianteProyecto,
        'carreraPrincipal' => $carreraPrincipal
      ]);
  }

  public function guardarForUVS13PorEstudiante(Request $request, $id_formulario, $id_estudiante) {
    //dd('testme '.$request->revisado_for_uvs_13);
    $formulario = Formulario::find($id_formulario);
    $estudiante = Estudiante::find($id_estudiante);

    $actividadesEstudianteProyecto = Actividad::allActividadesEstudianteProyecto($estudiante, $formulario->proyecto)->get();

    foreach ($actividadesEstudianteProyecto as $actividad) {
      //
      //$actividadCumplida = $request->get('actividadCumplida-'.$actividad->id);
      $cumplida = $this->estaCumplidaLaActividad($request->actividadesCumplidas, $actividad);
      //echo 'val:'.$cumplida.'---';
      if($cumplida == true) {
        //$actividad->pivot->actividad_completada = true;
        $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['actividad_completada' => true]);
      } else {
        //$actividad->pivot->actividad_completada = false;
        $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['actividad_completada' => false]);
      }
      //$actividad->pivot->sugerencia = $request->get('sugerencia-'.$actividad->id);
      $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['sugerencia' => $request->get('sugerencia-'.$actividad->id)]);
      //$actividad->save();
    }

    if ($request->revisado_for_uvs_13 == 'on') {
      //Si Formulario 13 se marca como revisado, entonces se crea el formulario FOR-UVS-14 al estudiante
      FormularioController::crearFormularioUVS14($formulario->proyecto, $estudiante);

      //Cambia el estado del formulario FOR-UVS-13 como Aprobado.
      $estadoFormulario = EstadoFormulario::find(3); //Aprobado
      $formulario->estado()->associate($estadoFormulario);
      $formulario->save();
    }

    $estudiante->save();

    return redirect()->route('formulariosDisponiblesTutor',$formulario->proyecto->id);
  }

  public function estaCumplidaLaActividad($actividadesCumplidas, $actividad) {
    if ($actividadesCumplidas != null) {
      foreach ($actividadesCumplidas as $idActividadCumplida) {
        //echo $idActividadCumplida.'-'.$actividad->id.'---';
        if ($idActividadCumplida == $actividad->id) {
          return true;
        }
      }
    }
    return false;
  }

  public function modoVerPorEstudianteForUVS13($id_formulario, $id_estudiante) {
      $formulario = Formulario::find($id_formulario);
      $estudiante = Estudiante::find($id_estudiante);

      $carreraPrincipal = null;
      foreach ($formulario->proyecto->carreras as $carrera) {
          if ($carrera->pivot->carrera_principal == true) {
              $carreraPrincipal = $carrera;
          }
      }

      $actividadesEstudianteProyecto = Actividad::allActividadesEstudianteProyecto($estudiante, $formulario->proyecto)->get();

      $actividadesCumplidas = array();
      foreach ($actividadesEstudianteProyecto as $actividad) {
        if($actividad->actividad_completada == true) {
          array_push($actividadesCumplidas, $actividad);
        }
      }

      return view('formularios.forUVS13.modoVerPorEstudiante')->with([
        'forUVS13' => $formulario,
        'estudiante' => $estudiante,
        'carreraPrincipal' => $carreraPrincipal,
        'actividadesCumplidas' => $actividadesCumplidas,
        'modoVer' => true
      ]);
  }

  public function modoVerForUVS13Completo($id_proyecto) {
    $proyecto = Proyecto::find($id_proyecto);
    $formularios = Formulario::all()->where('id_proyecto', $proyecto->id)->where('id_tipo_formulario', 4);

    //dd($formulariosUVS13->first()->formularioUVS13->estudiante);

    $carreraPrincipal = null;
    foreach ($proyecto->carreras as $carrera) {
        if ($carrera->pivot->carrera_principal == true) {
            $carreraPrincipal = $carrera;
        }
    }

    return view('formularios.forUVS13.modoVerCompleto')->with([
      'formularios' => $formularios,
      'proyecto' => $proyecto,
      'carreraPrincipal' => $carreraPrincipal,
      'modoVer' => true
    ]);
  }

  public function edit($id_forUVS13)
  {

  }

  public function store(Request $request)
  {

  }

  public function update(Request $request, $id_forUVS13)
  {

  }
}
