<?php

namespace SoftwareVinculos\Http\Controllers;

use Illuminate\Http\Request;
use SoftwareVinculos\Http\Requests;

use \SoftwareVinculos\Models\Proyecto;
use \SoftwareVinculos\Models\Formulario;
use \SoftwareVinculos\Models\EstadoFormulario;
use \SoftwareVinculos\Models\EstadoEstudiante;
use \SoftwareVinculos\Models\ForUVS16;
use \SoftwareVinculos\Models\InstrumentoEvaluacionPractica;
use \SoftwareVinculos\Models\EvidenciaResultadoAprendizaje;
use \SoftwareVinculos\Models\ResultadoAprendizaje;
use \SoftwareVinculos\Models\Actividad;

use Auth;

class ForUVS16Controller extends Controller
{
    public function create()
    {
        return view('formularios.forUVS16');
    }

    public function edit($id_forUVS16)
    {

    }

    public function store(Request $request)
    {

    }

    public function update(Request $request, $id_formulario)
    {
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;
      //$formulariouvs16 = $formulario->formulariosUVS16;

      $formulariouvs16 = ForUVS16::all()->where('id_formulario', $formulario->id)->first();

      if($request->get('aprobacion') == 'si'){
        $formulariouvs16->aprobacion = 1;
      }
      elseif($request->get('aprobacion') == 'no'){
        $formulariouvs16->aprobacion = 0;
      }

      $formulariouvs16->numero_hora_practicas_validadas = $request->get('numero_horas_practica');
      $formulariouvs16->save();

      $this->actualizarActividadesPlanificadasRevision($request, $formulariouvs16, $proyecto);
      $this->actualizarInstrumentosEvaluacion($request, $formulariouvs16);
      $this->guardarEvidenciasResultadosAprendizaje($request, $formulariouvs16);

      //Verificar si se esta guardando o enviando, y si se esta enviando se debe cambiar el estado del formulario a enviado
      if($request->submit == 'guardar') {
        return redirect()->route('formulariosDisponiblesTutor', [$proyecto->id]);
      } else if ($request->submit == 'enviar') {
        //dd('enviado');
        return redirect()->route('enviarForUVS16', [$proyecto->id, $formulariouvs16 ->id]);
      } else {
        return redirect()->route('formulariosDisponiblesTutor', [$proyecto->id]);
      }
    }

    public function guardarEvidenciasResultadosAprendizaje($request, $formularioUVS16){

      if ($request->get('resultadosAprendizaje') != null) {
        $evidenciasResultadosAprendizaje = $request->get('evidenciasResultadosAprendizaje');
        foreach($request->get('resultadosAprendizaje') as $index => $id_resultado_aprendizaje) {
            if ($id_resultado_aprendizaje != null) {
                $resultadoAprendizaje = ResultadoAprendizaje::find($id_resultado_aprendizaje);
                $evidencia = $resultadoAprendizaje->evidenciasResultadosAprendizaje->where('id_for_uvs_16', $formularioUVS16->id)->first();
                $evidencia->descripcion = $evidenciasResultadosAprendizaje[$index];

                $evidencia->save();
            }
        }
      }
    }
    
    public function enviarFormularioUVS16($id_proyecto, $id_forUVS16) {
        $proyecto = Proyecto::find($id_proyecto);

        $formularioUVS16 = ForUVS16::find($id_forUVS16);
        $formulario = $formularioUVS16->formulario;

        //dd($formularioUVS16->formulario);

        //Cambia el estado del formulario a 'Enviado'
        $estado = EstadoFormulario::find(2); //Enviado

        $formulario->estado()->associate($estado);
        $formulario->comentario_rechazo = null;

        $formulario->save();

        //return redirect()->route('home');
        return redirect()->route('formulariosDisponiblesTutor', [$proyecto->id]);
    }

    public function estaSeleccionadoInstrumentoEvaluacion($instrumentoEvaluacion, $instrumentoEvaluacionSeleccionados) {
        foreach ($instrumentoEvaluacionSeleccionados as $instrumentoEvaluacionSeleccionado) {
            if ($instrumentoEvaluacionSeleccionado->id == $instrumentoEvaluacion->id) {
                return true;
            }
        }
        return false;
    }

    public function actualizarInstrumentosEvaluacion($request, $formulariouvs16){
        //Guardamos los nuevos instrumentos de evaluación seleccionados.
        if ($request->instrumentosEvaluacion != null) {
            foreach ($request->instrumentosEvaluacion as $idInstrumentoEvaluacion) {
                if ($formulariouvs16->intrumentosEvaluacion()->find($idInstrumentoEvaluacion) == null) {
                    //Si el insturmento de evaluación no esta guardada, lo guardamos.
                    $formulariouvs16->intrumentosEvaluacion()->attach($idInstrumentoEvaluacion);
                }
            }
        }

        //Eliminamos los instrumentos de evaluación des-seleccionadas
        if ($formulariouvs16->intrumentosEvaluacion != null) {
          //Si una de los instrumentos de evaluación que estaban guardadas no fue recibida en el request, entonces lo borrramos.
          foreach ($formulariouvs16->intrumentosEvaluacion as $intrumentoEvaluacion) {

              if (collect($request->instrumentosEvaluacion)->contains($intrumentoEvaluacion->id) == false) {
                  $formulariouvs16->intrumentosEvaluacion()->detach($intrumentoEvaluacion);
              }
          }
        }
    }

    public function modoEditarFormularioUVS16(Request $request, $id_formulario){
      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $forUVS16 = $formulario->formularioUVS16;

      $estudiante = $forUVS16->estudiante;
      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      $instrumentosEvaluacion = InstrumentoEvaluacionPractica::all();
      $instrumentoEvaluacionConSeleccion = array();

      foreach ($instrumentosEvaluacion as $instrumentoEvaluacion) {
          $seleccionada = $this->estaSeleccionadoInstrumentoEvaluacion($instrumentoEvaluacion, $forUVS16->intrumentosEvaluacion);

          $objInstrumentoEvaluacion = new \stdClass();
          $objInstrumentoEvaluacion->instrumentoEvaluacion = $instrumentoEvaluacion;
          $objInstrumentoEvaluacion->seleccionada = $seleccionada;

          array_push($instrumentoEvaluacionConSeleccion, $objInstrumentoEvaluacion);
      }

      //$evaluacion_resultados_aprendizaje = \SoftwareVinculos\Models\EvaluacionResultadoAprendizaje::all()->where('id_for_uvs_16', $forUVS16->id);

      return view('formularios.forUVS16.modoEditar', compact('formulario', 'proyecto', 'instrumentoEvaluacionConSeleccion', 'actividadesAsignadasEstudiante'));
    }

    public function modoVerFormularioUVS16(Request $request, $id_formulario) {
      //dd('modo ver for uvs 16');

      $formulario = Formulario::find($id_formulario);
      $proyecto = $formulario->proyecto;

      $forUVS16 = $formulario->formularioUVS16;

      $estudiante = $forUVS16->estudiante;
      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      $instrumentosEvaluacion = InstrumentoEvaluacionPractica::all();
      $instrumentoEvaluacionConSeleccion = array();

      foreach ($instrumentosEvaluacion as $instrumentoEvaluacion) {
          $seleccionada = $this->estaSeleccionadoInstrumentoEvaluacion($instrumentoEvaluacion, $forUVS16->intrumentosEvaluacion);

          $objInstrumentoEvaluacion = new \stdClass();
          $objInstrumentoEvaluacion->instrumentoEvaluacion = $instrumentoEvaluacion;
          $objInstrumentoEvaluacion->seleccionada = $seleccionada;

          array_push($instrumentoEvaluacionConSeleccion, $objInstrumentoEvaluacion);
      }

      $mostrarBotones = false;
      if($formulario->estado->id == 2  && (Auth::user()->coordinadorCarreraEmpresariales != null || Auth::user()->coordinadorCarreraComunitarias != null)){ //Enviado
        $mostrarBotones = true;
      }

      return view('formularios.forUVS16.modoVer')->with([
        'proyecto' => $proyecto,
        'formulario' => $formulario,
        'actividadesAsignadasEstudiante' => $actividadesAsignadasEstudiante,
        'instrumentoEvaluacionConSeleccion' => $instrumentoEvaluacionConSeleccion,
        'modoVer' => true,
        'mostrarBotones' => $mostrarBotones
      ]);
    }

    public function revisarForUVS16(Request $request, $id_formulario){
      $formulario = Formulario::find($id_formulario);

      $proyecto = $formulario->proyecto;

      if($request->submit == 'aprobar_foruvs16') {
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(3); //Aprobado
        $estudiante = $formulario->formularioUVS16->estudiante;

        /* Se cambia el estado del estudiante en el proyecto con el status de "Actividades Terminadas. "*/
        if ($proyecto->tipo->id == 1){ // Proyecto comunitario
          $estadoEstProyectoComunitario = EstadoEstudiante::find(5); //Actividades Finalizadas
          $estudiante->estadoEstudianteProyectoComunitario()->associate($estadoEstProyectoComunitario);
        }
        else{
          $estadoEstProyectoEmpresarial =  EstadoEstudiante::find(5); //Actividades Finalizadas
          $estudiante->estadoEstudianteProyectoEmpresarial()->associate($estadoEstProyectoEmpresarial);
        }
        $estudiante->save();

      } else {
        $estadoFormulario = \SoftwareVinculos\Models\EstadoFormulario::find(4); //Rechazado
        $formulario->comentario_rechazo = $request->comentario_rechazo;
      }

      $formulario->estado()->associate($estadoFormulario);
      $formulario->save();

      return redirect()->route('revisar_formularios');
    }

    public function actualizarActividadesPlanificadasRevision($request, $formulariouvs16, $proyecto){
      $estudiante = $formulariouvs16->estudiante;
      $actividadesAsignadasEstudiante = Actividad::actividadesEstudiante($estudiante, $proyecto)->get();

      foreach($actividadesAsignadasEstudiante  as $index=>$actividad){
        $comentarioRevisionTutor = $request->get('comentarioRevisionTutor-'.$actividad->id);
        $cumplioActividad = $request->get('actividaddes-item-'.$actividad->id);

        if( $cumplioActividad == 'si' ){
          $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['cumplimiento_actividad_revision_por_tutor' => true]);
        }
        else{
          $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['cumplimiento_actividad_revision_por_tutor' => false]);
        }

        $estudiante->actividadesProyecto()->updateExistingPivot($actividad->id, ['comentario_revision_tutor' => $comentarioRevisionTutor]);
      }
    }
}
