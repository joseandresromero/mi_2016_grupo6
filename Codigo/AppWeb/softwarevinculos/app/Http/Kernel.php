<?php

namespace SoftwareVinculos\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \SoftwareVinculos\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \SoftwareVinculos\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \SoftwareVinculos\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \SoftwareVinculos\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'director' => \SoftwareVinculos\Http\Middleware\RedirectIfNotDirector::class,
        'administrador' => \SoftwareVinculos\Http\Middleware\RedirectIfNotAdministrador::class,
        'coordinador' => \SoftwareVinculos\Http\Middleware\RedirectIfNotCoordinador::class,
        'tutor' => \SoftwareVinculos\Http\Middleware\RedirectIfNotTutor::class,
        'informacion_actualizada' => \SoftwareVinculos\Http\Middleware\RedirectIfNotInformacionActualizada::class,
    ];
}
