<?php

namespace SoftwareVinculos\Providers;

use Auth;
use SoftwareVinculos\Auth\CustomUserProvider;
use Illuminate\Support\ServiceProvider;

/*
 * Provider personalizado de autenticacion
 */
class CustomAuthProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //REGISTRO EL DRIVER DE MI PROVIDER RETORNANDO MI USER PROVIDER PERSONALIZADO
        Auth::provider('usuarios', function($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            return new CustomUserProvider();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
