@extends('layouts.master')

@section('title','Lista de Proyectos')

@section('content')
  <div class="table-responsive">
     <table class="table table-bordered">
       <thead>
          <th width="40%">Formulario</th>
          <th width="40%">Estudiante</th>
          <th>Estado</th>
       </thead>
       <tbody>
          @if(isset($formularios))
            @foreach ($formularios as $formulario)
             <tr>
               <td>
                 <a href="{{ route('modoVerFormularioUVS01', $formulario->id) }}">{{$formulario->tipoFormulario->descripcion}}</a>
               </td>
               <td>{{$formulario->creadoPor->nombres}} {{$formulario->creadoPor->apellidos}}</td>
               @if($formulario->estado->id == 2) <!-- Enviado -->
                 <td> Pendiente de Revisión</td>
               @else
                  <td></td>
               @endif

             </tr>
           @endforeach
          @endif
       </tbody>
     </table>
  </div>
@endsection
