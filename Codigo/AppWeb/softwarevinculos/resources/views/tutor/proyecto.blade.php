@extends('layouts.master')

@section('title','Proyecto seleccionado')

@section('content')

  <ol class="breadcrumb">
   <li class="active">Proyectos</li>
  </ol>


  @if(isset($proyecto))
    @if(Auth::user()->tutor!= null)
      <a href="{{ route('modoVerProyecto', $proyecto->id) }}">FOR-UVS-04   {{$proyecto->titulo}}</a>
      <br><br>
    @endif

    <a href="{{ route('formulariospendientesrevision', $proyecto->id) }}">Formularios pendiente de revisión</a>
    <br><br>

    @if(Auth::user()->tutor!= null)
      <a href="#">Estudiantes asignados</a>
    @endif
  @endif
@endsection
