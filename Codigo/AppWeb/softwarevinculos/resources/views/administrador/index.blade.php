@extends('layouts.master')

@section('title','Administrador')

@section('content')
  <div class="row" style="margin-bottom:50px;">
    <div class="col-md-8" style="width:100%">
       <div class="panel panel-default">
         <div class="panel-heading">
           <p class="panel-title pull-left">
             Lista de Usuarios
           </p>
            <button class="btn btn-default pull-right" data-toggle="modal" data-backdrop="static" data-target="#agregarNuevoUsuario">Nuevo Usuario</button>
            <div class="clearfix"></div>

            <!-- Modal -->
            <div class="modal fade" id="agregarNuevoUsuario" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Agregar nuevo usuario</b></h4>
                  </div>

                  <div class="form-group" style="margin: 15px 20px;">
                    <br>
                     <div class="input-group">
                       {!!Form::text('cedula', null,['id'=>'cedula','class'=>'form-control', 'placeholder' => 'Ingrese número de cedula del usuario', 'style'=>'width: 100%;'])!!}
                       <span class="input-group-btn">
                         <button id="buscarUsuario" type="button" class="btn btn-default">Buscar</button>
                       </span>
                     </div>
                 </div>

                 {!!Form::open(['route'=>'guardar_usuario','method'=>'POST', 'class'=>'formulario'])!!}

                  <div class="modal-body">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                      <br>
                      <div id="infoUsuario" style="margin-left:25px; display:none;">
                        <div class="form-group">
                          {!!form::label('Nombres:')!!}
                          {!!Form::text('nombresUsuario', null,['id'=>'nombresUsuario','class'=>'form-control field-as-label', 'style'=>'width: 100%;', 'readonly' => 'readonly'])!!}
                          {!!form::label('Apellidos:')!!}
                          {!!Form::text('apellidosUsuario', null,['id'=>'apellidosUsuario','class'=>'form-control field-as-label', 'style'=>'width: 100%;', 'readonly' => 'readonly'])!!}
                          {!!form::label('Cedula:')!!}
                          {!!Form::text('cedulaUsuario', null,['id'=>'cedulaUsuario','class'=>'form-control field-as-label', 'style'=>'width: 100%;', 'readonly' => 'readonly'])!!}
                          {!!form::label('Correo electrónico:')!!}
                          {!!Form::text('correoUsuario', null,['id'=>'correoUsuario','class'=>'form-control field-as-label', 'style'=>'width: 100%;', 'readonly' => 'readonly'])!!}
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button id="guardarNuevoUsuario" type="submit" class="btn btn-primary">Agregar</button>
                  </div>
                 {!!Form::close()!!}
                </div>

              </div>
            </div>
          </div>

         <div class="panel-body">

          <div class="form-group" style="margin-top:20px; margin-bottom:20px;">
            {!!Form::label('nombreUsuario', 'Buscar usuario:', ['style' => 'margin-left:20px;'])!!}
            {{--{!!Form::text('nombreUsuario', null,['id'=>'nombreUsuario', 'class'=>'form-control', 'placeholder' => 'Ingrese el nombre del usuario', 'style'=>'margin-left:15px; width: 30%;'])!!}--}}
            <br>
            {!!Form::model(Request::only(['nombreUsuario']) ,['route'=>'home','method'=>'GET'])!!}
            <div class="col-sm-4">
             <div class="input-group">
                 {!!Form::text('nombreUsuario', null,['id'=>'nombreUsuario', 'class'=>'form-control', 'placeholder' => 'Ingrese el nombre del usuario', 'style'=>'width: 100%;'])!!}
                 <span class="input-group-btn">
                   <button id="buscarUsuarioGuardado" type="submit" class="btn btn-default">Buscar</button>
                 </span>
             </div>
           </div>
           {!!Form::close()!!}

         </div>

          <br>
            <table id="tablaUsuarios" class="table table-bordered">
              <thead>
                 <th width="30%">Nombre</th>
                 <th width="15%">Cedula</th>
                 <th>Correo</th>
                 <th width="30%">Roles</th>
              </thead>
              <tbody id="tablaUsuariosBody">
                @foreach($usuarios as $usuario)
                  @if($usuario->estudiante == null)
                    <tr class="usuario-row">
                      <td>{{$usuario->nombres}} {{$usuario->apellidos}}</td>
                      <td>{{$usuario->cedula}}</td>
                      <td>{{$usuario->correo_electronico}}</td>
                      <td>
                        @foreach($usuario->roles as $rol)
                          <div class="sv-tag-style">{{$rol->descripcion}}
                            @if($rol->id == 2 || $rol->id == 3 || $rol->id == 4)
                              <button type="button" class="sv-icon-button" data-toggle="modal" data-backdrop="static" data-target="#cambiarCarrera_{{$usuario->id}}_{{$rol->id}}">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                              </button>

                              <!-- Modal -->
                              <div class="modal fade" id="cambiarCarrera_{{$usuario->id}}_{{$rol->id}}" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title" style="color:#000000;"><b>Cambiar carrera</b></h4>
                                    </div>
                                    {!!Form::open(['route'=>'cambiar_carrera','method'=>'POST', 'class'=>'formulario'])!!}
                                    <div class="modal-body">
                                        <input type="hidden" name="id_usuario" value="{{$usuario->id}}">
                                        <input type="hidden" name="id_rol" value="{{$rol->id}}">
                                        <div>
                                          {!!form::label('nombre_carrera','Carrera actual:',['style'=>'color:#000000;'])!!}
                                          <p>
                                            @if($rol->id == 2)
                                              {{ $usuario->tutor->carrera->nombre }}
                                            @elseif($rol->id == 3)
                                              {{ $usuario->coordinadorCarreraComunitarias->carrera->nombre }}
                                            @elseif($rol->id == 4)
                                              {{ $usuario->coordinadorCarreraEmpresariales->carrera->nombre }}
                                            @endif
                                          </p>
                                          {!!form::label('id_carrera','Nueva carrera:',['style'=>'color:#000000;'])!!}
                                          @if($rol->id == 2)
                                            {!!form::select('id_carrera', $carreras, $usuario->tutor->carrera->id, ['id' => 'carrera', 'class' => 'form-control'])!!}
                                          @elseif($rol->id == 3)
                                            {!!form::select('id_carrera', $carreras, $usuario->coordinadorCarreraComunitarias->carrera->id, ['id' => 'carrera', 'class' => 'form-control'])!!}
                                          @elseif($rol->id == 4)
                                            {!!form::select('id_carrera', $carreras, $usuario->coordinadorCarreraEmpresariales->carrera->id, ['id' => 'carrera', 'class' => 'form-control'])!!}
                                          @endif

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                      <button type="submit" class="btn btn-primary">Guardar</button>
                                    </div>
                                    {!!Form::close()!!}
                                  </div>

                                </div>
                              </div>
                            @endif
                            <button type="button" name="remove-rol-{{$rol->id}}" class="sv-icon-button" onclick="confirmQuitarRol({{$usuario->id}},{{$rol->id}})">
                              <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                          </div>
                        @endforeach
                        <button type="button" id="addroluser-{{$usuario->id}}" class="sv-icon-button add-rol"><i class="fa fa-plus-circle fa-lg" style="color:rgb(52, 168, 48);" data-toggle="modal" data-backdrop="static" data-target="#asignarRol"></i></button>

                        <!-- Modal -->
                        <div class="modal fade" id="asignarRol" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><b>Asignar rol a usuario</b></h4>
                              </div>
                              {!!Form::open(['route'=>'asignar_rol','method'=>'POST', 'class'=>'formulario'])!!}
                              <div class="modal-body">
                                  <input type="hidden" name="id_usuario" id="id_usuario" value="">
                                  {!!form::label('Roles:')!!}
                                  <select class="form-control" name="rol" id="roles">
                                  </select>
                                  <br>
                                  <div id="carreras" style="display:none;">
                                    {!!form::label('Carrera:')!!}
                                    {!!form::select('id_carrera', $carreras, 1, ['id' => 'carrera', 'class' => 'form-control'])!!}
                                  </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                              {!!Form::close()!!}
                            </div>

                          </div>
                        </div>
                      </td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
         </div>
       </div>

    </div>
  </div>

  <script type="text/javascript">

    function confirmQuitarRol(id_usuario, id_rol) {
      var advertencia = "";
      if (id_rol == 1) {
        advertencia = "(Se quitara también el rol de Coordinador, ya que este requiere el rol de director)"
      }
      bootbox.confirm("¿Esta seguro que desea quitar este rol este usuario? " + advertencia, function(result) {
        if(result == true){
          window.location = "quitar_rol/" + id_usuario + "/" + id_rol;
        }
      });
    }

    //Se agrega el rol
    $(document).on('click', '.add-rol', function(){
         var button_id = $(this).attr("id");
         var indexSeparator = button_id.indexOf("-");
         var id_usuario = button_id.substring(indexSeparator+1);
         console.log('id usuario', id_usuario);
         $('#id_usuario').val(id_usuario);

         var token = $('input[name=_token]').val(),
             dataString = 'id_usuario=' + id_usuario + '&_token=' + token;

         $.ajax({
             url: '{!!URL::route('get_tipos_usuario')!!}',
             type: 'POST',
             data: dataString,
             success: function (data) {
                 console.log(data);
                   $('#roles').empty();
                   data.roles.forEach(function(rol, index) {
                     console.log('item', rol);
                     console.log('index', index);
                     $('#roles').append('<option value="' + rol.id + '">' + rol.descripcion + '</option>');

                     if (index == 0) {
                        if (rol.id == 2 || rol.id == 3 || rol.id == 4) {
                          $('#carreras').show();
                        } else {
                          $('#carreras').hide();
                        }
                     }
                   });
                   $('#asignarRol').modal('show');
             }
         });
    });

    $('#buscarUsuario').click(function() {
      $('#infoUsuario').hide();

      var token = $('input[name=_token]').val(),
          cedulaUsuario = $('#cedula').val(),

          dataString = 'cedulaUsuario=' + cedulaUsuario + '&_token=' + token;

      $.ajax({
          url: '{!!URL::route('buscar_usuario')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
              console.log(data);
              if (data.usuario != null) {
                if (data.webServiceUsed == true) {
                  $('#nombresUsuario').val(data.usuario.NOMBRES);
                  $('#apellidosUsuario').val(data.usuario.APELLIDOS);
                  $('#cedulaUsuario').val(data.usuario.CEDULA);
                  $('#correoUsuario').val(data.usuario.EMAIL);
                  $('#infoUsuario').show();
                } else {
                  /*
                  $('#nombresUsuario').val(data.usuario.nombres);
                  $('#apellidosUsuario').val(data.usuario.apellidos);
                  $('#cedulaUsuario').val(data.usuario.cedula);
                  $('#correoUsuario').val(data.usuario.correo_electronico);
                  */
                  bootbox.alert('El usuario ya existe');
                }
              } else {
                bootbox.alert('Ingrese correctamente el número de cedula del docente');
              }
          }
      });
    });

    function esEstudiante(roles, fn) {
      var estudiante = false;
      roles.forEach(function(rol, index) {
        console.log('roool: ', rol.descripcion);
        if (rol.descripcion == 'ALUMNO') {
          estudiante = true;
        }
      });

      fn(estudiante);
    }

    $('#roles').change(function(event) {
      var rol =  $('option:selected', this).val();

      //Tutor, Coordinador de Pasantías Comunitarias, o Coordinado de Pasantías Pre-profesionales
      if (rol == 2 || rol == 3 || rol == 4) {
        $('#carreras').show();
      } else {
        $('#carreras').hide();
      }
    });
  </script>
@endsection
