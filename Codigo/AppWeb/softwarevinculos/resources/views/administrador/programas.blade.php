@extends('layouts.master')

@section('title','Administrador')

@section('content')

  <!-- Date range picker -->
  {!!Html::script('js/moment.min.js')!!}
  {!!Html::script('js/daterangepicker.js')!!}
  {!!Html::style('css/daterangepicker.css')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}

  <div class="row">
    <div class="col-md-8" style="width:100%">
      <div class="panel panel-default">
        <div class="panel-heading">
          Lista de Programas
        </div>

        <div class="panel-body">
          <button id="btnAgregarPrograma" type="button" class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#agregarPrograma" style="margin-top:0px; margin-bottom:20px;">Agregar programa</button>

          <!-- Modal -->
          <div class="modal fade" id="agregarPrograma" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><b>Agregar nuevo programa</b></h4>
                </div>
                {!!Form::open(['route'=>'guardar_programa','method'=>'POST', 'class'=>'formulario'])!!}
                <div class="modal-body">
                    {!!form::label('Nombre:')!!}
                    {!!form::text('nombre',null,['id'=>'nombre','class'=>'form-control'])!!}

                    {!!form::label('Fechas de inicio y finalización:')!!}
                    {!!form::text('fecha_inicio_fin',null,['id'=>'fecha_inicio_fin','class'=>'form-control'])!!}

                    {!!form::label('Objetivo general:')!!}
                    {!!form::text('objetivo_general',null,['id'=>'objetivo_general','class'=>'form-control'])!!}

                    {!!form::label('Objetivos específicos:')!!}
                    {!!form::textarea('objetivos_especificos',null,['id'=>'objetivos_especificos','class'=>'form-control','rows'=>3])!!}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button id="guardarPrograma" type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!!Form::close()!!}
              </div>

            </div>
          </div>

          @if(Session::has('flash_type'))
            @include('flash.' . Session::get('flash_type'))
          @endif

          <table class="table table-bordered" style="margin-top:20px;">
            <thead>
               <th width="20%">Nombre</th>
               <th width="20%">Objetivo general</th>
               <th width="25%">Objetivos especificos</th>
               <th width="10%">Fecha de inicio</th>
               <th width="10%">Fecha de finalización</th>
               <th>Acción</th>
            </thead>
            <tbody>
              @foreach($programas as $programa)
                <tr>
                  <td>{{$programa->nombre}}</td>
                  <td style="text-align:left;">{{$programa->objetivo_general}}</td>
                  <td style="text-align:left;">{!!$programa->objetivos_especificos!!}</td>
                  <td>{{$programa->fecha_inicio}}</td>
                  <td>{{$programa->fecha_finalizacion}}</td>
                  <td class="sv-cell-contain-width">
                    <button type="button" name="edit-programa-{{$programa->id}}" class="sv-icon-button" onclick="window.location='{{ route('programa.edit', $programa->id) }}';">
                      <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                    </button>
                    <button type="button" name="remove-programa-{{$programa->id}}" class="sv-icon-button" onclick="confirmEliminarPrograma({{$programa->id}})">
                      <i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>
                    </button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  function confirmEliminarPrograma(id_programa) {
    bootbox.confirm("¿Esta seguro que desea eliminar este programa?", function(result) {
      if(result == true){
        window.location = "eliminar_programa/" + id_programa;
      }
    });
  }

    $('input[name="fecha_inicio_fin"]').daterangepicker({
      locale: {
        format: 'DD/MM/YYYY'
      }
    });

    $('#objetivos_especificos').ckeditor();
  </script>
@endsection
