@extends('layouts.master')

@section('title','Información de usuario')

@section('content')
  {!!Html::style('css/formularios.css')!!}
  <!-- Date range picker -->
  {!!Html::script('js/moment.min.js')!!}
  {!!Html::script('js/daterangepicker.js')!!}
  {!!Html::style('css/daterangepicker.css')!!}

  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}

  {!!Form::model($programa,['route'=>['programa.update',$programa->id],'method'=>'PUT', 'class'=>'formulario form-horizontal form-proyecto'])!!}

    @include('flash.success')

    <div class="form-group">
      {!!form::label('nombre', 'Nombre:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('nombre',null,['id'=>'nombre','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('objetivo_general', 'Objetivo general:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('objetivo_general',null,['id'=>'objetivo_general','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('objetivos_especificos', 'Objetivos específicos:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::textarea('objetivos_especificos',null,['id'=>'objetivos_especificos','class'=>'form-control','rows'=>3])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('fecha_inicio_fin', 'Fechas de inicio y finalización:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        @if($fechaInicio != null && $fechaFinalizacion != null)
          {!!form::text('fecha_inicio_fin',$fechaInicio.' - '.$fechaFinalizacion,['id'=>'fecha_inicio_fin','class'=>'form-control'])!!}
        @else
          {!!form::text('fecha_inicio_fin',null,['id'=>'fecha_inicio_fin','class'=>'form-control'])!!}
        @endif
      </div>
    </div>

    <div class="form-group" style="margin-top:50px;">
      <div class="col-sm-offset-2 col-sm-8">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>

  {!!Form::close()!!}

  <script type="text/javascript">
    $('#objetivos_especificos').ckeditor();

    $('input[name="fecha_inicio_fin"]').daterangepicker({
      locale: {
        format: 'DD/MM/YYYY'
      }
    });
  </script>

@endsection
