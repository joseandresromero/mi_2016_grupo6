@extends('layouts.master')

@section('title','Subir Formularios Firmados')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  <ol class="breadcrumb">
   <li class="active">Subir Formularios Firmados</li>
  </ol>

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista
           </div>
          <div class="panel-body">

             <table class="table table-bordered">
               <thead>
                  <th width="70%">Formulario</th>
                  <th>Descargar</th>
                  <th>Subir</th>
               </thead>
               <tbody>
                 @if(isset($formulariosCreados))
                   @foreach ($formulariosCreados as $formulario)
                     @if($formulario->proyecto->id == $proyectoEspecifico->id)
                       <tr>
                         <td>
                           {{$formulario->tipoFormulario->descripcion}}
                           @if($formulario->tipoFormulario->id == 1 || $formulario->tipoFormulario->id == 5) <!-- FOR-UVS-01 o FOR-UVS-14 -->
                             - {{ $formulario->creadoPor->nombres }} {{ $formulario->creadoPor->apellidos }}
                           @elseif($formulario->tipoFormulario->id == 6) <!-- FOR-UVS-15 -->
                             - {{ $formulario->formularioUVS15->estudiante->usuario->nombres }} {{ $formulario->formularioUVS15->estudiante->usuario->apellidos }}
                           @elseif($formulario->tipoFormulario->id == 7) <!-- FOR-UVS-16 -->
                             - {{ $formulario->formularioUVS16->estudiante->usuario->nombres }} {{ $formulario->formularioUVS16->estudiante->usuario->apellidos }}
                           @endif
                         </td>
                         <td class="sv-cell-contain-width">
                           <button type="button" class="sv-icon-button" style="margin: 0px auto; width: 65px;" onclick="window.location='{{ route('descargarFormularioFirmado', $formulario->id) }}';">
                             <i class="fa fa-download fa-lg" aria-hidden="true" style="color:rgb(0, 0, 119);"></i>
                           </button>
                         </td>
                         <td class="sv-cell-contain-width">
                           <button type="button" data-toggle="modal" data-backdrop="static" data-target="#subirFormulario" class="sv-icon-button" style="margin: 0px auto; width: 50px;">
                             <i class="fa fa-upload fa-lg" aria-hidden="true" style="color:rgb(0, 0, 119);"></i>
                           </button>

                           <!-- Modal -->
                           <div class="modal fade" id="subirFormulario" role="dialog">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title"><b>Subir Formulario</b></h4>
                                 </div>
                                 {!!Form::open(['route'=>'subirFormularioFirmado', 'method'=>'POST', 'files'=>true])!!}
                                   <div class="modal-body">
                                       <input type="hidden" name="id_formulario" value="{{ $formulario->id }}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <input type="file" name="formularioUpload" id="formularioUpload" style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
                                   </div>
                                   <div class="modal-footer">
                                     <button type="submit" class="btn btn-primary">Guardar</button>
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                   </div>
                                 {!!Form::close()!!}
                               </div>
                             </div>
                           </div>
                         </td>
                       </tr>
                     @endif
                   @endforeach
                 @endif
               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>
@endsection
