@extends('layouts.master')

@section('title','Lista de Formularios Disponibles - Director')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista
           </div>
          <div class="panel-body">

             <table class="table table-bordered">
               <thead>
                  <th width="70%">Formulario</th>
                  <th>Estado</th>
               </thead>
               <tbody>
                 @if(isset($formulariosDisponibles))
                   @foreach ($formulariosDisponibles as $formulario)
                     <tr>
                       <td>
                         <a href="{{ route('modoEditarFormularioUVS01', $formulario->id) }}"> {{$formulario->tipoFormulario->descripcion}} ({{$formulario->creadoPor->nombres}} {{$formulario->creadoPor->apellidos}})</a>
                       </td>
                       <td> No Enviado </td>
                     </tr>
                   @endforeach
                 @endif
               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>
@endsection
