@extends('layouts.master')

@section('title','Información de la organización')

@section('content')
  {!!Html::style('css/formularios.css')!!}
  <!-- Date range picker -->
  {!!Html::script('js/moment.min.js')!!}
  {!!Html::script('js/daterangepicker.js')!!}
  {!!Html::style('css/daterangepicker.css')!!}

  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}

  {!!Form::model($organizacion,['route'=>['organizacion.update',$organizacion->id],'method'=>'PUT', 'class'=>'formulario form-horizontal form-proyecto'])!!}

    @include('flash.success')

    <div class="form-group">
      {!!form::label('nombre', 'Razón Social de la Organización', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('nombre',null,['id'=>'nombre','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('direccion', 'Dirección', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('direccion',null,['id'=>'direccion','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('telefono', 'Teléfono', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('telefono',null,['id'=>'telefono','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('fax', 'Fax', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('fax',null,['id'=>'fax','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('correo_electronico', 'Correo electrónico', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('correo_electronico',null,['id'=>'correo_electronico','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('nombre_representante_legal', 'Representante Legal', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('nombre_representante_legal',null,['id'=>'nombre_representante_legal','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('fecha_creacion', 'Fecha de creación y acuerdo de legalización', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('fecha_creacion',null,['id'=>'fecha_creacion','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('nombramiento_representante_legal', 'Nombramiento del Representante Legal', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('nombramiento_representante_legal',null,['id'=>'nombramiento_representante_legal','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('resumen', 'Resumen', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('resumen',null,['id'=>'resumen','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group" style="margin-top:50px;">
      <div class="col-sm-offset-2 col-sm-8">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>

  {!!Form::close()!!}

@endsection
