@extends('layouts.master')

@section('title','Organizaciones')

@section('content')

  <!-- Date range picker -->
  {!!Html::script('js/moment.min.js')!!}
  {!!Html::script('js/daterangepicker.js')!!}
  {!!Html::style('css/daterangepicker.css')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}

  <div class="row">
    <div class="col-md-8" style="width:100%">
      <div class="panel panel-default">
        <div class="panel-heading">
          Lista de Organizaciones
        </div>

        <div class="panel-body">
          <button type="button" class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#agregarOrganizacion" style="margin-top:0px; margin-bottom:20px;">Agregar organización</button>

          <!-- Modal -->
          <div class="modal fade" id="agregarOrganizacion" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><b>Agregar nueva organización</b></h4>
                </div>
                {!!Form::open(['route'=>'guardar_organizacion_base','method'=>'POST', 'class'=>'formulario'])!!}
                <div class="modal-body">
                  <label for="razon_social_organizacion">Razón Social de la Organización</label>
                  <input type="text" class="form-control" name="razon_social_organizacion" id="razon_social_organizacion">
                  <br>
                  <label for="direccion_organizacion">Dirección</label>
                  <input type="text" class="form-control" name="direccion_organizacion" id="direccion_organizacion">
                  <br>
                  <label for="telefono_organizacion">Teléfono</label>
                  <input type="text" class="form-control" name="telefono_organizacion" id="telefono_organizacion">
                  <br>
                  <label for="fax_organizacion">Fax</label>
                  <input type="text" class="form-control" name="fax_organizacion" id="fax_organizacion">
                  <br>
                  <label for="correo_electronico_organizacion">Correo electrónico</label>
                  <input type="text" class="form-control" name="correo_electronico_organizacion" id="correo_electronico_organizacion">
                  <br>
                  <label for="representante_legal_organizacion">Representante Legal</label>
                  <input type="text" class="form-control" name="representante_legal_organizacion" id="representante_legal_organizacion">
                  <br>
                  <label for="fecha_creacion">Fecha de creación y acuerdo de legalización</label>
                  <input type="text" class="form-control" name="fecha_creacion" id="fecha_creacion">
                  <br>
                  <label for="nombramiento_representante_legal">Nombramiento del Representante Legal</label>
                  <input type="text" class="form-control" name="nombramiento_representante_legal" id="nombramiento_representante_legal">
                  <br>
                  <label for="resumen_organizacion">Resumen</label>
                  <input type="text" class="form-control" name="resumen_organizacion" id="resumen_organizacion">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!!Form::close()!!}
              </div>

            </div>
          </div>

          @if(Session::has('flash_type'))
            @include('flash.' . Session::get('flash_type'))
          @endif

          <table class="table table-bordered" style="margin-top:20px;">
            <thead>
               <th width="45%">Razón Social de la Organización</th>
               <th width="20%">Correo electrónico</th>
               <th width="20%">Representante legal</th>
               <th>Acción</th>
            </thead>
            <tbody>
              @foreach($organizaciones as $organizacion)
                <tr>
                  <td>{{$organizacion->nombre}}</td>
                  <td style="text-align:left;">{{$organizacion->correo_electronico}}</td>
                  <td style="text-align:left;">{!!$organizacion->nombre_representante_legal!!}</td>
                  <td class="sv-cell-contain-width">
                    <button type="button" name="edit-organizacion-{{$organizacion->id}}" class="sv-icon-button" onclick="window.location='{{ route('organizacion.edit', $organizacion->id) }}';">
                      <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                    </button>
                    <button type="button" name="remove-organizacion-{{$organizacion->id}}" class="sv-icon-button" onclick="confirmEliminarOrganizacion({{$organizacion->id}})">
                      <i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>
                    </button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  function confirmEliminarOrganizacion(id_organizacion) {
    bootbox.confirm("¿Esta seguro que desea eliminar esta organización?", function(result) {
      if(result == true){
        window.location = "eliminar_organizacion/" + id_organizacion;
      }
    });
  }
  </script>
@endsection
