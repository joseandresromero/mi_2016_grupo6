@extends('layouts.master')

@section('title','Formularios pendientes de revisión - Tutor')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('breadcrumb-path')
  <ol class="breadcrumb">
   <li class="active">{{$proyectoEspecifico->titulo}}</li>
  </ol>
@endsection

@section('vertical-navbar-content')
  <div class="table-responsive">
     <table class="table table-bordered">
       <thead>
          <th width="50%">Formulario</th>
          <th width="50%">Estudiante</th>
       </thead>
       <tbody>
          @if(isset($formularios))
            @foreach ($formularios as $formulario)
             <tr>
               <td>
                 @if($formulario->tipoFormulario->id == 1) <!-- FOR-UVS-01 -->
                   <a href="{{ route('modoVerFormularioUVS01', $formulario->id) }}">{{$formulario->tipoFormulario->descripcion}}</a>
                 @elseif($formulario->tipoFormulario->id == 5) <!-- FOR-UVS-14 -->
                   <a href="{{ route('modoVerFormularioUVS14', $formulario->id) }}">{{$formulario->tipoFormulario->descripcion}}</a>
                 @endif
               </td>
               <td>{{$formulario->creadoPor->nombres}} {{$formulario->creadoPor->apellidos}}</td>
             </tr>
           @endforeach
          @endif
       </tbody>
     </table>
  </div>
@endsection
