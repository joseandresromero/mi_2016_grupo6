@extends('layouts.master')

@section('title','Lista de Formularios Disponibles - Tutor')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  <ol class="breadcrumb"  style="width:95%">
   <li class="active">Formularios</li>
  </ol>

   <div class="row">
     <div class="col-md-8" style="width:95%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista
           </div>
          <div class="panel-body">

             <table class="table table-bordered">
               <thead>
                  <th width="60%">Formulario</th>
                  <th>Estado</th>
                  <th width="10%">Accion</th>
               </thead>
               <tbody>
                  @if(isset($formulariosDisponibles))
                    @foreach ($formulariosDisponibles as $formulario)
                      @if($formulario->tipoFormulario->id == 4) <!-- FOR-UVS-13 -->
                        <tr>
                          <td>
                            {{$formulario->tipoFormulario->descripcion}} - {{$formulario->formularioUVS13->estudiante->usuario->nombres}} {{$formulario->formularioUVS13->estudiante->usuario->apellidos}}
                          </td>
                          <td>
                            @if($formulario->estado->id == 3 ) <!-- Estado Aprobado -->
                              REVISADO
                            @else
                              {{$formulario->estado->descripcion}}
                            @endif
                          </td>
                          <td style="text-align: center;">
                            <button type="button" class="sv-icon-button" onclick="window.location='{{route('modoEditarPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$formulario->formularioUVS13->estudiante->id])}}'">
                              <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                            </button>
                            <button type="button" class="sv-icon-button"  onclick="window.location='{{route('modoVerPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$formulario->formularioUVS13->estudiante->id])}}'">
                              <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                            </button>
                          </td>
                        </tr>
                      @elseif($formulario->tipoFormulario->id == 6) <!-- FOR-UVS-15 -->
                        <tr>
                          <td>
                            {{$formulario->tipoFormulario->descripcion}} - {{$formulario->formularioUVS15->estudiante->usuario->nombres}} {{$formulario->formularioUVS15->estudiante->usuario->apellidos}}
                          </td>
                          <td>{{$formulario->estado->descripcion}}</td>
                          <td style="text-align: center;">
                            <button type="button" class="sv-icon-button"  data-toggle="modal" data-backdrop="static" data-target="#formularioUVS15Modal">
                              <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="formularioUVS15Modal" role="dialog">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b>Formulario FOR-UVS-15</b></h4>
                                  </div>
                                  {!!Form::open(['route'=>'subirFormularioFirmado', 'method'=>'POST', 'class'=>'formulario form-proyecto', 'files'=>true])!!}
                                    <div class="modal-body">
                                      <div class="container">
                                        <div class="row" style="text-align: center;">
                                          <div class="col-sm-6">
                                            <input type="hidden" name="id_formulario" value="{{ $formulario->id }}">
                                            <label for="formularioUVS05Upload">Subir formulario firmado:</label>
                                            <input type="file" name="formularioUpload" id="formularioUpload" style="margin-left: auto; margin-right: auto; margin-bottom: 10px; width: 200px;">
                                          </div>
                                          <div class="col-sm-6" style="border-left: 1px solid rgb(222, 222, 222)">
                                            <label>Ver plantilla:</label><br>
                                            <a href="{{ route('modoVerFormularioUVS15', $formulario->id) }}" target="_blank"><i class="fa fa-file-text fa-2x" aria-hidden="true"></i></a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button id="guardarCartaCompromiso" type="submit" class="btn btn-primary">Guardar</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                  {!!Form::close()!!}
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      @elseif($formulario->tipoFormulario->id == 7) <!-- FOR-UVS-16 -->
                        <tr>
                          <td>
                            {{$formulario->tipoFormulario->descripcion}} - {{$formulario->formularioUVS16->estudiante->usuario->nombres}} {{$formulario->formularioUVS16->estudiante->usuario->apellidos}}
                          </td>
                          <td>{{$formulario->estado->descripcion}}</td>
                          <td style="text-align: center;">
                            @if($formulario->estado->id == 1 || $formulario->estado->id == 4) <!-- No enviado o Rechazado -->
                              <button type="button" class="sv-icon-button" onclick="window.location='{{ route('modoEditarFormularioUVS16', $formulario->id) }}'">
                                <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                              </button>
                            @endif
                            <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS16', $formulario->id) }}'">
                              <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                            </button>
                          </td>
                        </tr>
                      @endif
                   @endforeach
                  @endif
               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>
@endsection
