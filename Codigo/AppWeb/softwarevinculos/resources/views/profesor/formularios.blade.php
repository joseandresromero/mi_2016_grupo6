@extends('layouts.master')

@section('title','Lista de Formularios Disponibles - Tutor')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  <ol class="breadcrumb">
   <li class="active">Formularios</li>
  </ol>

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista
           </div>
          <div class="panel-body">

             <table class="table table-bordered">
               <thead>
                  <th width="30%">Formulario</th>
                  <th width="40%">Proyecto</th>
                  <th>Estado</th>
                  <th width="10%">Accion</th>
               </thead>
               <tbody>
                  @if(isset($formulariosDisponibles))
                    @foreach ($formulariosDisponibles as $formulario)
                      @if($formulario->proyecto->id == $proyectoEspecifico->id)
                          @if($formulario->tipoFormulario->id == 4) <!-- FOR-UVS-13 -->
                            @if($formulario->proyecto->tipo->id == 1) <!-- Pasantias Comunitarias -->
                              @foreach($formulario->creadoPor->tutor->estudiantesComunitarias as $estudiante)
                                @if($formulario->proyecto->id == $estudiante->proyectoComunitaria->id)
                                <tr>
                                  <td>
                                  <a href="{{ route('modoEditarPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$estudiante->id]) }}"> {{$formulario->tipoFormulario->descripcion}} - {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a>
                                  </td>
                                  <td>{{$formulario->proyecto->titulo}}</td>
                                  <td>{{$formulario->estado->descripcion}}</td>
                                  <td>
                                    <a href="{{ route('modoVerPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$estudiante->id]) }}">Ver</a>
                                  </td>
                                </tr>
                                @endif
                              @endforeach
                            @elseif($formulario->proyecto->tipo->id == 2) <!-- Pasantias empresariales -->
                              @foreach($formulario->creadoPor->tutor->estudiantesEmpresarial as $estudiante)
                                @if($formulario->proyecto->id == $estudiante->proyectoEmpresarial->id)
                                <tr>
                                  <td>
                                  <a href="{{ route('modoEditarPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$estudiante->id]) }}">Formulario {{$formulario->tipoFormulario->descripcion}} - {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a>
                                  </td>
                                  <td>{{$formulario->proyecto->titulo}}</td>
                                  <td>{{$formulario->estado->descripcion}}</td>
                                  <td>
                                    <a href="{{ route('modoVerPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$estudiante->id]) }}">Ver</a>
                                  </td>
                                </tr>
                                @endif
                              @endforeach
                            @endif
                          @elseif($formulario->tipoFormulario->id == 7) <!-- FOR-UVS-16 -->
                            <tr>
                              <td>
                                <a href="{{ route('modoEditarFormularioUVS16', $formulario->id) }}"> {{$formulario->tipoFormulario->descripcion}} - {{$formulario->formularioUVS16->estudiante->usuario->nombres}} {{$formulario->formularioUVS16->estudiante->usuario->apellidos}}</a>
                              </td>
                              <td>{{$formulario->proyecto->titulo}}</td>
                              <td>{{$formulario->estado->descripcion}}</td>
                              <td></td>
                            </tr>
                          @elseif($formulario->tipoFormulario->id == 6) <!-- FOR-UVS-15 -->
                            <tr>
                              <td>
                                <a data-toggle="modal" data-backdrop="static" href="#formularioUVS15Modal"> {{$formulario->tipoFormulario->descripcion}}</a>

                                <!-- Modal -->
                                <div class="modal fade" id="formularioUVS15Modal" role="dialog">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b>Formulario FOR-UVS-15</b></h4>
                                      </div>
                                      <div class="modal-body">
                                          <input type="file" name="formularioUVS05Upload" id="cartaComunitariasUpload" style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
                                          <a href="{{ route('modoVerFormularioUVS15', $formulario->id) }}" target="_blank">Ver plantilla</a>
                                      </div>
                                      <div class="modal-footer">
                                        <button id="guardarCartaCompromiso" type="submit" class="btn btn-primary">Guardar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>

                              <td>{{$formulario->proyecto->titulo}}</td>
                              <td>{{$formulario->estado->descripcion}}</td>
                              <td></td>
                            </tr>
                          @endif
                      @endif
                   @endforeach
                  @endif
               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>
@endsection
