@extends('layouts.master')

@section('title','Lista de Proyectos')

@section('content')

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista de Proyectos asignados
           </div>
          <div class="panel-body">
            {{--
            {!!Form::model(Request::only(['estadoProyecto']) ,['route'=>'home','method'=>'GET', 'class'=>'navbar-form navbar-left pull-right', 'files'=>true])!!}
               <div class="form-group">
                 {!!Form::select('estadoProyecto', $estados_options,null,['id'=>'estadoProyecto','class'=>'form-control'])!!}
               </div>
              <button type="submit" class="btn btn-default"> Buscar</button>
            {!!Form::close()!!}
            --}}
            {!!Form::model(Request::only(['tituloProyecto']) ,['route'=>'home','method'=>'GET'])!!}
            <div class="col-sm-4">
             <div class="input-group">
                 {!!Form::text('tituloProyecto', null,['id'=>'tituloProyecto', 'class'=>'form-control', 'placeholder' => 'Ingrese titulo del proyecto', 'style'=>'width: 100%;'])!!}
                 <span class="input-group-btn">
                   <button id="filtrarListaProyectos" type="submit" class="btn btn-default">Buscar</button>
                 </span>
             </div>
           </div>
           {!!Form::close()!!}
           <br><br><br>

             <table class="table table-bordered">
               <thead>
                  <th width="40%">Título</th>
                  <th width="15%">Rol</th>
                  <th>Institución beneficiaria</th>
                  <th width="10%">Estado FOR-UVS-04</th>
                  <th width="10%">Estado Proyecto</th>
               </thead>
               <tbody>
                  @if(isset($proyectos))
                    @foreach ($proyectos as $proyecto)
                      <tr>
                        <td style="text-align: justify;">
                          <a href="{{ route('flujo_proyecto', $proyecto->id) }}">{{$proyecto->titulo}}</a>
                        </td>
                        @if(Auth::user()->director != null && Auth::user()->director == $proyecto->director)
                          <td>DIRECTOR</td>
                        @else
                          <td>TUTOR</td>
                        @endif
                        <td style="text-align: justify;">
                          @if($proyecto->organizacion != null)
                           {{$proyecto->organizacion->nombre}}
                          @endif
                        </td>
                        <td> {{$proyecto->formularioUVS04->estado->descripcion}}</td>
                       <td> {{$proyecto->estado->descripcion}}</td>
                     </tr>
                   @endforeach
                  @endif
               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>
@endsection
