@extends('layouts.master')

@section('title','Lista de estudiantes - Profesor')

@section('vertical-navbar')
  @include('profesor.proyecto.navbar_vertical');
@endsection

@section('breadcrumb-path')
  <ol class="breadcrumb">
   <li class="active">{{$proyectoEspecifico->titulo}}</li>
  </ol>
@endsection

@section('vertical-navbar-content')
  @if(Auth::user()->director != null)
    @include('proyecto.estudiantesProyecto')
  @endif

  {{--
  @if(Auth::user()->tutor != null)
    @include('proyecto.estudiantesTutor')
  @endif
  --}}
@endsection
