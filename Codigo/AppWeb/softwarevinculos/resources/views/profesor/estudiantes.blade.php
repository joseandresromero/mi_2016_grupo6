@extends('layouts.master')

@section('title','Lista de estudiantes')

@section('content')

  <ol class="breadcrumb">
   <li class="active">Proyectos</li>
  </ol>

  @if(Auth::user()->director != null)
    @include('proyecto.estudiantesProyecto')
  @endif

  {{--
  @if(Auth::user()->tutor != null)
    @include('proyecto.estudiantesTutor')
  @endif
  --}}

@endsection
