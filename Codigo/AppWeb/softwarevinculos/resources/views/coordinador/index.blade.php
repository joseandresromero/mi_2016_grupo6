@extends('layouts.master')

@section('title','Lista de Proyectos')

@section('content')

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Proyectos
           </div>
          <div class="panel-body">
            {!!Form::model(Request::only(['tituloProyecto']) ,['route'=>'home','method'=>'GET'])!!}
            <div class="col-sm-4">
             <div class="input-group">
                 {!!Form::text('tituloProyecto', null,['id'=>'tituloProyecto', 'class'=>'form-control', 'placeholder' => 'Ingrese titulo del proyecto', 'style'=>'width: 100%;'])!!}
                 <span class="input-group-btn">
                   <button id="filtrarListaProyectos" type="submit" class="btn btn-default">Buscar</button>
                 </span>
             </div>
           </div>
           {!!Form::close()!!}
           <br><br><br>

             <table class="table table-bordered">
               <thead>
                  <th width="40%">Titulo Proyecto</th>
                  <th>Institución beneficiaria</th>
                  <th>Estado</th>
               </thead>
               <tbody>
                 @if(isset($proyectos))
                   @foreach ($proyectos as $proyecto)
                    <tr>
                      <td>
                        <a href="{{ route('flujo_proyecto', $proyecto->id) }}">{{$proyecto->titulo}}</a>
                      </td>
                      @if($proyecto->delegados->first() != null)
                        <td> {{$proyecto->delegados->first()->organizacion->nombre}}</td>
                      @else
                        <td> </td>
                      @endif
                      <td> {{$proyecto->estado->descripcion}}</td>
                    </tr>
                  @endforeach
                 @endif
               </tbody>

             </table>
          </div>
        </div>
     </div>
   </div>

@endsection
