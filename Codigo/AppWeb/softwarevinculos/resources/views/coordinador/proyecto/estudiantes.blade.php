@extends('layouts.master')

@section('title','Lista de estudiantes - Coordinador')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  @if(Auth::user()->director != null || Auth::user()->coordinadorCarreraComunitarias != null || Auth::user()->coordinadorCarreraEmpresariales != null)
    @include('proyecto.estudiantesProyecto')
  @endif
@endsection
