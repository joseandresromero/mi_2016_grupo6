@extends('layouts.master')

@section('title','Lista de tutores asignados al proyecto')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('breadcrumb-path')
  <ol class="breadcrumb">
   <li class="active">{{$proyectoEspecifico->titulo}}</li>
  </ol>
@endsection

@section('vertical-navbar-content')

  {!!Html::script('http://code.jquery.com/jquery-1.10.2.js')!!}
  {!!Html::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')!!}
  {!!Html::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css')!!}

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista de Tutores
           </div>
          <div class="panel-body">
            {!!Form::open(['route'=>['asignarproyectotutor',$proyectoEspecifico->id],'method'=>'GET', 'class'=>'navbar-form navbar-left pull-left form-asignarTutor', 'files'=>true])!!}
              @include('flash.error')
              <div class="col-lg-15">
                 <div class="input-group">
                   {!!Form::text('buscar_tutor', null,['id'=>'buscar_tutor','class'=>'form-control', 'placeholder' => 'Nombre profesor'])!!}
                   <input name="tutorSeleccionado" type="hidden" id="tutorSeleccionado"/>
                   <span class="input-group-btn">
                     <button type="submit" class="btn btn-default"> Agregar</button>
                   </span>
                 </div>
               </div>
            {!!Form::close()!!}

             <table class="table table-bordered">
               <thead>
                  <th width="35%">Nombre</th>
                  <th width="20%">Cédula</th>
                  <th width="20%">Carrera</th>
                  <th width="20%">Correo</th>
                  <th width="5%">Acción</th>
               </thead>
               <tbody>
                 @if(isset($proyectoEspecifico))
                   @foreach ($proyectoEspecifico->tutores as $tutor)
                    <tr id="row-tutor-{{$tutor->id}}">
                      <td>{{$tutor->usuario->nombres}} {{$tutor->usuario->apellidos}}</td>
                      <td>{{$tutor->usuario->cedula}}</td>
                      <td>{{$tutor->carrera->nombre}}</td>
                      <td> {{$tutor->usuario->correo_electronico}}</td>
                      <td>
                        <a href="{{ route('desasignarproyectotutor', [$proyectoEspecifico->id, $tutor->id]) }}" class="btn btn-danger btn_remove remove-tutor-proyecto"><span class="glyphicon glyphicon-remove"></span></a>
                      </td>
                    </tr>
                  @endforeach
                 @endif

               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>

   <script type="text/javascript">
     /* Buscar Tutor */
     $('#buscar_tutor').autocomplete({
         source: '{!!URL::route('autocompleteTutor')!!}',
         minlength: 5,
         autoFocus: true,
         select: function(event, response){
           $('#tutorSeleccionado').val(response.item.id);
         }
     });

     /* Desasignar a tutor del proyecto, se muestra ventana de confirmación. */
     $(document).on("click", ".remove-tutor-proyecto", function(e) {
        var link = $(this).attr("href");
        e.preventDefault();
        bootbox.confirm("¿Esta seguro de des-asignar al tutor del presente proyecto?.", function(result) {
            if (result) {
                document.location.href = link;
            }
        });
      });

      /* Antes de hacer submit, verifico que se haya seleccionado un tutor. */
      $('.form-asignarTutor').submit(function(event) {
        var idTutorSeleccionado = $('#tutorSeleccionado').val();

        if(idTutorSeleccionado == ''){
          bootbox.alert('Busque un profesor por nombre para asignarlo al proyecto.');
          $('#buscar_tutor').focus();
          event.preventDefault();
          return;
        }
      });
   </script>
@endsection
