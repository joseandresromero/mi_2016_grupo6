@extends('layouts.master')

@section('title','Proyecto seleccionado - Cordinador')

@section('vertical-navbar')
  <li class="active"><a href="{{ route('coordinadorhomeproyecto', $proyectoEspecifico->id) }}">Ver Formularios</a></li>
  <li><a href="{{ route('hometutores', $proyectoEspecifico->id) }}">Asignar Tutores</a></li>
  <li><a href="{{ route('homeestudiantes', $proyectoEspecifico->id) }}">Asignar Estudiantes</a></li>
  <li><a href="{{ route('flujo_proyecto', $proyectoEspecifico->id) }}">Flujo del proyecto</a></li>
@endsection

@section('breadcrumb-path')
  <ol class="breadcrumb">
   <li class="active">{{$proyectoEspecifico->titulo}}</li>
  </ol>
@endsection

@section('vertical-navbar-content')
  <a href="{{ route('modoVerProyecto', $proyectoEspecifico->id) }} " target="_blank" >Formulario UVS 04</a> <br>
  <a href="#">Formulario UVS 13</a> <br>
  <a href="#">Formularios UVS 01</a>
@endsection
