@extends('layouts.master')

@section('title','Revisar formularios')

@section('content')

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista de formjularios pendientes de revisión.
           </div>
          <div class="panel-body">
            {!!Form::model(Request::only(['tituloProyecto']) ,['route'=>'revisar_formularios','method'=>'GET'])!!}
            <div class="col-sm-4">
             <div class="input-group">
                 {!!Form::text('tituloProyecto', null,['id'=>'tituloProyecto', 'class'=>'form-control', 'placeholder' => 'Ingrese titulo del proyecto', 'style'=>'width: 100%;'])!!}
                 <span class="input-group-btn">
                   <button id="filtrarListaProyectos" type="submit" class="btn btn-default">Buscar</button>
                 </span>
             </div>
           </div>
           {!!Form::close()!!}
           <br><br><br>

             <table class="table table-bordered">
               <thead>
                  <th width="40%">Formulario</th>
                  <th>Proyecto</th>
               </thead>
               <tbody>
                 @if(isset($formularios))
                   @foreach ($formularios as $formulario)
                     @if($formulario->estado->id == 2) <!-- Enviado -->
                       <tr>
                         <td>
                           @if($formulario->tipoFormulario->id == 2) <!-- FOR-UVS-04 -->
                             <a href="{{ route('modoVerProyecto', $formulario->proyecto->id) }}">{{$formulario->tipoFormulario->descripcion}} (Creación de proyecto)</a>
                           @elseif($formulario->tipoFormulario->id == 7) <!-- FOR-UVS-16 -->
                             <a href="{{ route('modoVerFormularioUVS16', $formulario->id) }}">{{$formulario->tipoFormulario->descripcion}} - {{$formulario->formularioUVS16->estudiante->usuario->nombres}} {{$formulario->formularioUVS16->estudiante->usuario->apellidos}}</a>
                           @endif
                         </td>
                         <td>{{$formulario->proyecto->titulo}}</td>
                       </tr>
                     @endif
                  @endforeach
                 @endif
               </tbody>
             </table>
          </div>
        </div>
     </div>
   </div>

@endsection
