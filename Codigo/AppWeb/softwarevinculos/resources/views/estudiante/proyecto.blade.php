@extends('layouts.master')

@section('title',$tipoProyecto)

@section('content')

  <div class="panel-body" style="width:60%; margin: 0px auto;">
    @if($proyecto != null)
      <div class="jumbotron" style="padding-top:10px; padding-bottom:10px;">
        <h2 style="margin-bottom:25px;"><b>{{$proyecto->titulo}}</b></h2>
        <h4><b>Estado del proyecto: </b>{{$proyecto->estado->descripcion}}</h4>
        <h4><b>Organización: </b>{{$proyecto->organizacion!=null ? $proyecto->organizacion->nombre : ''}}</h4>
        <h4><b>Director: </b>{{$proyecto->director->usuario->nombres}} {{$proyecto->director->usuario->apellidos}}</h4>
        <h4><b>Tutor asignado: </b>{{ $tutor != null ? $tutor->usuario->nombres.' '.$tutor->usuario->apellidos : 'No asignado' }}</h4>
        <h4><b>Estado del estudiante: </b>
          @if($proyecto->tipo->id == 1 ) <!-- proyecto Comunitario -->
            {{Auth::user()->estudiante->estadoEstudianteProyectoComunitario->descripcion}}
          @else
            {{Auth::user()->estudiante->estadoEstudianteProyectoEmpresarial->descripcion}}
          @endif
        </h4>
      </div>
      <table class="table table-bordered">
        <thead>
          <th colspan="3"><h3><b>Formularios</b></h3></th>
        </thead>
        <thead>
          <th width="60%">Tipo</th>
          <th>Estado</th>
          <th width = "10%">Acción</th>
        </thead>
        <tbody>
          @if(isset($formularios))
            @foreach ($formularios as $formulario)
              @if($formulario->tipoFormulario->id == 1 ) <!-- FOR-UVS-01 -->
                <tr>
                  <td>
                    {{$formulario->tipoFormulario->descripcion}}
                  </td>
                  <td>{{$formulario->estado->descripcion}}</td>
                  <td style="text-align: center;">
                    @if(($formulario->estado->id == 1 || $formulario->estado->id == 4) && $formulario->formularioUVS01->revisado_por_director == true) <!-- No enviado o Rechazado -->
                      <button type="button" class="sv-icon-button" onclick="window.location='{{ route('modoEditarFormularioUVS01', $formulario->id) }}'">
                        <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                      </button>
                    @elseif(($formulario->estado->id == 1 || $formulario->estado->id == 4) && $formulario->formularioUVS01->revisado_por_director == false)
                      <button type="button" class="sv-icon-button" id="editarFORUVS01NoRevisado">
                        <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                      </button>
                    @endif
                    <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS01', $formulario->id) }}'">
                      <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              @elseif($formulario->tipoFormulario->id == 5) <!-- FOR-UVS-14 -->
                <tr>
                  <td>
                    {{$formulario->tipoFormulario->descripcion}}
                  </td>
                  <td>{{$formulario->estado->descripcion}}</td>
                  <td style="text-align: center;">
                    @if($formulario->estado->id == 1 || $formulario->estado->id == 4 ) <!-- No enviado o Rechazado -->
                      <button type="button" class="sv-icon-button" onclick="window.location='{{ route('modoEditarFormularioUVS14', $formulario->id) }}'">
                        <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                      </button>
                    @endif
                    <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS14', $formulario->id) }}'">
                      <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              @endif
            @endforeach
          @endif
        </tbody>
      </table>
    @else
      <p style="text-align:center;">Usted aún no ha sido asinado a ningún proyecto de {{$tipoProyecto}}</p>
    @endif
  </div>

  <script>
   /* Mensaje de alerta de que no puede editar formulario FOR-UVS-01 aún. */
   $(document).on("click", "#editarFORUVS01NoRevisado", function(e) {
     bootbox.alert("Debe esperar a que el director del proyecto le asigne sus actividades para poder editar este formulario.") ;
    });
  </script>

@endsection
