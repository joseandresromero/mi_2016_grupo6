<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!-- Bootstrap -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/login.css')!!}
    {!!Html::script('js/bootstrap.min.js')!!}
  </head>

  <body>

    <div class="container">
        <div class="row vertical-offset-100">
        	<div class="col-md-4 col-md-offset-4">
            @include('flash.error')
        		<div class="panel panel-default">
    			  	<div class="panel-heading">
    			    	<h3 class="panel-title">Inicio de sesión</h3>
    			 	  </div>
    			  	<div class="panel-body">
                {!!Form::open(['route'=>'authenticate', 'method'=>'POST', 'files'=>true])!!}

                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group">
                    <input class="form-control" placeholder="Usuario" name="username" type="text">
                    @espol.edu.ec
                  </div>
                  <div class="form-group">
                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                  </div>
                  <input class="btn btn-lg btn-success btn-block" style="background-color: #03346b;" type="submit" value="Iniciar Sesión">

                {!!Form::close()!!}
    			    </div>
    			  </div>
    	  	</div>
    	  </div>
    </div>

  </body>
</html>
