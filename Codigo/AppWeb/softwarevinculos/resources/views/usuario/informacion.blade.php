@extends('layouts.master')

@section('title','Información de usuario')

@section('content')
  {!!Html::style('css/formularios.css')!!}

  {!!Form::model($usuario,['route'=>['usuario.update',$usuario->id],'method'=>'PUT', 'id'=>'form-informacion', 'class'=>'formulario form-horizontal form-proyecto form-informacion'])!!}

    @if(Session::has('flash_type'))
      @include('flash.' . Session::get('flash_type'))
    @endif

    <div class="form-group">
      {!!form::label('nombres', 'Nombres:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('nombres',null,['id'=>'nombres','class'=>'form-control', 'disabled' => 'disabled'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('apellidos', 'Apellidos:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('apellidos',null,['id'=>'apellidos','class'=>'form-control', 'disabled' => 'disabled'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('cedula', 'Cédula:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('cedula',null,['id'=>'cedula','class'=>'form-control', 'disabled' => 'disabled'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('telefono_fijo', 'Teléfono fijo:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::number('telefono_fijo',null,['id'=>'telefono_fijo','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('celular', 'Celular:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::number('celular',null,['id'=>'celular','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group">
      {!!form::label('correo_electronico', 'Correo electrónico:', ['class' => 'control-label col-sm-2'])!!}
      <div class="col-sm-8">
        {!!form::text('correo_electronico',null,['id'=>'correo_electronico','class'=>'form-control'])!!}
      </div>
    </div>

    <div class="form-group" style="margin-top:50px;">
      <div class="col-sm-offset-2 col-sm-8">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>

  {!!Form::close()!!}

  <script type="text/javascript">
    $('#form-informacion').submit(function(event) {

      //Telefono fijo
      if ($('#telefono_fijo').val() < 1) {
          bootbox.alert("Ingrese un número valido de teléfono fijo.");
          $('#telefono_fijo').focus();
          event.preventDefault();
          return;
      }

      //Celular
      if ($('#celular').val() < 1) {
          bootbox.alert("Ingrese un número valido de celular.");
          $('#celular').focus();
          event.preventDefault();
          return;
      }
    });
  </script>

@endsection
