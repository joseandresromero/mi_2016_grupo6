<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Vinculos con la Sociedad::@yield('title')::</title>

    <!-- Bootstrap -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {!!Html::script('js/jquery-2.2.4.min.js')!!}
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/master.css')!!}
    {!!Html::style('css/flujoProyecto.css')!!}
    {!!Html::script('js/bootstrap.js')!!}
    <!-- Font awesome icons -->
    {!!Html::style('font-awesome/css/font-awesome.min.css')!!}
    <!-- Menajes de alerta -->
    {!!Html::script('js/bootbox.min.js')!!}
    <!-- Eventos con delay -->
    {!!Html::script('js/bindWithDelay.js')!!}

    <!-- Date range picker -->
    {!!Html::script('js/moment.min.js')!!}
    {!!Html::script('js/daterangepicker.js')!!}
    {!!Html::style('css/daterangepicker.css')!!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
@if(!isset($modoVer))
    <div>
      <h2 id="masterTitle">Sistema de Gestión de Pasantías</h2>
    </div>

     <!-- Fixed navbar -->
    <nav class="navbar navbar-default sv-nav-bar">
      <div class="container">
        <!--
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"> <img src="{{url('img/logo.png')}}" alt=""/> </a>
        </div>
      -->

      {{-- NAVBAR HORIZONTALL --}}
      <div id="navbar" class="navbar-header pull-left dropdown">
        <ul class="nav navbar-nav">
          {{-- NAVBAR PARA EL PROFESOR (Director o Tutor)--}}
          @if(Auth::user()->tutor || (Auth::user()->director && Auth::user()->coordinadorCarreraEmpresariales == null))
            <li {{{ (Route::currentRouteNamed('home') ? 'class=active' : '') }}}><a href="{{ route('home') }}">Lista de Proyectos</a></li>
          @endif

          @if(Auth::user()->coordinadorCarreraComunitarias || Auth::user()->coordinadorCarreraEmpresariales)
            <li {{{ (Route::currentRouteNamed('home') ? 'class=active' : '') }}}><a href="{{route('home')}}">Lista de Proyectos</a></li>
            <li {{{ (Route::currentRouteNamed('revisar_formularios') ? 'class=active' : '') }}}><a href="{{ route('revisar_formularios') }}">Revisar Formularios</a></li>
          @endif

          @if(Auth::user()->director)
              <li {{{ (Route::currentRouteNamed('proyecto.create') ? 'class=active' : '') }}}>
                <a href="{{ route('proyecto.create') }}">Crear Proyecto</a>
              </li>
              <li {{{ (Route::currentRouteNamed('organizaciones') ? 'class=active' : '') }}}><a href="{{ route('organizaciones')  }}">Organizaciones</a></li>
              {{--<li {{{ (Route::currentRouteNamed('proyecto.create') ? 'class=active' : '') }}}>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Crear Proyecto</a>

                <ul class="dropdown-menu">
                  <li><a href="{{ route('proyecto.create') }}?tipo_proyecto=1">Pasantías Comunitarias</a></li>
                  <li><a href="{{ route('proyecto.create') }}?tipo_proyecto=2">Pasantías Pre-profesionales</a></li>
                </ul>
              </li>--}}
          @endif

          @if(Auth::user()->estudiante)
              <li {{{ (Route::currentRouteNamed('pasantiasComunitarias') ? 'class=active' : '') }}}><a href="{{ route('pasantiasComunitarias')  }}">Pasantías Comunitarias</a></li>
              <li {{{ (Route::currentRouteNamed('pasantiasEmpresariales') ? 'class=active' : '') }}}><a href="{{ route('pasantiasEmpresariales')  }}">Pasantías Empresariales</a></li>
          @endif

          {{-- Esta opción va para todos los usuarios --}}
          @if(Auth::user()->administrador == null)
            <li {{{ (Route::currentRouteNamed('usuario.edit') ? 'class=active' : '') }}}><a href="{{ route('usuario.edit', Auth::user()->id) }}">Información de usuario</a></li>
            <li {{{ (Route::currentRouteNamed('seguimiento') ? 'class=active' : '') }}}><a href="{{ route('seguimiento') }}">Notificaciones <span class="badge">{{ $numNotificaciones }}</span></a></li>
          @else
            <li {{{ (Route::currentRouteNamed('home') ? 'class=active' : '') }}}><a href="{{ route('home')  }}">Usuarios</a></li>
            <li {{{ (Route::currentRouteNamed('programas') ? 'class=active' : '') }}}><a href="{{ route('programas')  }}">Programas</a></li>
          @endif
        </ul>
      </div><!--/.nav-collapse -->


      <div class="navbar-header pull-right">
          <!--
           <button type="button" class="navbar-toggle"
                  data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        -->

          <p class="navbar-text">
            <span class="glyphicon glyphicon-user" style="margin-right:10px;"></span>
            <b> {{Auth::user()->username}} </b>
            |
            <a class="logoutBtn" onclick="window.location='{{ route('logout') }}';"; class="navbar-link">Cerrar sesión</a>&nbsp;
          </p>
      </div>

      </div>

    </nav>
@endif
  	<!-- /container -->
    <div class="container">
      @yield('content')
    </div>

    {{--Vertical NavBar--}}
    @if(isset($proyectoEspecifico) && !(Route::currentRouteNamed('proyecto.edit')))
      <div class="row">
        <div class="col-sm-2">
          <div class="sidebar-nav">
            <div class="navbar navbar-default sv-navbar-vertical" role="navigation">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <span class="visible-xs navbar-brand">Sidebar menu</span>
              </div>
              <div class="navbar-collapse collapse sidebar-navbar-collapse">
                <ul class="nav navbar-nav">
                  @yield('vertical-navbar')
                </ul>
              </div><!--/.nav-collapse -->
            </div>
          </div>
        </div>
        <div class="col-sm-10">
          <div class="sv-titulo-proyecto">
            <input type="hidden" name="id_proyecto" value="{{$proyectoEspecifico->id}}">
            <input type="hidden" name="_tokenIniciarProyecto" value="<?php echo csrf_token(); ?>">
            <h3 style="text-align:center;">{{$proyectoEspecifico->titulo}}</h3>
            <h4 id="estadoProyectoMaster" style="text-align:center;">
              <b>Estado:</b>
              @if($proyectoEspecifico->formularioUvs04->estado->id == 2) <!-- Enviado -->
                Formulario FOR-UVS-04 enviado (Pendiente de revisión)
              @elseif($proyectoEspecifico->formularioUvs04->estado->id == 1) <!-- No enviado -->
                Formulario FOR-UVS-04 No enviado
              @elseif($proyectoEspecifico->formularioUvs04->estado->id == 4) <!-- Rechazado -->
                  Formulario FOR-UVS-04 Rechazado
              @else
                {{$proyectoEspecifico->estado->descripcion}}
              @endif
            </h4>
            <div class="" style="margin-left:auto; margin-right:auto; width: 22%;">
              @if($proyectoEspecifico->estado->id == 2 && Auth::user()->director != null && Auth::user()->director->id == $proyectoEspecifico->director->id) <!-- Proyecto aprobado -->
                <button id="iniciarProyecto" type="button" class="btn btn-primary" style="width:100%;">Iniciar Proyecto</button>
              @endif
            </div>
          </div>

          @yield('vertical-navbar-content')
        </div>
    </div>
  @endif


  @if(!isset($modoVer))
      <footer class="footer">
        <div class="container">
          <p>© 2016 Vinculos con la Sociedad</p>
        </div>
      </footer>
  @endif
    </body>

    <script>

      $(".nav a").click( function(event){
        console.info("enter to script");
       $(".nav").find(".active").removeClass("active");
       $(this).parent().addClass("active");
      });

      $('#iniciarProyecto').click(function() {
        var token = $('input[name=_tokenIniciarProyecto]').val(),
            id_proyecto = $('input[name=id_proyecto]').val(),

            dataString = 'id_proyecto=' + id_proyecto + '&_token=' + token;

        $.ajax({
            //url: 'guardar_organizacion',
            url: '{!!URL::route('iniciar_proyecto')!!}',
            type: 'POST',
            data: dataString,
            success: function (data) {
                console.log(data);
                $('#estadoProyectoMaster').html('<b>Estado:</b> Iniciado');
                $('#iniciarProyecto').hide();
            }
        });
      });
    </script>
</html>
