@extends('layouts.master')

@section('title','Proyecto seleccionado')

{{--
@section('content')

    <div class="row">
      <div class="col-sm-2">
        <div class="sidebar-nav">
          <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <span class="visible-xs navbar-brand">Sidebar menu</span>
            </div>
            <div class="navbar-collapse collapse sidebar-navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="{{ route('proyecto.edit', $proyecto->id) }}">Editar Proyecto</a></li>
                <li><a href="#">Ver Formularios</a></li>
                <li><a href="{{ route('homeestudiantes', $proyecto->id) }}">Estudiantes Asignados</a></li>
                <li><a href="#">Tutores Asignados</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>
      <div class="col-sm-10">
        <ol class="breadcrumb">
         <li class="active">{{$proyecto->titulo}}</li>
        </ol>
      </div>
    </div>


@endsection
--}}

@section('vertical-navbar')
  <li class="active"><a href="#">Ver Formularios</a></li>
  <li ><a href="{{ route('proyecto.edit', $proyectoEspecifico->id) }}">Editar Proyecto</a></li>
  <li><a href="{{ route('homeestudiantes', $proyectoEspecifico->id) }}">Estudiantes Asignados</a></li>
  <li><a href="#">Tutores Asignados</a></li>
  <li><a href="{{ route('flujo_proyecto', $proyectoEspecifico->id) }}">Flujo del proyecto</a></li>
@endsection

@section('breadcrumb-path')
  <ol class="breadcrumb">
   <li class="active">{{$proyectoEspecifico->titulo}}</li>
  </ol>
@endsection

@section('vertical-navbar-content')
  <a href="#">Formulario UVS 04</a> <br>
  <a href="#">Formulario UVS 13</a> <br>
  <a href="#">Formularios UVS 01</a>
@endsection
