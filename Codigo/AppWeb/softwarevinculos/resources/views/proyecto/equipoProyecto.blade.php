{{-- Equipo del proyecto --}}
<h3><b>Equipo del proyecto</b></h3>
<hr>

@if(isset($proyectoEspecifico))
  @foreach($proyectoEspecifico->tutores as $tutor)

    <div class="panel panel-default">
      <div class="panel-heading">
         <th><b>{{$tutor->usuario->nombres}} {{$tutor->usuario->apellidos}}</b></th>
      </div>

      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_actividades">
            <thead>
              <tr>
                <th width="50%">Estudiante</th>
                <th>Matricula</th>
                <th>Correo Electrónico</th>
              </tr>
            </thead>
            <tbody>
              @if($proyectoEspecifico->tipo->id == 1) <!-- Pasantias Comunitarias -->
                @if(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoComunitaria($proyectoEspecifico->id, $tutor->id)->get()->isEmpty())
                  <tr>
                    <td colspan="3" style="text-align: center;">
                      El tutor aún no tiene estudiantes asignados
                    </td>
                  </tr>
                @else
                  @foreach(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoComunitaria($proyectoEspecifico->id, $tutor->id)->get() as $estudiante)
                    <tr>
                      <td><a href="{{ route('detalle_estudiante_proyecto', [$estudiante->id, $proyectoEspecifico->id]) }}"> {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a></td>
                      <td> {{$estudiante->matricula}}</td>
                      <td> {{$estudiante->usuario->correo_electronico}}</td>
                    </tr>
                  @endforeach
                @endif
              @elseif($proyectoEspecifico->tipo->id == 2) <!-- Pasantias Empresariales -->
                @if(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoEmpresarial($proyectoEspecifico->id, $tutor->id)->get()->isEmpty())
                  <tr>
                    <td colspan="3" style="text-align: center;">
                      El tutor aún no tiene estudiantes asignados
                    </td>
                  </tr>
                @else
                  @foreach(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoEmpresarial($proyectoEspecifico->id, $tutor->id)->get() as $estudiante)
                    <tr>
                      <td><a href="{{ route('detalle_estudiante_proyecto', [$estudiante->id, $proyectoEspecifico->id]) }}"> {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a></td>
                      <td> {{$estudiante->matricula}}</td>
                      <td> {{$estudiante->usuario->correo_electronico}}</td>
                    </tr>
                  @endforeach
                @endif
              @endif

            </tbody>
          </table>
        </div>
      </div>
    </div>

  @endforeach
@endif
