@extends('layouts.master')

@section('title','Lista de tutores asignados al proyecto')

@section('content')

  {!!Html::script('http://code.jquery.com/jquery-1.10.2.js')!!}
  {!!Html::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')!!}
  {!!Html::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css')!!}

  <ol class="breadcrumb">
   <li class="active">Proyectos</li>
  </ol>


   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default">
          <div class="panel-heading">
             Lista de Tutores
           </div>
          <div class="panel-body">
            {!!Form::open(['route'=>['asignarproyectotutor',$proyecto->id],'method'=>'GET', 'class'=>'navbar-form navbar-left pull-left', 'files'=>true])!!}
               <div class="form-group">
                 {!!Form::text('buscar_tutor', null,['id'=>'buscar_tutor','class'=>'form-control', 'placeholder' => 'Nombre profesor', 'style'=>'width: 100%;'])!!}
                 <input name="tutorSeleccionado" type="hidden" id="tutorSeleccionado"/>
               </div>
              <button type="submit" class="btn btn-default"> Agregar</button>
            {!!Form::close()!!}

             <table class="table table-bordered">
               <thead>
                  <th width="40%">Nombre</th>
                  <th width="30%">Cédula</th>
                  <th >Correo</th>
               </thead>
               <tbody>
                 @if(isset($proyecto))
                   @foreach ($proyecto->tutores as $tutor)
                    <tr>
                      <td>{{$tutor->usuario->nombres}} {{$tutor->usuario->apellidos}}</td>
                      <td>{{$tutor->usuario->cedula}}</td>
                      <td> {{$tutor->usuario->correo_electronico}}</td>
                    </tr>
                  @endforeach
                 @endif

               </tbody>
             </table>
          </div>
        </div>

     </div>
   </div>

   <script type="text/javascript">
     //Buscar Tutor
     $('#buscar_tutor').autocomplete({
         source: '{!!URL::route('autocompleteTutor')!!}',
         minlength: 3,
         autoFocus: true,
         select: function(event, response){
           console.info('response', response);
           $('#tutorSeleccionado').val(response.item.id);
         }
     });
   </script>


@endsection
