@extends('layouts.master')

@section('title','Lista de estudiantes')

@section('content')

  <ol class="breadcrumb">
   <li class="active">Proyectos</li>
  </ol>

  @include('proyecto.estudiantesProyecto')

@endsection
