
<li {{{ (Route::currentRouteNamed('flujo_proyecto') ? 'class=active' : '') }}}> <a href="{{ route('flujo_proyecto', $proyectoEspecifico->id) }}">Flujo del proyecto</a></li>
<li {{{ ( (Route::currentRouteNamed('detalle_proyecto') || Route::currentRouteNamed('detalle_estudiante_proyecto') ) ? 'class=active' : '')}}}><a href="{{ route('detalle_proyecto', $proyectoEspecifico->id) }}">Detalles del Proyecto</a></li>

@if((Auth::user()->director != null && Auth::user()->coordinadorCarreraEmpresariales == null) && $proyectoEspecifico->director->id == Auth::user()->director->id)
  <li {{{ (Route::currentRouteNamed('formulariosDisponiblesDirector') ? 'class=active' : '') }}}><a href="{{ route('formulariosDisponiblesDirector', $proyectoEspecifico->id) }}">Formularios Disponibles</a></li>
  @if(Auth::user()->director == $proyectoEspecifico->director && ($proyectoEspecifico->formularioUVS04->estado->id == 1 || $proyectoEspecifico->formularioUVS04->estado->id == 4 )) <!-- No enviado o Rechazado -->
      <li {{{ (Route::currentRouteNamed('proyecto.edit') ? 'class=active' : '') }}}><a href="{{ route('proyecto.edit', $proyectoEspecifico->id) }}">Editar Proyecto</a></li>
  @endif
@elseif((Auth::user()->director != null && Auth::user()->coordinadorCarreraEmpresariales != null) && $proyectoEspecifico->director->id == Auth::user()->director->id && Auth::user()->director == $proyectoEspecifico->director && ($proyectoEspecifico->formularioUVS04->estado->id == 1 || $proyectoEspecifico->formularioUVS04->estado->id == 4 ))
  <li {{{ (Route::currentRouteNamed('proyecto.edit') ? 'class=active' : '') }}}><a href="{{ route('proyecto.edit', $proyectoEspecifico->id) }}">Editar Proyecto</a></li>
@elseif(Auth::user()->tutor != null)
  <li {{{ (Route::currentRouteNamed('formulariosDisponiblesTutor') ? 'class=active' : '') }}}><a href="{{ route('formulariosDisponiblesTutor', $proyectoEspecifico->id) }}">Formularios</a></li>
  <li {{{ (Route::currentRouteNamed('formulariospendientesrevisiontutor') ? 'class=active' : '') }}}><a href="{{ route('formulariospendientesrevisiontutor', $proyectoEspecifico->id) }}">Revisar Formularios</a></li>
@endif

@if(Auth::user()->coordinadorCarreraComunitarias != null || Auth::user()->coordinadorCarreraEmpresariales != null && $proyectoEspecifico->estado->id == 2) <!-- Proyecto aprobado -->
  <li {{{ (Route::currentRouteNamed('listatutoresproyecto') ? 'class=active' : '') }}}><a href="{{ route('listatutoresproyecto', $proyectoEspecifico->id) }}">Asignar Tutores</a></li>
  <li {{{ (Route::currentRouteNamed('listaestudiantesproyecto') ? 'class=active' : '') }}}><a href="{{ route('listaestudiantesproyecto', $proyectoEspecifico->id) }}">Asignar Estudiantes</a></li>
@endif

@if(Auth::user()->tutor != null || Auth::user()->director != null)
  <li {{{ (Route::currentRouteNamed('subirFormularios') ? 'class=active' : '') }}}><a href="{{ route('subirFormularios', $proyectoEspecifico->id) }}">Subir Formularios</a></li>
@endif
