@extends('layouts.master')

@section('title','Detalles del Proyecto')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  {{-- Formularios del proyecto --}}
  <h3><b>Formularios principales del proyecto</b></h3>
  <hr>
  <div class="table-responsive" style="margin-top:40px">
    <table class="table table-bordered" id="tabla_actividades">
      <thead>
        <tr>
         <th >Formulario</th>
         <th width="15%">Acción</th>
         </tr>
      </thead>
      <tbody>
        <tr>
          <td>Formulación de Proyecto (FOR-UVS-04) </td>
          <td style="text-align: center;">
            <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerProyecto', $proyectoEspecifico->id) }}'" data-toggle="tooltip" data-placement="bottom" title="Vista previa">
              <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
            </button>
            <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('downloadForUVS04', $proyectoEspecifico->id) }}'"  data-toggle="tooltip" data-placement="bottom" title="Descargar">
              <i class="fa fa-download fa-lg" aria-hidden="true" style="color:rgb(0, 0, 119);"></i>
            </button>
          </td>
        </tr>
        <tr>
          <td>Presupuesto (FOR-UVS-08)</td>
          <td style="text-align: center;">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <button type="button" class="sv-icon-button" onclick="descargarPresupuesto({{$proyectoEspecifico->id}})" data-toggle="tooltip" data-placement="bottom" title="Descargar">
              <i class="fa fa-download fa-lg" aria-hidden="true" style="color:rgb(0, 0, 119);"></i>
            </button>
          </td>
        </tr>
        <tr>
          <td>Seguimiento actividades tutoriadas (FOR-UVS-13)</td>
          <td style="text-align: center;">
            <button type="button" class="sv-icon-button" onclick="window.location='{{ route('modoVerForUVS13Completo', $proyectoEspecifico->id) }}'"  data-toggle="tooltip" data-placement="bottom" title="Vista previa">
              <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>


  <br><br>
  @if((Auth::user()->director != null && $proyectoEspecifico->director->id == Auth::user()->director->id) || Auth::user()->coordinadorCarreraComunitarias || Auth::user()->coordinadorCarreraEmpresariales)
    @include('proyecto.equipoProyecto')
  @elseif(Auth::user()->tutor != null)
    @include('proyecto.estudiantesAsignadosTutor')
  @endif

  <script type="text/javascript">
    function descargarPresupuesto(id_proyecto) {

      var token = $('input[name=_token]').val(),
          dataString = 'id_proyecto=' + id_proyecto + '&_token=' + token;

      $.ajax({
          url: '{!!URL::route('existePresupuesto')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
            if (data.result != null) {
              var win = window.open(data.result, '_blank');
              if (win) {
                  //Browser has allowed it to be opened
                  win.focus();
              } else {
                  //Browser has blocked it
                  alert('Please allow popups for this website');
              }
            } else {
              bootbox.alert("No se ha subido archivo de presupuesto a este proyecto.");
            }
          }
      });
    }

  </script>
@endsection
