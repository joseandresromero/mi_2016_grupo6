@extends('layouts.master')

@section('title','Detalles del Estudiante en el Proyecto')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  <ol class="breadcrumb" style="width: 95%;">
    <li><a href="{{ route('detalle_proyecto', $proyectoEspecifico->id) }}">Detalle del proyecto</a></li>
    <li class="active">Información Estudiante</li>
  </ol>

  <div  style="padding-top:10px; padding-bottom:10px;">
    <h2 style="margin-bottom:25px;">
    <h4><b>Nombres y Apellidos: </b>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</h4>
    <h4><b>Matricula: </b>{{$estudiante->matricula}} </h4>
    <h4><b>Correo Electrónico: </b>{{$estudiante->usuario->correo_electronico}}</h4>
    <h4><b>Estado del estudiante: </b>
      @if($proyectoEspecifico->tipo->id == 1 ) <!-- proyecto Comunitario -->
        {{$estudiante->estadoEstudianteProyectoComunitario->descripcion}}
      @else
        {{$estudiante->estadoEstudianteProyectoEmpresarial->descripcion}}
      @endif
    </h4>
  </div>
  <br>
  <h3><b>Formularios Aprobados</b></h3>
  <hr style="margin-right:75px">
  <div class="table-responsive" style="width: 95%;">
    <table class="table table-bordered">
      <thead>
         <th >Formulario</th>
         <th width="12%">Acción</th>
      </thead>
      <tbody>
        @foreach($formularios as $formulario)
          @if($formulario->estado->id == 3) <!-- Solo formularios aprobados -->
            <tr>
              <td>
                {{$formulario->nombre}}
              </td>
              <td style="text-align: center">
                @if($formulario->tipoFormulario->id == 1) <!-- Formulario FOR-UVS-01 -->
                  <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS01', $formulario->id) }}'">
                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                  </button>
                @elseif($formulario->tipoFormulario->id == 4) <!-- Formulario FOR-UVS-13 -->
                  <button type="button" class="sv-icon-button"  onclick="window.location='{{route('modoVerPorEstudianteForUVS13', ['id_formulario'=>$formulario->id,'id_estudiante'=>$formulario->formularioUVS13->estudiante->id])}}'">
                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                  </button>
                @elseif($formulario->tipoFormulario->id == 5) <!-- Formulario FOR-UVS-14 -->
                  <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS14', $formulario->id) }}'">
                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                  </button>
                @elseif($formulario->tipoFormulario->id == 7) <!-- Formulario FOR-UVS-16 -->
                  <button type="button" class="sv-icon-button"  onclick="window.location='{{ route('modoVerFormularioUVS16', $formulario->id) }}'">
                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                  </button>
                @endif

                <button type="button" class="sv-icon-button" onclick="window.location='{{ route('descargarFormularioFirmado', $formulario->id) }}';">
                  <i class="fa fa-download fa-lg" aria-hidden="true" style="color:rgb(0, 0, 119);"></i>
                </button>
              </td>
            </tr>
          @endif
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
