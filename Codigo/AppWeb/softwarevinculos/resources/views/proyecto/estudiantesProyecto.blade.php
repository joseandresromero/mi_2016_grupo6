<div class="row">
  <div class="col-md-8" style="width:100%">
     <div class="panel panel-default">
       <div class="panel-heading">
          Lista de Estudiantes
        </div>

       <div class="panel-body">
         @if(Auth::user()->coordinadorCarreraComunitarias != null || Auth::user()->coordinadorCarreraEmpresariales != null)
           {!!Form::open(['route'=>['asignarproyectoestudiante',$proyecto->id],'method'=>'GET', 'class'=>'navbar-form navbar-left pull-left form-asignarEstudiante', 'files'=>true])!!}
             @include('flash.error')
             <div class="col-lg-15">
                <div class="input-group">
                  {!!Form::number('matriculaEstudiante', null,['id'=>'matriculaEstudiante', 'min' => '1' ,'class'=>'form-control', 'placeholder' => 'Ingrese matricula', 'style'=>'width: 100%;'])!!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default"> Agregar</button>
                  </span>
                </div>
              </div>
           {!!Form::close()!!}
          @endif

          <table class="table table-bordered">
            <thead>
               <th width="30%">Nombre</th>
               <th width="30%">Matricula</th>
               <th >Correo</th>
               <th width="30%">Tutor</th>
               <th width="5%">Acción</th>
            </thead>
            <tbody>
              @if(isset($proyecto))
                @if($proyecto->tipo->id == 2) <!-- Pasantias empresariales -->
                  @foreach ($proyecto->estudiantesEmpresarial as $estudiante)
                   <tr>
                     <td>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</td>
                     <td>{{$estudiante->matricula}}</td>
                     <td> {{$estudiante->usuario->correo_electronico}}</td>
                     <td>
                       @if($estudiante->tutorProyectoEmpresarial != null)
                         {{$estudiante->tutorProyectoEmpresarial->usuario->nombres}} {{$estudiante->tutorProyectoEmpresarial->usuario->apellidos}}
                         <a href="modal-selectutor-{{ $estudiante->id }}"  data-toggle="modal" data-target="#modal-selecttutor-{{ $estudiante->id }}">(Cambiar Tutor)</a>
                       @else
                         @if(Auth::user()->coordinadorCarreraComunitarias != null || Auth::user()->coordinadorCarreraEmpresariales != null)
                           <a href="modal-selectutor-{{ $estudiante->id }}"  data-toggle="modal" data-target="#modal-selecttutor-{{ $estudiante->id }}">Sin Tutor asignado</a>
                         @else
                           Sin Tutor asignado
                         @endif
                       @endif
                     </td>
                     <td>
                       <a href="{{ route('desasignarproyectoestudiante', [$proyectoEspecifico->id, $estudiante->id]) }}" class="btn btn-danger btn_remove remove-estudiante-proyecto"><span class="glyphicon glyphicon-remove"></span></a>
                     </td>
                   </tr>
                     <!-- Modal -->
                     <div class="modal fade" id="modal-selecttutor-{{ $estudiante->id }}" role="dialog">
                       <div class="modal-dialog">
                         <div class="modal-content">
                           <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                             <h4 class="modal-title"><b>Asignar Tutor</b></h4>
                           </div>
                           <div class="modal-body">
                             <label>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</label>
                             <br><br>
                             {!!Form::open(['route'=>['asignartutorestudiante',$estudiante->id, $proyecto->id],'method'=>'GET', 'class'=>'navbar-form navbar-left pull-left', 'files'=>true])!!}
                                <div class="form-group">
                                  {!!Form::select('tutoresDisponible',$tutores_options,null,['id'=>'tutoresDisponible','class'=>'form-control'])!!}
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                  <button type="submit" id="asignarTutorEstudiante" class="btn btn-primary">Guardar</button>
                                </div>
                             {!!Form::close()!!}

                           </div>

                         </div>
                       </div>
                     </div>
                   @endforeach
                 @endif

                 @if($proyecto->tipo->id == 1) <!-- Pasantias Comunitarias -->
                   @foreach ($proyecto->estudiantesComunitarias as $estudiante)
                    <tr>
                      <td>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</td>
                      <td>{{$estudiante->matricula}}</td>
                      <td> {{$estudiante->usuario->correo_electronico}}</td>
                      <td>
                        @if($estudiante->tutorProyectoComunitaria != null)
                          {{$estudiante->tutorProyectoComunitaria->usuario->nombres}} {{$estudiante->tutorProyectoComunitaria->usuario->apellidos}}
                          <a href="modal-selectutor-{{ $estudiante->id }}"  data-toggle="modal" data-target="#modal-selecttutor-{{ $estudiante->id }}">(Cambiar Tutor)</a>
                        @else
                          @if(Auth::user()->coordinadorCarreraComunitarias != null || Auth::user()->coordinadorCarreraEmpresariales != null)
                            <a href="modal-selectutor-{{ $estudiante->id }}"  data-toggle="modal" data-target="#modal-selecttutor-{{ $estudiante->id }}">Sin Tutor asignado</a>
                          @else
                            Sin Tutor asignado
                          @endif
                        @endif
                      </td>
                      <td>
                        <a href="{{ route('desasignarproyectoestudiante', [$proyectoEspecifico->id, $estudiante->id]) }}" class="btn btn-danger btn_remove remove-estudiante-proyecto"><span class="glyphicon glyphicon-remove"></span></a>
                      </td>
                    </tr>
                      <!-- Modal -->
                      <div class="modal fade" id="modal-selecttutor-{{ $estudiante->id }}" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title"><b>Asignar Tutor</b></h4>
                            </div>
                            <div class="modal-body">
                              <label>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</label>
                              <br><br>
                              {!!Form::open(['route'=>['asignartutorestudiante',$estudiante->id, $proyecto->id],'method'=>'GET', 'class'=>'navbar-form navbar-left pull-left', 'files'=>true])!!}
                                 <b> Seleccione tutor: </b>
                                 <div class="form-group">
                                   {!!Form::select('tutoresDisponible',$tutores_options,null,['id'=>'tutoresDisponible','class'=>'form-control'])!!}
                                 </div>
                                 <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                   <button type="submit" id="asignarTutorEstudiante" class="btn btn-primary">Guardar</button>
                                 </div>
                              {!!Form::close()!!}

                            </div>

                          </div>
                        </div>
                      </div>
                    @endforeach
                  @endif
              @endif
            </tbody>
          </table>
       </div>
     </div>
  </div>
</div>

<script>
 $('.form-asignarEstudiante').submit(function(event) {
    /* Numero de matricula */
    if ($('#matriculaEstudiante').val() <= 99999999  || $('#matriculaEstudiante').val() > 999999999)  {
        bootbox.alert("Ingrese un número de  matricula válido.");
        $('#matriculaEstudiante').focus();
        event.preventDefault();
        return;
    }
 });

 /* Desasignar a estudiante del proyecto, se muestra ventana de confirmación. */
 $(document).on("click", ".remove-estudiante-proyecto", function(e) {
    var link = $(this).attr("href");
    e.preventDefault();
    bootbox.confirm("¿Esta seguro de des-asignar al estudiante del presente proyecto?.", function(result) {
        if (result) {
            document.location.href = link;
        }
    });
  });
</script>
