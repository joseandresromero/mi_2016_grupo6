{{-- Estudiantes asignados --}}
<h3><b>Estudiantes asignados</b></h3>
<hr>

@if(isset($proyectoEspecifico))
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th width="50%">Estudiante</th>
          <th>Matricula</th>
          <th>Correo Electrónico</th>
        </tr>
      </thead>
      <tbody>
        {{-- Poyecto Comunitario --}}
        @if($proyectoEspecifico->tipo->id == 1 )
          @if(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoComunitaria($proyectoEspecifico->id, Auth::user()->tutor->id)->get()->isEmpty() == false)
            @foreach(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoComunitaria($proyectoEspecifico->id, Auth::user()->tutor->id)->get() as $estudiante)
              <tr>
                <td><a href="{{ route('detalle_estudiante_proyecto', [$estudiante->id, $proyectoEspecifico->id]) }}"> {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a></td>
                <td> {{$estudiante->matricula}}</td>
                <td> {{$estudiante->usuario->correo_electronico}}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="3" style="text-align: center;">
                Usted aún no tiene estudiantes asignados al proyecto.
              </td>
            </tr>
          @endif
        {{-- Poyecto Practica Pre-profesional --}}
        @elseif($proyectoEspecifico->tipo->id == 2 )
          @if(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoEmpresarial($proyectoEspecifico->id, Auth::user()->tutor->id)->get()->isEmpty() == false)
            @foreach(SoftwareVinculos\Models\Estudiante::estudiantesAsignadosTutorProyectoEmpresarial($proyectoEspecifico->id, Auth::user()->tutor->id)->get() as $estudiante)
              <tr>
                <td><a href="{{ route('detalle_estudiante_proyecto', [$estudiante->id, $proyectoEspecifico->id]) }}"> {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</a></td>
                <td> {{$estudiante->matricula}}</td>
                <td> {{$estudiante->usuario->correo_electronico}}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="3" style="text-align: center;">
                Usted aún no tiene estudiantes asignados al proyecto.
              </td>
            </tr>
          @endif
        @endif
      </tbody>
    </table>
  </div>
@endif
