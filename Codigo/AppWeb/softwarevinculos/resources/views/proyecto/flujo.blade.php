@extends('layouts.master')

@section('title','Flujo del Proyecto')

@section('vertical-navbar')
  @include('proyecto.navbar_vertical')
@endsection

@section('vertical-navbar-content')
  <div class="container-tabla">
    <table>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2">
            <div style="display: inline-flex; width: 100%; height: 100px;">
              <div style="width: 100%;">
                <div class="bloque-cuarto"></div>
                <div class="bloque-cuarto"></div>
              </div>
              <div style="width: 100%;">
                <div class="bloque-cuarto"></div>
                <div class="bloque-cuarto corner-top-left"></div>
              </div>
            </div>
          </td>
          <td>
            <div class="bloque {{$estudiantesUVS15->count() > 0 ? ($estudiantesUVS15->count() < $numEstudiantesProyecto ? 'bloque-pendiente' : 'bloque-aprobado') : ''}}">
              <span>FOR-UVS-15 ({{$estudiantesUVS15->count()}}/{{$numEstudiantesProyecto}})</span>
            </div>
          </td>
      </tr>
      <tr>
          <td style="width:15%;">
            <div class="bloque {{($proyectoEspecifico->estado->id == 2 || $proyectoEspecifico->estado->id == 4) ? 'bloque-aprobado' : ''}}" style="position:relative;">
              <span>Creación del proyecto (FOR-UVS-04)</span>
            </div>
          </td>
          <td style="width:6%;">
            <div class="flecha">
              <div class="bloque-cuarto flecha-bottom"></div>
              <div class="bloque-cuarto"></div>
            </div>
          </td>
          <td style="width:15%;">
            <div class="bloque {{$estudiantesUVS01->count() > 0 ? ($estudiantesUVS01->count() < $numEstudiantesProyecto ? 'bloque-pendiente' : 'bloque-aprobado') : ''}}">
              <span>FOR-UVS-01 ({{$estudiantesUVS01->count()}}/{{$numEstudiantesProyecto}})</span>
            </div>
          </td>
          <td style="width:6%;">
            <div class="flecha">
              <div class="bloque-cuarto flecha-bottom"></div>
              <div class="bloque-cuarto"></div>
            </div>
          </td>
          <td style="width:15%;">
            <div class="bloque {{$estudiantesUVS13->count() > 0 ? ($estudiantesUVS13->count() < $numEstudiantesProyecto ? 'bloque-pendiente' : 'bloque-aprobado') : ''}}">

              <span>FOR-UVS-13 ({{$estudiantesUVS13->count()}}/{{$numEstudiantesProyecto}})</span>
            </div>
          </td>
          <td style="width:6%;">
            <div class="flecha">
              <div class="bloque-cuarto flecha-bottom"></div>
              <div class="bloque-cuarto"></div>
            </div>
          </td>
          <td style="width:15%;">
            <div class="bloque {{$estudiantesUVS14->count() > 0 ? ($estudiantesUVS14->count() < $numEstudiantesProyecto ? 'bloque-pendiente' : 'bloque-aprobado') : ''}}">
              <span>FOR-UVS-14 ({{$estudiantesUVS14->count()}}/{{$numEstudiantesProyecto}})</span>
            </div>
          </td>
          <td style="width:6%;"></td>
          <td style="width:15%;"></td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2">
            <div style="display: inline-flex; width: 100%; height: 100px;">
              <div style="width: 100%;">
                <div class="bloque-cuarto"></div>
                <div class="bloque-cuarto"></div>
              </div>
              <div style="width: 100%;">
                <div class="bloque-cuarto corner-bottom-left"></div>
                <div class="bloque-cuarto"></div>
              </div>
            </div>
          </td>
          <td>
            <div class="bloque {{$estudiantesUVS16->count() > 0 ? ($estudiantesUVS16->count() < $numEstudiantesProyecto ? 'bloque-pendiente' : 'bloque-aprobado') : ''}}">
              <span>FOR-UVS-16 ({{$estudiantesUVS16->count()}}/{{$numEstudiantesProyecto}})</span>
            </div>
          </td>
      </tr>
    </table>
  </div>
@endsection
