@extends('layouts.master')

@section('title','FOR-UVS-18')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-18</h2>
      <h3 class="titulo-formulario">INFORME FINAL DEL PROYECTO</h3>
      <hr>

      @if (isset($forUVS18))
        {!!Form::model($forUV18,['route'=>['forUVS18.update',$forUVS18->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS18.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. DATOS GENERALES DEL PROYECTO -->
        <h4 class="titulo-seccion">1. DATOS GENERALES DEL PROYECTO</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Título del proyecto:')!!}
          <p>Talleres a mujeres</p>
        </div>
        <div class="form-group">
          {!!form::label('1.2. Número de personas como Beneficiarios Directos:')!!}
          <p>160</p>
        </div>
        <div class="form-group">
          {!!form::label('1.3. Número de personas como Beneficiarios Indirectos:')!!}
          <p>320</p>
        </div>
        <div class="form-group">
          {!!form::label('1.4. Área geográfica que cubre:')!!}
          <br>
          {!!form::label('Provincia:')!!}
          <p>Guayas</p>
          {!!form::label('Cantón:')!!}
          <p>Guayaquil</p>
          {!!form::label('Código de la zona:')!!}
          <p>Zona 8</p>
          {!!form::label('Código del distrito:')!!}
          <p>09D08</p>
          {!!form::label('Código del circuito:')!!}
          <p>09D08C05</p>
        </div>
        <div class="form-group">
          {!!form::label('1.5. Duración del proyecto:')!!}
          <br>
          {!!form::label('Fecha de Inicio:')!!}
          <p>20/08/2016</p>
          {!!form::label('Fecha de Finalización:')!!}
          <p>29/09/2016</p>
        </div>
        <div class="form-group">
          {!!form::label('1.6. Datos económicos del proyecto:')!!}
          <div class="table-responsive">
            <table class="table table-bordered" id="tabla_actividades_planificadas">
              <tr>
                <th>Valor total presupuestado</th>
                <th>Valor total ejecutado </th>
              </tr>
              <tr>
                <td>{!!form::text('valor_presupuestado',null,['id'=>'valor_presupuestado','class'=>'form-control'])!!}</td>
                <td>{!!form::text('valor_ejecutado',null,['id'=>'valor_ejecutado','class'=>'form-control'])!!}</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="form-group">
          {!!form::label('1.7. Área y sub-área del conocimiento y conocimiento específico:')!!}
          <br>
          {!!form::label('Área del Conocimiento')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          {!!form::label('Sub área del Conocimiento')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          {!!form::label('Sub área Especifica')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('1.8. Nómina de participación de los docentes:')!!}
          <div class="table-responsive">
            <table class="table table-bordered" id="tabla_docentes">
              <tr>
                <th>Función</th>
                <th>Nombres y Apellidos</th>
                <th>Cédula de Ciudadanía</th>
                <th># Horas de Participación</th>
              </tr>
              <tr>
                <td>Docente Responsable de Vinculación con la Sociedad de la Unidad/Carrera</td>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Docente Director/a del Proyecto</td>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Docente Tutor de la práctica pre profesional 1</td>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Docente Tutor de la práctica pre profesional 2</td>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>160</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="form-group">
          {!!form::label('1.9. Nómina de participación de los estudiantes:')!!}
          <div class="table-responsive">
            <table class="table table-bordered" id="tabla_estudiantes">
              <tr>
                <th>Nombres y Apellidos</th>
                <th>Cédula de Ciudadanía</th>
                <th># Matrícula</th>
                <th># Horas de Participación</th>
              </tr>
              <tr>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>042224422</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>042224422</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>042224422</td>
                <td>160</td>
              </tr>
              <tr>
                <td>Jose Andres Romero Trivino</td>
                <td>1234567890</td>
                <td>042224422</td>
                <td>160</td>
              </tr>
            </table>
          </div>
        </div>

        <!-- SECCION 2. ORGANIZACIÓN(ES) BENEFICIARIA(S) DEL PROYECTO -->
        <h4 class="titulo-seccion">2. ORGANIZACIÓN(ES) BENEFICIARIA(S) DEL PROYECTO</h4>
        <hr>
        <div class="form-group">
          {!!form::label('2.1. Razón Social de la organización, institución o comunidad beneficiaria:')!!}
          <p>Hogar de Cristo</p>
        </div>
        <div class="form-group">
          {!!form::label('2.2. Datos Generales de las organizaciones beneficiarias')!!}
          <div class="table-responsive">
            <table class="table table-bordered" id="tabla_estudiantes">
              <tr>
                <th>Dirección Domiciliaria</th>
                <th>Teléfono, Fax, Correo electrónico</th>
                <th>Representante Legal</th>
                <th>Nombre de la persona(s) responsable(s) del seguimiento del programa por parte de la organización beneficiaria</th>
              </tr>
              <tr>
                <td>Monte Sinii</td>
                <td>
                  <p>2045898</p>
                  <p>2888888</p>
                  <p>hdc@gmail.com</p>
                </td>
                <td>Jaime Macias</td>
                <td>Roberto Lopez</td>
              </tr>
            </table>
          </div>
        </div>

        <!-- SECCION 2. ORGANIZACIÓN(ES) BENEFICIARIA(S) DEL PROYECTO -->
        <h4 class="titulo-seccion">3. CONTENIDO DEL INFORME FINAL</h4>
        <hr>
        <div class="form-group">
          {!!form::label('3.1. Antecedentes')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.2. Planteamiento del Problema')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.3. Justificación')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.4. Pertinencia')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.5. Objetivos Generales')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.6. Objetivos Específicos')!!}
          <ul>
            <li>Elaborar todo el material didáctico para los talleres que se impartirán por niveles básico, intermedio y avanzado, con las temáticas: uso del computador, toma y edición de fotos con software libre, administración de redes sociales, y administración de sitios web con Wordpress.</li>
            <li>Elaborar el contenido para la grabación de los videos de los talleres como material adicional de consulta para el grupo de mujeres y como referente para los próximos pasantes politécnicos comunitarios.</li>
          </ul>
        </div>
        <div class="form-group">
          {!!form::label('3.7. Actividades Realizadas')!!}
          <br>
          {!!form::label('Fecha de Inicio de la actividad o servicio:')!!}
          <p>20/08/2016</p>
          {!!form::label('Fecha de finalización de la actividad o servicio:')!!}
          <p>29/09/2016</p>
          {!!form::label('Total de horas empleadas en la actividad o servicio:')!!}
          {!!form::text('horas',null,['id'=>'horas','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('3.8. Análisis y evaluación de la ejecución del proyecto')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.9. Resultados obtenidos en la experiencia')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('3.10. Productos Finales')!!}
          <ul>
            <li>Manuales o Guías</li>
            <li>Capacitaciones</li>
            <li>Software</li>
          </ul>
        </div>
        <div class="form-group">
          {!!form::label('3.11. Conclusiones y Recomendaciones')!!}
          {!!form::textarea('conclusiones_recomendaciones',null,['id'=>'conclusiones_recomendaciones','class'=>'form-control','rows'=>5])!!}
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">

    </script>

@endsection
