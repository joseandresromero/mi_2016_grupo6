@extends('formularios.forUVS01.base')

@section('breadcumb')
  @if (isset($proyecto))
    <ol class="breadcrumb">
      @if($proyecto->tipo->id == 1) <!-- Practica Comunitaria -->
        <li><a href="{{ route('pasantiasComunitarias') }}">Práctica Comunitaria - {{$proyecto->titulo}}</a></li>
      @else
        <li><a href="{{ route('pasantiasEmpresariales') }}">Práctica Pre Profesional - {{$proyecto->titulo}}</a></li>
      @endif

     <li class="active">Edición formulario FOR-UVS-01</li>
    </ol>
  @endif
@endsection

@section('title','FOR-UVS-01')

  @section('comentario_rechazo')
    @if(isset($formularioUVS01) && $formularioUVS01->comentario_rechazo != null)
      <div class="panel panel-warning" style="width: 60%; margin-left: auto; margin-right: auto;">
        <div class="panel-heading">Comentarios</div>
        <div class="panel-body">{{$formularioUVS01->comentario_rechazo}}</div>
      </div>
    @endif
  @endsection

@section('form-start')
  {!!Form::open(array('route'=>array('updateForUVS01',$proyecto->id, $estudiante->id), 'method'=>'PUT', 'class'=>'formulario', 'files'=>true))!!}
@endsection

@section('help-cronograma')
  <span id="btnCronogramaEstudiante" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpCronogramaEstudiante" class="help">
      @if(Auth::user()->director != null)
        Del siguiente cronograma, seleccione las actividades que realizará el estudiante.
      @elseif(Auth::user()->estudiante != null)
        Las siguientes actividades le fueron asignadas por el director del proyecto, puede agregar nuevas subactivadedes a cada una en caso de requerirlo.
      @endif
    </span>
  </span>
@endsection

@section('cronograma-estudiante')
  @if(Auth::user()->director != null)
    @include('formularios.forUVS01.tablaActividadesMacro')
  @elseif(Auth::user()->estudiante != null && $proyecto->tipo->id == 1) <!-- Pasantia Comunitaria -->
    @include('formularios.forUVS01.tablaActividadesMicro')
  @elseif(Auth::user()->estudiante != null && $proyecto->tipo->id == 2) <!-- Pasantia Empresarial -->
    @include('formularios.forUVS01.tablaActividadesEstudiante')
  @endif
@endsection

@section('form-buttons')
  <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
  <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar" style="margin-left: 20px; width: 100px;">Enviar</button>
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection
