@extends('layouts.master')

@section('content')
  @yield('breadcumb')
  
  {!!Html::style('css/formularios.css')!!}
  {!!Html::style('css/datepicker.css')!!}
  {!!Html::script('js/bootstrap-datepicker.js')!!}
  {!!Html::script('js/check-list.js')!!}

  <h2 class="titulo-formulario">FOR-UVS-01</h2>
  <h3 class="titulo-formulario">FORMULACIÓN DE PROYECTOS DE VINCULACIÓN CON LA SOCIEDAD</h3>
  <hr>

  @yield('comentario_rechazo')

  @yield('form-start')

  <!-- SECCION 1. DATOS GENERALES DEL PROYECTO -->
  <h4 class="titulo-seccion">1. DATOS GENERALES DEL PROYECTO</h4>
  <hr>


  <div class="form-group">
    {!!Form::label('1.1. Título de la actividad/servicios:')!!}
    <p>{{ $proyecto->titulo or '' }}</p>

  </div>

  <div class="form-group">
    {!!form::label('1.2. Organización beneficiaria:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.3. Número de personas como beneficiarios directo:')!!}
    <p>{{ $proyecto->numero_beneficiarios_directos or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.4. Número de personas como beneficiarios indirectos:')!!}
    <p>{{ $proyecto->numero_beneficiarios_indirectos or '' }}</p>
  </div>

  <div class="form-group" >
    {!!form::label('1.5.	Area geográfica que cubre:')!!}

    <div class="div-sublabels">

      @if($proyecto->provincia != null)
        <i>{!!form::label('Provincia:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Provincia:')!!}</i>
      @endif
      <p>{{ $proyecto->provincia or '' }}</p>

      @if($proyecto->canton != null)
        <i>{!!form::label('Cantón:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Cantón:')!!}</i>
      @endif
      <p>{{ $proyecto->canton or '' }}</p>


      @if($proyecto->codigo_zona != null)
        <i>{!!form::label('Código de la zona:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código de la zona:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_zona or '' }}</p>


      @if($proyecto->codigo_distrito != null)
        <i>{!!form::label('Código del distrito:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código del distrito:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_distrito or '' }}</p>


      @if($proyecto->codigo_circuito != null)
        <i>{!!form::label('Código del circuito:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código del circuito:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_circuito or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('1.6. Duración de la actividad/servicio:')!!}

    <div class="div-sublabels">
      @if($proyecto->fecha_inicio != null)
        <i>{!!form::label('Fecha de Inicio:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Inicio:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_inicio or '' }}</p>

      @if($proyecto->fecha_inicio != null)
        <i>{!!form::label('Fecha de Finalización:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Finalización:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_finalizacion or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('  1.7.	Área y sub-área del conocimiento y conocimiento específico:')!!}
    <br><br>

    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
           <th > AREA DE CONOCIMIENTO </th>
           <th > SUBAREA DE CONOCIMIENTO </th>
           <th > SUBAREA ESPECÍFICA </th>
        </thead>
        <tbody>
            <tr>
              <td>{{$proyecto->area_conocimiento}}</td>
              <td>{{$proyecto->sub_area_conocimiento}}</td>
              <td>{{$proyecto->sub_area_especifica}}</td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>



  <!-- SECCION 2. IDENTIFICACIÓN DEL PROBLEMA A RESOLVER -->
  <h4 class="titulo-seccion">2. IDENTIFICACIÓN DEL PROBLEMA A RESOLVER</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('2.1. Antecedentes')!!}
    <p>{!! $proyecto->antecedentes or '' !!}</p>
  </div>

  <div class="form-group">
    {!!Form::label('2.1. Definicion del problema')!!}
    <p>{!! $proyecto->definicion_problema or '' !!}</p>
  </div>

  <!-- SECCION 3. OBJETIVOS (General y Específicos) -->
  <h4 class="titulo-seccion">3. OBJETIVOS (General y Específicos)</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('General')!!}
    <p>{{ $proyecto->objetivo_general or '' }}</p>
  </div>

  <div class="form-group">
    {!!Form::label('Específicos')!!}

    <ul style="list-style-type: disc; text-align:justify">
      @foreach($proyecto->objetivosEspecificos as $objetivoEspecifico)
        <li>{{$objetivoEspecifico->descripcion}} </li>
      @endforeach
    </ul>
  </div>

  <!-- SECCION 4. Justificación -->
  <h4 class="titulo-seccion">4. JUSTIFICACIÓN</h4>
  <hr>

  <div class="form-group">
    <p>{!! $proyecto->justificacion or '' !!}</p>
  </div>

  <!-- SECCION 5.	METODOLOGÍA DE TRABAJO (TÉCNICAS Y PROCEDIMIENTOS) -->
  <h4 class="titulo-seccion">5.	METODOLOGÍA DE TRABAJO (TÉCNICAS Y PROCEDIMIENTOS)</h4>
  <hr>

  <div class="form-group">
    <p>{!! $proyecto->metodologia or '' !!}</p>
  </div>


  <!-- SECCION 6. CRONOGRAMA -->
  <div class="form-group">
    {!!form::label('6. CRONOGRAMA', null, ['class'=>'titulo-seccion'])!!}
    @yield('help-cronograma')
  </div>
  <hr>

  @yield('cronograma-estudiante')

  @yield('form-buttons')

  @yield('form-end')

@endsection
