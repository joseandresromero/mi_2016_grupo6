<div id="actividadesProyecto">
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr style="background-color: rgb(78, 129, 106);" >
          <th width="50%">Actividades</th>
          <th width="15%">Duración (Horas)</th>
          <th width="15%">Inicio</th>
          <th width="15%">Fin</th>
          <th width="5%"></th>
      </tr>
      </thead>
    <table>
  <div>
  @foreach ($objetivosEspecificos as $index => $objetivoEspecifico)
    <div class="table-responsive">
      <table class="table table-bordered" id="tabla_objetivo-{{$objetivoEspecifico->id}}">
        <thead>
          <tr style="background-color: rgb(150, 206, 185)">
            <th > {{$objetivoEspecifico->descripcion}} </th>
            <th colspan="4"> </th>
          </tr>
        </thead>
        <tbody>
          @foreach($actividadesTotales[$index] as $actividad)
            <tr>
              <td width="50%">{{$actividad->actividad->descripcion}}</td>
              <td width="15%">{{$actividad->actividad->duracion_horas}}</td>
              <td width="15%">{{$actividad->actividad->fecha_inicio}}</td>
              <td width="15%">{{$actividad->actividad->fecha_finalizacion}}</td>
              <td width="5%">
                @if ($actividad->seleccionada == true)
                  <input type="checkbox" name="actividadesObjetivo-{{$objetivoEspecifico->id}}[]" value="{{$actividad->actividad->id}}" checked="checked">
                @else
                  <input type="checkbox" name="actividadesObjetivo-{{$objetivoEspecifico->id}}[]" value="{{$actividad->actividad->id}}" >
                @endif
              </td>
            </tr>
            {{-- Row para ingresar subactividades de la actividad --}}
            {{--
            <tr>
              <td colspan="5">
                Subactividades actividad: {{$actividad->actividad->descripcion}}
                <div class="table-responsive">
                  <table class="table table-bordered" id="tabla_actividades_objetivoEspecificoSaved">
                    <tr id="row-AOESaved">
                     <td width="50%">
                       <input type="textarea" id="row-actOESaved" rows="2" class="form-control actividad-obj", style="resize:none; border:0;" placeholder="Descripcion subactividad"/>
                     </td>
                     <td width="15%">
                       <input type="text" class="form-control" name="horasActividadObjetivoEspecificoSaved" style="border:0" placeholder="Duracion(horas)"/>
                     </td>
                     <td  width="15%">
                       <input type="date" class="form-control fecha_picker" name="fechaInicioActividadObjetivoEspecificoSaved" style="border:0" placeholder="Fecha Inicio"/>
                     </td>
                     <td  width="15%">
                       <input type="date" class="form-control fecha_picker" name="fechaFinActividadObjetivoEspecificoSaved" style="border:0" placeholder="Fecha Fin"/>
                     </td>
                     <td width="5%">
                       <button type="button" name="remove" id="AOESaved" class="btn btn-danger btn_remove_actividad small-button"><span class="glyphicon glyphicon-remove"></span></button>
                     </td>
                   </tr>
                  </table>
                </div>
                <button type="button" name="addActividadObjEsp" id="addActividadObjEsp" value="Saved" class="btn btn-success btn_add_subactividad small-button">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </td>
            </tr>
            --}}
          @endforeach
        </tbody>
      </table>
    </div>
  @endforeach
</div>
