@extends('formularios.forUVS01.base')

@section('title','FOR-UVS-01 - Vista previa')

@section('form-start')
  {!!Form::model($formulario,['route'=>['revisar_foruvs01',$formulario->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('cronograma-estudiante')
  <div id="actividadesProyecto">
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr style="background-color: rgb(78, 129, 106);" >
            <th width="55%">Actividades</th>
            <th width="15%">Duración (Horas)</th>
            <th width="15%">Inicio</th>
            <th width="15%">Fin</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($objetivosEspecificos as $objetivoEspecifico)
            @if(\SoftwareVinculos\Models\Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivoEspecifico->id)->get()->isEmpty() == false)
              <tr style="background-color: rgb(150, 206, 185)">
                <td > {{$objetivoEspecifico->descripcion}} </td>
                <td colspan="3"> </td>
              </tr>
              @foreach(\SoftwareVinculos\Models\Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivoEspecifico->id)->get() as $actividad)
                <tr style="background-color: rgb(209, 248, 219);">
                  <td > {{$actividad->descripcion}} </td>
                  <td >{{$actividad->duracion_horas}}</td>
                  <td >{{$actividad->fecha_inicio}}</td>
                  <td >{{$actividad->fecha_finalizacion}}</td>
                </tr>
                {{-- Se muestran subactividades de la actividad, en caso de tenerlas --}}
                @if(\SoftwareVinculos\Models\Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividad)->get()->isEmpty() == false)
                  @foreach(\SoftwareVinculos\Models\Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividad)->get() as $subactividad)
                    <tr>
                      <td > <li style="margin-left: 20px">{{$subactividad->descripcion}} </li></td>
                      <td >{{$subactividad->duracion_horas}}</td>
                      <td >{{$subactividad->fecha_inicio}}</td>
                      <td >{{$subactividad->fecha_finalizacion}}</td>
                    </tr>
                  @endforeach
                @endif
              @endforeach
            @endif
          @endforeach
        </tbody>
      <table>
    <div>
@endsection


@section('form-buttons')
  @if(isset($mostrarBotones) && $mostrarBotones == true)
    <br><br>
    <button class="btn btn-success" type="submit" name="submit"  value="aprobar_foruvs01" style="margin-top: 0px; width: 120px;">Aprobar</button>
    <button type="button" class="btn btn-danger" name="submit" value="rechazar" style="margin-left: 30px; width: 120px;" data-toggle="modal" data-backdrop="static" data-target="#escribirComentario">Rechazar</button>

    <!-- Modal -->
    <div class="modal fade" id="escribirComentario" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><b>Comentario</b></h4>
          </div>
          <div class="modal-body">
            <textarea name="comentario_rechazo" id="comentario_rechazo" rows="4" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" name="submit" value="rechazar_formulario">Enviar</button>
          </div>
        </div>

      </div>
    </div>
  @endif
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection
