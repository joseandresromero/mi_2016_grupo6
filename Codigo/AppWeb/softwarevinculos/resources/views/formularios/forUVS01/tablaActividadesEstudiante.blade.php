
@if(isset($objetivosEspecificos))
  @foreach ($objetivosEspecificos as $objetivoEspecifico)
    <div class="table-responsive" style="margin-top: 25px">
      <table class="table table-bordered" id="tabla-actividades-cronograma-objetivospecific{{$objetivoEspecifico->id}}">
        <thead>
          <tr>
            <th colspan="5" name="tablecomponente">
              <p>{{$objetivoEspecifico->descripcion}}</p>
            </th>
          </tr>
          <tr>
            <th width="50%">Actividades </th>
            <th width="10%">Horas</th>
            <th width="32%">Fecha de Inicio - Fin</th>
            <th >Accion</th>
          </tr>
        </thead>
        <tbody>
          @foreach(\SoftwareVinculos\Models\Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivoEspecifico->id)->get() as $actividad)
            <tr id="rowactividad-cronograma-AOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}">
              <td>
                <input class="form-control" name="descripcionActividadObjetivoEspecificoSaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" rows="2" value="{{$actividad->descripcion}}"/>
              </td>
              <td>
                <input type="number" class="form-control" name="horasActividadObjetivoEspecificoSaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" style="border:0" value="{{$actividad->duracion_horas}}"/>
              </td>
              <td>
                {!!form::text("rangoFechaActividadObjetivoEspecificoSaved".$objetivoEspecifico->id."-".$actividad->id, date("d/m/Y", strtotime($actividad->fecha_inicio))." - ".date("d/m/Y", strtotime($actividad->fecha_finalizacion)),["class"=>"form-control date_ranger_picker"])!!}
              </td>
              <td>
                <button type="button" name="remove" value="Saved{{$objetivoEspecifico->id}}-{{$actividad->id}}" class="btn btn-danger btn_remove_actividad"><span class="glyphicon glyphicon-remove"></span></button>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <button type="button" value="specific{{$objetivoEspecifico->id}}" class="btn btn-success btn_add_actividad">
        <span class="glyphicon glyphicon-plus"></span>
      </button>
      <hr style="border-top: 3px solid #eee;">
    </div>
  @endforeach
@endif

<script type="text/javascript">
  $('input[class*="date_ranger_picker"]').daterangepicker({
    locale: {
      format: 'DD/MM/YYYY',
    }
  });

  var numActividad = 0;
  $(document).on('click', '.btn_add_actividad', function(){
    var button_value = $(this).attr("value");
    numActividad++;

    $('#tabla-actividades-cronograma-objetivo'+button_value+' tbody').append(
     '<tr id="rowactividad-cronograma-AOE'+button_value+'-'+numActividad+'">' +
       '<td>' +
         '<input class="form-control" name="descripcionActividadesObjetivo'+button_value+'[]" rows="3" style="border:0"></input>'+
       '</td>'+
       '<td>'+
         '<input type="number" class="form-control" name="horasActividadesObjetivo'+button_value+'[]" style="border:0"/>'+
       '</td>'+
       '<td>'+
         '<input type="text" class="form-control date_ranger_picker" name="rangoFechaActividadesObjetivo'+button_value+'[]" style="border:0"/>'+
       '</td>'+
       '<td>'+
         '<button type="button" value="'+button_value+'-'+numActividad+'" name="remove" class="btn btn-danger btn_remove_actividad"><span class="glyphicon glyphicon-remove"></span></button>'+
       '</td>'+
     '</tr>');

     $('input[class*="date_ranger_picker"]').daterangepicker({
       locale: {
         format: 'DD/MM/YYYY',
       }
     });
  });

  /*
  * Se elimina la actividad seleccionada
  */
  $(document).on('click', '.btn_remove_actividad', function(){
    var button_value = $(this).attr("value");

    bootbox.confirm("¿Está seguro de eliminar la actividad?", function(result) {
      if(result == true){
        /* Se elimina actividad de la sección del cronograma */
        $('#rowactividad-cronograma-AOE'+button_value+'').remove();
      }
    });
  });


</script>
