<div id="actividadesProyecto">
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr style="background-color: rgb(78, 129, 106);" >
          <th width="50%">Actividades</th>
          <th width="15%">Duración (Horas)</th>
          <th width="30%">Fecha Inicio - Fin</th>
          <th width="10%"></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($objetivosEspecificos as $objetivoEspecifico)
        @if(\SoftwareVinculos\Models\Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivoEspecifico->id)->get()->isEmpty() == false)
          <tr style="background-color: rgb(150, 206, 185)">
            <td > {{$objetivoEspecifico->descripcion}} </td>
            <td colspan="4"> </td>
          </tr>
          @foreach(\SoftwareVinculos\Models\Actividad::actividadAsignadaEstudiantePorObjetivo($estudiante, $proyecto, $objetivoEspecifico->id)->get() as $actividad)
            <tr style="background-color: rgb(209, 248, 219);">
              <td > {{$actividad->descripcion}} </td>
              <td >{{$actividad->duracion_horas}}</td>
              <td >{{$actividad->fecha_inicio}} - {{$actividad->fecha_finalizacion}}</td>
              <td></td>
            </tr>
            {{-- Row para ingresar subactividades de la actividad --}}
            <tr>
              <td colspan="5">
                Subactividades:
                <div class="table-responsive">
                  <table class="table table-bordered" id="tabla_subactividades_actividad{{$actividad->id}}">
                    @foreach(\SoftwareVinculos\Models\Actividad::subActividadesActividadMacroEstudiante($estudiante, $proyecto, $actividad)->get() as $subactividad)
                      <tr id="row-subactividad-Saved{{$actividad->id}}-{{$subactividad->id}}">
                        <td width="50%">
                          <input type="textarea" name="descripcionSubActividadSaved{{$actividad->id}}-{{$subactividad->id}}" rows="2" class="form-control actividad-obj", style="resize:none; border:0;" placeholder="Descripcion subactividad" value="{{$subactividad->descripcion}}"/>
                        </td>
                        <td width="15%">
                          <input type="number" class="form-control" name="horasSubActividadSaved{{$actividad->id}}-{{$subactividad->id}}" style="border:0" placeholder="Duracion(horas)" value="{{$subactividad->duracion_horas}}"/>
                        </td>
                        <td width="30%">
                          {!!form::text("rangoFechasSubActividadSaved".$actividad->id."-".$subactividad->id, date("d/m/Y", strtotime($subactividad->fecha_inicio))." - ".date("d/m/Y", strtotime($subactividad->fecha_finalizacion)),["class"=>"form-control date_ranger_picker", "style"=>"border:0;"])!!}
                        </td>
                        <td width="4%">
                          <button type="button" name="remove" value="Saved{{$actividad->id}}-{{$subactividad->id}}" class="btn btn-danger btn_remove_subactividad small-button"><span class="glyphicon glyphicon-remove"></span></button>
                        </td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                <button type="button" name="addSubActividad" id="addSubactividad-{{$actividad->id}}" value="actividad{{$actividad->id}}" class="btn btn-success btn_add_subactividad small-button">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </td>
            </tr>
          @endforeach
        @endif
      @endforeach
    </tbody>
    <table>
  <div>
</div>

<script type="text/javascript">
  var numSubActividad = 0;
  $(document).on('click', '.btn_add_subactividad', function(){
    var button_value = $(this).attr("value");
    numSubActividad++;

    /* Se agrega actividad en matriz de marco lógico */
    $('#tabla_subactividades_'+button_value+'').append(
      '<tr id="row-subactividad-'+button_value+'-'+numSubActividad+'">' +
        '<td width="50%">' +
          '<input type="text" class="form-control" name="descripcionSubActividades-'+button_value+'[]" rows="2" style="border:0"></input>'+
        '</td>'+
        '<td width="15%">'+
          '<input type="number" class="form-control" name="horasSubActividades-'+button_value+'[]" style="border:0"/>'+
        '</td>'+
        '<td width="30%">'+
          '<input type="text" class="form-control date_ranger_picker" name="rangoFechasSubactividades-'+button_value+'[]" style="border:0"/>'+
        '</td>'+
        '<td width="4%">'+
          '<button type="button" value="'+button_value+'-'+numSubActividad+'" name="remove" class="btn btn-danger btn_remove_subactividad small-button"><span class="glyphicon glyphicon-remove"></span></button>'+
        '</td>'+
      '</tr>');

      $('input[class*="date_ranger_picker"]').daterangepicker({
        locale: {
          format: 'DD/MM/YYYY',
        }
      });


  });

  /*
  * Se elimina la actividad seleccionada
  */
  $(document).on('click', '.btn_remove_subactividad', function(){
    var button_value = $(this).attr("value");

    bootbox.confirm("¿Está seguro de eliminar la actividad?", function(result) {
      if(result == true){
        /* Se elimina actividad de la sección del cronograma */
        $('#row-subactividad-'+button_value+'').remove();
      }
    });
  });

  $('input[class*="date_ranger_picker"]').daterangepicker({
    locale: {
      format: 'DD/MM/YYYY',
    }
  });

</script>
