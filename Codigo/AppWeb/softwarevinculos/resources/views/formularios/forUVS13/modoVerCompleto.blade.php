@extends('layouts.master')

@section('title','FOR-UVS-13')

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}

    <div class="container" style="margin-bottom: 70px;">
      <h2 class="titulo-formulario">FOR-UVS-13</h2>
      <h3 class="titulo-formulario">INFORME PARCIAL SEGUIMIENTO DE ACTIVIDADES TUTORIADAS</h3>
      <hr>

        <div class="table-responsive" style="margin-top:40px">
          <table class="table table-bordered" id="tabla_actividades">
            <tr>
              <th colspan="8">
                DOCENTES:
                @foreach($proyecto->tutores as $tutor)
                 {{$tutor->usuario->nombres}} {{$tutor->usuario->apellidos}} /
                @endforeach
              </th>
              {{--
              <th colspan="8">DOCENTE: {{$forUVS13->creadoPor->tutor->usuario->nombres}} {{$forUVS13->creadoPor->tutor->usuario->apellidos}}</th>
              --}}
            </tr>
            <tr>
              <th colspan="3">FACULTAD: {{$carreraPrincipal->unidadAcademica->nombre}}</th>
              <th colspan="5">CARRERA: {{$carreraPrincipal->nombre}}</th>
            </tr>
            <tr>
              <th>NOMBRES Y APELLIDOS DE LOS ESTUDIANTES</th>
              <th>ORGANIZACIÓN BENEFICIARIA/EMPRESA</th>
              <th>NOMBRE DEL PROYECTO/SERVICIO/ACTIVIDAD ESPECÍFICA</th>
              <th>FECHA</th>
              <th>ACTIVIDADES CUMPLIDAS A LA FECHA</th>
              <th>SUGERENCIAS Y RECOMENDACIONES</th>
              <th>FIRMA DE ESTUDIANTE</th>
              <th>FIRMA DEL TUTOR</th>
            </tr>

            @foreach($formularios as $formulario)
              @foreach(\SoftwareVinculos\Models\Actividad::allActividadesEstudianteProyecto($formulario->formularioUVS13->estudiante, $formulario->proyecto)->get() as $actividad)
                @if($actividad->actividad_completada)
                  <tr>
                    <td>{{$formulario->formularioUVS13->estudiante->usuario->nombres}} {{$formulario->formularioUVS13->estudiante->usuario->nombres}}</td>
                    <td>{{$formulario->proyecto->organizacion->nombre}}</td>
                    <td>{{$formulario->proyecto->titulo}}</td>
                    <td>21/10/2016</td>
                    <td>{{$actividad->descripcion or ''}}</td>
                    <td>{{$actividad->sugerencia or ''}}</td>
                    <td></td>
                    <td></td>
                  </tr>
                @endif
              @endforeach
            @endforeach
          </table>
        </div>
      </form>
    </div>
@endsection
