@extends('layouts.master')

@section('title','FOR-UVS-13')

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{ route('formulariosDisponiblesTutor', $forUVS13->proyecto->id) }}">{{$forUVS13->proyecto->titulo}}</a></li>
     <li class="active">Editar Formulario FOR-UVS-13</li>
    </ol>

    <div class="container" style="margin-bottom: 70px;">
      <h2 class="titulo-formulario">FOR-UVS-13</h2>
      <h3 class="titulo-formulario">INFORME PARCIAL SEGUIMIENTO DE ACTIVIDADES TUTORIADAS</h3>
      <hr>

      <form action="{{ route('guardarForUVS13PorEstudiante', ['id_formulario'=>$forUVS13->id,'id_estudiante'=>$estudiante->id]) }}" method="post">

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. INFORMACIÓN GENERAL-->
        <h4 class="titulo-seccion">1. INFORMACIÓN GENERAL</h4>
        <hr>
        <div class="form-group">
          <label for="">Docente (Tutor)</label>
          <p>{{$forUVS13->creadoPor->tutor->usuario->nombres}} {{$forUVS13->creadoPor->tutor->usuario->apellidos}}</p>
        </div>
        <div class="form-group">
          <label for="">Facultad</label>
          <p>{{$carreraPrincipal->unidadAcademica->nombre}}</p>
        </div>
        <div class="form-group">
          <label for="">Carrera</label>
          <p>{{$carreraPrincipal->nombre}}</p>
        </div>
        <div class="form-group">
          <label for="">Nombres y Apellidos del estudiante</label>
          <p>{{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}</p>
        </div>
        <div class="form-group">
          <label for="">Organización Beneficiaria</label>
          <p>{{$forUVS13->proyecto->organizacion->nombre}}</p>
        </div>
        <div class="form-group">
          <label for="">Nombre del proyecto / Actividad específica</label>
          <p>{{$forUVS13->proyecto->titulo}}</p>
        </div>

        <!-- SECCION 1. TABLA DE ACTIVIDADES -->
        <h4 class="titulo-seccion">2. TABLA DE ACTIVIDADES</h4>
        <hr>
        <div class="table-responsive" style="margin-top:40px">
          <table class="table table-bordered" id="tabla_actividades">
            <tr>
              <th width='20%'>Fecha</th>
              <th width='40%'>Actividades cumplidas a la fecha</th>
              <th width='30%'>Sugerencias</th>
              <th>Cumplida?</th>
            </tr>
            @foreach($actividadesEstudianteProyecto as $actividad)
              <tr>
                <td>21/10/2016</td>
                <td>{{$actividad->descripcion or ''}}</td>
                <td>
                  <textarea name="sugerencia-{{$actividad->id}}" rows="3" class="form-control name_list" style="resize: none;">{{$actividad->sugerencia or ''}}</textarea>
                </td>
                <td>
                  @if ($actividad->actividad_completada == false)
                    <input type="checkbox" name="actividadesCumplidas[]" value="{{$actividad->id}}">
                  @else
                    <input type="checkbox" name="actividadesCumplidas[]" value="{{$actividad->id}}" checked="checked">
                  @endif
                </td>
              </tr>
            @endforeach

          </table>
        </div>

        @if($forUVS13->estado->id != 3) <!-- Formulario  aún no ha sido marcado como revisado -->
          <div class="form-group" style="margin-top:60px;">

            <b style="font-size:17px;">Marcar formulario como revisado? </b>
            @if($estudiante->revisado_for_uvs_13 == true)
              <input type="checkbox" name="revisado_for_uvs_13" checked="checked" class="linea-accion">
            @else
              <input type="checkbox" name="revisado_for_uvs_13" class="linea-accion">
            @endif
          </div>
        @endif

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar" style="margin-top:10px;">Guardar</button>
        {{--<button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>--}}


      </form>
    </div>

@endsection
