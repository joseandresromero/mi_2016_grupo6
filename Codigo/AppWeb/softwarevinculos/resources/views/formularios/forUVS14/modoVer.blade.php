@extends('formularios.forUVS14.base')

@section('title','FOR-UVS-14 - Vista Previa')

@section('form-start')
  {!!Form::model($formulario,['route'=>['revisar_foruvs14',$formulario->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('analisis_actividades')
  <p>{!!$formulario->formulariosUVS14()->first()->analisis_evaluacion_actividades or '' !!}</p>
@endsection

@section('conclusiones')
  <p>{!!$formulario->formulariosUVS14()->first()->conclusiones or '' !!}</p>
@endsection

@section('recomendaciones')
  <p>{!!$formulario->formulariosUVS14()->first()->recomendaciones or '' !!}</p>
@endsection

@section('form-buttons')
  @if(isset($mostrarBotones) && $mostrarBotones == true)
    <br><br>
    <button class="btn btn-success" type="submit" name="submit"  value="aprobar_foruvs14" style="margin-top: 0px; width: 120px;">Aprobar</button>
    <button type="button" class="btn btn-danger" name="submit" value="rechazar" style="margin-left: 30px; width: 120px;" data-toggle="modal" data-backdrop="static" data-target="#escribirComentario">Rechazar</button>

    <!-- Modal -->
    <div class="modal fade" id="escribirComentario" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><b>Comentario</b></h4>
          </div>
          <div class="modal-body">
            <textarea name="comentario_rechazo" id="comentario_rechazo" rows="4" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" name="submit" value="rechazar_formulario">Enviar</button>
          </div>
        </div>

      </div>
    </div>
  @endif
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection
