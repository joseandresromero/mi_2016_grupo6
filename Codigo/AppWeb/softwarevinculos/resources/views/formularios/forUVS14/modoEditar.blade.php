@extends('formularios.forUVS14.base')

@section('breadcumb')
  @if (isset($proyecto))
    <ol class="breadcrumb">
      @if($proyecto->tipo->id == 1) <!-- Practica Comunitaria -->
        <li><a href="{{ route('pasantiasComunitarias') }}">Práctica Comunitaria - {{$proyecto->titulo}}</a></li>
      @else
        <li><a href="{{ route('pasantiasEmpresariales') }}">Práctica Pre Profesional - {{$proyecto->titulo}}</a></li>
      @endif

     <li class="active">Edición Formulario FOR-UVS-14</li>
    </ol>
  @endif
@endsection

@section('title','FOR-UVS-14')

@section('comentario_rechazo')
  @if(isset($formulario) && $formulario->comentario_rechazo != null)
    <div class="panel panel-warning" style="width: 60%; margin-left: auto; margin-right: auto;">
      <div class="panel-heading">Comentarios</div>
      <div class="panel-body">{{$formulario->comentario_rechazo}}</div>
    </div>
  @endif
@endsection

@section('form-start')
  {!!Form::model($formulario->formulariosUVS14()->first(),['route'=>['foruvs14.update', $formulario->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('analisis_actividades')
  {!!Form::textarea('analisis_evaluacion_actividades',null,['id'=>'analisis_evaluacion_actividades','class'=>'form-control','rows'=>4, 'style'=>'resize:none;'])!!}
@endsection

@section('conclusiones')
  {!!form::textarea('conclusiones',null,['id'=>'conclusiones','class'=>'form-control','rows'=>3, 'style'=>'resize:none;'])!!}
@endsection

@section('recomendaciones')
    {!!form::textarea('recomendaciones',null,['id'=>'recomendaciones','class'=>'form-control','rows'=>3, 'style'=>'resize:none;'])!!}
@endsection

@section('form-buttons')
  <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
  <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar" style="margin-left: 20px; width: 100px;">Enviar</button>
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection

@section('javascript-code')
  <script type="text/javascript">
    $('#analisis_evaluacion_actividades').ckeditor();
    $('#conclusiones').ckeditor();
    $('#recomendaciones').ckeditor();
  </script>
@endsection
