@extends('layouts.master')

@section('content')

  @yield('breadcumb')

  {!!Html::style('css/formularios.css')!!}
  {!!Html::style('css/datepicker.css')!!}
  {!!Html::script('js/bootstrap-datepicker.js')!!}
  {!!Html::script('js/check-list.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}

  <h2 class="titulo-formulario">FOR-UVS-14</h2>
  <h3 class="titulo-formulario">INFORME FINAL DEL PRACTICANTE</h3>
  <hr>

  @yield('comentario_rechazo')

  @yield('form-start')

  <!-- SECCION 1.	INFORMACIÓN DEL PRACTICANTE -->
  <h4 class="titulo-seccion">1.	INFORMACIÓN DEL PRACTICANTE</h4>
  <hr>


  <div class="form-group">
    {!!Form::label('1.1. Nombres y Apellidos del practicante:')!!}
    <p>{{ $formulario->creadoPor->nombres  or '' }} {{ $formulario->creadoPor->apellidos  or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.2. numero de matricula:')!!}
    <p>{{$formulario->creadoPor->estudiante->matricula or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.4. Correo electrónico:')!!}
    <p>{{$formulario->creadoPor->correo_electronico or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.5. Número de teléfono:')!!}
    <p>{{$formulario->creadoPor->telefono_fijo or '' }}</p>
  </div>

  <!-- SECCION 2.	INFORMACIÓN DEL DOCENTE TUTOR -->
  <h4 class="titulo-seccion">2.	INFORMACIÓN DEL DOCENTE TUTOR</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('2.1. Nombres y Apellidos del tutor:')!!}
    <p>{{ $tutor->usuario->nombres  or '' }} {{ $tutor->usuario->apellidos  or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('2.2.. Correo electrónico:')!!}
    <p>{{$tutor->usuario->correo_electronico or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('2.3. Número de teléfono:')!!}
    <p>{{$tutor->usuario->telefono_fijo or '' }}</p>
  </div>

  <!-- SECCION 3.	INFORMACIÓN DE LA ORGANIZACIÓN/EMPRESA -->
  <h4 class="titulo-seccion">3.	INFORMACIÓN DE LA ORGANIZACIÓN/EMPRESA</h4>
  <hr>

  <div class="form-group">
    {!!form::label('3.1. Número de personas como beneficiarios directo:')!!}
    <p>{{ $proyecto->numero_beneficiarios_directos or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('3.2. Número de personas como beneficiarios indirectos:')!!}
    <p>{{ $proyecto->numero_beneficiarios_indirectos or '' }}</p>
  </div>

  <div class="form-group" >
    {!!form::label('3.3.	Area geográfica que cubre la actividad/servicio realizado:')!!}

    <div class="div-sublabels">

      @if($proyecto->provincia != null)
        <i>{!!form::label('Provincia:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Provincia:')!!}</i>
      @endif
      <p>{{ $proyecto->provincia or '' }}</p>

      @if($proyecto->canton != null)
        <i>{!!form::label('Cantón:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Cantón:')!!}</i>
      @endif
      <p>{{ $proyecto->canton or '' }}</p>


      @if($proyecto->codigo_zona != null)
        <i>{!!form::label('Código de la zona:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código de la zona:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_zona or '' }}</p>


      @if($proyecto->codigo_distrito != null)
        <i>{!!form::label('Código del distrito:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código del distrito:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_distrito or '' }}</p>


      @if($proyecto->codigo_circuito != null)
        <i>{!!form::label('Código del circuito:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Código del circuito:')!!}</i>
      @endif
      <p>{{ $proyecto->codigo_circuito or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('3.4. Razón Social de la Organización/Empresa Beneficiaria:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('3.5. Dirección:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->direccion or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('3.6.	Teléfono, Fax, Correo electrónico:')!!}

  <div class="div-sublabels">
      @if($proyecto->delegados->first()->organizacion->telefono != null)
        <i>{!!form::label('Telefono:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Telefono:')!!}</i>
      @endif
      <p>{{$proyecto->delegados->first()->organizacion->correo_electronico or '' }}</p>

      @if($proyecto->delegados->first()->organizacion->correo_electronico != null)
        <i>{!!form::label('Correo:', null, ['class' => 'sub-label'])!!}</i>
      @else
       <i>{!!form::label('Correo:')!!}</i>
      @endif
      <p>{{$proyecto->delegados->first()->organizacion->fax or '' }}</p>

      @if($proyecto->delegados->first()->organizacion->fax != null)
        <i>{!!form::label('Fax:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fax:')!!}</i>
      @endif
      <p>{{$proyecto->delegados->first()->organizacion->fax or '' }}</p>

  </div>

  <div class="form-group">
    {!!form::label('3.7.	Nombre del Representante Legal de la organización/empresa:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre_representante_legal or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('3.8.	Nombre de la persona o personas responsables del seguimiento de la actividad o servicio por parte de la organización/empresa:')!!}
    <p>
      @if($proyecto->delegados != null)
        <ul>
          @foreach($proyecto->delegados as $delegado)
            <li>{{$delegado->nombre or '' }}</li>
          @endforeach
        </ul>
      @endif
    </p>
  </div>

    <!-- SECCION 4.	CONTENIDO DEL INFORME FINAL -->
  <h4 class="titulo-seccion">4.	CONTENIDO DEL INFORME FINAL</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('4.1. Planteamiento del problema')!!}
    <p>{!! $proyecto->definicion_problema or '' !!}</p>
  </div>

  <div class="form-group">
    {!!Form::label('4.2. Justificación')!!}
    <p>{!! $proyecto->justificacion or '' !!}</p>
  </div>

  <div class="form-group">
    {!!Form::label('4.3. Objetivos Generales')!!}
    <p>{{ $proyecto->objetivo_general or '' }}</p>
  </div>

  <div class="form-group">
    {!!Form::label('4.4. Objetivos Especificos')!!}

    <ul style="list-style-type: disc; text-align:justify">
      @foreach($proyecto->objetivosEspecificos as $objetivoEspecifico)
        <li>{{$objetivoEspecifico->descripcion}} </li>
      @endforeach
    </ul>
  </div>

  <div class="form-group">
    {!!form::label('4.5. Actividades Realizadas')!!}

    <div class="div-sublabels">
      @if($proyecto->fecha_inicio != null)
        <i>{!!form::label('Fecha de Inicio:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Inicio:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_inicio or '' }}</p>

      @if($proyecto->fecha_finalizacion != null)
        <i>{!!form::label('Fecha de Finalización:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Finalización:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_finalizacion or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('4.6.	Análisis y evaluación de las actividades o servicios realizados')!!}
    @yield('analisis_actividades')
  </div>

  <div class="form-group">
    {!!form::label('4.7.	Conclusiones y Recomendaciones')!!}
    <br>
    {!!form::label('Conclusiones:')!!}
    @yield('conclusiones')

    {!!form::label('Recomendaciones:')!!}
    @yield('recomendaciones')
  </div>

  @yield('form-buttons')

  @yield('form-end')

  @yield('javascript-code')
@endsection
