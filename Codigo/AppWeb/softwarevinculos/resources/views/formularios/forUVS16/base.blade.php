@extends('layouts.master')

@section('content')

  @yield('breadcumb')
  
  {!!Html::style('css/formularios.css')!!}
  {!!Html::style('css/datepicker.css')!!}
  {!!Html::script('js/bootstrap-datepicker.js')!!}
  {!!Html::script('js/check-list.js')!!}

  {!!Html::script('http://code.jquery.com/jquery-1.10.2.js')!!}
  {!!Html::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')!!}
  {!!Html::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css')!!}

  <h2 class="titulo-formulario">FOR-UVS-16</h2>
  <h3 class="titulo-formulario">INFORME FINAL DEL DOCENTE TUTOR</h3>
  <hr>

  @yield('comentario_rechazo')

  @yield('form-start')

  <!-- SECCION 1.	DATOS GENERALES DE LA PRÁCTICA -->
  <h4 class="titulo-seccion">1.	DATOS GENERALES DE LA PRÁCTICA</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('1.1.	Nombres y Apellidos del practicante:')!!}
    <p>{{ $formulario->formularioUVS16->estudiante->usuario->nombres  or '' }} {{$formulario->formularioUVS16->estudiante->usuario->apellidos  or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.2.	Nombre de la carrera:')!!}
    <p></p>
  </div>

  <div class="form-group">
    {!!form::label('1.3.	Nombre de la facultad:')!!}
    <p></p>
  </div>

  <div class="form-group">
    {!!form::label('1.4.	Fecha de inicio y fin de la práctica:')!!}
    <div class="div-sublabels">
      @if($proyecto->fecha_inicio != null)
        <i>{!!form::label('Fecha de Inicio:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Inicio:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_inicio or '' }}</p>

      @if($proyecto->fecha_finalizacion != null)
        <i>{!!form::label('Fecha de Finalización:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Finalización:')!!}</i>
      @endif
      <p>{{$proyecto->fecha_finalizacion or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('1.5.	Nombre del Proyecto asociado a la práctica:')!!}
    <p>{{$proyecto->titulo or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.6.	Nombre de la Organización Beneficiaria:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre or '' }}</p>
  </div>

  <!-- SECCION 2. ACTIVIDADES PLANIFICADAS -->
  <h4 class="titulo-seccion">2. ACTIVIDADES PLANIFICADAS</h4>
  <hr>
  @yield('actividades_planificadas')

  <!-- SECCION 3. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE -->
  <h4 class="titulo-seccion">3. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE</h4>
  <hr>

  @yield('evaluacion_resultados_aprendizaje')


  <!-- SECCION 4. EVALUACIÓN GENERAL DE LA PRÁCTICA: -->
  <h4 class="titulo-seccion">4. EVALUACIÓN GENERAL DE LA PRÁCTICA:</h4>
  <hr>

  <div class="form-group">
    {!!form::label('4.1. Aprobación:')!!}
    @yield('aprobacion')
  </div>

  <div class="form-group">
    {!!form::label('4.2. Número de horas de prácticas pre profesionales validadas:')!!}
    @yield('numero_horas_practica')
  </div>

  <div class="form-group">
    {!!form::label('4.3. Tipo de práctica pre profesional:')!!}
    @if($proyecto->tipo->id == 1) <!-- Pasantias Comunitarias -->
      <p>Servicio Comunitario</p>
    @elseif($proyecto->tipo->id == 2) <!-- Pasantias empresariales -->
      <p>Empresarial</p>
    @endif
  </div>

  <div class="form-group">
    {!!form::label('4.4. Instrumentos de evaluación usados en la práctica (ANEXAR A ESTE INFORME)')!!}
    @yield('instrumentos_evaluacion')
  </div>

  @yield('form-buttons')

  @yield('form-end')

  @yield('javascript-code')
@endsection
