@extends('formularios.forUVS16.base')

@section('title','FOR-UVS-16 - Vista Previa')

@section('form-start')
  {!!Form::model($formulario,['route'=>['revisar_foruvs16',$formulario->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('actividades_planificadas')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_actividades_planificadas">
      <thead>
        <tr>
          <th rowspan="2">Actividades Planificadas</th>
          <th colspan="2">Cumplió a cabalidad</th>
          <th rowspan="2">Comentarios</th>
        </tr>
        <tr>
          <th>Si</th>
          <th>No</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($actividadesAsignadasEstudiante as $actividad)
          <tr>
            <td>{{$actividad->descripcion}}</td>
            @if($actividad->cumplimiento_actividad_revision_por_tutor == true)
              <td style="text-align:center;">X</td>
              <td style="text-align:center;"></td>
            @else
              <td style="text-align:center;"></td>
              <td style="text-align:center;">X</td>
            @endif
            <td>{{$actividad->comentario_revision_tutor}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection

@section('evaluacion_resultados_aprendizaje')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_evaluacion_resultados_aprendizaje">
      <thead>
        <tr>
          <th width="25%">Materia</th>
          <th width="35%">Resultado de Aprendizaje del Curso</th>
          <th width="40%">Evidencia (descripción de la actividad realizada con la cual se cumplió el resultado de aprendizaje)</th>
        </tr>
      </thead>

      <tbody>
        @if (isset($proyecto))
          @foreach ($proyecto->resultadosAprendizaje as $resultadoAprendizaje)
            <tr>
              <td>
                {{ $resultadoAprendizaje->materia->nombre }}
              </td>
              <td>
                {{ $resultadoAprendizaje->descripcion }}
              </td>
              <td>
                {{ $resultadoAprendizaje->evidenciasResultadosAprendizaje->where('id_for_uvs_16', $formulario->formularioUVS16->id)->first()->descripcion }}
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('aprobacion')
  @if($formulario->formularioUVS16->aprobacion == true)
    <p>Sí Aprueba</p>
  @elseif($formulario->formularioUVS16->aprobacion == false)
    <p>No Aprueba</p>
  @endif
@endsection

@section('numero_horas_practica')
  <p> {{$formulario->formularioUVS16->numero_hora_practicas_validadas}} </p>
@endsection

@section('instrumentos_evaluacion')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_carreras">
      @foreach ($instrumentoEvaluacionConSeleccion as $instrumentoEvaluacion)
        <tr>
          <td>
            {{ $instrumentoEvaluacion->instrumentoEvaluacion->descripcion }}
          </td>
          <td width="5%" style="text-align:center;">
            @if($instrumentoEvaluacion->seleccionada == true)
              X
            @endif
          </td>
        </tr>
      @endforeach
    </table>
  <div>
@endsection

@section('form-buttons')
  @if(isset($mostrarBotones) && $mostrarBotones == true)
    <br><br>
    <button class="btn btn-success" type="submit" name="submit"  value="aprobar_foruvs16" style="margin-top: 0px; width: 120px;">Aprobar</button>
    <button type="button" class="btn btn-danger" name="submit" value="rechazar" style="margin-left: 30px; width: 120px;" data-toggle="modal" data-backdrop="static" data-target="#escribirComentario">Rechazar</button>

    <!-- Modal -->
    <div class="modal fade" id="escribirComentario" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><b>Comentario</b></h4>
          </div>
          <div class="modal-body">
            <textarea name="comentario_rechazo" id="comentario_rechazo" rows="4" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" name="submit" value="rechazar_formulario">Enviar</button>
          </div>
        </div>

      </div>
    </div>
  @endif
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection
