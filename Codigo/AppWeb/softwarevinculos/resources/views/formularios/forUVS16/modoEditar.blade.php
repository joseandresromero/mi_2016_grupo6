@extends('formularios.forUVS16.base')

@section('breadcumb')
  @if (isset($proyecto))
    <ol class="breadcrumb">
     <li><a href="{{ route('formulariosDisponiblesTutor', $proyecto->id) }}">{{$proyecto->titulo}}</a></li>
     <li class="active">Edición formulario FOR-UVS-16</li>
    </ol>
  @endif
@endsection


@section('title','FOR-UVS-16')

  @section('comentario_rechazo')
    @if(isset($formulario) && $formulario->comentario_rechazo != null)
      <div class="panel panel-warning" style="width: 60%; margin-left: auto; margin-right: auto;">
        <div class="panel-heading">Comentarios</div>
        <div class="panel-body">{{$formulario->comentario_rechazo}}</div>
      </div>
    @endif
  @endsection

@section('form-start')
  {!!Form::model($formulario,['route'=>['foruvs16.update', $formulario->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('actividades_planificadas')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_actividades_planificadas">
      <thead>
        <tr>
          <th rowspan="2">Actividades Planificadas</th>
          <th colspan="2">Cumplió a cabalidad</th>
          <th rowspan="2">Comentarios</th>
        </tr>
        <tr>
          <th>Si</th>
          <th>No</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($actividadesAsignadasEstudiante as $actividad)
          <tr>
            <td>{{$actividad->descripcion}}</td>
            @if($actividad->cumplimiento_actividad_revision_por_tutor == true)
              <td><input type="radio" name="actividaddes-item-{{$actividad->id}}" value="si" checked></td>
              <td><input type="radio" name="actividaddes-item-{{$actividad->id}}" value="no"></td>
            @else
              <td><input type="radio" name="actividaddes-item-{{$actividad->id}}" value="si"></td>
              <td><input type="radio" name="actividaddes-item-{{$actividad->id}}" value="no" checked></td>
            @endif
            <td>{!!Form::textarea('comentarioRevisionTutor-'.$actividad->id,$actividad->comentario_revision_tutor,['id'=>'comentarios-1','class'=>'form-control','rows'=>1])!!}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection

@section('evaluacion_resultados_aprendizaje')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_evaluacion_resultados_aprendizaje">
      <thead>
        <tr>
          <th width="25%">Materia</th>
          <th width="35%">Resultado de Aprendizaje del Curso</th>
          <th width="40%">Evidencia (descripción de la actividad realizada con la cual se cumplió el resultado de aprendizaje)</th>
        </tr>
      </thead>

      <tbody>
        @if (isset($proyecto))
          @foreach ($proyecto->resultadosAprendizaje as $resultadoAprendizaje)
            <tr>
              <td>
                {{ $resultadoAprendizaje->materia->nombre }}
              </td>
              <td>
                <input name="resultadosAprendizaje[]" type="hidden" value="{{ $resultadoAprendizaje->id }}" />{{ $resultadoAprendizaje->descripcion }}
              </td>
              <td>
                {!!form::textarea('evidenciasResultadosAprendizaje[]',$resultadoAprendizaje->evidenciasResultadosAprendizaje->where('id_for_uvs_16', $formulario->formularioUVS16->id)->first()->descripcion, ['class'=>'form-control name_list','rows'=>2, 'style'=>'resize:none;'])!!}
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('aprobacion')
  @if($formulario->formularioUVS16->aprobacion == true)
    <p>Sí Aprueba <input type="radio" name="aprobacion" value="si" checked="checked">No Aprueba <input type="radio" name="aprobacion" value="no"></p>
  @elseif($formulario->formularioUVS16->aprobacion == false)
    <p>Sí Aprueba <input type="radio" name="aprobacion" value="si">No Aprueba <input type="radio" name="aprobacion" value="no" checked="checked"></p>
  @endif
@endsection

@section('numero_horas_practica')
  {!!Form::number('numero_horas_practica',$formulario->formularioUVS16->numero_hora_practicas_validadas,['id'=>'numero_horas_practica','class'=>'form-control','min'=>1])!!}
@endsection

@section('instrumentos_evaluacion')
  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_carreras">
      @foreach ($instrumentoEvaluacionConSeleccion as $instrumentoEvaluacion)
        <tr>
          <td>
            {{ $instrumentoEvaluacion->instrumentoEvaluacion->descripcion }}
          </td>
          <td width="5%" style="text-align:center;">
            @if($instrumentoEvaluacion->seleccionada == true)
              <input type="checkbox" name="instrumentosEvaluacion[]" value="{{ $instrumentoEvaluacion->instrumentoEvaluacion->id }}" checked="checked">
            @else
              <input type="checkbox" name="instrumentosEvaluacion[]" value="{{ $instrumentoEvaluacion->instrumentoEvaluacion->id }}">
            @endif
          </td>
        </tr>
      @endforeach
    </table>
  <div>
@endsection

@section('form-buttons')
  <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
  <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar" style="margin-left: 20px; width: 100px;">Enviar</button>
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection

@section('javascript-code')
  <script type="text/javascript">
    var i=1;
    //Se agregan nuevos campos de texto para registro de nueva evaluacion de resultado de aprendizaje.
    $('#addEvaluacionResultadoAprendizaje').click(function(){
       i++;
       $('#tabla_evaluacion_resultados_aprendizaje').append('<tr id="rowERA'+i+'"> <td>{!!form::textarea('evaluacionERA-materia[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td>{!!form::textarea('evaluacionERA-resultadoAprendizajeCurso[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td>{!!form::textarea('evaluacionERA-descripcionEvidencia[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td><button type="button" name="remove" id="ERA'+i+'" class="btn btn-danger btn_remove_evaluacionResultadoAprendizaje"><span class="glyphicon glyphicon-remove"></span></button></td> </tr>');
    });

    //Se elimina fila de la tabla de evaluacion de resultado de aprendizaje.
    $(document).on('click', '.btn_remove_evaluacionResultadoAprendizaje', function(){
         var button_id = $(this).attr("id");
         $('#row'+button_id+'').remove();
    });
  </script>
@endsection
