@extends('formularios.forUVS04.base')

@section('title','FOR-UVS-04')

@section('comentario_rechazo')
@endsection

@section('form-start')
  {!!Html::script('js/next-page-control.js')!!}

  {!!Form::model($proyecto,['route'=>['revisar_proyecto',$proyecto->id], 'method'=>'PUT', 'class'=>'formulario', 'style' => (isset($print)) ? 'width: 90% !important;' : '', 'files'=>true])!!}
@endsection

@section('tipo_proyecto')
@endsection

@section('1.1')
  <p>{{ $proyecto->titulo or '' }}</p>
@endsection

@section('1.2')
  <p>{{ $proyecto->programa->nombre or '' }}</p>
@endsection

@section('1.3')
  <ul>
  @foreach ($proyecto->carreras as $carrera)
    <li>
      {{ $carrera->nombre }}
      @if($carrera->pivot->carrera_principal == true)
        (Principal)
      @endif
    </li>
  @endforeach
  </ul>
@endsection

@section('1.4')
  <br>
  {!!form::label('Provincia:')!!}
  <p>{{ $proyecto->provincia or '' }}</p>
  {!!form::label('Cantón:')!!}
  <p>{{ $proyecto->canton or '' }}</p>
  {!!form::label('Código de la zona:')!!}
  <p>{{ $proyecto->codigo_zona or '' }}</p>
  {!!form::label('Código del distrito:')!!}
  <p>{{ $proyecto->codigo_distrito or '' }}</p>
  {!!form::label('Código del circuito:')!!}
  <p>{{ $proyecto->codigo_circuito or '' }}</p>
@endsection

@section('1.5')
  <br>
  {!!form::label('Fecha de inicio:')!!}
  <p>{{ $proyecto->fecha_inicio or '' }}</p>
  {!!form::label('Fecha de finalización:')!!}
  <p>{{ $proyecto->fecha_finalizacion or '' }}</p>
@endsection

@section('1.6')
  <br>
  <div class="modo-ver-tabla">
    <table id="tablaObjetivosPnbv" class="">
      <thead>
        <tr>
          <th>Área del Conocimiento</th>
          <th>Sub área del Conocimiento</th>
          <th>Sub área Especifica</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{ $proyecto->area_conocimiento }}</td>
          <td>{{ $proyecto->sub_area_conocimiento }}</td>
          <td>{{ $proyecto->sub_area_especifica }}</td>
        </tr>
      </tbody>
    </table>
  </div>
@endsection

@section('1.7')
  <div class="modo-ver-tabla">
    <table class="" id="tabla_carreras">
      @foreach ($lineasAccion as $lineaAccion)
        <tr>
          <td>
            {{ $lineaAccion->lineaAccion->descripcion }}
          </td>
          @if ($lineaAccion->seleccionada == true)
            <td>X</td>
          @else
            <td></td>
          @endif
        </tr>
      @endforeach
    </table>
  <div>
@endsection

@section('1.8')
  <br>
  <div class="modo-ver-tabla">
    {!! $proyecto->informacion_pnbv !!}
  </div>
@endsection

@section('1.9')
  <div class="modo-ver-tabla">
    <table class="">
      <tr>
        <th>Función</th>
        <th>Nombres y Apellidos</th>
        <th>Cédula de Ciudadanía</th>
        <th>Teléfono Fijo, Celular Y Correo Electrónico</th>
      </tr>
      <tr>
        <td><b>Docente Responsable de Vinculación con la Sociedad de la Unidad/Carrera</b></td>
        @if ($carreraPrincipal != null)
          @if($proyecto->tipo->id == 1) <!-- Pasantias Comunitarias -->
            <td id="nombreCoordinador">{{ $carreraPrincipal->coordinadorComunitarias->usuario->nombres }} {{ $carreraPrincipal->coordinadorComunitarias->usuario->apellidos }}</td>
            <td id="cedulaCoordinador">{{ $carreraPrincipal->coordinadorComunitarias->usuario->cedula }}</td>
            <td>
              <ul class="list-group" style="margin-bottom:0px;">
                <li id="telefonoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->telefono_fijo }}</li>
                <li id="celularCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->celular }}</li>
                <li id="correoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->correo }}</li>
              </ul>
            </td>
          @else
            <td id="nombreCoordinador">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->nombres }} {{ $carreraPrincipal->coordinadorEmpresariales->usuario->apellidos }}</td>
            <td id="cedulaCoordinador">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->cedula }}</td>
            <td>
              <ul class="list-group" style="margin-bottom:0px;">
                <li id="telefonoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->telefono_fijo }}</li>
                <li id="celularCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->celular }}</li>
                <li id="correoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->correo }}</li>
              </ul>
            </td>
          @endif
        @else
          <td id="nombreCoordinador"></td>
          <td id="cedulaCoordinador"></td>
          <td>
          </td>
        @endif
      </tr>
      <tr>
        <td><b>Docente Director/a del Proyecto</b></td>
          <td>
            {{ $proyecto->director->usuario->nombres }} {{ $proyecto->director->usuario->apellidos }}
          </td>
          <td>{{ $proyecto->director->usuario->cedula }}</td>
          <td>
            <ul class="list-group" style="margin-bottom:0px;">
              <li class="list-group-item">{{ $proyecto->director->usuario->telefono_fijo }}</li>
              <li class="list-group-item">{{ $proyecto->director->usuario->celular }}</li>
              <li class="list-group-item">{{ $proyecto->director->usuario->correo_electronico }}</li>
            </ul>
          </td>
      </tr>
      @if($proyecto->tutores != null)
        @foreach($proyecto->tutores as $tutor)
          <tr>
            <td><b>Docente Tutor de los estudiantes de {{$tutor->carrera->nombre}}</b></td>
              <td>
                {{ $tutor->usuario->nombres }} {{ $tutor->usuario->apellidos }}
              </td>
              <td>{{ $tutor->usuario->cedula }}</td>
              <td>
                <ul class="list-group" style="margin-bottom:0px;">
                  <li class="list-group-item">{{ $tutor->usuario->telefono_fijo }}</li>
                  <li class="list-group-item">{{ $tutor->usuario->celular }}</li>
                  <li class="list-group-item">{{ $tutor->usuario->correo_electronico }}</li>
                </ul>
              </td>
          </tr>
        @endforeach
      @endif
    </table>
  </div>
@endsection

@section('1.10')
  <br>
  <div class="modo-ver-tabla">
    {!! $proyecto->perfil_practicante !!}
  </div>

  <div class="modo-ver-tabla" style="margin-top: 30px;">
    <table class="" id="tabla_estudiantes">
      <thead>
         <th width="35%">Nombres y Apellidos</th>
         <th width="20%">Matricula</th>
         <th width="20%">Teléfono</th>
         <th>Correo electrónico</th>
      </thead>
      <tbody>
        @if (isset($estudiantesProyecto))
          @foreach ($estudiantesProyecto as $estudiante)
            <tr>
              <td>
                {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}
              </td>
              <td>{{$estudiante->matricula}}</td>
              <td>{{$estudiante->usuario->telefono}}</td>
              <td>{{$estudiante->usuario->correo_electronico}}</td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('2.1')
  <p>{{ $proyecto->numero_beneficiarios_directos }}</p>
@endsection

@section('2.2')
  <p>{{ $proyecto->numero_beneficiarios_indirectos }}</p>
@endsection

@section('opciones-organizacion')
@endsection

@section('2.3')
  <p>{{{$proyecto->organizacion->nombre or ''}}}</p>
@endsection

@section('2.4')
  <p>{{{$proyecto->organizacion->direccion or ''}}}</p>
@endsection

@section('2.5')
  <p>{{{$proyecto->organizacion->telefono_fijo or ''}}}</p>
  <p>{{{$proyecto->organizacion->fax or ''}}}</p>
  <p>{{{$proyecto->organizacion->correo_electronico or ''}}}</p>
@endsection

@section('2.6')
  <p>{{{$proyecto->organizacion->nombre_representante_legal or ''}}}</p>
@endsection

@section('2.7')
  <p>{{{$proyecto->organizacion->fecha_creacion or ''}}}</p>
@endsection

@section('2.8')
  <p>{{{$proyecto->organizacion->nombramiento_representante_legal or ''}}}</p>
@endsection

@section('2.9')
  <ul>
    @foreach($proyecto->delegados as $delegado)
      <li>{{$delegado->nombre}}</li>
    @endforeach
@endsection

@section('3.1')
  <p>{!! $proyecto->antecedentes !!}</p>
@endsection

@section('3.2')
  <p>{!! $proyecto->contexto_proyecto !!}</p>
@endsection

@section('3.3')
  <p>{!! $proyecto->definicion_problema !!}</p>
@endsection

@section('4')
  <hr>
  <p>{!! $proyecto->justificacion !!}</p>
@endsection

@section('5')
  <hr>
  <p>{!! $proyecto->pertinencia !!}</p>
@endsection

@section('6')
  <hr>
  <div class="form-group">
    {!!Form::label('Objetivo General')!!}
    <p>{{ $proyecto->objetivo_general or '' }}</p>
  </div>

  <div class="form-group">
    {!!Form::label('Objetivos Específicos')!!}

    <ul style="list-style-type: disc; text-align:justify">
      @foreach($proyecto->objetivosEspecificos as $objetivoEspecifico)
        <li>{{$objetivoEspecifico->descripcion}} </li>
      @endforeach
    </ul>
  </div>
@endsection


@section('7')
  <hr>
  <div class="modo-ver-tabla">
    <table class="">
      <thead>
        <tr>
         <th width="40%">Objetivos Narrativos</th>
         <th width="20%">Indicadores Verificables</th>
         <th width="20%">Medios de verificación</th>
         <th width="20%">Supuestos</th>
       </tr>
      </thead>
      <tbody>
        <tr >
          <td class="first-td-table">
            <b>FIN</b>
            <p>{{ $proyecto->programa->objetivo_general or '' }}</p>
          </td>
          <td>
            <p> {{$proyecto->indicador_verificable_objetivo_programa}} </p>
          </td>
          <td>
            <p> {{$proyecto->medio_verificacion_objetivo_programa}} </p>
          </td>
          <td>
            <p> {{$proyecto->supuestos_objetivo_programa}} </p>
          </td>
        </tr>
        <tr>
          <td  class="first-td-table">
            <b>PROPÓSITO</b>
            <p id="textObjetivoGeneralMatrizML">
              @if(isset($proyecto))
                {{$proyecto->objetivo_general}}
              @endif
             </p>
          </td>
          <td>
            <p>{{$proyecto->indicador_verificable_objetivo_general}}</p>
          </td>
          <td>
            <p>{{$proyecto->medio_verificacion_objetivo_general}}</p>
          </td>
          <td>
            <p>{{$proyecto->supuestos_objetivo_general}}</p>
          </td>
        </tr>
        @if(isset($objetivosEspecificos))
          @foreach ($objetivosEspecificos as $index => $objetivoEspecifico)
            <tr >
              {{--
              <td colspan="4" style="background: rgb(170, 170, 158)">
              --}}
              <td class="first-td-table">
                <b>COMPONENTE {{$index + 1}}</b> <br>
                {{$objetivoEspecifico->descripcion}}
              </td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>
                <p>Actividades:</p>
                @if($objetivoEspecifico->actividades != null)
                  <ul style="list-style-type: decimal; text-align:justify">
                    @foreach($objetivoEspecifico->actividades as $actividad)
                      <li>{{$actividad->descripcion}} </li>
                    @endforeach
                  </ul>
                @endif
              </td>
              <td>
                {{$objetivoEspecifico->indicadores_verificables}}
              </td>
              <td>
                {{$objetivoEspecifico->medios_verificacion}}
              </td>
              <td>
                {{$objetivoEspecifico->supuestos}}
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('8')
  <hr>
  <p>{!! $proyecto->metodologia or '' !!}</p>
@endsection

@section('9')
  <hr>
  <div class="modo-ver-tabla">
    <table class="">
      <thead>
        <tr style="background: rgb(166, 203, 127)">
         <th width="55%">Actividades</th>
         <th width="15%">Duración (Horas)</th>
         <th width="15%">Inicio</th>
         <th width="15%">Fin</th>
       </tr>
      </thead>
      <tbody>
        @if(isset($objetivosEspecificos))
          @foreach($objetivosEspecificos as $objetivoEspecifico)
            <tr>
              <td colspan="4" style="background: rgb(205, 231, 228)" class="first-td-table">
                {{$objetivoEspecifico->descripcion}}
              </td>
            </tr>
            @if($objetivoEspecifico->actividades != null)
              @foreach($objetivoEspecifico->actividades as $actividad)
                <tr>
                  <td >{{$actividad->descripcion}} </td>
                  <td >{{$actividad->duracion_horas}}</td>
                  <td >{{$actividad->fecha_inicio}}</td>
                  <td >{{$actividad->fecha_finalizacion}}</td>
                </tr>
              @endforeach
            @endif
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('10')
  <hr>
  <p>{!! $proyecto->productos_esperados !!}</p>
@endsection

@section('11')
  <hr>
  <p>{!! $proyecto->compromisos_organizacion !!}</p>
@endsection

@section('12')
  <hr>
  <p>{!! $proyecto->presupuesto !!}</p>
@endsection

@section('13')
  <hr>
  <p>{!! $proyecto->evaluacion !!}</p>
  <div class="modo-ver-tabla">
    <table class="">
      <thead>
        <tr>
         <th width="40%">Objetivos Narrativos</th>
         <th width="30%">Indicadores Verificables</th>
         <th width="30%">Medios de verificación</th>
       </tr>
      </thead>
      <tbody>
        @if(isset($objetivosEspecificos))
          @foreach ($objetivosEspecificos as $objetivoEspecifico)
            <tr >
              <td colspan="4" style="background: rgb(205, 231, 228)">
                {{$objetivoEspecifico->descripcion}}
              </td>
            </tr>
            <tr>
              <td>
                @if($objetivoEspecifico->actividades != null)
                  <ul style="list-style-type: decimal; text-align:justify">
                    @foreach($objetivoEspecifico->actividades as $actividad)
                      <li>{{$actividad->descripcion}} </li>
                    @endforeach
                  </ul>
                @endif
              </td>
              <td>
                {{$objetivoEspecifico->indicadores_verificables}}
              </td>
              <td>
                {{$objetivoEspecifico->medios_verificacion}}
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>

@endsection

@section('14')
  <hr>
  <p>{!! $proyecto->bibliografia !!}</p>
@endsection

@section('form-buttons')
  @if((Auth::user()->coordinadorCarreraComunitarias != null && Auth::user()->coordinadorCarreraComunitarias->id == $carreraPrincipal->coordinadorComunitarias->id && ($proyecto->estado->id == 1 || $proyecto->estado->id == 3)) || (Auth::user()->coordinadorCarreraEmpresariales != null && Auth::user()->coordinadorCarreraEmpresariales->id == $carreraPrincipal->coordinadorEmpresariales->id && ($proyecto->estado->id == 1 || $proyecto->estado->id == 3)))
    <!-- Proyecto pendiente o Rechazado -->
    <button class="btn btn-success" type="submit" name="submit"  value="aprobar_proyecto" style="margin-top: 0px; width: 120px;">Aprobar</button>
    <button type="button" class="btn btn-danger" name="submit" value="rechazar" style="margin-left: 30px; width: 120px;" data-toggle="modal" data-backdrop="static" data-target="#escribirComentario">Rechazar</button>

    <!-- Modal -->
    <div class="modal fade" id="escribirComentario" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><b>Comentario</b></h4>
          </div>
          <div class="modal-body">
            <textarea name="comentario_rechazo" id="comentario_rechazo" rows="4" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary" name="submit" value="rechazar_proyecto">Enviar</button>
          </div>
        </div>

      </div>
    </div>
  @endif
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection

@section('javascript-code')
@endsection
