@extends('formularios.forUVS04.base')

@section('breadcumb')
  @if (isset($proyecto))
    <ol class="breadcrumb">
     <li><a href="{{ route('flujo_proyecto', $proyecto->id) }}">{{$proyecto->titulo}}</a></li>
     <li class="active">Editar Proyecto</li>
    </ol>
  @endif
@endsection

@section('title','FOR-UVS-04')

@section('comentario_rechazo')
  @if(isset($formularioUVS04) && $formularioUVS04->comentario_rechazo != null)
    <div class="panel panel-warning" style="width: 60%; margin-left: auto; margin-right: auto;">
      <div class="panel-heading">Comentarios</div>
      <div class="panel-body">{{$formularioUVS04->comentario_rechazo}}</div>
    </div>
  @endif
@endsection

@section('form-start')
  @if (isset($proyecto))
    {!!Form::model($proyecto,['route'=>['proyecto.update',$proyecto->id],'method'=>'PUT', 'class'=>'formulario form-proyecto', 'files'=>true])!!}
  @else
    {!!Form::open(['route'=>'proyecto.store', 'method'=>'POST', 'class'=>'formulario form-proyecto', 'files'=>true])!!}
  @endif
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
@endsection

@section('tipo_proyecto')
  {{--<div class="form-group">
    {!!form::label('tipo', 'Tipo de proyecto:')!!}
    {!!form::select('tipo_proyecto', $tiposProyecto, $tipoProyecto->id, ['id' => 'tipo_proyecto', 'class' => 'form-control'])!!}
  </div>--}}
  <input type="hidden" name="tipo_proyecto" id="tipo_proyecto" value="{{ $idTipoProyecto }}">
@endsection

@section('1.1')
  {!!form::text('titulo',null,['id'=>'titulo','class'=>'form-control'])!!}
@endsection

@section('1.2')
  <div class="form-group">
    @if (isset($programa) && $programa != null)
      {{Form::select('programaProyecto', $programas_options, $programa->id,['id'=>'programaProyecto','class'=>'form-control'])}}
    @else
      {{Form::select('programaProyecto', $programas_options, null,['id'=>'programaProyecto','class'=>'form-control'])}}
    @endif
  </div>
  <p> * Si su programa no se encuentra en la lista, solicite al administrador del Sistema que lo agregue. </p>
@endsection

@section('1.3')
  <span id="btnHelpCarreras" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpCarreras" class="help">
      Si el proyecto es multidisciplinario, determinar a qué carrera(s) se le(s) asignará el valor del presupuesto.
    </span>
  </span>
  <br>
  {!!form::label('carrera_principal', 'Carrera principal', ['class' => 'top-space'])!!}
  @if(Auth::user()->coordinadorCarreraEmpresariales != null)
    <div class="table-responsive">
      <table class="table sv-list-table" id="tabla_carrera_principal">
        <tr class="carrera-principal carrera-{{ Auth::user()->coordinadorCarreraEmpresariales->carrera->id }}">
          <td>
            <input name="carreraPrincipal" type="hidden" value="{{ Auth::user()->coordinadorCarreraEmpresariales->carrera->id }}" />
            {{ Auth::user()->coordinadorCarreraEmpresariales->carrera->nombre }}
          </td>
        </tr>
      </table>
    </div>
  @else
    {!!form::text('carrera_principal',null,['id'=>'carrera_principal','class'=>'form-control'])!!}
    <div class="table-responsive">
      <table class="table sv-list-table" id="tabla_carrera_principal">
        @if (isset($proyecto))
          @if ($carreraPrincipal != null)
            <tr class="carrera-principal carrera-{{ $carreraPrincipal->id }}">
              <td>
                <input name="carreraPrincipal" type="hidden" value="{{ $carreraPrincipal->id }}" />
                {{ $carreraPrincipal->nombre }}
              </td>
              <td class="columna-small">
                <button type="button" name="remove" id="{{ $carreraPrincipal->id }}" class="btn btn-danger btn_remove remove-carrera">
                  <span class="glyphicon glyphicon-remove"></span>
                </button>
              </td>
            </tr>
          @endif
        @endif
      </table>
    </div>
  @endif

  {!!form::label('carrera', 'Carreras adicionales', ['class' => 'top-space'])!!}
  {!!form::text('carrera',null,['id'=>'carrera','class'=>'form-control'])!!}
  <div class="table-responsive">
    <table class="table sv-list-table" id="tabla_carreras">
      @if (isset($proyecto))
        @foreach ($carrerasAdicionales as $carrera)
          <tr class="carrera-{{ $carrera->id }}">
            <td>
              <input name="carrerasAdicionales[]" type="hidden" value="{{ $carrera->id }}" />
              {{ $carrera->nombre }}
            </td>
            <td class="columna-small">
              <button type="button" name="remove" id="{{ $carrera->id }}" class="btn btn-danger btn_remove remove-carrera">
                <span class="glyphicon glyphicon-remove"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </table>
  </div>
@endsection

@section('1.4')
  <a href="{{ URL::route('verForUVS12') }}" target="_blank" style="margin-left: 10px;">(Ver FOR-UVS-12)</a>
  <br>
  {!!form::label('provincia', 'Provincia:', ['class' => 'top-space'])!!}
  {!!form::text('provincia',null,['id'=>'provincia','class'=>'form-control'])!!}
  {!!form::label('canton', 'Cantón:', ['class' => 'top-space'])!!}
  {!!form::text('canton',null,['id'=>'canton','class'=>'form-control'])!!}
  {!!form::label('codigo_zona', 'Código de la zona:', ['class' => 'top-space'])!!}
  {!!form::text('codigo_zona',null,['id'=>'codigo_zona','class'=>'form-control'])!!}
  {!!form::label('codigo_distrito', 'Código del distrito:', ['class' => 'top-space'])!!}
  {!!form::text('codigo_distrito',null,['id'=>'codigo_distrito','class'=>'form-control'])!!}
  {!!form::label('codigo_circuito', 'Código del circuito:', ['class' => 'top-space'])!!}
  {!!form::text('codigo_circuito',null,['id'=>'codigo_circuito','class'=>'form-control'])!!}
@endsection

@section('1.5')
  <br>
  {!!form::label('fechas', 'Fechas de inicio y finalización:', ['class' => 'top-space'])!!}
  <div class="hero-unit">
    @if(isset($proyecto))
      @if($fechaInicio != null && $fechaFinalizacion != null)
        {!!form::text('rango_fechas_proyecto',$fechaInicio.' - '.$fechaFinalizacion,['class'=>'form-control date_ranger_picker'])!!}
      @else
        {!!form::text('rango_fechas_proyecto',null,['class'=>'form-control date_ranger_picker'])!!}
      @endif
    @else
      {!!form::text('rango_fechas_proyecto',null,['class'=>'form-control date_ranger_picker'])!!}
    @endif
  </div>
@endsection

@section('1.6')
  <a href="{{ URL::route('verForUVS07') }}" target="_blank" style="margin-left: 10px;">(Ver FOR-UVS-07)</a>
  <span id="btnHelpAreaConocimiento" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpAreaConocimiento" class="help">
      Escoger del formato FOR-UVS-07, el área, sub área y conocimiento específico al que pertenece el proyecto. Datos requeridos de acuerdo a la plataforma del SNIESE. Fuente: UNESCO (1997) Clasificación Internacional Normalizada de la Educación CINE.
    </span>
  </span>
  <br>
  {!!form::label('area_conocimiento', 'Área del Conocimiento', ['class' => 'top-space'])!!}
  {!!form::textarea('area_conocimiento',null,['id'=>'area_conocimiento','class'=>'form-control','rows'=>2])!!}
  {!!form::label('sub_area_conocimiento', 'Sub área del Conocimiento', ['class' => 'top-space'])!!}
  {!!form::textarea('sub_area_conocimiento',null,['id'=>'sub_area_conocimiento','class'=>'form-control','rows'=>2])!!}
  {!!form::label('sub_area_especifica', 'Sub área Especifica', ['class' => 'top-space'])!!}
  {!!form::textarea('sub_area_especifica',null,['id'=>'sub_area_especifica','class'=>'form-control','rows'=>2])!!}
@endsection

@section('1.7')
  <a href="{{ URL::route('verForUVS02') }}" target="_blank" style="margin-left: 10px;">(Ver FOR-UVS-02)</a>

  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_carreras">
      @foreach ($lineasAccion as $lineaAccion)
        @if (isset($proyecto))
          @if ($lineaAccion->seleccionada == true)
            <tr>
              <td>
                {{ $lineaAccion->lineaAccion->descripcion }}
              </td>
              <td>
                <input type="checkbox" name="lineasAccion[]" value="{{ $lineaAccion->lineaAccion->id }}" checked="checked" class="linea-accion">
              </td>
            </tr>
          @else
            <tr>
              <td>
                {{ $lineaAccion->lineaAccion->descripcion }}
              </td>
              <td>
                <input type="checkbox" name="lineasAccion[]" value="{{ $lineaAccion->lineaAccion->id }}" class="linea-accion">
              </td>
            </tr>
          @endif
        @else
          <tr>
            <td>
              {{ $lineaAccion->descripcion }}
            </td>
            <td>
              <input type="checkbox" name="lineasAccion[]" value="{{ $lineaAccion->id }}" class="linea-accion">
            </td>
          </tr>
        @endif
      @endforeach
    </table>
  <div>
@endsection

@section('1.8')
  <span id="btnHelpPlanNacional" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpPlanNacional" class="help">
      Escoger del formato FOR-UVS-28, los Objetivos de Desarrollo del Milenio, asociados al proyecto.<br><br>
      Escoger del formato FOR-UVS-29, los Objetivos del Buen Vivir, asociados al proyecto.<br><br>
      Escoger del formato FOR-UVS-21, los contextos, ejes y tensiones y problemas del PNBV, asociados al proyecto.<br><br>
      En la Justificación del Proyecto, se debe incluir por qué el proyecto se articula a cada uno de estos criterios.
    </span>
  </span>
  <br>
  <ul>
    <li><a href="{{ URL::route('verForUVS28') }}" target="_blank">(Ver FOR-UVS-28)</a></li>
    <li><a href="{{ URL::route('verForUVS29') }}" target="_blank">(Ver FOR-UVS-29)</a></li>
    <li><a href="{{ URL::route('verForUVS21') }}" target="_blank">(Ver FOR-UVS-21)</a></li>
  </ul>
  <br>
  @if (isset($proyecto))
    {!!form::textarea('informacion_pnbv',null,['id'=>'informacion_pnbv','class'=>'form-control','rows'=>5])!!}
  @else
    {!!form::textarea('informacion_pnbv','<table border="1" cellpadding="1" cellspacing="1" style="height:97px; width:1005px">
    	<thead>
    		<tr>
    			<th scope="col">Objetivo de Desarrollo del Milenio</th>
    			<th scope="col">Objetivo del PNBV</th>
    			<th scope="col">Contextos PNBV</th>
    			<th scope="col">Ejes PNBV</th>
    			<th scope="col">Tensiones y Problemas PNBV</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    		</tr>
    	</tbody>
    </table>',['id'=>'informacion_pnbv','class'=>'form-control','rows'=>5])!!}
  @endif
@endsection

@section('1.9')
  <div class="table-responsive">
    <table class="table table-bordered">
      <tr>
        <th>Función</th>
        <th>Nombres y Apellidos</th>
        <th>Cédula de Ciudadanía</th>
        <th>Teléfono Fijo, Celular Y Correo Electrónico</th>
      </tr>
      <tr>
        <td><b>Docente Responsable de Vinculación con la Sociedad de la Unidad/Carrera</b></td>
        @if (isset($carreraPrincipal))

          @if($tipoProyecto->id == 1) <!-- Pasantias Comunitarias -->
            <td id="nombreCoordinador">{{ $carreraPrincipal->coordinadorComunitarias->usuario->nombres }} {{ $carreraPrincipal->coordinadorComunitarias->usuario->apellidos }}</td>
            <td id="cedulaCoordinador">{{ $carreraPrincipal->coordinadorComunitarias->usuario->cedula }}</td>
            <td>
              <ul class="list-group" style="margin-bottom:0px;">
                <li id="telefonoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->telefono_fijo }}</li>
                <li id="celularCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->celular }}</li>
                <li id="correoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorComunitarias->usuario->correo }}</li>
              </ul>
            </td>
          @else
            <td id="nombreCoordinador">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->nombres }} {{ $carreraPrincipal->coordinadorEmpresariales->usuario->apellidos }}</td>
            <td id="cedulaCoordinador">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->cedula }}</td>
            <td>
              <ul class="list-group" style="margin-bottom:0px;">
                <li id="telefonoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->telefono_fijo }}</li>
                <li id="celularCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->celular }}</li>
                <li id="correoCoordinador" class="list-group-item">{{ $carreraPrincipal->coordinadorEmpresariales->usuario->correo }}</li>
              </ul>
            </td>
          @endif

        @else
          <td id="nombreCoordinador"></td>
          <td id="cedulaCoordinador"></td>
          <td>
            <ul class="list-group" style="margin-bottom:0px;">
              <li id="telefonoCoordinador" class="list-group-item"></li>
              <li id="celularCoordinador" class="list-group-item"></li>
              <li id="correoCoordinador" class="list-group-item"></li>
            </ul>
          </td>
        @endif
      </tr>
      <tr>
        <td><b>Docente Director/a del Proyecto</b></td>
        @if (isset($proyecto))
          <td>
            {{ $proyecto->director->usuario->nombres }} {{ $proyecto->director->usuario->apellidos }}
          </td>
          <td>{{ $proyecto->director->usuario->cedula }}</td>
          <td>
            <ul class="list-group" style="margin-bottom:0px;">
              <li class="list-group-item">{{ $proyecto->director->usuario->telefono_fijo }}</li>
              <li class="list-group-item">{{ $proyecto->director->usuario->celular }}</li>
              <li class="list-group-item">{{ $proyecto->director->usuario->correo_electronico }}</li>
            </ul>
          </td>
        @else
          <td>
            {{ Auth::user()->director->usuario->nombres }} {{ Auth::user()->director->usuario->apellidos }}
          </td>
          <td>{{ Auth::user()->director->usuario->cedula }}</td>
          <td>
            <ul class="list-group" style="margin-bottom:0px;">
              <li class="list-group-item">{{ Auth::user()->director->usuario->telefono_fijo }}</li>
              <li class="list-group-item">{{ Auth::user()->director->usuario->celular }}</li>
              <li class="list-group-item">{{ Auth::user()->director->usuario->correo_electronico }}</li>
            </ul>
          </td>
        @endif
      </tr>
    </table>
  </div>
@endsection

@section('1.10')
  <br>
  @if (isset($proyecto))
    {!!form::textarea('perfil_practicante',null,['id'=>'perfil_practicante','class'=>'form-control','rows'=>5])!!}
  @else
    {!!form::textarea('perfil_practicante','<table border="1" cellpadding="1" cellspacing="1" style="height:97px; width:1005px">
    	<thead>
    		<tr>
    			<th scope="col">Perfil de Egreso (Descripción de la Carrera)</th>
    			<th scope="col">Objetivos Educacionales</th>
    			<th scope="col">Resultado de aprendizaje</th>
    			<th scope="col">Requisitos del practicante</th>
    			<th scope="col">Número de practicantes</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    		</tr>
    	</tbody>
    </table>',['id'=>'perfil_practicante','class'=>'form-control','rows'=>5])!!}
  @endif

  {!!form::label('Lista de estudiantes',null,['style'=>'margin-top: 20px'])!!}
  <div class="form-group" style="margin-top:10px">
      <div class="col-lg-4"  style="padding-left: 0px !important;">
        <form id="form_estudiante">
        <input type="number" class="form-control" id="matriculaEstudiante" name="matriculaEstudiante" placeholder="Número de matricula">
        <form>
      </div>
    <button id="guardarEstudiante" type="button" class="btn btn-default"> Agregar</button>
  </div>

  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_estudiantes">
      <thead>
         <th width="35%">Nombres y Apellidos</th>
         <th width="20%">Matricula</th>
         <th width="20%">Teléfono</th>
         <th >Correo electrónico</th>
         <th >Acción</th>
      </thead>
      <tbody>
        @if (isset($estudiantesProyecto))
          @foreach ($estudiantesProyecto as $estudiante)
            <tr class="rowEstudiante{{ $estudiante->id }}">
              <td>
                {{$estudiante->usuario->nombres}} {{$estudiante->usuario->apellidos}}
                <input name="estudianteProyecto-{{$estudiante->id}}" type="hidden" value="{{$estudiante->id}}"/>
              </td>
              <td>{{$estudiante->matricula}}</td>
              <td>{{$estudiante->usuario->telefono}}</td>
              <td>{{$estudiante->usuario->correo_electronico}}</td>
              <td><button type="button" name="remove" id="Estudiante{{ $estudiante->id }}" class="btn btn-danger btn_remove_estudiante"><span class="glyphicon glyphicon-remove"></span></button></td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endsection

@section('2.1')
    {!!form::number('numero_beneficiarios_directos',null,['id'=>'numero_beneficiarios_directos','class'=>'form-control'])!!}
@endsection

@section('2.2')
  {!!form::number('numero_beneficiarios_indirectos',null,['id'=>'numero_beneficiarios_indirectos','class'=>'form-control'])!!}
@endsection

@section('opciones-organizacion')
  <hr>
  {!!form::label('Opciones de organización:')!!}
  <br>
  <button id="btnAgregarOrganizacion" type="button" class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#agregarOrganizacion" style="margin-top:0px;">Agregar nueva</button>
  <button id="btnElegirOrganizacion" type="button" class="btn btn-primary">Elegir existente</button>
  @if(isset($proyecto) && $proyecto->organizacion != null)
    <button id="btnQuitarOrganizacion" type="button" class="btn btn-danger">Quitar</button>
  @else
    <button id="btnQuitarOrganizacion" type="button" class="btn btn-danger" style="display:none;">Quitar</button>
  @endif

  <br>
  {!!form::text('nombre_organizacion',null,['id'=>'nombre_organizacion','class'=>'form-control', 'placeholder' => 'Razón Social de la Organización', 'style' => 'margin-top: 10px; display: none;'])!!}
  <br>
  @if(isset($proyecto->organizacion))
   <div id="seccionConvenio">
   @else
   <div id="seccionConvenio" style="display:none;">
   @endif
     {!!form::label('Convenio o carta de compromiso:')!!}
     <br>
     @if(isset($proyecto->organizacion))
       <button type="button" class="sv-gray-button" onclick="descargarConvenio({{$proyecto->organizacion->id}})">Ver convenio adjunto</button>
       {{--}}<a id="urlConvenio" href="verConvenio/{{ $proyecto->organizacion->id }}" target="_blank">Ver convenio (Si existe alguno)</a>--}}
     @else
       <button type="button" class="sv-gray-button" onclick="descargarConvenio(0)">Ver convenio adjunto</button>
       {{--}}<a id="urlConvenio" href="verConvenio/0" target="_blank">Ver convenio (Si existe alguno)</a>--}}
     @endif
     <input type="file" name="convenioOrganizacionUpload" id="convenioOrganizacionUpload" style="margin-bottom: 10px; margin-top: 10px;">
   </div>
   <hr>
  <!-- Modal -->
  <div class="modal fade" id="agregarOrganizacion" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Agregar nueva organización</b></h4>
        </div>
        <div class="modal-body">
          <form id="">
            <label for="razon_social_organizacion">Razón Social de la Organización</label>
            <input type="text" class="form-control" name="razon_social_organizacion" id="razon_social_organizacion">
            <br>
            <label for="direccion_organizacion">Dirección</label>
            <input type="text" class="form-control" name="direccion_organizacion" id="direccion_organizacion">
            <br>
            <label for="telefono_organizacion">Teléfono</label>
            <input type="text" class="form-control" name="telefono_organizacion" id="telefono_organizacion">
            <br>
            <label for="fax_organizacion">Fax</label>
            <input type="text" class="form-control" name="fax_organizacion" id="fax_organizacion">
            <br>
            <label for="correo_electronico_organizacion">Correo electrónico</label>
            <input type="text" class="form-control" name="correo_electronico_organizacion" id="correo_electronico_organizacion">
            <br>
            <label for="representante_legal_organizacion">Representante Legal</label>
            <input type="text" class="form-control" name="representante_legal_organizacion" id="representante_legal_organizacion">
            <br>
            <label for="fecha_creacion">Fecha de creación y acuerdo de legalización</label>
            <input type="text" class="form-control" name="fecha_creacion" id="fecha_creacion">
            <br>
            <label for="nombramiento_representante_legal">Nombramiento del Representante Legal</label>
            <input type="text" class="form-control" name="nombramiento_representante_legal" id="nombramiento_representante_legal">
            <br>
            <label for="resumen_organizacion">Resumen</label>
            <input type="text" class="form-control" name="resumen_organizacion" id="resumen_organizacion">

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button id="guardarOrganizacion" type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
        </div>
      </div>

    </div>
  </div>


@endsection

@section('2.3')
  <span id="btnHelpRazonSocialOrganizacion" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpRazonSocialOrganizacion" class="help">
      Indique cual es el nombre que adopta legalmente la organización beneficiaria con quien se realizará el proyecto.
    </span>
  </span>

  <input id="id_organizacion" type="hidden" name="id_organizacion" value="{{{$proyecto->organizacion->id or ''}}}">
{{--  {!!form::text('razon_social_organizacion',null,['id'=>'razon_social_organizacion','class'=>'form-control'])!!} --}}
  <!-- Trigger the modal with a button -->

  <p id="label_razon_social_organizacion">{{{$proyecto->organizacion->nombre or ''}}}</p>
@endsection

@section('2.4')
  {{--{!!form::text('direccion_organizacion',null,['id'=>'direccion_organizacion','class'=>'form-control'])!!}--}}
  <p id="label_direccion_organizacion">{{{$proyecto->organizacion->direccion or ''}}}</p>
@endsection

@section('2.5')
  <p id="label_telefono_organizacion">{{{$proyecto->organizacion->telefono_fijo or ''}}}</p>
  <p id="label_fax_organizacion">{{{$proyecto->organizacion->fax or ''}}}</p>
  <p id="label_correo_electronico_organizacion">{{{$proyecto->organizacion->correo_electronico or ''}}}</p>
@endsection

@section('2.6')
  <span id="btnHelpRepresentanteLegalOrganizacion" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpRepresentanteLegalOrganizacion" class="help">
      Indique los nombres y apellidos completos de la persona que representa legalmente a la organización que promociona o ejecuta el proyecto.
    </span>
  </span>
  <p id="label_representante_legal_organizacion">{{{$proyecto->organizacion->nombre_representante_legal or ''}}}</p>
@endsection

@section('2.7')
  <p id="label_fecha_creacion">{{{$proyecto->organizacion->fecha_creacion or ''}}}</p>
@endsection

@section('2.8')
  <p id="label_nombramiento_representante_legal">{{{$proyecto->organizacion->nombramiento_representante_legal or ''}}}</p>
@endsection

@section('2.9')
  @if(isset($proyecto) && $proyecto->organizacion != null)
  <div id="seccionDelegado">
  @else
  <p id="mensajeSeleccioneOrganizacion">Seleccione una organización para poder seleccionar delegados.</p>
  <div id="seccionDelegado" style="display:none;">
  @endif
    <div class="form-group" id="contenedorBuscarDelegado">
      <input type="text" name="buscar_delegado" id="buscar_delegado" class="form-control" placeholder="Ingrese el nombre del delegado para buscarlo">
    </div>
    <button id="btnAgregarDelegado" type="button" class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#agregarDelegado">Crear nuevo delegado</button>

    <!-- Modal -->
    <div class="modal fade" id="agregarDelegado" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><b>Crear nuevo delegado</b></h4>
          </div>
          <div class="modal-body">
            <form id="">

              <label for="nombre_delegado">Nombre</label>
              <input type="text" class="form-control" name="nombre_delegado" id="nombre_delegado">

              <label for="cargo_delegado">Cargo</label>
              <input type="text" class="form-control" name="cargo_delegado" id="cargo_delegado">

              <label for="area_seccion_delegado">Area o sección</label>
              <input type="text" class="form-control" name="area_seccion_delegado" id="area_seccion_delegado">

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button id="guardarDelegado" type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
          </div>
        </div>

      </div>
    </div>

    <div class="table-w60pe">
      <div class="table-responsive">
        <table id="tablaDelegados" class="table table-bordered">
          @if (isset($proyecto))
            @foreach ($proyecto->delegados as $delegado)
              <tr class="delegado{{$delegado->id}} item-delegado">
                <td>
                  <input type="hidden" name="delegados[]" value="{{$delegado->id}}" class="delegado">
                  <p>
                    {{$delegado->nombre}}
                  </p>
                  {{--{!!Form::text('nombreDelegado', null,['id'=>'nombreDelegado','class'=>'form-control field-as-label', 'style'=>'width: 100%;', 'readonly' => 'readonly'])!!}--}}
                </td>
                <td class="columna-small">
                  <button type="button" name="remove" id="{{ $delegado->id }}" class="btn btn-danger btn_remove remove-delegado">
                    <span class="glyphicon glyphicon-remove"></span>
                  </button>
                </td>
              </tr>
            @endforeach
          @endif
        </table>
      </div>
    </div>
  </div>
@endsection

@section('3.1')
  <span id="btnHelpAntecedentes" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpAntecedentes" class="help">
      En la primera parte se describe a la organización beneficiaria, quienes son, qué hacen, desde cuando lo hacen, quiénes y cuántos son sus beneficiarios, qué tipo de servicios ofrecen, por qué esa organización merece que se la ayude. <br><br>
      Se sugiere un análisis integrado de la o las problemáticas, derivadas de un cuadro general de involucrados y de un árbol de problemas.
    </span>
  </span>
  {!!form::textarea('antecedentes',null,['id'=>'antecedentes','class'=>'form-control','rows'=>5])!!}
@endsection

@section('3.2')
  <span id="btnHelpContexto" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpContexto" class="help">
      Describir todas las características físicas, geográficas, demográficas, sociales, culturales, económicas y políticas de la organización beneficiaria y del área donde se va a realizar el proyecto, incluyendo aquellos aspectos que sean relevantes para comprender la naturaleza de los problemas que se intentan resolver.
      <br><br>
      Al describir las condiciones de contexto, utilizar información y estadísticas secundarias de sustentación que permitan tener una comprensión cabal de la naturaleza y características del contexto que rodea y condiciona a la población que se constituirá en los beneficiarios del proyecto. El proyecto debe evidenciar que es participativo y no impuesto.
    </span>
  </span>
  {!!form::textarea('contexto_proyecto',null,['id'=>'contexto_proyecto','class'=>'form-control','rows'=>5])!!}
@endsection

@section('3.3')
  <span id="btnHelpDefinicionProblema" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpDefinicionProblema" class="help">
      La definición del problema es un proceso de conceptualización en el que se expresan algunas explicaciones acerca del estado actual que guarda el problema que se pretende disminuir o transformar, y sus posibles soluciones. Se debe indicar qué sucede, donde, sucede, desde cuando sucede, y cuáles son las principales implicaciones. De esta manera, quedará explícito que el proyecto vale la pena realizarse y que será útil a la sociedad y a la comunidad universitaria.
    </span>
  </span>
  {!!form::textarea('definicion_problema',null,['id'=>'definicion_problema','class'=>'form-control','rows'=>5])!!}
@endsection

@section('4')
  <span id="btnHelpJustificacion" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpJustificacion" class="help">
      Incluye la factibilidad de poner en ejecución el proyecto; plantea el aporte o alternativa de solución a un problema o necesidad específica; constituye una especie de evaluación social del proyecto. Relaciones con políticas, estrategias nacionales, provinciales, municipales, sectoriales, etc.<br><br>

      Se describe la problemática que pretende resolver el proyecto, fundamentando de manera organizada y sistemática, cuales son los ámbitos y problemas que pretende abordar el proyecto.  Describa la problemática partiendo por el problema central o focal, hacia las causas directas y problemas específicos.<br><br>

      Explicar cuáles son los problemas u obstáculos que el proyecto debe sortear para aprovechar de manera efectiva las oportunidades y potencialidades detectadas en el entorno social, económico, tecnológico, político y comunitario que dan sentido y coherencia a esta alternativa de intervención frente a otras posibles.<br><br>

      El proyecto en correspondencia a la realización de las prácticas pre profesionales, debe estar articulado a los ejes y sectores estratégicos de atención e intervención nacional, regional y en concordancia con los planes zonales, sectoriales y locales, cuya pertinencia debe estar orientada a soluciones creativas e innovadoras en atención a los problemas y tensiones de la presente realidad del Plan Nacional del Buen Vivir. (Ver FOR-UVS-02 y FOR-UVS-21).<br><br>

      Describir como sería el escenario de condiciones deterioradas o agravadas, en caso de que la tendencia actual de la problemática se mantenga, y no se tomen acciones para controlarla o eliminarla, enfatizando los factores que contribuyen actualmente a su agravamiento, así como aquellos que ejercen una influencia positiva a favor de su control y solución. <br><br>

      Para el desarrollo de esta parte se recomienda utilizar el árbol de objetivos y el análisis de involucrados, como los principales insumos para la descripción y análisis de los problemas sobre los cuales se pretende intervenir.<br><br>

      La justificación debe explicar además qué conocimientos nuevos o cuáles de los ya existentes se aplicará al elaborar el proyecto, la utilidad práctica o de otro tipo, su impacto social y los grupos que serán beneficiados.
    </span>
  </span>

  <hr>
  <div class="form-group">
    {!!form::textarea('justificacion',null,['id'=>'justificacion','class'=>'form-control','rows'=>5])!!}
  </div>
@endsection

@section('5')
  <span id="btnHelpPertinencia" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpPertinencia" class="help">
      En la formulación de los proyectos, las unidades académicas deben tomar en cuenta los siguientes estamentos legales del Reglamento de Régimen Académico: <br><br>

      <p style="font-style: italic;">
        Artículo 77: Pertinencia de las carreras y programas académicos.- Se entenderá como pertinencia de las carreras y programas académicos a la articulación de la oferta formativa, de investigación y de vinculación con la sociedad, con el régimen constitucional del Buen Vivir el Plan Nacional de Desarrollo, los planes regionales y locales, los requerimientos sociales en cada nivel territorial y las corrientes internacionales científicas y humanísticas de pensamiento, establecidos en la respectiva normativa”
      </p>
      <br>

      El proyecto debe declarar en forma expresa de qué manera se encuentra articulado a las necesidades de la sociedad del futuro profesional, en total concordancia con el principio de pertinencia de la carrera. De acuerdo al CEAACES este indicador evalúa los programas y proyectos relacionados con las necesidades identificadas en el análisis del estado actual y prospectivo de la carrera.
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('pertinencia',null,['id'=>'pertinencia','class'=>'form-control','rows'=>5])!!}
  </div>
@endsection

@section('6')
  <span id="btnHelpObjetivos" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpObjetivos" class="help">
      Constituye lo que se pretende alcanzar (¿qué?, ¿dónde?, ¿para  qué?). Precisa la finalidad del proyecto, en cuanto a sus expectativas más amplias. Orienta el proyecto. Son aquellos que expresan un logro sumamente amplio y son formulados como propósito general del proyecto.<br><br>

      El objetivo general de un proyecto debe plantearse con claridad y como respuesta a la pregunta sobre lo que el proyecto intentará alcanzar, permiten introducir flexibilidad en los  medios para alcanzar los objetivos. Además debe solucionar el problema descrito en la DEFINICIÓN DEL PROBLEMA de este documento.<br><br>

      Los objetivos específicos representan los pasos que se han de realizar para alcanzar el objetivo general. Facilitan el cumplimiento del objetivo general, mediante la determinación de etapas o la precisión y cumplimiento de los aspectos necesarios de este proceso. Señalan propósitos o requerimientos en orden a la naturaleza del proyecto. Se derivan del general y, como su palabra lo dice, inciden directamente en los logros a obtener. Deben ser formulados en términos operativos, incluyen las variables o indicadores que se desean medir. Las causas del problema orientan su redacción, mediante el gráfico se puede ubicar y hacer una relación entre el problema y el objetivo del proyecto.
    </span>
  </span>
  <hr>

  {!!form::label('Objetivo General')!!}
  {!!form::textarea('objetivo_general',null,['id'=>'objetivo_general','class'=>'form-control','rows'=>3, 'style'=>'resize:none;'])!!}

  {{-- SECCION DE OBJETIVOS ESPECIFICOS --}}
  <div class="row" style="margin-top:15px">
    <div class="col-md-8" style="width:100%">
       <div class="panel panel-default">
         <div class="panel-heading">
            Lista de Objetivos Especificos
          </div>
          <div class="panel-body">
            <div class="form-group" style="margin-top:10px">
                <div class="col-lg-4"  style="padding-left: 0px !important;">
                  <input type="text" class="form-control" id="nombreObjetivoEspecifico"  placeholder="Ingrese objetivo específico">
                </div>
                @if(Auth::user()->coordinadorCarreraEmpresariales == null)
                  <button type="button" id="addObjetivoEspecifico" class="btn btn-success" name="comunitarias" >Agregar Objetivo especifico</button>
                @else
                  <button type="button" id="addObjetivoEspecifico" class="btn btn-success" name="empresarial" >Agregar Objetivo especifico</button>
                @endif
            </div>

            <div class="table-responsive">
              <table class="table table-bordered" id="tabla_objetivos_especificos">
                <thead>
                   <th> Descripción Objetivo </th>
                   <th width="10%"> Accion </th>
                </thead>
                <tbody>
                  @if(isset($objetivosEspecificos))
                    @foreach ($objetivosEspecificos as $objetivoEspecifico)
                      <tr id="tablaObjetivosEspecificosRow-saved{{$objetivoEspecifico->id}}">
                        <td>
                          <p id="textoObjetivoEspecificosaved{{$objetivoEspecifico->id}}">{{$objetivoEspecifico->descripcion}}</p>
                        </td>
                        <td class="sv-cell-contain-width">
                          @if(Auth::user()->coordinadorCarreraEmpresariales == null)
                            <button type="button" value="comunitarias" name="saved{{$objetivoEspecifico->id}}" class="sv-icon-button edit-objetivoEspecifico">
                              <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                            </button>
                            <button type="button" value="comunitarias" name="saved{{$objetivoEspecifico->id}}" class="sv-icon-button remove-objetivoEspecifico">
                              <i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>
                            </button>
                          @else
                            <button type="button" value="empresarial" name="saved{{$objetivoEspecifico->id}}" class="sv-icon-button edit-objetivoEspecifico">
                              <i class="fa fa-pencil fa-lg" aria-hidden="true" style="color:rgb(45, 73, 131);"></i>
                            </button>
                            <button type="button" value="empresarial" name="saved{{$objetivoEspecifico->id}}" class="sv-icon-button remove-objetivoEspecifico">
                              <i class="fa fa-minus-circle fa-lg" aria-hidden="true" style="color:rgb(255, 0, 0);"></i>
                            </button>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
       </div>
     </div>
    </div>
@endsection

@section('7')
  <span id="btnHelpMatrizMarcoLogico" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpMatrizMarcoLogico" class="help">
      La Matriz de Marco Lógico, es una herramienta de apoyo, que sirve para poder medir los avances del proyecto, y establecer de manera clara los resultados o productos esperados.<br><br>

      Los indicadores verificables: Son las acciones o resultados esperados que indican que el objetivo o las actividades se han cumplido, por lo general se miden y se establecen en cantidades o porcentajes, para revisar sus cumplimientos.<br><br>

      Los Medios de Verificación: Son las evidencias con las que se contará para comprobar que el indicador se cumplió, ejemplo, actas de reuniones, listados de asistencia, informes técnicos, avances de trabajos, reportes de horas prácticas, matriz de evaluación, etc.<br><br>

      Los Supuestos, son los acontecimientos, condiciones o decisiones que tienen que  suceder para cumplir con los Componentes u objetivos del proyecto.<br><br>

      Los objetivos narrativos se redactan en pasado, es decir considerando que las acción ya ha ocurrido. Ejemplo:<br><br>

      <ul style="list-style-type: disc">
        <li>Objetivo General: Diseñar una campaña publicitaria sobre la equidad de género.</li>
        <li>Objetivo Narrativo: Campaña publicitaria sobre la equidad de género diseñada.</li>
      </ul>
    </span>
  </span>
  <hr>

  <div class="table-responsive">
    <table class="table table-bordered" id="tabla_matriz_MarcoLogico">
      <thead>
        <tr>
         <th width="40%">Objetivos Narrativos</th>
         <th width="20%">Indicadores Verificables</th>
         <th width="20%">Medios de verificación</th>
         <th width="20%">Supuestos</th>
       </tr>
      </thead>
      <tbody id="body-matriz-marcoLogico">
        <tr>
          <td>
            <b>FIN</b>
            <p id="textObjetivoProgramaMatrizML">
              @if(isset($proyecto))
                @if($programa != null)
                  {{ $programa->objetivo_general }}
                @endif
              @endif
            </p>
          </td>
          <td>
            {!!form::textarea('indicador_verificable_objetivo_programa',null,['id'=>'indicador_verificable_objetivo_programa','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'  ])!!}
          </td>
          <td>
            {!!form::textarea('medio_verificacion_objetivo_programa',null,['id'=>'medio_verificacion_objetivo_programa','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'])!!}
          </td>
          <td>
            {!!form::textarea('supuestos_objetivo_programa',null,['id'=>'supuestos_objetivo_programa','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'])!!}
          </td>
        </tr>
        <tr>
          <td>
            <b>PROPÓSITO</b>
            <p id="textObjetivoGeneralMatrizML">
              @if(isset($proyecto))
                {{$proyecto->objetivo_general}}
              @endif
             </p>
          </td>
          <td>
            {!!form::textarea('indicador_verificable_objetivo_general',null,['id'=>'indicador_verificable_objetivo_general','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'  ])!!}
          </td>
          <td>
            {!!form::textarea('medio_verificacion_objetivo_general',null,['id'=>'medio_verificacion_objetivo_general','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'])!!}
          </td>
          <td>
            {!!form::textarea('supuestos_objetivo_general',null,['id'=>'supuestos_objetivo_general','class'=>'form-control','rows'=>5, 'style'=>'resize:none; border:0;'])!!}
          </td>
        </tr>
        @if(isset($objetivosEspecificos))
          @foreach ($objetivosEspecificos as $objetivoEspecifico)
            <tr id="tablaMarcoLogicoRow-saved{{$objetivoEspecifico->id}}">
              <td colspan="4" style="background: rgb(170, 170, 158)">
                <p id="textOEMatrizMLsaved{{$objetivoEspecifico->id}}">{{$objetivoEspecifico->descripcion}}</p>
                <input id="valueOEMatrizMLsaved{{$objetivoEspecifico->id}}" name="valueObjetivoEspecifico-{{$objetivoEspecifico->id}}" type="hidden" value="{{$objetivoEspecifico->descripcion}}" />
                <input name="idObjetivosEspecificosSaved[]" type="hidden" value="{{$objetivoEspecifico->id}}" />
              </td>
            </tr>
            <tr id="tablaMarcoLogicoRow-saved{{$objetivoEspecifico->id}}-1">
              <td>
                @if(Auth::user()->coordinadorCarreraEmpresariales == null)
                  <b>Actividades:</b>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="tabla_actividades_objetivoEspecificoSaved{{$objetivoEspecifico->id}}">
                      @if($objetivoEspecifico->actividades != null)
                        @foreach($objetivoEspecifico->actividades as $actividad)
                          <tr id="row-AOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}">
                           <td>
                             <input type="textarea" id="row-actOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" rows="2" class="form-control actividad-obj", style="resize:none; border:0;" value="{{$actividad->descripcion}}"/>
                           </td>
                           <td width="5%">
                             <button type="button" name="remove" id="AOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" class="btn btn-danger btn_remove_actividad small-button"><span class="glyphicon glyphicon-remove"></span></button>
                           </td>
                         </tr>
                        @endforeach
                      @endif
                    </table>
                    <button type="button" name="addActividadObjEsp" id="addActividadObjEsp" value="Saved{{$objetivoEspecifico->id}}" class="btn btn-success btn_add_actividad small-button">
                      <span class="glyphicon glyphicon-plus"></span>
                    </button>
                  </div>
                @endif
              </td>
              <td>
                <textarea name="indicadorVerificableObjetivoEspecifico-{{$objetivoEspecifico->id}}" rows="5" class="form-control ", style="resize:none; border:0;">{{$objetivoEspecifico->indicadores_verificables}}</textarea>
                {{--
                {!!Form::textarea('indicadorVerificableObjetivoEspecifico-'.$objetivoEspecifico->id,$objetivoEspecifico->indicadores_verificables,['class'=>'form-control objCkEditor','rows'=>5])!!}
                --}}
              </td>
              <td>
                <textarea name="mediosVerificacionObjetivoEspecifico-{{$objetivoEspecifico->id}}" rows="5" class="form-control", style="resize:none; border:0;">{{$objetivoEspecifico->medios_verificacion}}</textarea>
              </td>
              <td>
                <textarea name="supuestosObjetivoEspecifico-{{$objetivoEspecifico->id}}" rows="5" class="form-control", style="resize:none; border:0;">{{$objetivoEspecifico->supuestos}}</textarea>
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
 </div>
@endsection

@section('8')
  <span id="btnHelpMetodologiaTrabajo" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpMetodologiaTrabajo" class="help">
      Se explica brevemente los lineamientos metodológicos que utilizará el proyecto, mencionando las principales estrategias y líneas de acción, relacionadas con las metas y actividades. Es decir, se describe cómo se va a desarrollar paso a paso el proyecto, empezando por la formulación, el diseño, la ejecución, la gestión, el monitoreo, la evaluación y la operación. También es importante hacer referencia a aspectos como la utilización de técnicas e instrumentos para la recolección de información. <br><br>

      La metodología a utilizar para el proyecto debe estar relacionada a una o varias cátedras de la carrera. En esta sección se debe mencionar explícitamente qué cátedras y que objetivos educacionales o resultados de aprendizaje se alcanzarán con la ejecución del proyecto. <br><br>

      Para efectos de publicación, si es el caso, sería conveniente que se mencione los métodos y tipos de investigación, así como técnicas y herramientas utilizadas para la recolección o levantamiento de información utilizadas (Entrevistas, Observación, Encuestas, Grupos Focales, Estudios de Caso, Prototipos,  Documental, etc.)
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('metodologia',null,['id'=>'metodologia','class'=>'form-control','rows'=>5])!!}
  </div>

  <hr>
  {!!form::label('resultados','Resultados de aprendizaje')!!}
  {!!form::text('buscar_materia',null,['id'=>'buscar_materia','class'=>'form-control', 'placeholder'=>'Ingrese nombre de materia'])!!}
  <div class="table-responsive" style="margin-top:10px">
    <table class="table table-bordered" id="tabla_resultados_aprendizaje">
      <tr>
        <td width='30%'>Materia</td>
        <td width='60%'>Resultado de Aprendizaje</td>
        <td>Accion</td>
      </tr>
      @if(isset($proyecto))
        @foreach($proyecto->resultadosAprendizaje as $resultado)
          <tr id="row-resultado-aprendizaje{{$resultado->materia->id}}">
            <td>
              <input name="materias[]" type="hidden" value="{{$resultado->materia->id}}" /> {{$resultado->materia->nombre}}
            </td>
            <td>
              {!!form::textarea('resultadosAprendizaje[]',$resultado->descripcion, ['class'=>'form-control name_list', 'placeholder' => 'Resultado Aprendizaje','rows'=>2, 'style'=>'resize:none;'])!!}
            </td>
            <td>
              <button type="button" name="remove" id="resultado-aprendizaje{{$resultado->materia->id}}" class="btn btn-danger btn_remove_resultado_aprendizaje"><span class="glyphicon glyphicon-remove"></span></button>
            </td>
          </tr>
        @endforeach
      @endif
    </table>
  </div>
@endsection

@section('9')
  <span id="btnHelpCronograma" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpCronograma" class="help">
      Se recomienda presentar el cronograma de actividades utilizando un diagrama Gantt, que consiste en una matriz de doble entrada que relaciona a cada una de las actividades principales del proyecto con los tiempos que durarán dichas actividades (Matriz de Marco Lógico). Dado que generalmente es difícil saber con certeza en qué mes y año se iniciara el proyecto, es conveniente numerar las semanas, meses y años en vez de la denominación. En su momento, se reemplazan los números por sus nombres.
    </span>
  </span>
  <hr>
  <div class="form-group">
    @if(isset($proyecto))
      <button type="button" class="sv-gray-button" onclick="descargarCronograma({{$proyecto->id}})">Ver cronograma adjunto</button>
      {{--}}<a id="urlCronograma" href="{{ route('verCronograma', $proyecto->id) }}" target="_blank">Ver cronograma (Si existe alguno)</a>--}}
    @endif
    {{--<p>Seleccione el archivo correspondiente al cronograma:</p>--}}
    <input type="file" name="cronogramaUpload" id="cronogramaUpload" style="margin-top:15px;">
  </div>

  @if(Auth::user()->coordinadorCarreraEmpresariales == null)
    <div id="componenteActividades">
      @if(isset($objetivosEspecificos))
        @foreach ($objetivosEspecificos as $objetivoEspecifico)
          <div class="table-responsive" id="div-componenteActividadesObjetivo-saved{{$objetivoEspecifico->id}}" style="margin-top: 25px">
            <table class="table table-bordered" id="tabla-actividades-cronograma-objetivoSaved{{$objetivoEspecifico->id}}">
              <thead>
                <tr>
                  <th colspan="5" name="tablecomponente">
                    <p id="tituloOECronogramasaved{{$objetivoEspecifico->id}}">{{$objetivoEspecifico->descripcion}}</p>
                  </th>
                </tr>
                <tr>
                  <th width="50%">Actividades </th>
                  <th width="10%">Horas</th>
                  <th width="35%">Fecha de Inicio - Fin</th>
                </tr>
              </thead>
              <tbody>
                @if($objetivoEspecifico->actividades != null)
                  @foreach($objetivoEspecifico->actividades as $actividad)
                    <tr id="rowactividad-cronograma-AOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}">
                      <td>
                        <p id="textActividad-row-actOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}"> {{$actividad->descripcion}} </p>
                        <input id="inputActividad-row-actOESaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" name="descripcionActividadObjetivoEspecificoSaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" rows="2" type="hidden" value="{{$actividad->descripcion}}"/>
                      </td>
                      <td>
                        <input type="text" class="form-control" name="horasActividadObjetivoEspecificoSaved{{$objetivoEspecifico->id}}-{{$actividad->id}}" style="border:0" value="{{$actividad->duracion_horas}}"/>
                      </td>
                      <td>
                        {!!form::text("rangoFechaActividadObjetivoEspecificoSaved".$objetivoEspecifico->id."-".$actividad->id, date("d/m/Y", strtotime($actividad->fecha_inicio))." - ".date("d/m/Y", strtotime($actividad->fecha_finalizacion)),["class"=>"form-control date_ranger_picker"])!!}
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            <hr style="border-top: 3px solid #eee;">
          </div>
        @endforeach
      @endif
    </div>
  @else
    <p>
      El cronograma será llenado individualmente por cada estudiante
    </p>
  @endif

@endsection

@section('10')
  <span id="btnHelpProductosEsperados" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpProductosEsperados" class="help">
      Este es uno de los elementos más importantes del proyecto. Se refiere a lo que se va a conseguir al término de ejecución del proyecto. Constituye la solución a la problemática identificada en el proyecto.<br><br>

      Por su redacción se debe acudir a la ejecución de todas las actividades y cumplimiento de indicadores propuestos al inicio del proyecto. Se debe detallar que productos o servicios se va a entregar a la organización beneficiaria.<br><br>

      Se debe señalar qué es lo que lo que implementarán según el caso, si hace una capacitación determinar: ¿a quiénes?, ¿número de personas capacitadas?, ¿cuántas horas durará el proceso de capacitación?, ¿de qué se trata la capacitación?… adicionalmente deben señalar lo que entregarán físicamente a la institución (manuales, discos, material, etc.). Y finalmente indicar la fecha de entrega de resultados  del proyecto a Unidad de Vinculación con la Sociedad.<br><br>

      Detalle sobre lo que se entregará a la organización beneficiaria, esto es el detalle del producto o servicio si fuera el caso, por ejemplo para el proyecto de una página web, los productos serían:<br><br>

      <ul style="list-style-type: decimal">
        <li>Una red………</li>
        <li>Un curso de… de XX horas</li>
        <li>Una capacitación a….  en…… de XX horas</li>
        <li>Un informe técnico</li>
        <li>Una evaluación</li>
        <li>Un Plan….</li>
        <li>Un manual impreso y/o digital de……</li>
        <li>Material didáctico digital o impreso</li>
        <li>Otros</li>
      </ul>
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('productos_esperados',null,['id'=>'productos_esperados','class'=>'form-control','rows'=>5])!!}
  </div>
@endsection

@section('11')
  <span id="btnHelpCompromisosOrganizacion" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpCompromisosOrganizacion" class="help">
      Detalle de las actividades que la organización beneficiaria se compromete a realizar para facilitar el trabajo del estudiante. Ejemplo:<br><br>

      <ul style="list-style-type: disc">
        <li>Designar a una persona como responsable por parte de la organización beneficiaria para que supervise el trabajo desarrollado por el estudiante. </li>
        <li>Brindar un espacio físico para… </li>
        <li>Permitir el uso de equipos…</li>
        <li>Accesibilidad a internet…</li>
        <li>Controlar la asistencia mediante….</li>
      </ul>
    </span>
  </span>

  <hr>
  <div class="form-group">
    {!!form::textarea('compromisos_organizacion',null,['id'=>'compromisos_organizacion','class'=>'form-control','rows'=>5])!!}
  </div>
@endsection

@section('12')
  <span id="btnHelpPresupuesto" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpPresupuesto" class="help">
      Para elaborar el presupuesto por favor utilice el formato UVS-08 para cada año de ejecución del proyecto y anexarlo a este documento. <br><br>

      Es importante elaborar un presupuesto detallado de los costos del proyecto, indicando el monto de los diferentes rubros. Se debe incluir todos los rubros que abarca el proyecto, ya que de esto depende la asignación de los fondos, además se debe utilizar el formato sugerido. (Ver FOR-UVS-08).
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('presupuesto',null,['id'=>'presupuesto','class'=>'form-control','rows'=>5])!!}
  </div>
  <div class="form-group">
    @if(isset($proyecto))
      <button type="button" class="sv-gray-button" onclick="descargarPresupuesto({{$proyecto->id}})">Ver presupuesto adjunto</button>
      {{--}}<a id="urlPresupuesto" href="{{ route('verPresupuesto', $proyecto->id) }}" target="_blank">Ver presupuesto (Si existe alguno)</a>--}}
    @endif
    {{--<p>Seleccione el archivo correspondiente al cronograma:</p>--}}
    <input type="file" name="presupuestoUpload" id="presupuestoUpload" style="margin-top:15px;">
  </div>
@endsection

@section('13')
  <span id="btnHelpEvaluacion" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpEvaluacion" class="help">
      Se explica brevemente el sistema de monitoreo y evaluación que se utilizará. Aquí se debe hacer mención a los indicadores objetivamente verificables y los medios de verificación que han sido identificadas para cada nivel de la ejecución. <br><br>

      Adicionalmente se puede explicar qué personal se hará cargo del monitoreo (Ver FOR-UVS-13) y evaluación estableciendo las fechas respectivas. <br><br>

      Cuando se haya terminado la ejecución del proyecto, el docente director del proyecto deberá realizar un informe final de cierre del proyecto (Ver FOR-UVS-17) y entregarlo al docente responsable de vinculación, adjuntando todos los reportes, listas de asistencia, actas de las reuniones, informe final del estudiante (Ver FOR-UVS-14), informe final de la organización beneficiaria (Ver FOR-UVS-15), informe final del docente tutor (Ver FOR-UVS-16) y demás informes que se hayan hecho contar como indicadores de que el proyecto ha finalizado, para proceder a la entrega final a la organización beneficiaria. <br><br>

      En cuanto a la evaluación de los resultados de la práctica preprofesional que realizan los estudiantes, se recomienda, que el profesor tutor adjunte la matriz de evaluación.
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('evaluacion',null,['id'=>'evaluacion','class'=>'form-control','rows'=>5])!!}
    <ul stylt="margin-bottom: 10px">
      <li><a href="{{ URL::route('verForUVS13') }}" target="_blank">(Ver FOR-UVS-13)</a></li>
      <li><a href="{{ URL::route('verForUVS17') }}" target="_blank">(Ver FOR-UVS-17)</a></li>
    </ul>
  </div>
@endsection

@section('14')
  <span id="btnHelpBibliografia" class="glyphicon glyphicon-question-sign btn-help">
    <span id="helpBibliografia" class="help">
      Recomendamos utilizar la Norma APA Sexta Edición, para citas, formato y bibliografía.<br><br>

      Colocar en esta sección los libros y/o artículos científicos consultados para la realización del proyecto.  Es importante que el proyecto cuente con una revisión de literatura a fin de que pueda ser publicado en una revista científica. No es necesario estructurar un marco teórico, sin embargo las consultas bibliográficas se pueden evidenciar a lo largo del proyecto.
    </span>
  </span>
  <hr>
  <div class="form-group">
    {!!form::textarea('bibliografia',null,['id'=>'bibliografia','class'=>'form-control','rows'=>5])!!}
  </div>
@endsection

@section('form-buttons')
  <button id="btnGuardar" type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>

  @if(Auth::user()->coordinadorCarreraEmpresariales != null)
    <button id="btnAprobar" type="submit" class="btn btn-primary btn-enviar" name="submit" value="aprobar" style="margin-left: 30px; width: 120px;">Aprobar</button>
  @else
    <button id="btnEnviar" type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar" style="margin-left: 30px; width: 120px;">Enviar</button>
  @endif

@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection

@section('javascript-code')
  {!!Html::script('js/formularios/forUVS04.js')!!}

  <script type="text/javascript">
    $('#carrera_principal').autocomplete({
        source: '{!!URL::route('autocompleteCarrera')!!}',
        minlength: 3,
        autoFocus: true,
        select: function(event, response){
            $('.carrera-principal').remove();

            var idTipoProyecto = $('#tipo_proyecto').val();

            if (parseInt(idTipoProyecto) == 1 && response.item.coordinadorComunitarias == null) {
              //La carrera no tiene coordinador de pasantias comunitarias

              bootbox.alert("No puede seleccionar esta carrera, ya no tiene asignado coordinador de pasantías comunitarias.");
            } else if (parseInt(idTipoProyecto) == 2 && response.item.coordinadorEmpresariales == null) {
              //La carrera no tiene coordinador de pasantias empresariales

              bootbox.alert("No puede seleccionar esta carrera, ya no tiene asignado coordinador de pasantías empresariales.");
            } else if ($('.carreras-proyecto #'+response.item.id).length > 0) {
              //La carrera ya esta como carrera adicional

              bootbox.alert("La carrera ya forma parte del proyecto como carrera adicional. Si desea agregarla como carrera principal, quítela de la sección de carreras adicionales primero.");

            } else {
              //La carrera no esta como carrera adicional
              $('#tabla_carrera_principal').append('<tr class="carrera-principal carrera-' + response.item.id + '"><td><input name="carreraPrincipal" type="hidden" value="' + response.item.id + '" />' + response.item.value + '</td><td class="columna-small"><button type="button" name="remove" id="' + response.item.id + '" class="btn btn-danger btn_remove remove-carrera"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');

              var idTipoProyecto = $('#tipo_proyecto').val();

              if (parseInt(idTipoProyecto) == 1) { //PAsantias Comunitarias
                //Seteo la informacion del coordinador en la seccion 1.9
                $('#nombreCoordinador').html(response.item.coordinadorComunitarias.nombres + ' ' + response.item.coordinadorComunitarias.apellidos);
                $('#cedulaCoordinador').html(response.item.coordinadorComunitarias.cedula);
                $('#telefonoCoordinador').html(response.item.coordinadorComunitarias.telefono_fijo);
                $('#celularCoordinador').html(response.item.coordinadorComunitarias.celular);
                $('#correoCoordinador').html(response.item.coordinadorComunitarias.correo_electronico);
              } else {
                //Seteo la informacion del coordinador en la seccion 1.9
                $('#nombreCoordinador').html(response.item.coordinadorEmpresariales.nombres + ' ' + response.item.coordinadorEmpresariales.apellidos);
                $('#cedulaCoordinador').html(response.item.coordinadorEmpresariales.cedula);
                $('#telefonoCoordinador').html(response.item.coordinadorEmpresariales.telefono_fijo);
                $('#celularCoordinador').html(response.item.coordinadorEmpresariales.celular);
                $('#correoCoordinador').html(response.item.coordinadorEmpresariales.correo_electronico);
              }
            }
        }
    });

    $('#carrera').autocomplete({
          source: '{!!URL::route('autocompleteCarrera')!!}',
          minlength: 3,
          autoFocus: true,
          select: function(event, response){
            if ($('.carreras-proyecto #'+response.item.id).length > 0) {
              //La carrera ya esta en el proyecto

              bootbox.alert("La carrera ya forma parte del proyecto (Si la carrera esta agregada como carrera principal y desea agregarla como carrera adicional, quítela de la sección de carrera principal primero).");

            } else {
              $('#tabla_carreras').append('<tr class="carrera-' + response.item.id + '"><td><input name="carrerasAdicionales[]" type="hidden" value="' + response.item.id + '" />' + response.item.value + '</td><td class="columna-small"><button type="button" name="remove" id="' + response.item.id + '" class="btn btn-danger btn_remove remove-carrera"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
            }
        }
    });

    $('#guardarOrganizacion').click(function() {
      var token = $('input[name=_token]').val(),
          razon_social_organizacion = $('#razon_social_organizacion').val(),
          telefono_organizacion = $('#telefono_organizacion').val(),
          direccion_organizacion = $('#direccion_organizacion').val(),
          fax_organizacion = $('#fax_organizacion').val(),
          correo_electronico_organizacion = $('#correo_electronico_organizacion').val(),
          representante_legal_organizacion = $('#representante_legal_organizacion').val(),
          fecha_creacion = $('#fecha_creacion').val(),
          nombramiento_representante_legal = $('#nombramiento_representante_legal').val(),
          resumen_organizacion = $('#resumen_organizacion').val(),

          dataString = 'razon_social_organizacion=' + razon_social_organizacion + '&telefono_organizacion=' + telefono_organizacion + '&direccion_organizacion=' + direccion_organizacion + '&fax_organizacion=' + fax_organizacion + '&correo_electronico_organizacion=' + correo_electronico_organizacion + '&representante_legal_organizacion=' + representante_legal_organizacion + '&nombramiento_representante_legal=' + nombramiento_representante_legal + '&resumen_organizacion=' + resumen_organizacion + '&fecha_creacion=' + fecha_creacion + '&_token=' + token;

      $.ajax({
          //url: 'guardar_organizacion',
          url: '{!!URL::route('guardar_organizacion')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
              console.log(data);
              $('#id_organizacion').val(data.id_organizacion);
              $('#mensajeSeleccioneOrganizacion').hide();
              $('.item-delegado').remove();
              $('#buscar_delegado').remove();
              $('#contenedorBuscarDelegado').append('<input type="text" name="buscar_delegado" id="buscar_delegado" class="form-control" placeholder="Ingrese el nombre del delegado para buscarlo">');
              $('#buscar_delegado').autocomplete({
                    source: 'autocompleteDelegado/'+data.id_organizacion,
                    minlength: 3,
                    autoFocus: true,
                    select: function(event, response){
                      $('#tablaDelegados').append('<tr class="delegado' + response.item.id + '" value="' + response.item.id + '"><td><input type="hidden" name="delegados[]" value="' + response.item.id + '"  class="delegado"><p>' + response.item.value + '</p></td><td class="columna-small"><button type="button" name="remove" id="' + response.item.id + '" class="btn btn-danger btn_remove remove-delegado"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
                  }
                });
                $('#seccionConvenio').show();
                $('#urlConvenio').attr('href', 'verConvenio/'+data.id_organizacion);
                $('#seccionDelegado').show();
                $('#label_razon_social_organizacion').html(razon_social_organizacion);
                $('#label_telefono_organizacion').html(telefono_organizacion);
                $('#label_direccion_organizacion').html(direccion_organizacion);
                $('#label_fax_organizacion').html(fax_organizacion);
                $('#label_correo_electronico_organizacion').html(correo_electronico_organizacion);
                $('#label_representante_legal_organizacion').html(representante_legal_organizacion);
                $('#label_fecha_creacion').html(fecha_creacion);
                $('#label_nombramiento_representante_legal').html(nombramiento_representante_legal);
                $('#btnQuitarOrganizacion').show();          }
      });
    });

    $('#guardarEstudiante').click(function() {
      var matricula_estudiante = $('#matriculaEstudiante').val();

      /* Primero valido que el usuario haya ingresado un numero de matricula valido */
      if (matricula_estudiante <= 99999999  || matricula_estudiante > 999999999)  {
          bootbox.alert("Ingrese un número de  matricula válido.");
          return;
      }

      /*
      * Bandera que me sirve para saber si se trata de agregar a la lista de estudiantes
      * un estudiante que ya se encuentra en dicha lista.
      */
      var estudianteAgregado = false;
      // Recorro la tabla de estudiante
      $("#tabla_estudiantes tbody tr").each(function (index)
       {
           var campo2;
           $(this).children("td").each(function (index2)
           {
               switch (index2)
               {
                   case 1: campo2 = $(this).text();
                           break;
               }
           })
           if(campo2 == matricula_estudiante) {
             estudianteAgregado = true;
           }
       });

       /*
       * Si el estudiante aún no ha sido agregado, entonces se envia un requerimiento para
       * obtener los datos del estudiante, siempre y cuando el estudiante exista.
       */
       if(estudianteAgregado == false) {
         var token = $('input[name=_token]').val(),
             id_tipo_proyecto = $('#tipo_proyecto').val(),
             dataString = 'matricula_estudiante=' + matricula_estudiante + '&id_tipo_proyecto=' + id_tipo_proyecto + '&_token=' + token;

         $.ajax({
             url: '{!!URL::route('obtener_estudiante')!!}',
             type: 'POST',
             data: dataString,
             success: function (data) {
                 if(data.sucess == true) {
                   $('#tabla_estudiantes').append('<tr class="rowNewEstudiante' + data.id_estudiante + '"><td>'+data.nombrecompleto+'<input name="estudianteProyecto[]" type="hidden" value="' + data.id_estudiante + '"/></td> <td>'+matricula_estudiante+'</td><td>'+data.telefono+'</td><td>'+data.correo_electronico+'</td><td class="columna-small"><button type="button" name="remove" id="NewEstudiante'+data.id_estudiante+'" class="btn btn-danger  btn_remove_estudiante"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');

                    $('#matriculaEstudiante').val('');
                 }
                 else{
                   bootbox.alert(data.message);
                 }
             }
         });
       }
       else{
         bootbox.alert("El estudiante ya fue agregado al proyecto.");
       }
    });

    /* Se elimina el estudiante seleccionado */
    $(document).on('click', '.btn_remove_estudiante', function(){
      var button_id = $(this).attr("id");

      bootbox.confirm("¿Esta seguro de desasignar al estudiante del proyecto?", function(result) {
        if(result == true){
          //Se eliminar row del estudiante en la tabla de Estudiantes.
          $('.row'+button_id+'').remove();
        }
      });
    });

    $('#guardarDelegado').click(function() {
      var token = $('input[name=_token]').val(),
          nombre_delegado = $('#nombre_delegado').val(),
          cargo_delegado = $('#cargo_delegado').val(),
          area_seccion_delegado = $('#area_seccion_delegado').val(),
          id_organizacion = $('#id_organizacion').val(),

          dataString = 'nombre_delegado=' + nombre_delegado + '&cargo_delegado=' + cargo_delegado + '&area_seccion_delegado=' + area_seccion_delegado + '&id_organizacion=' + id_organizacion + '&_token=' + token;

      $.ajax({
          //url: 'guardar_organizacion',
          url: '{!!URL::route('guardar_delegado')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
              $('#tablaDelegados').append('<tr class="delegado' + data.id_delegado + '" value="' + data.id_delegado + '"><td><input type="hidden" name="delegados[]" value="' + data.id_delegado + '"  class="delegado"><p>' + data.nombre_delegado + '</p></td><td class="columna-small"><button type="button" name="remove" id="' + data.id_delegado + '" class="btn btn-danger btn_remove remove-delegado"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
          }
      });
    });

    $('#nombre_organizacion').autocomplete({
          source: '{!!URL::route('autocompleteOrganizacion')!!}',
          minlength: 3,
          autoFocus: true,
          select: function(event, response){
            $('#id_organizacion').val(response.item.id);
            $('#mensajeSeleccioneOrganizacion').hide();
            $('#buscar_delegado').remove();
            $('#contenedorBuscarDelegado').append('<input type="text" name="buscar_delegado" id="buscar_delegado" class="form-control" placeholder="Ingrese el nombre del delegado para buscarlo">');
            $('#buscar_delegado').autocomplete({
                  source: 'autocompleteDelegado/'+response.item.id,
                  minlength: 3,
                  autoFocus: true,
                  select: function(event, response){
                    console.log(response);
                    $('#tablaDelegados').append('<tr class="delegado' + response.item.id + '" value="' + response.item.id + '"><td><input class="delegado" type="hidden" name="delegados[]" value="' + response.item.id + '"><p>' + response.item.value + '</p></td><td class="columna-small"><button type="button" name="remove" id="' + response.item.id + '" class="btn btn-danger btn_remove remove-delegado"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
                }
              });
              $('#seccionConvenio').show();
              $('#urlConvenio').attr('href', 'verConvenio/'+response.item.id);
             $('#seccionDelegado').show();
             $('#label_razon_social_organizacion').html(response.item.organizacion.nombre);
             $('#label_telefono_organizacion').html(response.item.organizacion.telefono_fijo);
             $('#label_direccion_organizacion').html(response.item.organizacion.direccion);
             $('#label_fax_organizacion').html(response.item.organizacion.fax);
             $('#label_correo_electronico_organizacion').html(response.item.organizacion.correo_electronico);
             $('#label_representante_legal_organizacion').html(response.item.organizacion.nombre_representante_legal);
             $('#label_fecha_creacion').html(response.item.organizacion.fecha_creacion);
             $('#label_nombramiento_representante_legal').html(response.item.organizacion.nombramiento_representante_legal);
             $('#btnQuitarOrganizacion').show();
           }
    });

    $('#programaProyecto').on('change', function() {

      var token = $('input[name=_token]').val(),
          id_programa = this.value,
          dataString = 'id_programa=' + id_programa + '&_token=' + token;

          $.ajax({
              url: '{!!URL::route('obtener_programa')!!}',
              type: 'POST',
              data: dataString,
              success: function (data) {
                  if(data.sucess == true) {
                    /* Inserto el objetivo general del programa en el row correspondiente de la matriz de marco lógica */
                    $('#textObjetivoProgramaMatrizML').text(data.objetivoGeneral);
                  }
                  else{
                      $('#textObjetivoProgramaMatrizML').text('');
                  }
              }
          });

    });

    //Buscar Materia
    $('#buscar_materia').autocomplete({
        source: '{!!URL::route('autocompleteMateria')!!}',
        minlength: 3,
        autoFocus: true,
        select: function(event, response){
          $('#tabla_resultados_aprendizaje').append('' +
          '<tr id="row-resultado-aprendizaje' + response.item.id + '">' +
            '<td>' +
              '<input name="materias[]" type="hidden" value="' + response.item.id + '" />' + response.item.value +
            '</td>' +
            '<td>' +
              '{!!form::textarea('resultadosAprendizaje[]',null, ['class'=>'form-control name_list', 'placeholder' => 'Resultado Aprendizaje','rows'=>2, 'style'=>'resize:none;'])!!}' +
            '</td>' +
            '<td>' +
              '<button type="button" name="remove" id="resultado-aprendizaje' + response.item.id + '" class="btn btn-danger btn_remove_resultado_aprendizaje"><span class="glyphicon glyphicon-remove"></span></button>' +
            '</td>' +
          '</tr>');
        }
    });

    function descargarPresupuesto(id_proyecto) {

      var token = $('input[name=_token]').val(),
          dataString = 'id_proyecto=' + id_proyecto + '&_token=' + token;

      $.ajax({
          url: '{!!URL::route('existePresupuesto')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
            if (data.result != null) {
              var win = window.open(data.result, '_blank');
              if (win) {
                  //Browser has allowed it to be opened
                  win.focus();
              } else {
                  //Browser has blocked it
                  alert('Please allow popups for this website');
              }
            } else {
              bootbox.alert("No se ha subido archivo de presupuesto a este proyecto.");
            }
          }
      });
    }

    function descargarCronograma(id_proyecto) {

      var token = $('input[name=_token]').val(),
          dataString = 'id_proyecto=' + id_proyecto + '&_token=' + token;

      $.ajax({
          url: '{!!URL::route('existeCronograma')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
            if (data.result != null) {
              var win = window.open(data.result, '_blank');
              if (win) {
                  //Browser has allowed it to be opened
                  win.focus();
              } else {
                  //Browser has blocked it
                  alert('Please allow popups for this website');
              }
            } else {
              bootbox.alert("No se ha subido archivo de cronograma a este proyecto.");
            }
          }
      });
    }

    function descargarConvenio(id_organizacion) {

      var token = $('input[name=_token]').val(),
          dataString = 'id_organizacion=' + id_organizacion + '&_token=' + token;

      $.ajax({
          url: '{!!URL::route('existeConvenio')!!}',
          type: 'POST',
          data: dataString,
          success: function (data) {
            if (data.result != null) {
              var win = window.open(data.result, '_blank');
              if (win) {
                  //Browser has allowed it to be opened
                  win.focus();
              } else {
                  //Browser has blocked it
                  alert('Please allow popups for this website');
              }
            } else {
              bootbox.alert("No se ha subido archivo de convenio para esta organización.");
            }
          }
      });
    }
  </script>
@endsection
