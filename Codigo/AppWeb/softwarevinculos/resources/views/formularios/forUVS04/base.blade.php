@extends('layouts.master')

@section('content')

  @yield('breadcumb')

  {!!Html::style('css/formularios.css')!!}
  {!!Html::script('http://code.jquery.com/jquery-1.10.2.js')!!}
  {!!Html::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')!!}
  {!!Html::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css')!!}

  {!!Html::script('/vendor/unisharp/laravel-ckeditor/ckeditor.js')!!}
  {!!Html::script('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')!!}
  <!-- Date range picker -->
  {!!Html::script('js/moment.min.js')!!}
  {!!Html::script('js/daterangepicker.js')!!}
  {!!Html::style('css/daterangepicker.css')!!}

    <div class="container" style="padding:0;">
      <h2 class="titulo-formulario">FOR-UVS-04</h2>
      <h3 class="titulo-formulario">FORMULACIÓN DE PROYECTOS DE VINCULACIÓN CON LA SOCIEDAD</h3>
      <hr>

      @yield('comentario_rechazo')

        @yield('form-start')

        <!-- SECCION 1. DATOS GENERALES DEL PROYECTO -->
        <h4 class="titulo-seccion">1. DATOS GENERALES DEL PROYECTO</h4>
        <hr>

        @yield('tipo_proyecto')

        <div class="form-group">
          {!!form::label('titulo', '1.1. Título del proyecto:')!!}
          @yield('1.1')
        </div>

        <div class="form-group">
          {!!form::label('programa', '1.2. Programa al cual pertenece el proyecto:')!!}
          @yield('1.2')
        </div>

        <div class="form-group carreras-proyecto">
          {!!form::label('carrerasProyecto', '1.3. Carrera(s) que participa(n) en el proyecto:')!!}
          @yield('1.3')
        </div>

        <div class="form-group">
          {!!form::label('grografia', '1.4. Área geográfica que cubre el proyecto:')!!}
          @yield('1.4')
        </div>

        <div class="form-group">
          {!!form::label('duracion', '1.5.	Duración del Proyecto:', ['class' => 'top-space'])!!}
          @yield('1.5')
        </div>

        <div class="form-group">
          {!!form::label('areas', '1.6. Área y sub-área del conocimiento y conocimiento específico:', ['class' => 'top-space'])!!}
          @yield('1.6')
        </div>

        <div class="form-group">
          {!!form::label('lineas', '1.7. Línea de acción asociada al proyecto:', ['class' => 'top-space'])!!}
          @yield('1.7')
        </div>

        <div class="form-group">
          {!!form::label('pnbv', '1.8. Información relativa al Plan Nacional del Buen Vivir (PNBV) y a los Objetivos de Desarrollo del Milenio (ODM), al que está asociado el proyecto:', ['class' => 'top-space'])!!}
          @yield('1.8')
        </div>

        <div class="form-group">
          {!!form::label('equipo', '1.9.	Datos Generales del Equipo del Proyecto:', ['class' => 'top-space'])!!}
          @yield('1.9')
        </div>

        <div class="form-group">
          {!!form::label('perfil', '1.10. Perfil del Practicante:', ['class' => 'top-space'])!!}
          @yield('1.10')
        </div>

        <!-- SECCION 2. CAMPOS PARA ONG -->
        <h4 class="titulo-seccion">2. ORGANIZACIÓN BENEFICIARIA DEL PROYECTO</h4>
        <hr>

        <div class="form-group">
          {!!form::label('beneficiarios_directos', '2.1.	Número aproximado de personas como Beneficiarios Directos')!!}
          @yield('2.1')
          {!!form::label('beneficiarios_indirectos', '2.2.	Número aproximado de personas como Beneficiarios Indirectos')!!}
          @yield('2.2')

          @yield('opciones-organizacion')

          {!!form::label('razonSocial', '2.3. Razón Social de la Organización')!!}
          @yield('2.3')
          {!!form::label('dirccion', '2.4. Dirección')!!}
          @yield('2.4')
          {!!form::label('telefono', '2.5. Teléfono, Fax, Correo electrónico')!!}
          @yield('2.5')
          {!!form::label('representante','2.6. Representante Legal',['style'=>'margin-top:15px'])!!}
          @yield('2.6')
          {!!form::label('legalizacion','2.7. Fecha de creación y acuerdo de legalización',['style'=>'margin-top:15px'])!!}
          @yield('2.7')
          {!!form::label('nombramiento','2.8. Nombramiento del Representante Legal',['style'=>'margin-top:15px'])!!}
          @yield('2.8')
          {!!form::label('responsable','2.9. Nombre de la persona o personas responsables del seguimiento del proyecto por parte de la organización beneficiaria:',['style'=>'margin-top:15px'])!!}
          @yield('2.9')
        </div>

        <!-- SECCION 3. DESCRIPCIÓN DEL PROBLEMA -->
        <h4 class="titulo-seccion">3. DESCRIPCIÓN DEL PROBLEMA</h4>
        <hr>
        <div class="form-group">
          {!!form::label('antecedentes', '1. Antecedentes', ['class' => 'top-space'])!!}
          @yield('3.1')
        </div>
        <div class="form-group">
          {!!form::label('contexto_proyecto', '2. Contexto del Proyecto', ['class' => 'top-space'])!!}
          @yield('3.2')
        </div>
        <div class="form-group">
          {!!form::label('definicion_problema','3. Definición del problema', ['class' => 'top-space'])!!}
          @yield('3.3')
        </div>

        <!-- SECCION 4. JUSTIFICACIÓN -->
        <div class="form-group">
          {!!form::label('4. JUSTIFICACIÓN', null, ['class'=>'titulo-seccion'])!!}
          @yield('4')
        </div>

        <!-- SECCION 5. PERTINENCIA -->
        <div class="form-group">
          {!!form::label('5. PERTINENCIA', null, ['class'=>'titulo-seccion'])!!}
          @yield('5')
        </div>

        <!-- SECCION 6. CAMPOS PARA OBJETIVOS DEL PROYECTO -->
        <div class="form-group">
          {!!form::label('6. OBJETIVOS DEL PROYECTO (General y Específico)', null, ['class'=>'titulo-seccion'])!!}
          @yield('6')
        </div>

        <br>
        <!-- SECCION 7. MATRIZ DE MARCO LÓGICO) -->
        <div class="form-group">
          {!!form::label('7. MATRIZ DE MARCO LÓGICO', null, ['class'=>'titulo-seccion'])!!}
          @yield('7')
        </div>

        <br>
        <!-- SECCION 8. METODOLOGÍA DE TRABAJO (TÉCNICAS, HERRAMIENTAS Y PROCEDIMIENTOS) -->
        <div class="form-group">
          {!!form::label('8. METODOLOGÍA DE TRABAJO (TÉCNICAS, HERRAMIENTAS Y PROCEDIMIENTOS)', null, ['class'=>'titulo-seccion'])!!}
          @yield('8')
        </div>

        <br>
        <!-- SECCION 9. CRONOGRAMA -->
        <div class="form-group">
          {!!form::label('9. CRONOGRAMA', null, ['class'=>'titulo-seccion'])!!}
          @yield('9')
        </div>

        <!-- SECCION 10. PRODUCTOS ESPERADOS -->
        <div class="form-group">
          {!!form::label('10. PRODUCTOS ESPERADOS', null, ['class'=>'titulo-seccion'])!!}
          @yield('10')
        </div>

        <br>
        <!-- SECCION 11. COMPROMISOS DE LA ORGANIZACIÓN -->
        <div class="form-group">
          {!!form::label('11. COMPROMISOS DE LA ORGANIZACIÓN', null, ['class'=>'titulo-seccion'])!!}
          @yield('11')
        </div>

        <br>
        <!-- SECCION 12. PRESUPUESTO -->
        <div class="form-group">
          {!!form::label('12. PRESUPUESTO', null, ['class'=>'titulo-seccion'])!!}
          @yield('12')
        </div>

        <!-- SECCION 13. EVALUACIÓN -->
        <div class="form-group">
          {!!form::label('13. EVALUACIÓN', null, ['class'=>'titulo-seccion'])!!}
          @yield('13')
        </div>

        <!-- SECCION 14. BIBLIOGRAFÍA -->
        <div class="form-group">
          {!!form::label('14. BIBLIOGRAFÍA', null, ['class'=>'titulo-seccion'])!!}
          @yield('14')
        </div>

        @yield('form-buttons')

      @yield('form-end')

    </div>

    @yield('javascript-code')

@endsection
