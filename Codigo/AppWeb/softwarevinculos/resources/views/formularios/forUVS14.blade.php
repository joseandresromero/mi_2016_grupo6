@extends('layouts.master')

@section('title','FOR-UVS-14')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-14</h2>
      <h3 class="titulo-formulario">INFORME FINAL DEL PRACTICANTE</h3>
      <hr>

      @if (isset($forUVS14))
        {!!Form::model($forUVS14,['route'=>['forUVS14.update',$forUVS14->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS14.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. INFORMACIÓN DEL PRACTICANTE -->
        <h4 class="titulo-seccion">1. INFORMACIÓN DEL PRACTICANTE</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Nombres y Apellidos del practicante:')!!}
          <p>Jose Andres Romero Triviño</p>
        </div>
        <div class="form-group">
          {!!form::label('1.2. Número de matrícula:')!!}
          <p>201021664</p>
        </div>
        <div class="form-group">
          {!!form::label('1.3. Nombre de la carrera:')!!}
          <p>Ingeniería en Ciencias Computacionales</p>
        </div>
        <div class="form-group">
          {!!form::label('1.4. Número de personas como Beneficiarios Indirectos:')!!}
          {!!form::text('titulo',null,['id'=>'titulo','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('1.5. Número de teléfono:')!!}
          {!!form::text('telefono',null,['id'=>'telefono','class'=>'form-control'])!!}
        </div>

        <!-- SECCION 2. INFORMACIÓN DEL DOCENTE TUTOR -->
        <h4 class="titulo-seccion">2. INFORMACIÓN DEL DOCENTE TUTOR</h4>
        <hr>
        <div class="form-group">
          {!!form::label('2.1. Nombres y Apellidos del tutor::')!!}
          <p>Marcos Calderon</p>
        </div>
        <div class="form-group">
          {!!form::label('2.2. Correo electrónico::')!!}
          <p>mcalderon@espol.edu.ec</p>
        </div>
        <div class="form-group">
          {!!form::label('2.3. Número de teléfono:')!!}
          <p>2547896</p>
        </div>

        <!-- SECCION 3. INFORMACIÓN DE LA ORGANIZACIÓN/EMPRESA -->
        <h4 class="titulo-seccion">3. INFORMACIÓN DE LA ORGANIZACIÓN/EMPRESA</h4>
        <hr>
        <div class="form-group">
          {!!form::label('3.1. Número de personas como Beneficiarios Directos:')!!}
          <p>160</p>
        </div>
        <div class="form-group">
          {!!form::label('3.2. Número de personas como Beneficiarios Indirectos:')!!}
          <p>320</p>
        </div>
        <div class="form-group">
          {!!form::label('3.3. Área geográfica que cubre:')!!}
          <br>
          {!!form::label('Provincia:')!!}
          <p>Guayas</p>
          {!!form::label('Cantón:')!!}
          <p>Guayaquil</p>
          {!!form::label('Código de la zona:')!!}
          <p>Zona 8</p>
          {!!form::label('Código del distrito:')!!}
          <p>09D08</p>
          {!!form::label('Código del circuito:')!!}
          <p>09D08C05</p>
        </div>
        <div class="form-group">
          {!!form::label('3.4. Razón Social de la Organización')!!}
          {!!form::text('razon_social_ong',null,['id'=>'razon_social_ong','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('3.5. Dirección')!!}
          {!!form::text('direccion_ong',null,['id'=>'direccion_ong','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('3.6. Teléfono, Fax, Correo electrónico')!!}
          <p>2456565</p>
          <p>4565258</p>
          <p>hdc@gmail.com</p>
        </div>
        <div class="form-group">
          {!!form::label('3.7. Nombre del Representante Legal de la organización/empresa:')!!}
          <p>Jaime macias</p>
        </div>
        <div class="form-group">
          {!!form::label('3.8. Nombre de la persona o personas responsables del seguimiento de la actividad o servicio por parte de la organización/empresa:')!!}
          <p>Roberto Lopez</p>
        </div>
        <div class="form-group">
          {!!form::label('3.9. Resumen de la organización/empresa beneficiaria')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- SECCION 4. CONTENIDO DEL INFORME FINAL -->
        <h4 class="titulo-seccion">4. CONTENIDO DEL INFORME FINAL</h4>
        <hr>
        <div class="form-group">
          {!!form::label('4.1. Planteamiento del Problema')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('4.2. Justificación')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('4.3. Objetivos Generales')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('4.4. Objetivos Específicos')!!}
          <ul>
            <li>Elaborar todo el material didáctico para los talleres que se impartirán por niveles básico, intermedio y avanzado, con las temáticas: uso del computador, toma y edición de fotos con software libre, administración de redes sociales, y administración de sitios web con Wordpress.</li>
            <li>Elaborar el contenido para la grabación de los videos de los talleres como material adicional de consulta para el grupo de mujeres y como referente para los próximos pasantes politécnicos comunitarios.</li>
          </ul>
        </div>
        <div class="form-group">
          {!!form::label('4.5. Actividades Realizadas')!!}
          <br>
          {!!form::label('Fecha de Inicio de la actividad o servicio:')!!}
          <p>20/08/2016</p>
          {!!form::label('Fecha de finalización de la actividad o servicio:')!!}
          <p>29/09/2016</p>
          {!!form::label('Total de horas empleadas en la actividad o servicio:')!!}
          {!!form::text('horas',null,['id'=>'horas','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('4.6. Análisis y evaluación de las actividades o servicios realizados')!!}
          {!!form::textarea('analisis_actividades',null,['id'=>'analisis_actividades','class'=>'form-control','rows'=>5])!!}
        </div>
        <div class="form-group">
          {!!form::label('4.7. Conclusiones y Recomendaciones')!!}
          {!!form::textarea('conclusiones_recomendaciones',null,['id'=>'conclusiones_recomendaciones','class'=>'form-control','rows'=>5])!!}
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">

    </script>

@endsection
