@extends('layouts.master')

@section('title','FOR-UVS-15')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-15</h2>
      <h3 class="titulo-formulario">INFORME FINAL DE LA ORGANIZACIÓN O EMPRESA</h3>
      <hr>

      @if (isset($forUVS15))
        {!!Form::model($forUVS15,['route'=>['forUVS15.update',$forUVS15->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS15.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. INFORMACIÓN DEL PRACTICANTE -->
        <h4 class="titulo-seccion">1. INFORMACIÓN DEL PRACTICANTE</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Nombres y Apellidos del practicante:')!!}
          <p>Jose Andres Romero Triviño</p>
        </div>
        <div class="form-group">
          {!!form::label('1.2. Nombre de la carrera:')!!}
          <p>Ingeniería en Ciencias Computacionales</p>
        </div>
        <div class="form-group">
          {!!form::label('1.3. Fecha de inicio y fin de la práctica:')!!}
          <br>
          {!!form::label('Fecha de Inicio:')!!}
          <p>20/08/2016</p>
          {!!form::label('Fecha de Finalización:')!!}
          <p>29/09/2016</p>
        </div>
        <div class="form-group">
          {!!form::label('1.4. Horas totales de prácticas:')!!}
          <p>160</p>
        </div>

        <!-- SECCION 2. INFORMACIÓN DEL RESPONSABLE POR PARTE DE LA ORGANIZACIÓN/EMPRESA -->
        <h4 class="titulo-seccion">2. INFORMACIÓN DEL RESPONSABLE POR PARTE DE LA ORGANIZACIÓN/EMPRESA</h4>
        <hr>
        <div class="form-group">
          {!!form::label('2.1. Nombres y Apellidos del Responsable por la empresa:')!!}
          <p>Jaime Macias</p>
        </div>
        <div class="form-group">
          {!!form::label('2.2. Cargo:')!!}
          <p>Coordinador</p>
        </div>
        <div class="form-group">
          {!!form::label('2.3. Empresa:')!!}
          <p>Hogar de Cristo</p>
        </div>
        <div class="form-group">
          {!!form::label('2.4. Área o Sección:')!!}
          <p>Viviendas</p>
        </div>

        <!-- SECCION 3. ACTIVIDADES PLANIFICADAS -->
        <h4 class="titulo-seccion">3. ACTIVIDADES PLANIFICADAS</h4>
        <hr>
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_actividades_planificadas">
            <tr>
              <th rowspan="2">Actividades Planificadas</th>
              <th colspan="2">Cumplió a cabalidad</th>
              <th rowspan="2">Comentarios</th>
            </tr>
            <tr>
              <th>Si</th>
              <th>No</th>
            </tr>
            <tr>
              <td>Grabación de talleres</td>
              <td><input type="radio" name="actividaddes-item-1" value="si"></td>
              <td><input type="radio" name="actividaddes-item-1" value="no"></td>
              <td>{!!form::textarea('comentarios-1',null,['id'=>'comentarios-2','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
            <tr>
              <td>Escritura de libretos</td>
              <td><input type="radio" name="actividaddes-item-2" value="si"></td>
              <td><input type="radio" name="actividaddes-item-2" value="no"></td>
              <td>{!!form::textarea('comentarios-2',null,['id'=>'comentarios-2','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
          </table>
        </div>

        <!-- SECCION 4. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE -->
        <h4 class="titulo-seccion">4. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE</h4>
        <hr>
        {!!form::label('a) Trabajo en equipos multidisciplinarios')!!}
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_multidiciplinarios">
            <tr>
              <th rowspan="2">Criterios</th>
              <th colspan="6">Calificación </th>
            </tr>
            <tr>
              <th>N/A</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
            <tr>
              <td>1. Busca y obtiene información pertinente al trabajo del equipo.</td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="n_a"></td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="1"></td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="2"></td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="3"></td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="4"></td>
              <td><input type="radio" name="multidisciplinarios-item-1" value="5"></td>
            </tr>
            <tr>
              <td>2. Cumple con las tareas de los roles asignados.</td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="n_a"></td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="1"></td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="2"></td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="3"></td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="4"></td>
              <td><input type="radio" name="multidisciplinarios-item-2" value="5"></td>
            </tr>
            <tr>
              <td>3. Realiza el trabajo asignado por el equipo sin que se lo recuerden.</td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="n_a"></td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="1"></td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="2"></td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="3"></td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="4"></td>
              <td><input type="radio" name="multidisciplinarios-item-3" value="5"></td>
            </tr>
            <tr>
              <td>4. Escucha a los otros miembros del equipo permitiendo su participación.</td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="n_a"></td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="1"></td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="2"></td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="3"></td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="4"></td>
              <td><input type="radio" name="multidisciplinarios-item-4" value="5"></td>
            </tr>
          </table>
        </div>

        {!!form::label('b) Comprender la responsabilidad ética y profesional')!!}
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_multidiciplinarios">
            <tr>
              <th rowspan="2">Criterios</th>
              <th colspan="6">Calificación </th>
            </tr>
            <tr>
              <th>N/A</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
            <tr>
              <td>1. Respeta y preserva las normas establecidas en la organización beneficiaria/empresa.</td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="n_a"></td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="1"></td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="2"></td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="3"></td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="4"></td>
              <td><input type="radio" name="responsabilidad-etica-item-1" value="5"></td>
            </tr>
            <tr>
              <td>2. Actúa con responsabilidad en cada una de las actividades asignadas, respondiendo siempre por sus acciones.</td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="n_a"></td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="1"></td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="2"></td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="3"></td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="4"></td>
              <td><input type="radio" name="responsabilidad-etica-item-2" value="5"></td>
            </tr>
            <tr>
              <td>3. Usa la empatía para relacionarse con las demás personas.</td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="n_a"></td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="1"></td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="2"></td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="3"></td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="4"></td>
              <td><input type="radio" name="responsabilidad-etica-item-3" value="5"></td>
            </tr>
            <tr>
              <td>4. Toma en cuenta el contexto para ejecutar las acciones en su trabajo, tomando en cuenta las repercusiones e impactos personales y de los demás.</td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="n_a"></td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="1"></td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="2"></td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="3"></td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="4"></td>
              <td><input type="radio" name="responsabilidad-etica-item-4" value="5"></td>
            </tr>
          </table>
        </div>

        {!!form::label('c) Comunicarse de manera efectiva')!!}
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_multidiciplinarios">
            <tr>
              <th rowspan="2">Criterios</th>
              <th colspan="6">Calificación </th>
            </tr>
            <tr>
              <th>N/A</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
            <tr>
              <td>1. Escucha con atención antes de emitir una respuesta sin interrumpir a las demás personas.</td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="n_a"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="1"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="2"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="3"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="4"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-1" value="5"></td>
            </tr>
            <tr>
              <td>2. Se comunica de manera clara y efectiva utilizando un lenguaje adecuado a su receptor.</td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="n_a"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="1"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="2"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="3"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="4"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-2" value="5"></td>
            </tr>
            <tr>
              <td>3. Demuestra actitud personal positiva al dirigirse a los demás con palabras como: gracias, perdón y por favor.</td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="n_a"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="1"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="2"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="3"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="4"></td>
              <td><input type="radio" name="comunicacion-efectiva-item-3" value="5"></td>
            </tr>
          </table>
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">

    </script>

@endsection
