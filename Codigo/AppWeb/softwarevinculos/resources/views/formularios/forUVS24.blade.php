@extends('layouts.master')

@section('title','FOR-UVS-24')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-24</h2>
      <h3 class="titulo-formulario">INFORME PARCIAL DEL PROYECTO</h3>
      <hr>

      @if (isset($forUVS24))
        {!!Form::model($forUVS24,['route'=>['forUVS24.update',$forUVS24->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS24.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. INFORMACIÓN GENERAL-->
        <h4 class="titulo-seccion">1. INFORMACIÓN GENERAL</h4>
        <hr>
        <div class="form-group">
          {!!form::label('Docente (Tutor):')!!}
          <p>Rafael Bonilla</p>
        </div>
        <div class="form-group">
          {!!form::label('Facultad:')!!}
          <p>FIEC</p>
        </div>
        <div class="form-group">
          {!!form::label('Carrera:')!!}
          <p>Ingeniería en Ciencias Compiutacionales</p>
        </div>
        <div class="form-group">
          {!!form::label('Nombres y Apellidos del estudiante:')!!}
          <p>José Luis Monar Jaime</p>
        </div>
        <div class="form-group">
          {!!form::label('Organización Beneficiaria')!!}
          <p>Hogar de Cristo</p>
        </div>
        <div class="form-group">
          {!!form::label('Nombre del proyecto / Actividad específica')!!}
          <p>Desarrollo de una App de Lego para el aprendizaje de Lego mindstorm EV3</p>
        </div>

        <!-- SECCION 1. TABLA DE ACTIVIDADES -->
        <h4 class="titulo-seccion">2. TABLA DE OBJETIVOS NARRATIVOS</h4>
        <hr>
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_objetivos_narrativos">
            <tr >
              <th rowspan="2">Resumen Narrativo de Objetivos</th>
              <th rowspan="2">Indicadores Verificables</th>
              <th rowspan="2">Medios de verificación (Evidencias)</th>
              <th colspan="2">Presupuesto</th>
              <th rowspan="2">Factores de éxito</th>
              <th rowspan="2">Factores críticos</th>
              <th rowspan="2">% Avance</th>
              <th rowspan="2">Acción</th>
            </tr>
            <tr>
              <th width='12%'>Presupuestado</th>
              <th width='12%'>Ejecutado</th>
            </tr>
            <tr style="vertical-align: middle; background-color: rgb(194, 98, 46); font-weight: bold;">
              <td > FIN</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
            </tr>
            <tr id="row22" style="vertical-align: middle; background-color: rgb(194, 98, 46); font-weight: bold;">
              <td > OBJETIVO GENERAL (PROPÓSITO)</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
            </tr>

            <tr style="vertical-align: middle;  background-color: rgb(236, 168, 131); font-weight: bold;">
              <td > OBJETIVO ESPECIFICO 1</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td > <button type="button" name="addActividadOE1" id="addActividadOE1" style="margin-top:0px" class="btn btn-success">Añadir Actividad</button></td>
            </tr>


            <script type="text/javascript">
              var i=10;
              //Se agrega una nueva fila de actividad
              $('#addActividadOE'+1+'').click(function(){
                i++;
                console.info('clicked button');
                var actividades = document.getElementsByName('rowActividadOE1');
                var ultimaActividad = actividades[actividades.length -1].id;
                var tareas = document.getElementsByName(ultimaActividad+'-tarea');

                if(tareas.length != 0){
                  var ultimaTarea = tareas[tareas.length -1].id;
                  //Nueva fila se agrega al final de la ultima tarea de la ultima actividad
                  $('#'+ultimaTarea).after('<tr name="rowActividadOE1", id="rowActividadOE1-'+i+'" style="background-color: bisque;"> <td > 1.1 Actividad new '+i+'</td> <td ></td>  <td ></td><td ></td> <td ></td> <td ></td>  <td ></td> <td ></td> <td >  <div>  <button type="button" name="addTareaActividad1OE1" id="addTareaActividad1OE1-'+i+'" style="margin-top:0px" class="btn btn-success">Añadir Tarea</button>  <button type="button" name="remove" id="actividad" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>  </div> </td> </tr>');
                }
                else{
                  console.info('ultima actividad no tene tareas');
                    //Nueva fila se agrega al final de la ultima actividad
                  $('#'+ultimaActividad).after('<tr name="rowActividadOE1", id="rowActividadOE1-'+i+'" style="background-color: bisque;"> <td > 1.1 Actividad new '+i+'</td> <td ></td>  <td ></td><td ></td> <td ></td> <td ></td>  <td ></td> <td ></td> <td >  <div>  <button type="button" name="addTareaActividad1OE1-1" id="addTareaActividad1OE1-'+i+'" style="margin-top:0px" class="btn btn-success">Añadir Tarea</button>  <button type="button" name="remove" id="actividad" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>  </div> </td> </tr>');
                }

                var btnAddTarea = document.getElementById('addTareaActividad1OE1-'+i);



                btnAddTarea.onclick = function() {
                  var rowActividad = document.getElementById( 'rowActividadOE1-'+i+'');

                  console.info('row actividad: ', rowActividad);


                  var idActividad = rowActividad.id;

                  var tareass = document.getElementsByName(idActividad+'-tarea');

                  var lasttarea = tareass.length + 1;

                  console.info('size tareas: ', tareass.length);

                  console.info('lasttarea: ', tareass[tareass.length -1 ]);

                  var sizetareas = tareass.length + 4;



                  if(tareass.length != 0){
                     var lasttr = tareass[tareass.length -1].id;

                     console.info('lasttr', lasttr);
                    $('#'+lasttr).after('<tr name="'+idActividad+'-tarea", id="rowActividadOE1-1-tarea-'+sizetareas+'">  <td > Tarea OE1 Actividad 1.1</td> <td ></td>                    <td ></td> <td ></td> <td ></td> <td ></td><td ></td> <td ></td> <td > </td> </tr>');
                  }
                  else{
                    $('#'+idActividad).after('<tr name="'+idActividad+'-tarea", id="rowActividadOE1-1-tarea-'+sizetareas+'">  <td > Tarea OE1 Actividad 1.1</td> <td ></td>                    <td ></td> <td ></td> <td ></td> <td ></td><td ></td> <td ></td> <td > </td> </tr>');
                  }



                };


              });
            </script>
            <!--



            -->
            <tr name='rowActividadOE1', id='rowActividadOE1-1' style='background-color: bisque;'>
              <td > 1.1 Actividad</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td >
                <div>
                  <button type="button" name="addTareaActividad1OE1" id="addTareaActividad1OE1" style="margin-top:0px" class="btn btn-success">Añadir Tarea</button>
                  <button type="button" name="remove" id="actividad" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </div>
              </td>
            </tr>
            <tr name='rowActividadOE1-1-tarea', id='rowActividadOE1-1-tarea-1'>
              <td > Tarea OE1 Actividad 1.1</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td > </td>
            </tr>
            <tr name='rowActividadOE1-1-tarea'  id='rowActividadOE1-1-tarea-2'>
              <td > Tarea OE1 Actividad 1.1</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td > </td>
            </tr>

            <tr style="vertical-align: middle;  background-color: rgb(236, 168, 131); font-weight: bold;">
              <td > OBJETIVO ESPECIFICO 2</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td > <button type="button" name="addActividadOE2" id="addActividadOE2>" style="margin-top:0px" class="btn btn-success">Añadir Actividad</button></td>
            </tr>
            <tr style='background-color: bisque;'>
              <td style='background-color: bisque;'> 1.2 Actividad</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td >
                <div>
                  <button type="button" name="addTareaActividad1OE2" id="addTareaActividad1OE2" style="margin-top:0px" class="btn btn-success">Añadir Tarea</button>
                  <button type="button" name="remove" id="actividad" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </div>
              </td>
            </tr>
            <tr>
              <td > Tarea OE2 Actividad 1.2</td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td ></td>
              <td > </td>
            </tr>
          </table>
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">
      // When the document is ready
      $(document).ready(function () {
        $('#fecha_actividad').datepicker({
          format: "yyyy-mm-dd"
        });
      });
      var i=1;
      //Se agrega una nueva fila de actividad
      $('#addActividad').click(function(){
        i++;
        $('#tabla_actividades').append('<tr id="row'+i+'"> <td style="vertical-align: middle;">{!!form::date('fecha_actividad[]',null, ['id'=>'fecha_actividad2','class'=>'form-control'])!!}</td> <td>{!!form::textarea('listaActividades[]',null,['class'=>'form-control name_list', 'rows'=>3, 'style'=>'resize:none;'])!!}</td> <td>{!!form::textarea('sugerencia[]',null,['class'=>'form-control name_list', 'rows'=>3, 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button></td></tr> ');
      });

      //Elimina fila seleccionada de una tabla
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");
           $('#row'+button_id+'').remove();
      });
    </script>

@endsection
