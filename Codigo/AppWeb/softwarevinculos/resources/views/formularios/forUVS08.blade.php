@extends('layouts.master')

@section('title','FOR-UVS-08')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-08</h2>
      <h3 class="titulo-formulario">PRESUPUESTO</h3>
      <hr>

      @if (isset($forUVS08))
        {!!Form::model($forUVS08,['route'=>['forUVS08.update',$forUVS08->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS08.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!--INFORMACIÓN DEL PRACTICANTE -->
        <h4 class="titulo-seccion">INFORMACIÓN GENERAL</h4>
        <hr>
        <div class="form-group">
          {!!form::label('Proyecto:')!!}
          <p>Desarrollo Aplicación Móvil para el aprendizaje de LEGO Mindstrom EV3</p>
        </div>
        <div class="form-group">
          {!!form::label('Facultad:')!!}
          <p>FIEC</p>
        </div>
        <div class="form-group">
          {!!form::label('Carrera:')!!}
          <p>Ingeniería en Ciencias Computacionales</p>
        </div>
        <div class="form-group">
          {!!form::label('TAMAÑO DEL PROYECTO (No. Beneficiarios):')!!}
          <p>5</p>
        </div>

        <!-- SECCION 1. GASTOS EN PERSONAL -->
        <h4 class="titulo-seccion">1. GASTOS EN PERSONAL</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Remuneraciones')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_remuneraciones">
              <tr >
                <td width='40%'>Descripción del cargo</td>
                <td width='10%'>No. Participantess</td>
                <td width='10%'>Horas de dedicación</td>
                <td width='15%'>Costos sueldo por hora</td>
                <td width='15%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowremuneracion">
                <td>{!!form::textarea('descripcionCargoRemuneracion[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('numeroPersonasRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('horasDedicacionRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoSueldoPorHoraRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="remuneracion" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addRemuneracion" id="addRemuneracion" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <div class="form-group">
          {!!form::label('1.2. Honorarios')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_honorarios">
              <tr >
                <td width='40%'>Descripción del rubro</td>
                <td width='10%'>No. Personas</td>
                <td width='10%'>Horas de dedicación</td>
                <td width='15%'>Costo unitario</td>
                <td width='15%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowhonorario">
                <td>{!!form::textarea('descripcionRubroHonorario[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('numeroPersonasHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('horasDedicacionHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoUnitarioHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="honorario" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addHonorario" id="addHonorario" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <!-- SECCION 2. GASTOS OPERACIONALES -->
        <h4 class="titulo-seccion">2. GASTOS OPERACIONALES</h4>
        <hr>
        <div class="form-group">
          {!!form::label('2.1. Viáticos y Subsistencias')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_viaticos">
              <tr >
                <td width='40%'>Descripción del rubro</td>
                <td width='10%'>No. Participantes</td>
                <td width='10%'>Cantidad</td>
                <td width='15%'>Costo unitario</td>
                <td width='15%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowviatico">
                <td>{!!form::textarea('descripcionRubroViatico[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('numeroParticipantesViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('cantidadViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoUnitarioViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="viatico" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addViatico" id="addViatico" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <div class="form-group">
          {!!form::label('2.2. Movilización')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_movilizacion">
              <tr >
                <td width='40%'>Descripción del rubro</td>
                <td width='10%'>No. Participantes</td>
                <td width='10%'>Cantidad</td>
                <td width='15%'>Costo unitario</td>
                <td width='15%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowmovilizacion">
                <td>{!!form::textarea('descripcionRubroMovilizacion[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('numeroParticipantesMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('cantidadMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoUnitarioMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="movilizacion" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addMovilizacion" id="addMovilizacion" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <div class="form-group">
          {!!form::label('2.3. Materiales e Insumos')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_materiales_insumos">
              <tr >
                <td width='50%'>Descripción del rubro</td>
                <td width='13%'>Cantidad</td>
                <td width='13%'>Costo unitario</td>
                <td width='13%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowmaterial">
                <td>{!!form::textarea('descripcionRubroMateriales[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('cantidadMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="material" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addMaterial" id="addMaterial" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <div class="form-group">
          {!!form::label('2.4. Gastos de Servicios (Socialización, sistematización y otros)')!!}
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_servicios">
              <tr >
                <td width='50%'>Descripción</td>
                <td width='13%'>Cantidad</td>
                <td width='13%'>Costo unitario</td>
                <td width='13%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowservicio">
                <td>{!!form::textarea('descripcionServicio[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('cantidadServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="servicio" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addServicio" id="addServicio" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>

        <!-- SECCION 3. EQUIPAMIENTO -->
          <h4 class="titulo-seccion">3. EQUIPAMIENTO</h4>
        <hr>
        <div class="form-group">
          <div class="table-responsive" style="margin-top:10px">
            <table class="table table-bordered" id="tabla_equipamientos">
              <tr >
                <td width='50%'>Descripción del bien</td>
                <td width='13%'>Cantidad</td>
                <td width='13%'>Costo unitario</td>
                <td width='13%'>Costo Total</td>
                <td>Acción</td>
              </tr>
              <tr id="rowequipamiento">
                <td>{!!form::textarea('descripcionEquipamiento[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('cantidadEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="vertical-align: middle;">{!!form::text('costoTotalEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td>
                <td style="text-align:center; vertical-align: middle;">
                  <button type="button" name="remove" id="equipamiento" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button>
                </td>
              </tr>
            </table>
            <button type="button" name="addEquipamiento" id="addEquipamiento" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>


        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">
      var i=1;

      //***********//
      // Eventos que se disparan cuando se añadira una fila a una de las tablas de la sección GASTOS EN PERSONAL
      //**********//
      //Se agrega una nueva fila de gastos en remuneraciones
      $('#addRemuneracion').click(function(){
        i++;
        $('#tabla_remuneraciones').append('<tr id="row'+i+'"> <td>{!!form::textarea('descripcionCargoRemuneracion[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('numeroPersonasRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('horasDedicacionRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('costoSueldoPorHoraRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('costoTotalRemuneracion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="text-align:center; vertical-align: middle;"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });

      //Se agrega una nueva fila de gastos en honorarios
      $('#addHonorario').click(function(){
        i++;
        $('#tabla_honorarios').append('<tr id="row'+i+'"> <td>{!!form::textarea('descripcionRubroHonorario[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('numeroPersonasHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('horasDedicacionHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('costoUnitarioHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('costoTotalHonorario[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="text-align:center; vertical-align: middle;"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });

      //***********//
      // Eventos que se disparan cuando se añadira una fila a una de las tablas de la sección GASTOS OPERACIONALES
      //**********//
      //Se agrega una nueva fila de gastos en viaticos
      $('#addViatico').click(function(){
        i++;
        $('#tabla_viaticos').append('<tr id="row'+i+'"> <td>{!!form::textarea('descripcionRubroViatico[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('numeroParticipantesViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('cantidadViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoUnitarioViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoTotalViatico[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"><button type="button" name="remove" id="'+i+'"  class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
      });

      //Se agrega una nueva fila de gastos en movilizacion
      $('#addMovilizacion').click(function(){
        i++;
        $('#tabla_movilizacion').append('<tr id="row'+i+'"><td>{!!form::textarea('descripcionRubroMovilizacion[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('numeroParticipantesMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td><td style="vertical-align: middle;">{!!form::text('cantidadMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoUnitarioMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoTotalMovilizacion[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });

      //Se agrega una nueva fila de gastos en materiales e insumos
      $('#addMaterial').click(function(){
        i++;
        $('#tabla_materiales_insumos').append('<tr id="row'+i+'"><td>{!!form::textarea('descripcionRubroMateriales[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('cantidadMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoTotalMateriales[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });

      //Se agrega una nueva fila de gastos en servicios
      $('#addServicio').click(function(){
        i++;
        $('#tabla_servicios').append('<tr id="row'+i+'"> <td>{!!form::textarea('descripcionServicio[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('cantidadServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoTotalServicio[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });
      //***********//
      // Eventos que se disparan cuando se añadira una fila a una de las tablas de la sección EQUIPAMIENTO
      //**********//
      //Se agrega una nueva fila de gastos en equipamientos
      $('#addEquipamiento').click(function(){
        i++;
        $('#tabla_equipamientos').append('<tr  id="row'+i+'"> <td>{!!form::textarea('descripcionEquipamiento[]',null,['class'=>'form-control name_list', 'rows'=>2, 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('cantidadEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('ccostoUnitarioEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="vertical-align: middle;">{!!form::text('costoTotalEquipamiento[]',null,['class'=>'form-control name_list', 'style'=>'resize:none;'])!!}</td> <td style="text-align:center; vertical-align: middle;"> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove "><span class="glyphicon glyphicon-remove"></span></button> </td> </tr>');
      });

      //Elimina fila seleccionada de una tabla
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");
           $('#row'+button_id+'').remove();
      });
    </script>

@endsection
