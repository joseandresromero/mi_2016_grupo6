@extends('layouts.master')

@section('title','FOR-UVS-01')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-01</h2>
      <h3 class="titulo-formulario">PLAN DE ACTIVIDADES</h3>
      <hr>

      @if (isset($forUVS01))
        {!!Form::model($forUVS01,['route'=>['forUVS01.update',$forUVS01->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS01.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. DATOS GENERALES -->
        <h4 class="titulo-seccion">1. DATOS GENERALES</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Título de la Actividad/Servicios:')!!}
          {!!form::text('titulo',null,['id'=>'titulo','class'=>'form-control'])!!}
        </div>
        <div class="form-group">
          {!!form::label('1.2. Organización Beneficiaria:')!!}
          <p>Corporación Viviendas del Hogar de Cristo</p>
        </div>
        <div class="form-group">
          {!!form::label('1.3. Número de personas como Beneficiarios Directos:')!!}
          <p>160</p>
        </div>
        <div class="form-group">
          {!!form::label('1.4. Número de personas como Beneficiarios Indirectos:')!!}
          <p>320</p>
        </div>
        <div class="form-group">
          {!!form::label('1.5. Área geográfica que cubre:')!!}
          <br>
          {!!form::label('Provincia:')!!}
          <p>Guayas</p>
          {!!form::label('Cantón:')!!}
          <p>Guayaquil</p>
          {!!form::label('Código de la zona:')!!}
          <p>Zona 8</p>
          {!!form::label('Código del distrito:')!!}
          <p>09D08</p>
          {!!form::label('Código del circuito:')!!}
          <p>09D08C05</p>
        </div>
        <div class="form-group">
          {!!form::label('1.6.	Duración de la Actividad/Servicio:')!!}
          <br>
          {!!form::label('Fecha de inicio:')!!}
          <div class="hero-unit">
            {!!form::date('fecha_inicio',null,['id'=>'fecha_inicio','class'=>'form-control'])!!}
          </div>
          {!!form::label('Fecha de finalización:')!!}
          <div class="hero-unit">
            {!!form::date('fecha_finalizacion',null,['id'=>'fecha_finalizacion','class'=>'form-control'])!!}
          </div>
        </div>
        <div class="form-group">
          {!!form::label('1.7. Área y sub-área del conocimiento y conocimiento específico:')!!}
          <br>
          {!!form::label('Área del Conocimiento')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          {!!form::label('Sub área del Conocimiento')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          {!!form::label('Sub área Especifica')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- SECCION 2. IDENTIFICACIÓN DEL PROBLEMA A RESOLVER -->
        <h4 class="titulo-seccion">2. IDENTIFICACIÓN DEL PROBLEMA A RESOLVER</h4>
        <hr>
        <div class="form-group">
          {!!form::label('2.1. Antecedentes')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="form-group">
          {!!form::label('2.2. Definición del problema')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- SECCION 3. OBJETIVOS (General y Específicos) -->
        <h4 class="titulo-seccion">3. OBJETIVOS (General y Específicos)</h4>
        <hr>
        <div class="form-group">
          {!!form::label('Objetivo General')!!}
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

          {!!form::label('Objetivos Específicos:',null,['style'=>'margin-top:15px'])!!}
          <ul>
            <li>Elaborar todo el material didáctico para los talleres que se impartirán por niveles básico, intermedio y avanzado, con las temáticas: uso del computador, toma y edición de fotos con software libre, administración de redes sociales, y administración de sitios web con Wordpress.</li>
            <li>Elaborar el contenido para la grabación de los videos de los talleres como material adicional de consulta para el grupo de mujeres y como referente para los próximos pasantes politécnicos comunitarios.</li>
          </ul>
        </div>

        <!-- SECCION 4. JUSTIFICACIÓN -->
        <h4 class="titulo-seccion">4. JUSTIFICACIÓN</h4>
        <hr>
        <div class="form-group">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- SECCION 5.	METODOLOGÍA DE TRABAJO (TÉCNICAS Y PROCEDIMIENTOS) -->
        <h4 class="titulo-seccion">5. METODOLOGÍA DE TRABAJO (TÉCNICAS Y PROCEDIMIENTOS)</h4>
        <hr>
        <div class="form-group">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- SECCION 6. CRONOGRAMA -->
        <h4 class="titulo-seccion">6. CRONOGRAMA</h4>
        <hr>
        <div class="form-group">
          <button type="button" name="adjuntarCronograma" id="adjuntarCronograma" class="btn btn-default">Adjuntar Cronograma</button>
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">

      $(document).ready(function () {
        $('#fecha_inicio').datepicker({
          format: "yyyy-mm-dd"
        });
        $('#fecha_finalizacion').datepicker({
            format: "yyyy-mm-dd"
        });
      });

    </script>

@endsection
