@extends('formularios.forUVS15.base')

@section('form-start')
  {!!Form::model($proyecto,['route'=>['revisar_proyecto',$proyecto->id], 'method'=>'PUT', 'class'=>'formulario', 'files'=>true])!!}
@endsection

@section('form-buttons')
  @if(isset($mostrarBotones) && $mostrarBotones == true)
    <button class="btn btn-success" type="button" name="submit"  value="descargar_foruvs15" style="margin-top: 25px; width: 120px;" onclick="window.location='{{ route('downloadForUVS15',$formulario) }}';">Descargar</button>
  @endif
@endsection

@section('form-end')
  {!!Form::close()!!}
@endsection
