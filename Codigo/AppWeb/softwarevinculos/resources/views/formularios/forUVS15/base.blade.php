@extends('layouts.master')

@section('content')
  {!!Html::style('css/formularios.css')!!}
  {!!Html::style('css/datepicker.css')!!}
  {!!Html::script('js/bootstrap-datepicker.js')!!}
  {!!Html::script('js/check-list.js')!!}

  {!!Html::script('http://code.jquery.com/jquery-1.10.2.js')!!}
  {!!Html::script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')!!}
  {!!Html::style('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css')!!}

  <h2 class="titulo-formulario">FOR-UVS-15</h2>
  <h3 class="titulo-formulario">INFORME FINAL DE LA ORGANIZACIÓN O EMPRESA</h3>
  <hr>

  @yield('comentario_rechazo')
  @yield('form-start')

  <!-- SECCION 1.	INFORMACIÓN DEL PRACTICANTE -->
  <h4 class="titulo-seccion">1.	INFORMACIÓN DEL PRACTICANTE</h4>
  <hr>

  <div class="form-group">
    {!!Form::label('1.1.	Nombres y Apellidos del practicante:')!!}
    <p>{{ $formulario->formularioUVS15->estudiante->usuario->nombres  or '' }} {{$formulario->formularioUVS15->estudiante->usuario->apellidos  or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('1.2.	Nombre de la carrera:')!!}
    <p></p>
  </div>

  <div class="form-group">
    {!!form::label('1.3.	Fecha de inicio y fin de la práctica:')!!}
    <div class="div-sublabels">
      @if($proyecto->fecha_inicio != null)
        <i>{!!form::label('Fecha de Inicio:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Inicio:')!!}</i>
      @endif
      <p>{{ $proyecto->fecha_inicio or '' }}</p>

      @if($proyecto->fecha_finalizacion != null)
        <i>{!!form::label('Fecha de Finalización:', null, ['class' => 'sub-label'])!!}</i>
      @else
        <i>{!!form::label('Fecha de Finalización:')!!}</i>
      @endif
      <p>{{$proyecto->fecha_finalizacion or '' }}</p>
    </div>
  </div>

  <div class="form-group">
    {!!form::label('1.4.	Horas totales de prácticas:')!!}
    <p></p>
  </div>

  <!-- SECCION 2.	INFORMACIÓN DEL RESPONSABLE POR PARTE DE LA ORGANIZACIÓN/EMPRESA -->
  <h4 class="titulo-seccion">2.	INFORMACIÓN DEL RESPONSABLE POR PARTE DE LA ORGANIZACIÓN/EMPRESA</h4>
  <hr>
  <div class="form-group">
    {!!form::label('2.1.	Nombres y Apellidos del Responsable por la empresa:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre_representante_legal or '' }}</p>
  </div>

  <div class="form-group">
    {!!form::label('2.2.	Cargo:')!!}
    <p></p>
  </div>

  <div class="form-group">
    {!!form::label('2.3.	Empresa:')!!}
    <p>{{$proyecto->delegados->first()->organizacion->nombre}}</p>
  </div>

  <div class="form-group">
    {!!form::label('2.4.	Área o Sección:')!!}
    <p></p>
  </div>

  <!-- SECCION 3. ACTIVIDADES PLANIFICADAS -->
  <h4 class="titulo-seccion">3. ACTIVIDADES PLANIFICADAS</h4>
  <hr>
  <div class="modo-ver-tabla">
    <table class="" id="tabla_actividades_planificadas">
        <tr>
          <th rowspan="2" width="50%">Actividades Planificadas</th>
          <th colspan="2" width="15%">Cumplió a cabalidad</th>
          <th rowspan="2">Comentarios</th>
        </tr>
        <tr>
          <th>Si</th>
          <th>No</th>
        </tr>
        @foreach ($actividadesAsignadasEstudiante as $actividad)
          <tr style="height:120px">
            <td>{{$actividad->descripcion}}</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        @endforeach
    </table>
  </div>

  <!-- SECCION 4. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE -->
  <h4 class="titulo-seccion">4. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE</h4>
  <hr>
  {!!form::label('a) Trabajo en equipos multidisciplinarios', null, ['style' => 'margin: 20px 0px;'])!!}
  <div class="modo-ver-tabla">
    <table class="" id="tabla_multidiciplinarios">
        <tr>
          <th rowspan="2" width="70%">Criterios</th>
          <th colspan="6">Calificación </th>
        </tr>
        <tr>
          <th>N/A</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
        </tr>
        <tr>
          <td>1. Busca y obtiene información pertinente al trabajo del equipo.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2. Cumple con las tareas de los roles asignados.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3. Realiza el trabajo asignado por el equipo sin que se lo recuerden.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>4. Escucha a los otros miembros del equipo permitiendo su participación.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
    </table>
  </div>

  {!!form::label('b) Comprender la responsabilidad ética y profesional', null, ['style' => 'margin: 20px 0px;'])!!}
  <div class="modo-ver-tabla">
    <table class="" id="tabla_multidiciplinarios">
        <tr>
          <th rowspan="2" width="70%">Criterios</th>
          <th colspan="6">Calificación </th>
        </tr>
        <tr>
          <th>N/A</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
        </tr>
        <tr>
          <td>1. Respeta y preserva las normas establecidas en la organización beneficiaria/empresa.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2. Actúa con responsabilidad en cada una de las actividades asignadas, respondiendo siempre por sus acciones.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3. Usa la empatía para relacionarse con las demás personas.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>4. Toma en cuenta el contexto para ejecutar las acciones en su trabajo, tomando en cuenta las repercusiones e impactos personales y de los demás.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
    </table>
  </div>

  {!!form::label('c) Comunicarse de manera efectiva', null, ['style' => 'margin: 20px 0px;'])!!}
  <div class="modo-ver-tabla">
    <table class="" id="tabla_multidiciplinarios">
        <tr>
          <th rowspan="2" width="70%">Criterios</th>
          <th colspan="6">Calificación </th>
        </tr>
        <tr>
          <th>N/A</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
        </tr>
        <tr>
          <td>1. Escucha con atención antes de emitir una respuesta sin interrumpir a las demás personas.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2. Se comunica de manera clara y efectiva utilizando un lenguaje adecuado a su receptor.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3. Demuestra actitud personal positiva al dirigirse a los demás con palabras como: gracias, perdón y por favor.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
    </table>
  </div>

  @yield('form-buttons')
  @yield('form-end')
@endsection
