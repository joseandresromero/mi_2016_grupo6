@extends('layouts.master')

@section('title','FOR-UVS-16')

  @section('navbar')
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Lista de Proyectos</a></li>
      <li><a href="#">Notificaciones</a></li>
    </ul>
  @endsection

@section('content')
    {!!Html::style('css/formularios.css')!!}
    {!!Html::style('css/datepicker.css')!!}
    {!!Html::script('js/bootstrap-datepicker.js')!!}
    {!!Html::script('js/check-list.js')!!}


    <ol class="breadcrumb">
     <li><a href="{{url('/')}}">Proyectos</a></li>
     <li class="active">Nuevo Proyecto</li>
    </ol>

    <div class="container">
      <h2 class="titulo-formulario">FOR-UVS-16</h2>
      <h3 class="titulo-formulario">INFORME FINAL DEL DOCENTE TUTOR</h3>
      <hr>

      @if (isset($forUVS16))
        {!!Form::model($forUV16,['route'=>['forUVS16.update',$forUVS16->id], 'method'=>'PUT', 'class'=>'formulario'])!!}
      @else
        {!!Form::open(['route'=>'forUVS16.store','method'=>'POST', 'class'=>'formulario'])!!}
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <!-- SECCION 1. INFORMACIÓN DEL PRACTICANTE -->
        <h4 class="titulo-seccion">1. INFORMACIÓN DEL PRACTICANTE</h4>
        <hr>
        <div class="form-group">
          {!!form::label('1.1. Nombres y Apellidos del practicante:')!!}
          <p>Jose Andres Romero Triviño</p>
        </div>
        <div class="form-group">
          {!!form::label('1.2. Nombre de la carrera:')!!}
          <p>Ingeniería en Ciencias Computacionales</p>
        </div>
        <div class="form-group">
          {!!form::label('1.3. Nombre de la facultad:')!!}
          <p>FIEC</p>
        </div>
        <div class="form-group">
          {!!form::label('1.4. Fecha de inicio y fin de la práctica:')!!}
          <br>
          {!!form::label('Fecha de Inicio:')!!}
          <p>20/08/2016</p>
          {!!form::label('Fecha de fin:')!!}
          <p>29/09/2016</p>
        </div>
        <div class="form-group">
          {!!form::label('1.5. Nombre del Proyecto asociado a la práctica:')!!}
          <p>Talleres a mujeres</p>
        </div>
        <div class="form-group">
          {!!form::label('1.6. Nombre de la Organización Beneficiaria:')!!}
          <p>Hogar de Cristo</p>
        </div>

        <!-- SECCION 2. ACTIVIDADES PLANIFICADAS -->
        <h4 class="titulo-seccion">2. ACTIVIDADES PLANIFICADAS</h4>
        <hr>
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_actividades_planificadas">
            <tr>
              <th rowspan="2">Actividades Planificadas</th>
              <th colspan="2">Cumplió a cabalidad</th>
              <th rowspan="2">Comentarios</th>
            </tr>
            <tr>
              <th>Si</th>
              <th>No</th>
            </tr>
            <tr>
              <td>Grabación de talleres</td>
              <td><input type="radio" name="actividaddes-item-1" value="si"></td>
              <td><input type="radio" name="actividaddes-item-1" value="no"></td>
              <td>{!!form::textarea('comentarios-1',null,['id'=>'comentarios-1','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
            <tr>
              <td>Escritura de libretos</td>
              <td><input type="radio" name="actividaddes-item-2" value="si"></td>
              <td><input type="radio" name="actividaddes-item-2" value="no"></td>
              <td>{!!form::textarea('comentarios-2',null,['id'=>'comentarios-2','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
          </table>
        </div>

        <!-- SECCION 3. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE -->
        <h4 class="titulo-seccion">3. EVALUACIÓN DE LOS RESULTADOS DE APRENDIZAJE</h4>
        <hr>
        <div class="table-responsive">
          <table class="table table-bordered" id="tabla_evaluacion_resultados_aprendizaje">
            <tr>
              <th>Materia</th>
              <th>Resultado de Aprendizaje del Curso</th>
              <th>Evidencia (descripción de la actividad realizada con la cual se cumplió el resultado de aprendizaje)</th>
            </tr>
            <tr>
              <td>{!!form::text('materia-1',null,['id'=>'materia-1','class'=>'form-control'])!!}</td>
              <td>{!!form::textarea('resultado-aprendizaje-1',null,['id'=>'resultado-aprendizaje-1','class'=>'form-control','rows'=>1])!!}</td>
              <td>{!!form::textarea('evidencia-1',null,['id'=>'evidencia-1','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
            <tr>
              <td>{!!form::text('materia-2',null,['id'=>'materia-2','class'=>'form-control'])!!}</td>
              <td>{!!form::textarea('resultado-aprendizaje21',null,['id'=>'resultado-aprendizaje-2','class'=>'form-control','rows'=>1])!!}</td>
              <td>{!!form::textarea('evidencia-2',null,['id'=>'evidencia-2','class'=>'form-control','rows'=>1])!!}</td>
            </tr>
          </table>
        </div>

        <!-- SECCION 4. EVALUACIÓN GENERAL DE LA PRÁCTICA: -->
        <h4 class="titulo-seccion">4. EVALUACIÓN GENERAL DE LA PRÁCTICA:</h4>
        <hr>
        <div class="form-group">
          {!!form::label('4.1. Aprobación:')!!}
          <p>Sí Aprueba <input type="radio" name="aprueba" value="si">No Aprueba <input type="radio" name="aprueba" value="no"></p>
        </div>
        <div class="form-group">
          {!!form::label('4.2. Número de horas de prácticas pre profesionales validadas:')!!}
          <p>160</p>
        </div>
        <div class="form-group">
          {!!form::label('4.3. Tipo de práctica pre profesional:')!!}
          <p>Empresarial <input type="radio" name="tipo_practica" value="empresarial"></p>
          <p>Servicio Comunitario <input type="radio" name="tipo_practica" value="comunitaria"></p>
        </div>
        <div class="form-group">
          {!!form::label('4.4. Instrumentos de evaluación usados en la práctica (ANEXAR A ESTE INFORME)')!!}
          <div class="table-responsive">
            <table class="table table-bordered" id="tabla_instrumentos_evaluacion">
              <tr>
                <td>Rúbrica de evaluación</td>
                <td><input type="checkbox" value="instrumento-1"></td>
              </tr>
              <tr>
                <td>Prueba escrita</td>
                <td><input type="checkbox" value="instrumento-2"></td>
              </tr>
              <tr>
                <td>Prueba oral</td>
                <td><input type="checkbox" value="instrumento-3"></td>
              </tr>
              <tr>
                <td>Desarrollo de innovaciones</td>
                <td><input type="checkbox" value="instrumento-4"></td>
              </tr>
              <tr>
                <td>Desarrollo de manuales o guías</td>
                <td><input type="checkbox" value="instrumento-5"></td>
              </tr>
              <tr>
                <td>Sustentaciones</td>
                <td><input type="checkbox" value="instrumento-6"></td>
              </tr>
              <tr>
                <td>{!!form::text('otros[]',null,['id'=>'otros[]','class'=>'form-control'])!!}</td>
                <td><input type="checkbox" value=""></td>
              </tr>
            </table>
          </div>
        </div>

        <button type="submit" class="btn btn-default btn-enviar" name="submit" value="guardar">Guardar</button>
        <button type="submit" class="btn btn-primary btn-enviar" name="submit" value="enviar">Enviar</button>

      {!!Form::close()!!}
      <!--</form>-->
    </div>

    <script type="text/javascript">

    </script>

@endsection
