<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Prueba Ong search autocomplete:</title>

    <!-- Bootstrap -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/master.css')!!}
    {!!Html::script('js/bootstrap.js')!!}


  </head>
  <body>

    <input type="text" name="searchong" class="form-control" id="searchong" placeholder="Search Ong" >

  </body>

  <script type="text/javascript">
    $('#searchong').autocomplete({
      source: '{!!URL::route('autocomplete')!!}',
      minlength:1,
      autoFocus:true,
      select:function(e, ui){
        console.info("An item was selected", ui);
      }
    });
  </script>
</html>
