@extends('layouts.master')

@section('title','Seguimiento')

@section('navbar')

@endsection

@section('content')

   <div class="row">
     <div class="col-md-8" style="width:100%">
        <div class="panel panel-default" style="width: 60%; margin-left:auto; margin-right:auto;">
          <div class="panel-heading">
             Historial de eventos
           </div>
          <div class="panel-body">
            {!!Form::model(Request::only(['proyecto']) ,['route'=>'seguimiento','method'=>'GET', 'class'=>'navbar-form navbar-left pull-right', 'files'=>true])!!}
               <div class="form-group">
                 {!!form::select('proyecto', $proyectos, $proyectoDefault, ['id'=>'proyecto','class'=>'form-control'])!!}
               </div>
              <button type="submit" class="btn btn-default"> Buscar</button>
            {!!Form::close()!!}
            <table class="table table-bordered">
              <tr>
                @if(isset($notificaciones))
                  <ul class="list-group" style="margin-bottom:0px; margin-top:60px;">
                    @foreach ($notificaciones->reverse() as $notificacion)
                      <div class="notification-item {{ $notificacion->visto == true ? 'notificacion-vista' : '' }}">
                          @if($notificacion->tipo->id == 7) <!-- Rol Asignado -->
                            <h4 class="item-title">Se le ha asignado el rol {{$notificacion->rolAsignado->descripcion}}.
                              @if($notificacion->rolAsignado->id == 1) <!-- Director -->
                                Usted puede crear proyectos dando click en la opcion Crear Proyecto, que se encuentra en la barra de navegación.
                              @elseif($notificacion->rolAsignado->id == 2) <!-- Tutor -->
                                Pronto usted será asignado a algún proyecto por el coordinador de la carrera.
                              @endif
                            </h4>
                          @elseif($notificacion->tipo->id == 1) <!-- Formulario Creado -->
                            <h4 class="item-title">{{$notificacion->formulario->creadoPor->nombres}} {{$notificacion->formulario->creadoPor->apellidos}} ha llenado el formulario {{$notificacion->formulario->tipoFormulario->descripcion}}
                            @if($notificacion->formulario->tipoFormulario->id == 2) <!-- FOR-UVS-04 -->
                              (Creación del proyecto {{$notificacion->formulario->proyecto->titulo}})
                            @endif
                            .</h4>
                            {{--<a class="content" href="{{ route('modoVerProyecto', $notificacion->formulario->proyecto->id) }}" target="_blank">
                              <p class="item-info">Ver formulario</p>
                            </a>--}}
                          @elseif($notificacion->tipo->id == 4) <!-- Tutor asignado a proyecto -->
                            <h4 class="item-title">El docente {{$notificacion->tutorAsignado->usuario->nombres}} {{$notificacion->tutorAsignado->usuario->apellidos}} ha sido asignado como tutor al proyecto {{$notificacion->proyecto->titulo}}.</h4>
                          @elseif($notificacion->tipo->id == 6) <!-- Estudiante asignado a tutor -->
                            <h4 class="item-title">El docente {{$notificacion->tutorAsignado->usuario->nombres}} {{$notificacion->tutorAsignado->usuario->apellidos}} ha sido asignado como tutor del estudiante {{$notificacion->estudianteAsignado->usuario->nombres}} {{$notificacion->estudianteAsignado->usuario->apellidos}} en el proyecto {{$notificacion->proyecto->titulo}}.</h4>
                          @elseif($notificacion->tipo->id == 3) <!-- Formulario rechazado -->
                            <h4 class="item-title">
                              {{$notificacion->revisadoPor->nombres}} {{$notificacion->revisadoPor->apellidos}} ha rechazado el formulario {{$notificacion->formulario->tipoFormulario->descripcion}}
                              @if($notificacion->formulario->tipoFormulario->id == 2) <!-- FOR-UVS-04 -->
                                (Creación del proyecto {{$notificacion->proyecto->titulo}})
                              @endif
                              llenado por {{$notificacion->formulario->creadoPor->nombres}} {{$notificacion->formulario->creadoPor->apellidos}}.
                            </h4>
                            <p class="item-info">Ver formulario</p>
                          @elseif($notificacion->tipo->id == 2) <!-- Formulario aprobado -->
                            <h4 class="item-title">
                              {{$notificacion->revisadoPor->nombres}} {{$notificacion->revisadoPor->apellidos}} ha aprobado el formulario {{$notificacion->formulario->tipoFormulario->descripcion}}
                              @if($notificacion->formulario->tipoFormulario->id == 2) <!-- FOR-UVS-04 -->
                                (Creación del proyecto {{$notificacion->proyecto->titulo}})
                              @endif
                              llenado por {{$notificacion->formulario->creadoPor->nombres}} {{$notificacion->formulario->creadoPor->apellidos}}.
                            </h4>
                          @else
                            <h4 class="item-title">{{$notificacion->id}} notificacion</h4>
                          @endif
                        <p class="item-time">{{ $notificacion->created_at->format('d M Y - H:i') }}</p>
                      </div>
                    @endforeach
                  </ul>
                @endif
              </tr>
            </table>
          </div>
        </div>

     </div>
   </div>
@endsection
